# Life In The Frontier - an **Oolite** mod - REVIVAL version (0.10.0+)

## **Indice**

- [**TODOLIST**](#todolist)
    - [PHASE 1](#phase-1)
    - [PHASE 2](#phase-2)
    - [PHASE 3](#phase-3)
- [**CHANGELOG**](#changelog)
- [**Notes**](#notes)
- [Station Navigation](#station-navigation)
- [**GroupID**](#groupid)
- [**Feedback and Rankings**](#feedback-and-rankings)
- [**Groups and System Government Types**](#groups-and-system-government-types)
- [**Missions Templates**](#missions-templates)
- [**Commodities and Sub-Commodities**](#commodities-and-sub-commodities)
- [**Implemented Missions**](#implemented-missions)
- [**Old Version Mission Templates**](#old-version-mission-templates)
- [**Frontier: Elite Missions**](#frontier--elite-missions)
- [**Frontier: Elite Dialogues**](#frontier--elite-dialogues)

## **TODOLIST**

- [ ] Quando verrà implementata CustomMap, capire se è possibile cambiare tra i vari livelli di zoom e centrare la mappa sui pianeti oggetto della missione selezionata.

- [ ] Una schermata nel profilo online che permette di cancellare una missione in corso perdendo feedback? Eventuale denaro anticipato verrà restituito al mittente. Se questo non è possibile, verrà perso ulteriore feedback o aumentata la bounty del giocatore.

- [ ] Se il giocatore ha una reputazione sufficientemente bassa, fornire una chance di generazione di un messaggio alternativo GalShade per il completamento della missione (non-GalShade), gli dà 50% in più di ricompensa ma abbassa il feedback come se avesse fallito la missione originaria. Il messaggio potrebbe richiedere che il giocatore si diriga in un altro sistema.
TRANSFER, RETRIEVE nel sistema dove il giocatore raccoglie la merce.
CARGO, nel sistema dove il giocatore deve consegnare la merce.

- [ ] Se il giocatore non è sicuro di riuscire a portare a termine la missione in tempo può chiedere una proroga preventiva. La proroga aggiunge il 10% del tempo concesso al totale (50 ore -> 55 ore), ma il giocatore riceverà solo il 40% del totale (vengono ricalcolati anche eventuali anticipi). Una volta accettata una proroga non può venire più dismessa. La proroga viene fornita solo da un certo livello di reputazione in poi.

- [ ] All'interno di un task potrei inserire una proprietà "dialog": una stringa contenente l'etichetta di un waiting item (o simile). Quando viene avviato quel task comincia un dialogo secondo l'oggetto corrispondente, che termina con l'accettazione di una missione o l'avanzamento della stessa. Potrebbe costituire la base per una missione LOCAL.

- [ ] Implementare il linguaggio di scripting di Novel all'interno dei dialoghi dell'OXP.

### PHASE 1

- [ ] La schermata di mappa galattica come dettaglio missione andrà modificata con la release della versione 1.87 per sfruttare CUSTOM_MAP.

- [ ] Missione SEARCH: il contraente chiede al giocatore di trovare una persona che dovrebbe trovarsi in un sistema. Lo avverte che la persona si sposta di continuo, e deve trovarla prima che scompaia di nuovo. Il giocatore dovrebbe trovare degli indizi nei sistemi che la persona ha visitato. Il limite di tempo non è indispensabile dato che la missione fallisce automaticamente al termine del percorso della persona. La missione ha un solo task. In questo task verrà indicato il nome della persona da trovare e la lista dei sistemi che visiterà, insieme al momento in cui passerà al sistema successivo. Se la lista arriva alla fine, la persona "scompare" e la missione fallisce. I sistemi vengono scelti scegliendone due a casi e calcolando la rotta necessaria. Il processo viene ripetuto finchè in lista non ci sono almeno dieci sistemi. Per ogni sistema vengono generati i messaggi da inserire dopo che la persona se ne è andata, e che verranno aggiunti automaticamente alla BBS in modo che il giocatore possa trovare degli indizi. Questa potrebbe anche non essere una missione come le altre. Potrei generare un numero di persone che vagano per la galassia, e le opportune missione di chi desidera trovarle. Ogni volta che una persona visita un tot sistemi "scompare", e ne viene generata un'altra.

- [ ] Missione INFO: il contraente chiede al giocatore di trovare informazioni su una persona che si nasconde in un sistema. Il giocatore deve recarsi in quel sistema e parlare con persone che si trovano nella BBS. Incrociando le info tra le persone il giocatore dovrebbe accedere ad informazioni più preziose. Tornando a parlare con il contraente, questo pagherà rispetto a quanto sono importanti le informazioni trovate.

- [ ] Missione KILL: Il giocatore deve trovare un'astronave nel sistema indicato e distruggerla.

- [ ] Possibilità di abbandonare una missione? Potrebbe essere utile farlo dalla schermata delle missioni attive, scegliendo la missione e poi rimuovendola dalla lista, con conseguente perdita di feedback (specialmente se il giocatore ha già ricevuto del denaro dal contraente).

### PHASE 2

- [ ] Un item che contiene un dialogo che porta all'attivazione di una missione (o che indirizza il giocatore verso un secondo item missione).

- [ ] Fornire al giocatore la possibilità di aggiungere un nuovo item nella BBS, grazie ad una serie di opzioni-template. Questi item dovrebbero servire a contattare qualcuno. Potrebbero venire utilizzati per le missioni CONTACT, ad esempio.

- [ ] Aggiungere item pubblicitari relativi alle strutture di servizio presenti nella stazione. Permettere al giocatore di girare per la stazione fino a raggiungerle. Utilizzare le strutture per far passare una quantità di tempo variabile al giocatore.

- [ ] Completare certe missioni GalShady potrebbe aumentare la bounty del giocatore. Il profilo online potrebbe contenere una schermata che faccia da fedina penale (criminal record), con tipo di crimine e bounty aggiunta.

- [ ] Locazioni che permettono al giocatore di far passare il tempo.

- [ ] Generazioni di diversi layout per le stazioni a seconda dei parametri del sistema in cui si trovano.

- [ ] Restore Legal Status. Tre diversi annunci [GalCop, GalShady, GalGovernment] che offrono al giocatore la possibilità di ripulire la sua fedina penale previo pagamento. Il costo sarà piuttosto alto: player.bounty * [10, 15, 20].

- [ ] Missioni o eventi che modificano player.bounty.

- [ ] Missione TOUR: il contraente chiede al giocatore di trasportare una persona o un gruppo di persone per una serie di sistemi. Al termine del tour il giocatore riceve la sua ricompensa. La persona o il gruppo dovrebbero scendere dall'astronave ad ogni tappa, e chiedere al giocatore di aspettare un'ora prima di riprenderli a bordo e ripartire per la prossima destinazione. Per questo sarebbe utile qualche locazione della stazione in cui perdere un'ora. Oppure potrei creare un item BBS che consente di fare la stessa cosa (a pagamento).

- [ ] In una missione KILL il bersaglio potrebbe parlare al giocatore quando inizia il combattimento, e proporre di evitare la sua uccisione pagando - per poi procedere verso la vicina stazione dove potrebbe generare un annuncio e permettere al giocatore di scegliere se farsi corrompere.

### PHASE 3

## **CHANGELOG**

|Data|Descrizione|
|:--:|:--|
|**03/05/2018**|- Definizione dell'item e del dialogo associato all'item da utilizzare per completare le missioni SEARCH.|
||- Inizio del processo di integrazione del linguaggio di scripting da utilizzare per i dialoghi in modo da aprire più possibilità di interazione e di complessità.|
|**01/05/2018**|- Non mostrare il tempo rimasto per le missioni attive senza una scadenza (SEARCH).|
||- Creazione degli item di raccolta informazioni per le missioni SEARCH.|
|**23/04/2018**|- Creazione item di missione per le missioni SEARCH.|
|**22/04/2018**|- Aggiunti effetti sonori per l'accettazione o l'avanzamento di una misssione, e per il fallimento di una missione attiva.|
|**21/04/2018**|- Riorganizzato il debug, creata una schermata di "console" in cui posso modificare varie impostazioni. La schermata viene abilitata impostando litfVars.debug = true.|
||- Eliminato litfVars.config.enableLog, sostituito con litfVars.debug.logLevel. Creati tre diversi metodi, uno per ogni livello di log: 0 = log disabilitato; 1 = info; 2 = debug; 3 = severe|
||- Eliminati i console messages di notifica all'interno delle stazioni (che non comparivano in più in quanto l'hud viene reso invisibile), e sostituiti con messaggi di log che è possibile esaminare da una schermata apposita nel profilo online.|
|**19/04/2018**|- Le schermate di lista BBS e status online possono venire switchate con le altre schermate della stazione.|
||- Implementati metodi per creare/rimuovere missioni standard parcel/passenger/cargo che uso come supporto alle missioni dell'OXP.|
||- Implementato un controllo in modo che nelle missioni CARGO il carico venga aggiunto alla nave nel momento appropriato.|
||- Implementati metodi per memorizzare i dati relativi alle missioni attivi quando si arriva in una stazione. Le missioni di supporto vengono eliminate. Quando il giocatore riparte da una stazione, viene ciclata la lista delle missioni attive, e ricreate le missioni di supporto associate.|
|**18/04/2018**|- Spostato testo e descrizione missioni dentro missiontext.plist. Adattato codice.|
||- Adattate item missioni e waiting alla nuova struttura dati.|
|**15/04/2018**|- Implementata una schermata di mappa, accessibile dal dettaglio missione, che mostra i sistemi associati alla missione selezionata.|
|**14/04/2018**|- Implementati metodi addParcel e removeParcel sulle missioni di tipo CONTACT, TRANSFER, RETRIEVE, con la stessa metodologia già usata per le missioni TRANSPORT e CARGO.|
|**13/04/2018**|- Primi test su possibile struttura e navigazione stazione.|
|**12/04/2018**|- Implementate nuove missioni TRANSPORT.|
||- Se il giocatore cambia galassia, tutti i dati relativi al suo "profilo" vengono resettati (missioni attive, feedback etc.)|
||- Il calcolo della ricompensa considera sia il feedback assoluto (in maniera minore a prima) sia il numero di missioni completate per quel gruppo.|
|**11/04/2018**|- Implementato oggetto con dati statistici all'interno di litfCommander.|
||- Implementata schermata statistiche.|
||- Aggiornamenti dati statistici su missioni e feedback.|
||- Refactoring dell'inizializzazione delle variabili dell'OXP all'avvio.|
|**10/04/2018**|- Spostato l'oggetto bbsData dentro litfVars.|
||- Le missioni CARGO hanno un peso variabile (non prendono automaticamente l'intero spazio cargo dell'astronave) da cui dipende la ricompensa della missione.|
||- Se il giocatore accetta una missione e riceve un anticipo, viene segnalato in un log a video.|
|**09/04/2018**|- Implementato limite massimo missioni attive, calcolato in base al feedback: ogni 25 punti è possibile accettare una missione in più (base: 1 missione attiva).|
||- Implementate nuove missioni CARGO.|
|**08/04/2018**|- Implementato controllo fallimento missione anche per le missioni CARGO. Il carico viene rimosso automaticamente e viene visualizzata una notifica.|
||- Implementati nuove missioni CARGO per vari gruppi.|
||- Implementata modifica nel fattore di rischio delle missioni se la ricompensa calcolata è molto alta.|
|**06/04/2018**|- Aggiunti sfondi missione con volti presi da Frontier:Elite (qualcuno creato ex-novo dal materiale originale).|
||- Implementata impostazione sfondi nelle missioni (proprietà faceBackground) e visualizzazione nella schermata di dettaglio missione.|
|**05/04/2018**|- Spostati gli array di risposte casuali all'interno di descriptions.plist|
||- Uniti in un unico metodo parametrizzato i metodi $generateMission e $generateDebugMission|
||- Impostati valori di default ai flag di missione (urgent, unaware, showCredits, local...)|
||- Sostituita la proprietà contractorPassenger (boolean) con firstPassenger (number): se è uguale a 0, viene inserito come primo passeggero il contraente della missione, altrimenti viene pescato il primo nome dal task con indice uguale al valore della proprietà.|
|**04/04/2018**|- La proprietà di missione startingPassengers è stata rinominata passengers, ed ora è un array: se contiene un solo valore viene preso quel valore come numero passeggeri, se sono presenti due valori viene estratto un numero casuale tra i valori impostati. Creata chiave %PASSENGERS% per la stampa a video del valore calcolato.|
||- Modificato il metodo per il calcolo della ricompensa di missione. Dimezzato il premio per ora stimata di completamento missione.|
||- Spostati i template di missione all'interno di LITF_MissionData.js|
||- Nuovo oggetto dialogo "transport", sostituisce UNAWARE con HOWMANY.|
||- Per distinguere i passeggeri da altri, quando si caricano a bordo viene aggiunto il prefisso "(BBS) ".|
|**03/04/2018**|- Implementate missioni TRANSPORT: sia dal sistema in cui si accetta la missione ad un altro, sia dal sistema A al sistema B; sia per una persona singola che per gruppi; contraente compreso od escluso; possibilità di utilizzare la proprietà risk per indicare la possibilità di venire attaccati (valori permessi: 0, 1, 2). Per la gestione delle cabine uso addPassenger e removePassenger, con le seguenti conseguenze: 1) viene riportata la lista dei passeggeri su F5-F5, con pagamento previsto uguale a 0cr; 2) quando si arriva a destinazione compare la schermata che annuncia la discesa dei passeggeri dalla nave; 3) se la missione fallisce quando si arriva in una stazione compare la schermata che annuncia la discesa (arrabbiata) dei passeggeri dalla nave.|
||- Il controllo per missioni fallite avviene solo attraccati ad una stazione.|
||- Durante il controllo per misisoni fallite, per ogni missione vengono rimossi dalla nave eventuali passeggeri, ed ogni passeggero contribuisce con una penalità di -1 sul feedback del giocatore.|
||- Inserito messaggio in descrizione missioni TRANSPORT che ricorda al giocatore di andare sulla BBS per chiudere il lavoro e riscuotere il pagamento dovuto.|
||- Fix su calcolo ricompensa missioni.|
|**02/04/2018**|- Implementata nuova lista di item BBS (in parte spostati dalla lista degli ad), associati ad una lista di tipi di governo, per una migliore personalizzazione delle BBS a seconda del governo di sistema.|
|**01/04/2018**|- Implementate customResponses, per consentire ad ogni missione di rispondere in maniera personalizzata alle interazioni del giocatore. Le proprietà sono array per poter estrarre casualmente una risposta quando viene generata la missione.|
||- Implementata interazione NEXTTASK per avanzare il task della missione, aggiornando tutte le informazioni a video ed i controlli.|
||- Implementata missione RETRIEVE (2-step).|
||- Implementata perdita di feedback se una missione fallisce.|
||- Aggiunta proprietà hideSystemData nell'oggetto task. Se presente, non viene visualizzato il nome o la distanza del sistema nella schermata di dettaglio della missione.|
|**31/03/2018**|- Completata la modifica di gestione delle missioni e dei messaggi di attesa con dialogo multi-step.|
||- Implementata missione CONTACT (1-step).|
||- FIX: Posizione cursore quando si elimina l'ultimo item dalla lista della BBS.|
||- FIX: Controllo sulla scadenza dei messaggi, notifica ed eliminazione dalla lista delle missioni attive.|
|**30/03/2018**|- FIX: Ora una missione completata viene correttamente eliminata dalla lista delle missioni attive.|
||- FIX: Corretta sostituzione delle costanti nei dialoghi di completamento missioni per il giocatore.|
||- Sostituita l'opzione di dialogo missione che chiede se sono necessari permessi (05_PERMIT) - alla quale viene data risposta fissa di no - con l'opzione 05_UNAWARE con cui il giocatore può chiedere se il destinatario è a conoscenza del suo arrivo. Le risposte, in entrambi i casi, sono scelte a caso da due array.|
||- L'annuncio sulla BBS relativo al destinatario di una missione viene ora scelto a seconda del valore della proprietà unaware, estratto a caso dagli array waitingStore (per unaware = false) o adStore con groupId = 101 (per unaware = true).|
|**28/03/2018**|- La proprietà type nell'oggetto missione ora è un oggetto con le proprietà { groupId: INT, typeId: INT, index: INT }.|
||- L'oggetto system (generato casualmente) ora contiene le proprietà { id: INT, name: STRING, index: INT }.|
||- Invece delle proprietà system1, system2... l'oggetto missioni ora avrà una proprietà systems: ARRAY (la proprietà index identifica il sistema).|
||- L'oggetto ottenuto da $replaceConstants non contiene più proprietà STRING, ma ogni proprietà sarà un ARRAY in modo da poter contenere più stringhe di testo con lo stesso riferimento (ad es. due sistemi, o tre nomi di persona).|
||- Le stringhe chiave RAND_SYSTEM, RAND_NAME sono state sostituite con %SYSTEM% e %NAME%. Tutte le altre stringhe seguono lo stesso schema.|
||- Aumentato il tempo a disposizione per il completamento della missione calcolato in $missionAllocatedTime per avere uno scarto maggiore.|
||- Aumento di reputazione per donazioni di denaro alle organizzazioni di beneficenza.|
||- Creato oggetto di prova per inserire un messaggio del destinatario della missione all'interno della BBS nel sistema di destinazione, in modo che il giocatore possa parlarci per completare la missione.|
|**27/03/2018**|- Concezione della nuova direzione dell'OXP.|
|**26/03/2018**|- Calcolo punti reputazione per successo o fallimento missione|
||- Modifiche in metodi Common|
||- Visualizzazione tempo rimanente in giorni/ore/minuti/secondi|
||- Memorizzazione missione una volta accettata|
||- Visualizzazione dati missioni accettate nella schermata del profilo|
||- Sostituiti i messaggi ad presi da YAH con slogan di sci-fi corporations o altro materiale.|
|**25/03/2018**|- Implementati i dialoghi per ottenere un anticipo sulla ricompensa.|
||- Implementati i dialoghi per aumentare la ricompensa.|
||- Implementati controlli sulla reputazione (feedback) del giocatore prima di iniziare il dialogo.|
||- Prima implementazione schermata profilo online.|
||- Calcolo (soggetto ad eventuali modifiche) tempo necessario per completamento missione.|
||- Calcolo (soggetto ad eventuali modifiche) della ricompensa per le missioni.|
||- Accettare una missione, controllo che non ne sia stata già accettata una per carico simile.|
||- Memorizzazione delle missioni accettate.|
||- Visualizzazione delle missioni accettate nella schermata di profilo online.|
|**24/03/2018**|- Apertura della schermata di dettaglio missione.|
||- Presentazione a video dei dettagli della missione.|
||- Visualizzazione scelte possibili e comportamenti delle stesse.|
||- Implementati i dialoghi per ottenere informazioni sulla missione.|
|**XX/03/2018**|- Creazione di una voce di menù nella schermata F4 per l'accesso alla BBS.|
||- Generazione di una lista di inserzioni (missioni e annunci pubblicitari) dipendente dal tipodi governo del sistema.|
||- Visualizzazione della lista delle inserzioni.|
||- Navigazione all'interno della lista delle inserzioni.|
||- Distinzione tra missioni e annunci pubblicitari. Questi ultimi non hanno a disposizione una schermata di dettaglio.|

## **Notes**

- Il controllo sulle cabine viene eseguito al momento di caricare i passeggeri a bordo. Quindi una missione TRANSPORT dal sistema A al sistema B verrà accettata, ma una volta arrivati al sistema A la missione non potrà procedere se non si avranno sufficienti cabine libere per i passeggeri.
- Lo stesso vale per le missioni CARGO, che richiedono che la stiva dell'astronave del giocatore sia vuota prima di caricare le merci.

## Station Navigation

Il giocatore avrà la possibilità di girare all'interno della stazione in cui è atterrato, passando da una locazione all'altra e da un livello all'altro, tramite l'uso di ascensori.

La mappa della stazione sarà determinata dal tipo di governo del sistema, mentre l'economia ed il livello tecnologico modificheranno i nomi di alcune locazioni.

||||
|:-|:-|:-|
|**L0 - Ship Level**|
|hangar|Hangar.||
|docks|Docks.||
|storage|Storage Area.|GALSHOPPING|
|corridors|Maintenance Corridors.|GALSHADY|
|**L1 - Production Area**|
|production|(1)|GALGOVERNMENT|
|**L2 - Civic Area**|
|houses|(2)|GALCIVIC|
|library|Library.|GALBOOK|
|square|Public Square.|GALCIVIC|
|alleys|Alleys.|GALSHADY|
|mall|Mall.|GALSHOPPING|
|church2|Church.|GALCHURCH|
|**L3 - Entertainment Area**|
|medical|(3)|GALMEDICAL|
|church3|Faitch Center|GALCHURCH|
|museum|Natural Museum.|GALCULTURE|
|gallery|Art Gallery.|GALCULTURE|
|cinema|Cinema Hall.|GALCULTURE|
|opera|Opera House.|GALCULTURE|
|wwvt|WildWorld Virtual Tour.|GALNATURE|
|bmarket|Black Market.|GALSHADY|
|**L4 - Tourism Area**|
|lounge|Passengers Lounge.|GALTOURISM|
|hotel|DeLuxor Hotel.|GALTOURISM|
|shop|Souvenirs Shop.|GALSHOPPING|
|**L5 - Administration Area**|
|gccenter|GalCop Center|GALCOP|
|prison|GalCop Detainment Zone|GALCOP|
|palace|(4)|GALGOVERNMENT|
	
(1)
- Industrial economy: Robot Factories / Replicating Factories / Nano-AssemblyLines (TL: low, medium, high).
- Agricultural economy: Commune Farms / Hydroponic Farms / Protein-Cloning Centers (TL: low, medium, high).

(2)
- Common Hives / Apartments / MegaCondos. (TL: low, medium, high).

(3)
- First-Aid Center / Public Clinic / Hospital (TL: low, medium, high).

(4)
- Soviet Palace (communist) / Empire Palace (dicatorial)

## **GroupID**

|Id |Group                       |
|:-:|:---------------------------|
|0	|GalCop                      |
|1	|GalBook                     |
|2	|GalChurch                   |
|3	|GalShady                    |
|4	|GalMedical                  |
|5	|GalCivic                    |
|6	|GalTourism                  |
|7	|GalShopping                 |
|8	|GalNature                   |
|9	|GalCulture                  |
|10	|GalGovernment               |
|   |                            |
|11	|Debug Missions              |
|   |                            |
|12	|SEARCH missions messages    |
|   |                            |
|98	|Charity Messages            |
|99	|Waiting Messages            |
|   |                            |
|101|Generic messages, spam      |
|102|Messages about items        |
|103|Looking for job messages    |
|104|Racial spam                 |
|105|Government-specific messages|

## **Feedback and Rankings**

| Feedback Score | Ranking     |
| :------------: | :---------- |
| -101 / (-150)  | Outcast     |
| -51 / -100     | Criminal    |
| -26 / -50      | Unlawful    |
| -11 / -25      | Untrusted   |
|                |             |
| 9 / -10        | Neutral     |
|                |             |
| 24 / 10        | Appreciated |
| 49 / 25        | Helpful     |
| 99 / 50        | Trusted     |
| (150) / 100    | Renowned    |

Idea for a feedback-based mission filter:

|           |             |                     |
| :-------: | :---------: | :------------------ |
| Neutral   |             | CONTACT             |
| Untrusted | Appreciated | RETRIEVE / TRANSFER |
| Unlawful  | Helpful     | TRANSPORT           |
| Criminal  | Trusted     | SEARCH / TOUR       |
| Outcast   | Renowned    | KILL                |

## **Groups and System Government Types**

| Name          | Description                           |
| :------------ | :------------------------------------ |
| GalCop        | Police and security                   |
| GalBook       | Intergalactic libraries               |
| GalChurch     | Religious activities                  |
| GalShady      | Unlawful proposals                    |
| GalMedical    | Medical emergencies                   |
| GalCivic      | Missions provided by station citizens |
| GalTourism    | Touristic activities                  |
| GalShopping   | Local markets                         |
| GalNature     | Animal and plants protection          |
| GalCulture    | Cultural activities                   |
| GalGovernment | Missions from authoritarian regimes   |

This table contains the relations between a group and a system government (or where the group can provide missions)

|               | CORP  | DEMO  | CONF  | COMM  | DICT  | MULT  | FEUD  | ANAR  |
| :------------ | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| GalCop        |       | X     | X     |       |       | X     |       |       |
| GalBook       |       | X     | X     |       |       | X     |       |       |
| GalChurch     |       | X     | X     |       | X     | X     | X     |       |
| GalShady      | X     |       |       |       |       |       | X     | X     |
| GalMedical    | X     | X     | X     |       |       | X     |       |       |
| GalCivic      |       | X     | X     | X     | X     | X     | X     | X     |
| GalTourism    | X     | X     | X     |       |       | X     |       |       |
| GalShopping   | X     | X     | X     |       |       | X     |       |       |
| GalNature     |       | X     | X     |       |       | X     |       |       |
| GalCulture    |       | X     | X     |       |       | X     |       |       |
| GalGovernment |       |       |       | X     | X     |       |       |       |

## **Missions Templates**

|Id |Name     |Description                                                       |
|:-:|:--------|:-----------------------------------------------------------------|
| 0 |LOCAL    |Local mission/event. **(PHASE 2)**                                    |
| 1 |CONTACT  |Go to A. Talk to B.                                               |
| 2 |TRANSFER |Go to A. Talk to B. go to C. Talk to D.                           |
| 3 |RETRIEVE |Go to A. Talk to B. Return to starting system. Talk to contractor.|
| 4 |TRANSPORT|1, 2 and 3, but with passengers (free cabins required)            |
| 5 |CARGO    |1, 2 and 3, but with special cargo                                |
| 6 |SEARCH   |TBD                                                               |
| 7 |INFO     |TBD                                                               |
| 8 |TOUR     |TBD                                                               |
| 9 |KILL     |TBD                                                               |

## **Commodities and Sub-Commodities**

| OOLITE COMMODITIES | OXP SUBCOMMODITIES                                                                                                                                                                               |
| :----------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Food**           | Grain, Generic Foods, Food Dispensers, Medicines, Farm Animals, Colture Plants, Junk Foods                                                                                                       |
| **Textiles**       | Textiles                                                                                                                                                                                         |
| **Machinery**      | Factory Equipment, Mining Equipment, Robo-Servants, Robo-Workers, Robo-Fighters, Robo-Pleasures, Robo-Medics                                                                                     |
| **Gem Stones**     | Industrial Gems                                                                                                                                                                                  |
| **Luxuries**       | Luxury Foods, Electric Pets, Artwork, Games, Movies, Holographics, Home Entertainment, Books, Furniture, Paleo-Arts, Archeo-Sims, Relics, Religious Texts, Wood, Dream Cards, Hacked Dream Cards |
| **Firearms**       | Weaponry, Combat Drones, Recon Drones                                                                                                                                                            |
| **Platinum**       |                                                                                                                                                                                                  |
| **Slaves**         | Slaves, Black Market Organs                                                                                                                                                                      |
| **Furs**           | Furs                                                                                                                                                                                             |
| **Radioactives**   | Plutonium, Uranium, Advanced Fuels, Radium, Thorium                                                                                                                                              |
| **Alien Items**    | Holy Symbols, Prayer Books, Fossils                                                                                                                                                              |
| **Alloys**         | Construction Materials, Pre-Fabs, Space Salvage, Plastics, Synthetics                                                                                                                            |
| **Liquor & Wines** | Liquors                                                                                                                                                                                          |
| **Narcotics**      | Brilliance, Tobacco, Cheap Spice                                                                                                                                                                 |
| **Minerals**       | Iron, Tungsten, Nickel, Quartz                                                                                                                                                                   |
| **Computers**      | Communication Equipment, Computers, Home Appliances, Medical Equipment, Music Equipment, Industrial Capacitors, Artificial Limbs, Artificial Organs, Neural Interfaces                           |
| **Gold**           | Gold                                                                                                                                                                                             |

## **Implemented Missions**

**GalCop**

| Code   | Title                                               | Waiting Items      |
| :----: | :-------------------------------------------------- | :----------------- |
| 0.1.1  | GALCOP: Deliver sensitive info to an agent.         | W0.1.1             |
| 0.1.2  | GALCOP: Deliver equipment to an agent.              | W0.1.2             |
| 0.2.5  | GALCOP: Courier needed.                             | W0.2.5.1, W0.2.5.2 |
| 0.3.3  | GALCOP: A fast ship to retrieve data from an agent. | W0.3.3.1, W0.3.3.2 |
| 0.3.4  | GALCOP: Contact an agent and report info.           | W0.3.4.1, W0.3.4.2 |
| 0.4.6  | GALCOP: Transport an agent to another system. (h>A) | W0.4               |
| 0.4.7  | GALCOP: A ship for an agent relocation. (A>B)       | W0.4.7, W0.4       |
| 0.4.8  | GALCOP: Transport an agent here. (B>h)              | W0.4.7, W0.4       |
| 0.4.9  | GALCOP: Transport a witness to a trial. (h>A)       | W0.4               |
| 0.4.10 | GALCOP: Transport a witness to a trial. (A>B)       | W0.4.8, W0.4       |
| 0.4.12 | GALCOP: Emergency transport for an agent. (h>A)     | W0.4.12.1, W0.4    |
| 0.5.11 | GALCOP: Looking for a ship to transport cargo.      | G3                 |

**GalBook**

| Code  | Title                                       | Waiting Items      |
| :---: | :------------------------------------------ | :----------------- |
| 1.1.1 | NOTICE: Book delivery to another system.    | W1.1.1             |
| 1.2.3 | SHIP REQUESTED: A safe transfer for a book. | W1.2.3.1, W1.2.3.2 |
| 1.3.2 | SHIP REQUESTED: I need to get back a book.  | W1.3.2.1, W1.3.2.2 |
| 1.5.4 | SHIPMENT: Can you transport some stuff?     | G3                 |

**GalChurch**

| Code  | Title                                                                  | Waiting Items      |
| :---: | :--------------------------------------------------------------------- | :----------------- |
| 2.1.2 | INSPIRATION: Contact a wise man to join our Church.                    | W2.1.2             |
| 2.2.3 | SACRED: We need a ship to transfer some holy alien artifacts.          | W2.2.3.1, W2.2.3.2 |
| 2.3.1 | HOLY: We need to transport back some Church material.                  | W2.3.1.1, W2.3.1.2 |
| 2.4.4 | SPIRITUAL: An eminent priest is awaited for a religious service. (h>A) | W2.4               |
| 2.4.5 | SHIP WANTED: A group of missionaries is leaving for another system.    | W2.4               |
| 2.4.6 | SPIRITUAL: An eminent priest is awaited for a religious service. (A>B) | W2.4.6, W2.4       |
| 2.4.7 | TRANSFER: Moving a priest from one church to another.                  | W2.4.6, W2.4       |
| 2.4.8 | PILGRIMAGE: A group of faithful followers needs help.                  | W2.4.8, W2.4       |

**GalShady**

| Code  | Title                                                                 | Waiting Items      |
| :---: | :-------------------------------------------------------------------- | :----------------- |
| 3.1.1 | URGENT: We need someone to deliver important data to an affiliate.    | W3.1.1             |
| 3.2.3 | SWITCH: We need to move something from a system to another.           | W3.2.3.1, W3.2.3.2 |
| 3.3.2 | JOB OFFER: We need to retrieve a package.                             | W3.3.2.1, W3.3.2.2 |
| 3.4.4 | URGENT: We need to quickly transport an affiliate away from a system. | W3.4.4, G1         |
| 3.4.5 | SHIP REQUESTED: A business trip.                                      | W3.4.5, G1         |
| 3.5.6 | SHIP REQUESTED: I need to move stuff.                                 | G3                 |
| 3.5.7 | URGENT: Reacquire my goods. (A>h)                                     | G3.1, G3           |
| 3.5.8 | ONLY INTERESTED: Discreet goods relocation. (A>B)                     | G3.1, G3           |

**GalMedical**

| Code   | Title                                                         | Waiting Items      |
| :----: | :------------------------------------------------------------ | :----------------- |
| 4.1.1  | MISSING: Someone to give updates about a medical team.        | W4.1.1             |
| 4.1.2  | MISSING: Someone to give updates about a research group.      | W4.1.2             |
| 4.1.3  | MISSING: Someone to give updates about an emergency team.     | W4.1.1             |
| 4.2.5  | URGENT: A fast ship for transport of rare donor blood.        | W4.2.5.1, W4.2.5.2 |
| 4.3.4  | SHIP REQUESTED: We need clinical records from another system. | W4.3.4.1, W4.3.4.2 |
| 4.4.6  | EMERGENCY: Fast travel for a medic.                           | G1                 |
| 4.4.7  | SHIP REQUESTED: Transport for a medical team.                 | G1                 |
| 4.4.8  | SHIP REQUESTED: Transport for a research team.                | G1                 |
| 4.5.9  | SHIPMENT: Need ship for transporting medical equipment.       | G3                 |
| 4.5.10 | URGENT: We need this stuff as soon as possible!               | G3.1, G3           |

**GalCivic**

| Code  | Title                                            | Waiting Items      |
| :---: | :----------------------------------------------- | :----------------- |
| 5.1.1 | WANTED: A ship to deliver a package.             | W5.1.1             |
| 5.1.2 | NEED HELP: I wish for some news from my family.  | W5.1.2             |
| 5.3.3 | TAKE BACK: I forgotten %ITEM% in another system. | W5.3.3.1, W5.3.3.2 |
| 5.4.4 | TAXI JOB: Ship for one.                          | G1                 |
| 5.4.5 | TAXI JOB: Ship for a group.                      | G1                 |
| 5.5.6 | CARGO: Help me moving stuff.                     | G3                 |
| 5.5.7 | HELP: I need someone to regain a bunch of stuff. | G3.1, G3           |

**GalTourism**

| Code  | Title                                                                   | Waiting Items      |
| :---: | :---------------------------------------------------------------------- | :----------------- |
| 6.1.1 | URGENT: Need to cancel a booking.                                       | W6.1.1             |
| 6.2.2 | HELP NEEDED: Someone to transfer baggage misdirected at another system. | W6.2.2.1, W6.2.2.2 |
| 6.3.3 | HELP NEEDED: Someone to recover baggage misdirected at another system.  | W6.3.3.1, W6.3.3.2 |
| 6.4.4 | HELP US: Group of stranded tourists without passage.                    | W6.4               |
| 6.5.5 | DELUXOR: We need to deliver materials.                                  | G3                 |
| 6.5.6 | DELUXOR: Someone to deliver us materials.                               | G3.1, G3           |

**GalShopping**

| Code  | Title                                                             | Waiting Items |
| :---: | :---------------------------------------------------------------- | :------------ |
| 7.5.1 | COMMERCIAL: Need a ship to deliver cargo to another system. (h>A) | G3            |
| 7.5.2 | COMMERCIAL: Need a ship to deliver cargo. (A>B)                   | G3.1, G3      |
| 7.5.3 | COMMERCIAL: Need a ship to bring cargo here. (A>h)                | G3.1, G3      |

**GalNature**

| Code  | Title                                                        | Waiting Items      |
| :---: | :----------------------------------------------------------- | :----------------- |
| 8.3.1 | IMPORTANT: Someone with a ship to recover plants specimens.  | W8.3.1.1, W8.3.1.2 |
| 8.4.2 | JOB OFFER: Need ship for transport.	(h>A)                    | G1                 |
| 8.4.3 | JOB OFFER: Need ship for transport. (A>B)                    | G2, G1             |
| 8.4.4 | JOB OFFER: Need ship for transport. (A>h)                    | G2, G1             |
| 8.5.5 | SHIP REQUESTED: Deliver materials to another system. (h>A)   | G3                 |
| 8.5.6 | STUDY: Looking for a ship to transport materials here. (A>h) | G3.1, G3           |

**GalCulture**

| Code  | Title                                                                        | Waiting Items      |
| :---: | :--------------------------------------------------------------------------- | :----------------- |
| 9.1.1 | DELIVER: A painting to a buyer.                                              | W9.1.1             |
| 9.2.2 | TRANSFER: Move a painting to its new location.                               | W9.2.2.1, W9.2.2.2 |
| 9.3.3 | RECOVER: A painting from an art restorer.                                    | W9.3.3.1, W9.3.3.2 |
| 9.4.4 | ART: Transport a famous artist to an event. (h>A)                            | G1                 |
| 9.4.5 | ART: Transport a famous artist to an event. (A>B)                            | G2, G1             |
| 9.5.6 | HELP NEEDED: Deliver materials to organize an event in another system. (h>A) | G3                 |
| 9.5.7 | ORGANIZING: We need a ship to transport equipment. (A>h)                     | G3.1, G3           |

**GalGovernment**

| Code   | Title                                                             | Waiting Items        |
| :----: | :---------------------------------------------------------------- | :------------------- |
| 10.1.1 | GOVERNMENT DUTY: Meeting a government agent.                      | W10.1.1              |
| 10.2.3 | GOVERNMENT DUTY: Moving papers between two government consulates. | W10.2.3.1, W10.2.3.2 |
| 10.3.2 | GOVERNMENT DUTY: Recover papers from another system.              | W10.3.2.1, W10.3.2.2 |
| 10.4.4 | GOVERNMENT DUTY: Transport a government officer to a meeting.     | G1                   |
| 10.4.5 | GOVERNMENT DUTY: Transport an agent to its destination.           | G1                   |
| 10.5.6 | GOVERNMENT DUTY: Deliver a shipment to destination. (h>A)         | G3                   |
| 10.5.7 | GOVERNMENT DUTY: Bring here a shipment from another system. (A>h) | G3.1, G3             |

## **Old Version Mission Templates**

| Gruppo             | Tipo Missione | Descrizione                                                                                                                                                    |
| :----------------- | :------------ | :------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0 - GalCop         |               |                                                                                                                                                                |
|                    | 0 - LOCAL     |                                                                                                                                                                |
|                    |               | 1. WANTED. Join the Security Auxiliary Corp! Keep safe the station! Apply now!                                                                                 |
|                    |               | 2. HELP. Searching volunteers to gather and report info on petty crimes.                                                                                       |
|                    |               | 3. HELP. Searching volunteers to gather and report info about suspicious behaviours.                                                                           |
|                    | 3 - CONTACT   |                                                                                                                                                                |
|                    |               | 1. SHIP REQUESTED. Deliver sensitive info to an agent at the RAND_SYSTEM system.                                                                               |
|                    | 4 - RETRIEVE  |                                                                                                                                                                |
|                    |               | 1. URGENT. Retrieve data from a compromised agent at the RAND_SYSTEM system.                                                                                   |
|                    |               | 2. IMPORTANT. Contact an agent and report important info.                                                                                                      |
|                    | 5 - TRANSFER  |                                                                                                                                                                |
|                    |               | 1. DANGEROUS: Transport an important witness to the RAND_SYSTEM system for the trial.                                                                          |
|                    |               | 2. DANGEROUS: Need ship for agent extraction.                                                                                                                  |
|                    | 6 - SEARCH    |                                                                                                                                                                |
|                    |               | 1. MISSING. Find and identify a missing suspect keeping a low profile.                                                                                         |
|                    |               | 2. MISSING. Find and identify a criminal on the run.                                                                                                           |
|                    | 8 - KILL      |                                                                                                                                                                |
|                    |               | 1. VERY DANGEROUS. Need ship to terminate an efferate criminal                                                                                                 |
|                    |               | 2. VERY DANGEROUS. Need ship to terminate a dangerous killer.                                                                                                  |
|                    |               | 3. VERY DANGEROUS. Need ship to terminate a corrupt CEO.                                                                                                       |
|                    |               | 4. VERY DANGEROUS. Need ship to terminate a Mafia operative.                                                                                                   |
|                    |               | 5. VERY DANGEROUS. Need ship to terminate a double agent.                                                                                                      |
|                    |               | 6. HIGHLY URGENT: Killer on the run - termination authorized.                                                                                                  |
|                    |               | 7. HIGHLY URGENT: Gang Member on the run - termination authorized.                                                                                             |
|                    |               | 8. HIGHLY URGENT: Mob Leader on the run - termination authorized.                                                                                              |
| 1 - GalBook        |               | Goods: Books                                                                                                                                                   |
|                    | 0 - LOCAL     |                                                                                                                                                                |
|                    |               | 1. GREAT OFFERS. Big book selling at the local library! Big discount!                                                                                          |
|                    |               | 2. HAVE BOOKS? We buy books. Come and see our prices.                                                                                                          |
|                    | 1 - TRANSPORT |                                                                                                                                                                |
|                    |               | 1. CARGO BULK. Searching someone to transport RAND_GOOD to the RAND_SYSTEM system.                                                                             |
|                    | 3 - CONTACT   |                                                                                                                                                                |
|                    |               | 1. BUYER NOTICE. RAND_NAME at the RAND_SYSTEM system is searching for a copy of RAND_BOOK.                                                                     |
|                    |               | 2. SELLER NOTICE. RAND_NAME at the RAND_SYSTEM system want to sell a copy of RAND_BOOK.                                                                        |
|                    | 4 - RETRIEVE  |                                                                                                                                                                |
|                    |               | 1. SHIP REQUESTED. I need to retrieve a book from RAND_SYSTEM.                                                                                                 |
| 2 - GalChurch      |               | Goods: Relics, Pamphlets, Sacred Materials, Holy Symbols                                                                                                       |
|                    | 0 - LOCAL     |                                                                                                                                                                |
|                    |               | 1. GOOD CITIZEN. The church asks for a donation. Help us to help everyone.                                                                                     |
|                    |               | 2. DEVOUT PEOPLE. Join the church social activities and share your time.                                                                                       |
|                    |               | 3. CALL TO FAITH. Come and participate at the next mass.                                                                                                       |
|                    |               | 4. CHURCH SUPPORT. Join our pacific evangelic parade! Everyone welcome!                                                                                        |
|                    | 1 - TRANSPORT |                                                                                                                                                                |
|                    |               | 1. CARGO RUN. Need a ship to transport church RAND_GOOD to the RAND_SYSTEM system.                                                                             |
|                    |               | 2. CHARITY RUN. Need a ship to transport essential goods to the RAND_SYSTEM system.                                                                            |
|                    | 4 - RETRIEVE  |                                                                                                                                                                |
|                    |               | 1. RETURN HOME. We need to transport back church material from the RAND_SYSTEM system.                                                                         |
|                    | 6 - SEARCH    |                                                                                                                                                                |
|                    |               | 1. MISSING. Trying to contact an hermit after several years.                                                                                                   |
|                    | 7 - TOUR      |                                                                                                                                                                |
|                    |               | 1. HOLY VOYAGE. Father RAND_NAME need a ship for touring different systems.                                                                                    |
| 3 - GalShady       |               | Goods: plutonium, uranium, slaves, brilliance, tobacco, liquors, weaponry, gems, gold, platinum, Robo-Fighters, Robo-Pleasures                                 |
|                    | 0 - LOCAL     |                                                                                                                                                                |
|                    |               | 1. WANTED: RAND_GOOD.                                                                                                                                          |
|                    |               | 2. SURPLUS: RAND_GOOD in stock! Get it before it sells out!                                                                                                    |
|                    | 1 - TRANSPORT |                                                                                                                                                                |
|                    |               | 1. WANTED: A ship for a small group of people for a discreet passage to the RAND_SYSTEM system.                                                                |
|                    |               | 2. WANTED: A ship for a low-profile transfer to the RAND_SYSTEM system.                                                                                        |
|                    | 2 - SMUGGLE   |                                                                                                                                                                |
|                    |               | 1. JOB OFFER: A ship to transport goods to the RAND_SYSTEM system.                                                                                             |
|                    | 3 - CONTACT   |                                                                                                                                                                |
|                    |               | 1. URGENT: We need someone to deliver important data to an affiliate.                                                                                          |
|                    | 4 - RETRIEVE  |                                                                                                                                                                |
|                    |               | 1. JOB OFFER: We need to retrieve a parcel from the RAND_SYSTEM system.                                                                                        |
|                    | 6 - SEARCH    |                                                                                                                                                                |
|                    |               | 1. MISSING PERSON: RAND_NAME location's unknown. We need to contact him.                                                                                       |
|                    | 8 - KILL      |                                                                                                                                                                |
|                    |               | 1. REMOVAL REQUIRED: RAND_NAME is no longer wanted in the RAND_SYSTEM system.                                                                                  |
|                    |               | 2. EARLY RETIREMENT: We wish to encourage RAND_NAME in the RAND_SYSTEM system to stop work permanently.                                                        |
|                    |               | 3. WANTED: Someone for a removal job. RAND_NAME needs removing from the RAND_SYSTEM system.                                                                    |
|                    |               | 4. BIOGRAPHICAL: Some admirers wish RAND_NAME to have a fitting career end in the RAND_SYSTEM system.                                                          |
| 4 - GalMedical     |               | Goods: Medicines, Robo-Medics, Medical Equipment, Software                                                                                                     |
|                    | 0 - LOCAL     |                                                                                                                                                                |
|                    |               | ???                                                                                                                                                            |
|                    | 1 - TRANSPORT |                                                                                                                                                                |
|                    |               | 1. URGENT: Transport of RAND_GOOD to help stopping an epidemic at the RAND_SYSTEM system.                                                                      |
|                    |               | 2. DELIVER: RAND_GOOD needs to be taken to the RAND_SYSTEM system.                                                                                             |
|                    | 3 - CONTACT   |                                                                                                                                                                |
|                    |               | 1. MISSING: Someone to give updates about a medical team at the RAND_SYSTEM system.                                                                            |
|                    |               | 2. MISSING: Someone to give updates about a research group at the RAND_SYSTEM system.                                                                          |
|                    |               | 3. MISSING: Someone to give updates about an emergency team at the RAND_SYSTEM system.                                                                         |
|                    | 4 - RETRIEVE  |                                                                                                                                                                |
|                    |               | 1. SHIP REQUESTED: Research and clinical records to be retrieved from the RAND_SYSTEM system.                                                                  |
| 5 - GalCivic       |               | Goods: Generic Foods, Luxury Foods, Textiles, Liquors, Pets, Games, Holographics, Home Entertainment, Furs, Furniture, Robo-Servants                           |
|                    | 0 - LOCAL     |                                                                                                                                                                |
|                    |               | 1. COMMUNITY HELP: Work for the local social services.                                                                                                         |
|                    | 1 - TRANSPORT |                                                                                                                                                                |
|                    |               | 1. TAXI JOB: Needs a ship to transport a person to the RAND_SYSTEM system.                                                                                     |
|                    |               | 2. DELIVER: RAND_GOOD to the RAND_SYSTEM system.                                                                                                               |
|                    |               | 3. DELIVER: Take a small package to the RAND_SYSTEM system.                                                                                                    |
|                    |               | 4. WANTED: A ship to transport a parcel to the RAND_SYSTEM system.                                                                                             |
|                    | 3 - CONTACT   |                                                                                                                                                                |
|                    |               | 1. WANTED: A ship to return a package to RAND_NAME at the RAND_SYSTEM system.                                                                                  |
|                    |               | 2. NEED HELP: I must meet my family at the RAND_SYSTEM system!                                                                                                 |
|                    | 6 - SEARCH    |                                                                                                                                                                |
|                    |               | 1. MISSING PERSON: Need info on RAND_NAME from the RAND_SYSTEM system.                                                                                         |
| 6 - GalTourism     |               |                                                                                                                                                                |
|                    | 0 - LOCAL     |                                                                                                                                                                |
|                    |               | 1. QUALITY TIME: Relax yourself at our luxury hotels!                                                                                                          |
|                    | 3 - CONTACT   |                                                                                                                                                                |
|                    |               | 1. WANT A JOB? Meet a job counselor for info!                                                                                                                  |
|                    | 7 - TOUR      |                                                                                                                                                                |
|                    |               | 1. WANTED: Touring ship to transport a group of people in a number of systems.                                                                                 |
| 7 - GalShopping    |               | Goods: Junk Foods, Grain, Food Dispensers, Textiles, Liquors, Luxury Foods, Wood, Computers, Home Appliances, Furs                                             |
|                    | 0 - LOCAL     |                                                                                                                                                                |
|                    |               | ???                                                                                                                                                            |
|                    | 4 - RETRIEVE  |                                                                                                                                                                |
|                    |               | 1. JOB OPPORTUNITY: Someone to get back RAND_GOOD shipment from the RAND_SYSTEM system.                                                                        |
| 8 - GalNature      |               | Goods: Endangered Animals, Endangered Plants, Animal, Plants, Paleo-Art, Archeo-Sim                                                                            |
|                    | 0 - LOCAL     |                                                                                                                                                                |
|                    |               | 1. RELAX TIME: A wonderful voyage into the Wild Word Virtual Tour!                                                                                             |
|                    | 1 - TRANSPORT |                                                                                                                                                                |
|                    |               | 1. JOB OFFER: RAND_GOOD to be delivered to the RAND_SYSTEM system.                                                                                             |
|                    | 4 - RETRIEVE  |                                                                                                                                                                |
|                    |               | 1. JOB OFFER: Someone to get back RAND_GOOD from the RAND_SYSTEM system.                                                                                       |
| 9 - GalCulture     |               | Goods: Paleo-Art, Archeo-Sim, Fossils, Artwork, Movies, Holographics, Music Equipment;<br/>Locations: Natural Museum, Art Gallery, Opera House, Virtual Cinema |
|                    | 0 - LOCAL     |                                                                                                                                                                |
|                    |               | 1. ENJOY: Spend some quality time at the RAND_GALCULTURE_LOCATION!                                                                                             |
|                    | 1 - TRANSPORT |                                                                                                                                                                |
|                    |               | 1. DELIVER: RAND_GOOD to be delivered to the *system* system.                                                                                                  |
|                    | 4 - RETRIEVE  |                                                                                                                                                                |
|                    |               | 1. SENSITIVE TASK: Someone to get back RAND_GOOD from the *system* system.                                                                                     |
| 10 - GalGovernment |               |                                                                                                                                                                |
|                    | 0 - LOCAL     |                                                                                                                                                                |
|                    |               | 1. COMRADE! Support the local propaganda!                                                                                                                      |
|                    |               | 2. COMRADE! Join the pamphlets distribution raids!                                                                                                             |
|                    | 1 - TRANSPORT |                                                                                                                                                                |
|                    |               | 1. WANTED. A ship for the transport of a government member to the RAND_SYSTEM system.                                                                          |
|                    |               | 2. WANTED. A ship for the transport of a government agent to the RAND_SYSTEM system.                                                                           |
|                    | 3 - CONTACT   |                                                                                                                                                                |
|                    |               | 1. DATA RETRIEVAL. Meeting an undercover government agent.                                                                                                     |
|                    | 4 - RETRIEVE  |                                                                                                                                                                |
|                    |               | 1. RETURN DOCUMENTS. Travel to the RAND_SYSTEM system to recover stolen dossiers.                                                                              |
|                    | 8 - KILL      |                                                                                                                                                                |
|                    |               | 1. OFFICIAL INVESTIGATIONS. Find a government traitor on the run.                                                                                              |
|                    |               | 2. OFFICIAL INVESTIGATIONS. Find a government spy.                                                                                                             |
|                    |               | 3. OFFICIAL INVESTIGATIONS. Find a political dissident.                                                                                                        |
|                    |               | 4. OFFICIAL INVESTIGATIONS. Find a dangerous revolutionary.                                                                                                    |

**Descriptions**

| Label  | Description                                                                                                                                                                                                                                                                                                                    |
| :----: | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0.0.1  | Hello citizen. GalCop needs some extra personnel to guarantee a sufficient level of safety around here. Do you want to apply?                                                                                                                                                                                                  |
| 0.0.2  | Hello citizen. GalCop needs some extra personnel to help eradicate any trace of criminal activity at this location. Do you want to apply?                                                                                                                                                                                      |
| 0.0.3  | Hello citizen. GalCop needs some extra personnel to help eradicate any trace of criminal activity at this location. Do you want to apply?                                                                                                                                                                                      |
| 0.5.1  | Hello citizen, we need someone who will pickup and transport RECIPIENT, a witness in a delicate trial, from the SYSTEM1 system to a destination that will be revealed to you when you'll arrive at destination. The pay is REWARD.                                                                                             |
| 0.5.2  | Hello citizen, we need a fast extraction for our agent RECIPIENT, who is currently blocked on the SYSTEM1 system. Your task is to pickup the agent and go to the SYSTEM2 system for security reasons. We will pay REWARD.                                                                                                      |
| 0.6.1  | Hello citizen, we got info on the possible locations of a suspect that we are searching. Our info states that you could find RECIPIENT on the systems SYSTEM1, SYSTEM2 or SYSTEM3. You will have to check the BBS of these systems and make contact with the suspect without causing alerts. The pay is REWARD.                |
| 0.6.2  | Hello citizen, we got info on the possible locations of a fugitive criminal. Our info states that you could find RECIPIENT on the systems SYSTEM1, SYSTEM2 or SYSTEM3. You will have to check the BBS of these systems and make contact with the suspect without causing alerts. The pay is REWARD.                            |
| 0.8.1  | Hello citizen. RECIPIENT is a dangerous criminal that needs to be terminated. The target has been located in the SYSTEM1 system. Are you available for this work? We will pay REWARD.                                                                                                                                          |
| 0.8.2  | Hello citizen. RECIPIENT is a serial killer that needs to be terminated. The target has been located in the SYSTEM1 system. Are you available for this work? The pay is REWARD.                                                                                                                                                |
| 0.8.3  | Hello citizen. RECIPIENT is a corrupt CEO that needs to be terminated. The target has been located in the SYSTEM1 system. Are you available for this work? We will pay REWARD.                                                                                                                                                 |
| 0.8.4  | Hello citizen. RECIPIENT is a high-order Mafia operative that needs to be terminated. The target has been located in the SYSTEM1 system. Are you available for this work? The pay is REWARD.                                                                                                                                   |
| 0.8.5  | Hello citizen. RECIPIENT is a merciless double agent that needs to be terminated. The target has been located in the SYSTEM1 system. Are you available for this work? We will pay REWARD.                                                                                                                                      |
| 0.8.6  | Hello citizen. We received authorization to ask for volunteers to quickly eliminate RECIPIENT, a dangerous killer currently on the run in the SYSTEM1 system. Time is of the essence. The pay is REWARD.                                                                                                                       |
| 0.8.7  | Hello citizen. We received authorization to ask for volunteers to quickly eliminate RECIPIENT, a reckless criminal gang member currently on the run in the SYSTEM1 system. Time is of the essence. We will pay REWARD.                                                                                                         |
| 0.8.8  | Hello citizen. We received authorization to ask for volunteers to quickly eliminate RECIPIENT, a powerful mob leader currently on the run in the SYSTEM1 system. Time is of the essence. The pay is REWARD.                                                                                                                    |
| 1.0.1  | The SYSTEM0 system library is here to give his customer the chance to look for rare and ancient tomes! We aren't responsible for spending too much time in our endless stack of books.                                                                                                                                         |
| 1.0.2  | It's a painful affair, but sometimes you have to choose between a beloved book and some money. We're here to help you in the last case.                                                                                                                                                                                        |
| 1.1.1  | Hi, I'm CONTRACTOR and I need a ship for carrying MISSIONGOODS to the SYSTEM1 system. I pay REWARD.                                                                                                                                                                                                                            |
| 1.3.2  | Hello, RECIPIENT in the SYSTEM1 system has a spare copy of MISSIONBOOK to sell. Are you interested?                                                                                                                                                                                                                            |
| 2.0.1  | Good day to you, benign soul. Do you feel in the mood to lift your spirit through separation from material values?                                                                                                                                                                                                             |
| 2.0.2  | Good day to you, propositive soul. Do you wish to help your brothers through working in social initiatives around this location?                                                                                                                                                                                               |
| 2.0.3  | Good day, enlightened soul. The time of the ritual is approaching. Why don't you join us in partaking the holy spirit of the Universe?                                                                                                                                                                                         |
| 2.0.4  | Good day, open minded soul. We are organizing an evangelic parade to bring the word of the Universe to the people of this location. Do you wish to help us in spreading the word?                                                                                                                                              |
| 2.1.1  | Hello, kindred soul. I'm CONTRACTOR and in behalf of the church I need someone to bring MISSIONGOODS to the SYSTEM1 system. The church will pay REWARD.                                                                                                                                                                        |
| 2.1.2  | Hello, kindred soul. I'm CONTRACTOR and in behalf of the church I'm searching for a pious individual to bring essential goods to the church of the SYSTEM1 system very quickly. The church will pay REWARD.                                                                                                                    |
| 2.6.1  | Hello, kindred soul. I'm CONTRACTOR and in behalf of the church I'm looking for a way to contact RECIPIENT, an hermit that went in pilgrimage time ago. We need someone to find his current location and assess his conditions. Thon should be in the systems SYSTEM1, SYSTEM2 or SYSTEM3. The church will pay REWARD.         |
| 2.7.1  | Hello, kindred soul. I'm CONTRACTOR and in behalf of the church I need a ship to transport Father RECIPIENT in an evangelical tour in the systems SYSTEM1, SYSTEM2 and SYSTEM3. Do you wish to help? The church will pay REWARD.                                                                                               |
| 3.0.1  | Hi. I need MISSIONGOODS. Do you have some to sell?                                                                                                                                                                                                                                                                             |
| 3.0.2  | Hi. I have MISSIONGOODS to sell. Are you interested?                                                                                                                                                                                                                                                                           |
| 3.1.1  | Hi. I'm CONTRACTOR. There's a small group of people that needs a ship to go to the SYSTEM1 system without making too much noise. Are you available? We pay REWARD.                                                                                                                                                             |
| 3.1.2  | Hi. I'm CONTRACTOR. I need a fast ship. Destination: the SYSTEM1 system. Are you interested? I pay REWARD.                                                                                                                                                                                                                     |
| 3.2.1  | Hi. I'm CONTRACTOR. I'm looking for a ship to carry MISSIONGOODS to the SYSTEM1 system. I pay REWARD.                                                                                                                                                                                                                          |
| 3.6.1  | Hi. I'm CONTRACTOR. I need to get in contact with RECIPIENT, but I don't exactly remember where you can find thon. Maybe the SYSTEM1 system... or SYSTEM2, or even SYSTEM3. You should do a little research on the local BBS of these systems... I pay REWARD for the disturb.                                                 |
| 3.8.1  | Hi. Some people is annoyed by certain actions and words of RECIPIENT, in the SYSTEM1 system. They want an efficient and clean solution to the matter, in the form of a big explosion in space. They pay REWARD.                                                                                                                |
| 3.8.2  | Hi. RECIPIENT works in the SYSTEM1 system, and we believe that is the right moment to grant thon a vacation. Therefore we want to encourage thon to stop work permanently. We pay REWARD.                                                                                                                                      |
| 3.8.3  | Hi. There's a person in the SYSTEM1 system, named RECIPIENT, that could suffer an accident soon. Do you wish to join in grinding the gears of doom? I pay REWARD.                                                                                                                                                              |
| 3.8.4  | Hi. We think that RECIPIENT has done a lot for the SYSTEM1 system, and now is the moment for thon to step aside in a definitive manner. We will remember him with great care. We pay REWARD.                                                                                                                                   |
| 4.1.1  | Hello, I'm CONTRACTOR. There is currently an epidemic at the SYSTEM1 system and we need to transport there MISSIONGOODS as soon as possible. Once you arrive you can talk to doctor RECIPIENT. Can you help us? The pay is REWARD.                                                                                             |
| 4.1.2  | Hello, I'm doctor CONTRACTOR, and I need someone to transport MISSIONGOODS to the SYSTEM1 system. The goods are to be delivered to doctor RECIPIENT. I pay REWARD.                                                                                                                                                             |
| 4.7.1  | Hello, I'm doctor CONTRACTOR and I have to visit several systems to assess the medical conditions on site and prevent further epidemics. The systems I need to go are: SYSTEM1, SYSTEM2 and SYSTEM3. The pay is REWARD.                                                                                                        |
| 5.0.1  | Hello citizen, I'm CONTRACTOR and I want to increase the quality of life of this station. There are a series of programmes you can join if you want. Your help will be appreciated.                                                                                                                                            |
| 5.1.1  | Hello, I'm CONTRACTOR and I need a passage to the SYSTEM1 system. I have to meet RECIPIENT there. Can you help me? I pay REWARD.                                                                                                                                                                                               |
| 5.1.2  | Hello, I'm CONTRACTOR and I'm looking for a ship to deliver MISSIONGOODS to RECIPIENT in the SYSTEM1 system. The pay is REWARD.                                                                                                                                                                                                |
| 5.1.3  | Hi, I'm CONTRACTOR. RECIPIENT is awaiting a package from me in the SYSTEM1 system. Can you bring it to thon? The pay is REWARD.                                                                                                                                                                                                |
| 5.1.4  | Hi. Can you deliver some stuff to RECIPIENT in the SYSTEM1 system? I pay REWARD.                                                                                                                                                                                                                                               |
| 5.1.5  | Hello, I'm CONTRACTOR and I looking for a passage for two people to the SYSTEM1 system. RECIPIENT is awaiting us there. Can you help me? I pay REWARD.                                                                                                                                                                         |
| 5.1.6  | Hello, I'm CONTRACTOR and I looking for a passage for a few people to the SYSTEM1 system. RECIPIENT is awaiting us there. Can you help me? I pay REWARD.                                                                                                                                                                       |
| 5.1.7  | Hello, I'm CONTRACTOR and I looking for a passage for a group of people to the SYSTEM1 system. RECIPIENT is awaiting us there. Can you help me? I pay REWARD.                                                                                                                                                                  |
| 5.6.1  | Hi. I'm CONTRACTOR and I'm looking for RECIPIENT. For what I know, thon could be in the systems SYSTEM1, SYSTEM2 or SYSTEM3. Can you help find thon? The pay is REWARD.                                                                                                                                                        |
| 6.0.1  | Are you tired? Stressed? Do you want to retire from the crowd and the chaos of intergalactic travels? Book a room in one of our hotels and spend some quality time for your body and spirit!                                                                                                                                   |
| 6.3.1  | Hi. If you need a job I can organize a meeting in a hotel room in the SYSTEM1 system with RECIPIENT - a famous job counselor with excellent opportunities. Are you interested?                                                                                                                                                 |
| 6.7.1  | Hi, I'm CONTRACTOR of Galaxy Travel and I have a group of tourists without a ship. We need to visit the SYSTEM1, SYSTEM2 and SYSTEM3 systems. I'll pay REWARD if you can bring us there.                                                                                                                                       |
| 7.1.1  | Hello, I'm CONTRACTOR. I have a cargo of MISSIONGOODS that I have to transport to RECIPIENT, in the SYSTEM1 system, but I don't have a ship. I pay REWARD if you take the job.                                                                                                                                                 |
| 7.4.1  | Hello, I'm CONTRACTOR. There's a shipment of MISSIONGOODS that I have to get back from RECIPIENT in the SYSTEM1 system. Cold you bring back it? I'll pay REWARD.                                                                                                                                                               |
| 8.0.1  | Have you ever seen the wilderness? Have you seen animals roaming free, in their natural habitat? If not, you can't imagine it. Buy a ticket ed enjoy the Wild Word Virtual Tour - a voyage into 16 different natural worlds, pure and incontaminated.                                                                          |
| 8.1.1  | Hello, my name is CONTRACTOR. We have to bring MISSIONGOODS to RECIPIENT in the SYSTEM1 system. Can you help us? The pay is REWARD.                                                                                                                                                                                            |
| 9.0.1  | If you have some spare time, why don't you allow yourself to enjoy the beauty of the galactic cultures? Spend some remarkable time to enrich your world view, and to experience something new.                                                                                                                                 |
| 9.1.1  | Hi, I'm CONTRACTOR and I need a ship to transport MISSIONGOODS to RECIPIENT in the SYSTEM1 system. I'll pay REWARD.                                                                                                                                                                                                            |
| 9.4.1  | Hi, my name is CONTRACTOR. I need someone that can meet RECIPIENT in the SYSTEM1 system and get back my shipment of MISSIONGOODS. The pay is REWARD.                                                                                                                                                                           |
| 10.0.1 | The mind of the people is always vulnerable to the persuasive messages of the decadent cultures. We have to reinforce their spirit! Pamphlet, posters, public inspirational recordings. LEND A HAND TO HELP YOU!                                                                                                               |
| 10.0.2 | It's time to move! To the streets! To the houses! Spread the word of the government! Show the people the loyalty, the faith! Join us and DO YOUR PART!                                                                                                                                                                         |
| 10.1.1 | Citizen, a ship is requested for the transport of the egregious RECIPIENT to the SYSTEM1 system for an irrevocable rally! Your government will cover the expenses with REWARD.                                                                                                                                                 |
| 10.1.2 | Citizen, a ship is requested for the transport of an important member of the secret services to the SYSTEM system. No questions! Your government will cover the expenses with REWARD.                                                                                                                                          |
| 10.8.1 | Citizen, we need to find a traitor named RECIPIENT, that has fled the system to bring crucial info to our enemies! Find it and kill him! We suspect that the traitor should be in the SYSTEM1 system. Your government will cover the expenses with REWARD.                                                                     |
| 10.8.2 | Citizen, we need to find a spy named RECIPIENT that has developed some aberrant behaviours towards the government! We can't allow thon to defame the regime! You have to stopped thon! The spy should be in the SYSTEM system. Your government will cover the expenses with REWARD.                                            |
| 10.8.3 | Citizen, you have to find RECIPIENT, a political dissident that should be fleeing through the SYSTEM1 system. Find it and terminate this annoying ideological saboteur! Your government will cover the expenses with REWARD.                                                                                                   |
| 10.8.4 | Citizen, rebels are trying to destabilize the government platform. We can't allow this. We have to send a strong message of power! For this we wish to find RECIPIENT, a prominent revolutionary that you should find in the SYSTEM1 system, and terminate thon activity. Your government will cover the expenses with REWARD. |

# **Frontier: Elite Missions**

- Deliver -- to the -- system by -- on --. You will be paid --.

        some superconducting wire
        requisition forms
        paperwork
        some documents
        a message
        blank military data cards
        some medals
        some messages
        cryptographic code cards
        gravitic detonators
        military grade components
        weapons grade explosive capsules
        secret orders
        secret plans
        secret dossiers
        High Command communications

- Assassinate a -- in the -- system on --. Payment --.

        enemy senator
        disloyal company president
        Mafia operative
        traitorous merchant
        spying company official
        enemy governor
        double agent and company director
        double crossing spy

- Photograph the -- in the -- system and return by midnight on --. Payment --.

        military installation
        bio-weapons facility
        research station
        training camp
        weapons factory
        special forces base
        listening station
        interrogation centre

- Destroy the -- in the -- system on --. Payment --. Return by midnight on --.

        military installation
        training camp
        weapons factory
        interrogation centre
        research station
        bio-weapons facility
        special forces base
        listening station

- MISSING PERSON: - - last seen in the -- system. -- paid for useful information. 

- -- REWARD: for useful information on -- from the -- system.

- SHIP REQUIRED: payment of -- for a ship to take a small package to the -- system.

- WANTED: a ship to take a small package to the -- system. Will pay --.

- WANTED: passage on a fast ship to the -- system. Will pay --.

- WANTED: passage on a ship for a group to the -- system. Will pay --.

- SHIP REQUIRED: payment of -- for passage for -- people on a fast ship to the -- system.

- SHIP REQUIRED: payment of -- for passage on a ship for a group to the -- system.

- REMOVAL REQUIRED: Governor -- is no longer wanted in the -- system. Will pay --.

- REMOVAL REQUIRED: Director -- is no longer wanted in the -- system. Will pay --.

- RETIREMENT: for -- we wish to encourage -- in the -- system to stop work permanently.

- WANTED: someone for a removal job. Mr. -- needs removing from the -- system. Payment --.

- BIOGRAPHICAL: some admirers wish -- to have a fitting career end in the -- system for --.

- VACANCY REQUIRED: as a crew member on a starship.

- UNEMPLOYED CREW: need work on a starship. Anything considered.

- WANTED: position on a reputable starship.

- JOB WANTED: Need job to feed disabled spouse and starving family.

- WANTED: --. Will pay -- per tonne.

- GOODS BOUGHT AND SOLD: at --. Highest prices paid for items interesting or otherwise.

- FEELING GENEROUS? Then give as much as you can to --. All gifts gratefully received.

- DONATIONS: to -- for --. We need YOUR cash NOW!

# **Frontier: Elite Dialogues**

- WANTED: Someone for a removal job. -- of -- needs removing from the -- system. Payment --

    > Hello, thanks for contacting us, but we want someone with a higher Elite Federation combat rating.
	
    (Hang up.)

- DONATIONS: To -- We need YOUR cash NOW!
- FEELING GENEROUS? Then gives as much as you can to -- All gifts gratefully received.
	
    > Please select an amount to give to --
	
	(Donate 0.1)  
	(Donate 1)  
	(Donate 10)  
	(Donate 100)  
	(Donate 1000)  
	(Donate 10000)  

    > Thank you. All donations are gratefully received.

    > You don't have enough cash.

	(Hang up)

- FEDERAL MILITARY: We want YOU for adventures unlimited

    > Deliver some -- to the -- system by -- You will be paid --

	(Agreed, I'll do it Sir)  
	(I want more money)  
	(I want half the money now)  
	(I want all the money now)  

    > It's our policy to offer a fixed payment on completion of the mission only.

- SHIP REQUIRED: Payment of -- for a ship to take a small package to the -- system.	
	
    > I'm -- and I need a ship to take a small package to the -- system. I will pay --

	(Ok - agreed.)
	(Why so much money?)
	
    > It's a duplication copy of the latest dream card starring (Heather Goldstein).
	
    > It's just a normal parcel - a friend needs some components urgently
	
    > It's just a normal parcel. My friend will pay on arrival.

	(I want half the money now)
	
    > I can't pay any now.

	(Will there be any problems?)
	
    > You might get some interest from th epress. Just ignore them.
	
    > Someone is chasing me.
	
- SHIP REQUIRED: Payment of -- for passage on a fast ship to the -- system.

- WANTED: passage for two people on a fast ship to the -- system. Will pay --

- WANTED: passage for a small group to the -- system. Will pay --

- SHIP REQUIRED: Payment of -- for passage for a small group to the -- system.

- SHIP REQUIRED: Payment of -- for passage on a ship for a group to the -- system.
	
    > I'm -- and I need passage for a small group to the -- system. I will pay --

	(Ok - agreed.)
	(How many of you are there?)

    > There are 1 of us.
    
    > There are 2 of us.
    
    > There are (3, 4) of us.
    
    > There are (5, 6, 7) of us. 
	
    (Why so much money?)
	
    > I'm an executive of -- and they are paying

        Sirius Corporation
        Vega Line Corporation
	
    > I'm visiting a friend
	
    > I'm a travelling salesman
	
    > It's a normal business trip
	
    > The -- is after me. They may want me killed.

        Vega Line Corporation
	
    > I would rather someone didn't find me.
	
    > I'm a freelance journalist.

	(Will there be any problems?)
	
    > No.
	
    > I think the (Vega Line Corporation) has assassins on my trail.
	
    > I think someone is following me

	("Hold on while I make room." then hang up.)
	("Could you repeat your original request")
	("Do I need a permit, and if so can I have one?")

    > Don't be silly - you don't need a permit.

	(I want more money)
	
    > -- is all I can pay
	
    (I want half the money now)
	
    > Ok. I'll pay half once you agree, and half on arrival.
	
    > What sort of fool do you take me for?
	
    (I want all the money now)
	
    > We've agreed on payment.
	
    > What sort of fool do you take me for?
	
    ("I haven't enough room in my ship" then hang up)
	("Sorry, I'm not going that way" then hang up)
	(Hang up)

- WANTED: -- Will pay -- per tonne.
	
    > I'm --. I need -- urgently and I'll pay twice the market price (--) for them.

	(Sell 1 tonne of -- for -- (-- tonnes in cargo bay))
	(Hang up)

- MISSING PERSON: -- last seen in the -- system. -- paid for useful information.

- -- REWARD: for useful information on -- from the -- system

    > Hello, I'm looking for -- and I need some help with my search. Can you help me?
	
    > I need information on -- but I hear you are not trustworthy, so there is no point talking to you.

	(-- is in a cabin on my ship)
	
    > Oh really! Then why is -- not on your roster?
	
    (-- has an ad on this bulletin board)
	
    > Pah! Wrong. I checked five minutes ago.
	
    (I will look for --)
	
    > That would be appreciated, but I will only pay for solid information.
	
    (Hang up.)
