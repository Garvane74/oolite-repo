/* eslint-disable semi, no-multi-spaces, quotes, indent, no-tabs, yoda */
/* global worldScripts, system, mission, player, clock, randomName, setScreenBackground, expandMissionText */

"use strict";

this.name        = "LITF_BBS";
this.author      = "BeeTLe BeTHLeHeM";
this.copyright   = "2018 BeeTLe BeTHLeHeM";
this.description = "Code for BBS management - message list, mission details, mission map";
this.version     = "0.10.0";
this.licence     = "CC BY-NC-SA 4.0";

// Feedback requirements defaults

// Currently not used
this.minFeedbackDefault = -10; // Default minimum feedback required for missions (except GalShady ones)
this.maxFeedbackDefault = 24; // Default maximum feedback required for missions (only GalShady ones)

this.halfAdvanceMinFeedbackDefault = 10; // Default minimum feedback required for obtaining half advance reward
this.fullAdvanceMinFeedbackDefault = 50; // Default minimum feedback required for obtaining full advance reward
this.moreMoneyMinFeedbackDefault   = 25; // Default minimum feedback required for obtaining more reward (10-25% more)

this.bbsData = null;

// v1.77
var starChartModes = [ "SHORT_RANGE_CHART", "LONG_RANGE_CHART" ];
var starChartModesDesc = [ "Short Range", "Long Range" ];

// v1.87
// var starChartModes = [ "CUSTOM_CHART", "CUSTOM_CHART_SHORTEST", "CUSTOM_CHART_QUICKEST" ];

var _litf = null;
var _co   = null;
var _md   = null;
var _mi   = null;
var _sm   = null;
var _pa   = null;

// Local var reference for callback methods

var $display = this.$display;
var $executeItemAction = this.$executeItemAction;

this.$init = function () {
    _litf = worldScripts.LITF;

    _co = _litf.LITFInstances.co;
    _md = _litf.LITFInstances.md;
    _mi = _litf.LITFInstances.mi;
    _sm = _litf.LITFInstances.sm;
    _pa = _litf.LITFInstances.pa;

    _co.$logInfo(this.name, ">>> init");
}

this.$run = function () {
    _co.$logInfo(this.name, ">>> run");

    this.bbsData = _litf.litfVars.bbsData;

    // Load the BBS list
    this.$initBBSList();

    // Display the BBS page
    this.$display();
}

this.$initBBSList = function () {
    _co.$logInfo(this.name, ">>> initBBSList");

    // What to do with this?
    if (_litf.litfVars.enterBBSFlag === true) {
        _litf.litfVars.enterBBSFlag = false;
    }
}

//

this.$createBBSItems = function () {
    _co.$logInfo(this.name, ">>> createBBSItems");

    var bbsData = _litf.litfVars.bbsData;

    bbsData.bbsItemsArr = [];
    bbsData.checkDuplicatesArr = [];

    _co.$logInfo(this.name, "_mi = " + _mi);
    _co.$logInfo(this.name, "mi = " + _litf.LITFInstances.mi);

    // From the system government we get the active groups
    var stationData = _litf.litfVars.stationParams;
    var activeGroups = _mi.groupsByGov[stationData.government];

    // Cycle the active groups and eventually set some mission each group
    var totGroupMissions = 5 + _co.$roll(0, activeGroups.length / 2);
    for (var g = 0; g < totGroupMissions; g++) {
        var mObj = _mi.$generateMission(activeGroups);
        if (mObj != null) {
            mObj.missionId = _co.$getGeneralId();
            bbsData.bbsItemsArr.push(mObj);
        }
    }

    // Dialog mission generation (dummy)
    var totDialogMissions = _co.$roll(1, 4);
    for (var w = 0; w < totDialogMissions; w++) {
        var wObj = _mi.$generateRandomItem(_md.dialogStore);
        wObj.missionId = _co.$getGeneralId();
        bbsData.bbsItemsArr.push(wObj);
    }

    // Government specific messages
    var totGovItems = _co.$roll(3, 6);
    for (var go = 0; go < totGovItems; go++) {
        var gObj = _mi.$generateGovernmentItem();
        gObj.missionId = _co.$getGeneralId();
        bbsData.bbsItemsArr.push(gObj);
    }

    // Advertising messages generation
    var totAdItems = activeGroups.length + _co.$roll(0, 4);
    for (var a = 0; a < totAdItems; a++) {
        var aObj = _mi.$generateRandomItem(_md.adStore);
        aObj.missionId = _co.$getGeneralId();
        bbsData.bbsItemsArr.push(aObj);
    }

    // Charity request messages generation
    var totCharityItems = _co.$roll(1, 1);
    for (var c = 0; c < totCharityItems; c++) {
        var cObj = _mi.$generateRandomItem(_md.charityStore);
        cObj.missionId = _co.$getGeneralId();
        bbsData.bbsItemsArr.push(cObj);
    }

    // Debug //
    _co.$logDebug(this.name, "generate debug items");
    this.$createDebugItems();

    // Create items for active SEARCH missions
    var smObjArr = _sm.$checkSearchMissionBBSItems();
    bbsData.bbsItemsArr.concat(smObjArr);
    _co.$logDebug(this.name, "smObjArr = " + JSON.stringify(smObjArr));

    //

    _co.$logDebug(this.name, "generate dialog items");

    // Check if there's a mission to complete in this system - create BBS message accordingly.
    var activeMissions = _litf.litfCommander.activeMissions;

    _co.$logDebug(this.name, "activeMissions length = " + activeMissions.length);

    if (activeMissions.length > 0) {
        for (var am = 0; am < activeMissions.length; am++) {
            var amItem = activeMissions[am];

            if (this.$checkActiveMissionHere(amItem) === true) {
                var pwObj = _mi.$generateDialogMission(amItem);

                bbsData.bbsItemsArr.push(pwObj);
                _co.$logDebug(this.name, "pwObj = " + JSON.stringify(pwObj));
            }
        }
    }

    //

    _co.$logDebug(this.name, "shuffleArray");

    // Shuffle the list
    bbsData.bbsItemsArr = _co.$shuffleArray(bbsData.bbsItemsArr);
}

this.$createDebugItems = function () {
    var bbsData = _litf.litfVars.bbsData;

    var dObj = null;
    for (var d = 0; d < 2; d++) {
        dObj = _mi.$generateMission(["debug"], 1);
        if (dObj !== null) { bbsData.bbsItemsArr.push(dObj); }

        // dObj = _mi.$generateMission(["debug"], 2);
        // if (dObj !== null) { bbsData.bbsItemsArr.push(dObj); }

        // dObj = _mi.$generateMission(["debug"], 3);
        // if (dObj !== null) { bbsData.bbsItemsArr.push(dObj); }

        // dObj = _mi.$generateMission(["debug"], 4);
        // if (dObj !== null) { bbsData.bbsItemsArr.push(dObj); }
    }
}

//

this.$updateMissionStarChart = function (choice) {
    _co.$logInfo(this.name, "updateMissionStarChart");

    var bbsData = _litf.litfVars.bbsData;

    if (!choice) {
        _co.$logDebug(this.name, "NULL choice? What's happened there?");
        return;
    } else if (choice === "01_CYCLEMODE") {
        var anaOK = (player.ship.equipmentStatus("EQ_ADVANCED_NAVIGATIONAL_ARRAY") === "EQUIPMENT_OK");

        var topLimit = anaOK === true ? starChartModes.length : 2;

        bbsData.starChartModeIdx = bbsData.starChartModeIdx + 1;
        if (bbsData.starChartModeIdx >= topLimit) {
            bbsData.starChartModeIdx = 0;
        }

        $display();
    } else if (choice === "02_CYCLEZOOM") {
        bbsData.starChartZoom = bbsData.starChartZoom + 1;
        if (bbsData.starChartZoom > 4) {
            bbsData.starChartZoom = 1;
        }

        $display();
    } else if (choice === "03_CYCLESYSTEMS") {
        bbsData.starChartSystemCentre = bbsData.starChartSystemCentre + 1;
        if (bbsData.starChartSystemCentre >= bbsData.starChartMarkedSystems.length) {
            bbsData.starChartSystemCentre = 0;
        }

        $display();
    } else if (choice === "99_RETURN") {
        bbsData.bbsMode = 1;

        for (var ma = 0; ma < bbsData.starChartMarkedSystems.length; ma++) {
            var removed = mission.unmarkSystem(bbsData.starChartMarkedSystems[ma]);
            _co.$logDebug(this.name, "remove marker = " + removed + " :: " + JSON.stringify(bbsData.starChartMarkedSystems[ma]));
        }

        $display();
    }
}

this.$updateMissionDetails = function (choice) {
    _co.$logInfo(this.name, ">>> updateMissionDetails");

    var bbsData = _litf.litfVars.bbsData;
    var selectedItemObj = bbsData.selectedItemObj;

    _co.$logDebug(this.name, "choice = " + choice);

    if (!choice) {
        _co.$logDebug(this.name, "NULL choice? What's happened there?");
        return;
    } else {
        $executeItemAction(selectedItemObj, choice);
        $display();
    }
}

this.$updateMissionList = function (choice) {
    var bbsData = _litf.litfVars.bbsData;

    if (!choice) {
        _co.$logDebug(this.name, "NULL choice? What's happened there?");
        return;
    } else if (choice === "99_DISCONNECT") {
        bbsData.firstItemIdx = 0;
        bbsData.selectedItem = 0;

        _litf.$LITFinterface();
    } else {
        if (choice === "01_PREV_ITEM") {
            if (bbsData.selectedItem > 0) {
                bbsData.selectedItem = bbsData.selectedItem - 1;
            }
        } else if (choice === "02_NEXT_ITEM") {
            if (bbsData.selectedItem < bbsData.bbsItemsArr.length - 1) {
                bbsData.selectedItem = bbsData.selectedItem + 1;
            }
        } else if (choice === "03_SELECT_ITEM") {
            // Open the details screen

            bbsData.selectedItemObj = bbsData.bbsItemsArr[bbsData.selectedItem];
            bbsData.selectedItemObj.response = null;

            bbsData.bbsMode = 1;
        }

        bbsData.lastChoice = choice;

        $display();
    }
}

//

this.$checkActiveMissionHere = function (amItem) {
    _co.$logInfo(this.name, ">>> checkActiveMissionHere");
    _co.$logDebug(this.name, "amItem = " + JSON.stringify(amItem));

    var cTask = this.$getMissionTask(amItem, amItem.currentTask);
    _co.$logDebug(this.name, "cTask = " + JSON.stringify(cTask));

    var systems = cTask.info.systems;
    var amHere = false;

    for (var sy = 0; sy < systems.length; sy++) {
        if (this.$isSystems(systems) === true) {
            amHere = true;
            break;
        }
    }

    return amHere;
}

// Check if the player is in one of the systems of the input array
this.$isSystems = function (systemArr) {
    _co.$logInfo(this.name, ">>> isSystem");

    var isSystems = false;

    for (var s = 0; s < systemArr.length; s++) {
        // System exists and its not already "completed" (visit + talk)

        _co.$logDebug(this.name, "system.ID = " + system.ID + " systemArr[" + s + "] = " + JSON.stringify(systemArr[s]));

        if (!systemArr[s].hidden || systemArr[s].hidden === false) {
            if (systemArr[s].id === system.ID) {
                isSystems = true;
            }
        }
    }

    return isSystems;
}

this.$getCurrentStep = function (pwObj) {
    _co.$logInfo(this.name, ">>> getCurrentStep");

    var steps = pwObj.steps;
    var currentStep = pwObj.currentStep;
    var sObj = {};

    for (var s = 0; s < steps.length; s++) {
        var sItem = steps[s];
        if (sItem.index === currentStep) {
            sObj = sItem;
            break;
        }
    }

    _co.$logDebug(this.name, ">>> step[" + currentStep + "] = " + JSON.stringify(sObj));

    return sObj;
}

this.$getChoices = function (cLabel) {
    _co.$logDebug(this.name, ">>> getChoices[" + cLabel + "]");

    var sChoices = _md.scriptChoices;
    var cTemp = {};

    cTemp["QUIT"] = sChoices["QUIT"];

    if (cLabel === "charity") {
        cTemp["DONATE01"] = sChoices["DONATE01"];
        cTemp["DONATE02"] = sChoices["DONATE02"];
        cTemp["DONATE03"] = sChoices["DONATE03"];
        cTemp["DONATE04"] = sChoices["DONATE04"];
        cTemp["DONATE05"] = sChoices["DONATE05"];
        cTemp["DONATE06"] = sChoices["DONATE06"];
    } else if (cLabel !== "hangup") {
        cTemp["OK"] = sChoices["OK"];
        cTemp["MONEY"] = sChoices["MONEY"];
        cTemp["PROBLEMS"] = sChoices["PROBLEMS"];

        if (_litf.litfVars.debug.enableStarChartScreen === true) {
            cTemp["STARCHART"] = sChoices["STARCHART"];
        }

        if (cLabel === "transport") {
            cTemp["HOWMANY"] = sChoices["HOWMANY"];
        }

        if (cLabel === "mission") {
            cTemp["UNAWARE"] = sChoices["UNAWARE"];
        }

        if (cLabel === "mission" || cLabel === "transport" || cLabel === "cargo") {
            cTemp["MOREMONEY"] = sChoices["MOREMONEY"];
            cTemp["HALFADVANCE"] = sChoices["HALFADVANCE"];
            cTemp["FULLADVANCE"] = sChoices["FULLADVANCE"];
        }
    }

    var choices = _pa.$buildChoiceArray(cTemp);
    _co.$logDebug(this.name, "choices = " + JSON.stringify(choices));

    // var choices = {
    //     "99_QUIT": "Hang up."
    // };

    // if (cLabel === "charity") {
    //     choices["01_DONAATE01"]   = "Donate 0.1 credits.";
    //     choices["02_DONATE1"]     = "Donate 1 credit.";
    //     choices["03_DONATE10"]    = "Donate 10 credits.";
    //     choices["04_DONATE100"]   = "Donate 100 credits.";
    //     choices["05_DONATE1000"]  = "Donate 1000 credits.";
    //     choices["06_DONATE10000"] = "Donate 10000 credits.";
    // } else if (cLabel !== "hangup") {
    //     choices["10_OK"]       = "Ok - agreed.";
    //     choices["30_MONEY"]    = "Why so much money?";
    //     choices["40_PROBLEMS"] = "Will there be any problems?";

    //     if (_litf.litfVars.debug.enableStarChartScreen === true) {
    //         choices["15_STARCHART"] = "Examine the Star Chart Map.";
    //     }

    //     if (cLabel === "transport") {
    //         choices["20_HOWMANY"] = "How many of you are there?";
    //     }

    //     if (cLabel === "mission") {
    //         choices["50_UNAWARE"] = "Do the recipient will know of my arrival?";
    //     }

    //     if (cLabel === "mission" || cLabel === "transport" || cLabel === "Cargo") {
    //         choices["60_MOREMONEY"]   = "I want more money.";
    //         choices["70_HALFADVANCE"] = "I want half the money now.";
    //         choices["80_FULLADVANCE"] = "I want all the money now.";
    //     }
    // }

    return choices;
}

this.$setPageListEdges = function () {
    var bbsData = _litf.litfVars.bbsData;

    if (bbsData.selectedItem < bbsData.firstItemIdx) {
        bbsData.firstItemIdx = bbsData.selectedItem;
    }

    if (bbsData.selectedItem > (bbsData.firstItemIdx + bbsData.rowsSize - 1)) {
        bbsData.firstItemIdx = (bbsData.selectedItem - bbsData.rowsSize + 1);
    }
}

this.$definePageListChoices = function () {
    // _co.$logInfo(this.name, ">>> definePageListChoices");

    var bbsData = _litf.litfVars.bbsData;

    var choices = {};

    if (bbsData.selectedItem > 0) {
        choices["01_PREV_ITEM"] = { text: "Select previous item", color: "yellowColor" };
    } else {
        choices["01_PREV_ITEM"] = { text: "Select previous item", color: "darkGrayColor", unselectable: true };
    }

    if (bbsData.selectedItem < bbsData.bbsItemsArr.length - 1) {
        choices["02_NEXT_ITEM"] = { text: "Select next item", color: "yellowColor" };
    } else {
        choices["02_NEXT_ITEM"] = { text: "Select next item", color: "darkGrayColor", unselectable: true };
    }

    // Check mission object to see if it allows for a detail screen

    var sObj = bbsData.bbsItemsArr[bbsData.selectedItem];
    // _co.$logDebug(this.name, "selectedItem = " + bbsData.selectedItem + " sObj = " + JSON.stringify(sObj));

    if (sObj.choices) {
        choices["03_SELECT_ITEM"] = { text: "Contact Sender", color: "yellowColor" };
    } else {
        choices["03_SELECT_ITEM"] = { text: "Contact Sender", color: "darkGrayColor", unselectable: true };
    }

    // choices["99_DISCONNECT"] = { text: "Disconnect from BBS", color: "yellowColor" };

    return choices;
}

this.$getParentMissionObject = function (pwObj) {
    _co.$logInfo(this.name, ">>> getParentMissionObject :: " + JSON.stringify(pwObj));

    var pmObj = null;

    for (var am = 0; am < _litf.litfCommander.activeMissions.length; am++) {
        var amItem = _litf.litfCommander.activeMissions[am];
        _co.$logDebug(this.name, "active mission id = " + amItem.missionId);

        if (amItem.missionId === pwObj.parentMissionId) {
            pmObj = amItem;
            break;
        }
    }

    return pmObj;
}

this.$advanceActiveMissionTask = function (advanceMissionId) {
    _co.$logInfo(this.name, ">>> deleteActiveMission");

    var updated = false;

    for (var am = 0; am < _litf.litfCommander.activeMissions.length; am++) {
        var amItem = _litf.litfCommander.activeMissions[am];

        _co.$logDebug(this.name, "[" + am + "] item.missionId[" + amItem.missionId + "] advanceMissionId = " + advanceMissionId);

        if (amItem.missionId === advanceMissionId) {
            amItem.currentTask = amItem.currentTask + 1;
            updated = true;
            break;
        }
    }

    _co.$logDebug(this.name, "updated = " + updated);
}

this.$deleteActiveMission = function (deleteMissionId) {
    _co.$logInfo(this.name, ">>> deleteActiveMission");

    var ret = {
        groupId: -1,
        deleted: false,
        removedPassengers: 0,
        deadPassengers: 0,
        cargo: null,
        parcel: null,
        feedback: {}
    }

    var removedPassengers = 0;
    var deadPassengers = 0;

    for (var am = 0; am < _litf.litfCommander.activeMissions.length; am++) {
        var amItem = _litf.litfCommander.activeMissions[am];

        _co.$logDebug(this.name, "[" + am + "] item.missionId[" + amItem.missionId + "] deleteMissionId = " + deleteMissionId);

        if (amItem.missionId === deleteMissionId) {
            if (amItem.tasks[0].typeId === 5) {
                // Cargo contract
                ret.cargo = amItem.tasks[0].info.goods[0];
            }

            if (amItem.tasks[0].typeId < 4) {
                // Parcel contract
                if (amItem.parcel) {
                    ret.parcel = amItem.parcel;
                } else {
                    for (var t = 0; t < amItem.tasks.length; t++) {
                        var task = amItem.tasks[t];
                        if (task.parcel) {
                            ret.parcel = task.parcel;
                        }
                    }
                }
            }

            // Remove passengers, if any
            if (amItem.passengerList) {
                if (amItem.failureDead === true) {
                    deadPassengers = deadPassengers + amItem.passengerList.length;
                } else {
                    removedPassengers = removedPassengers + amItem.passengerList.length;
                }

                // this.$deletePassengerMission(amItem);
            }

            ret.groupId = amItem.groupId;
            ret.feedback.gained = amItem.feedbackSuccess;
            ret.feedback.lost = amItem.feedbackFailure;

            _litf.litfCommander.activeMissions.splice(am, 1);
            ret.deleted = true;
            break;
        }
    }

    ret.removedPassengers = removedPassengers;
    ret.deadPassengers = deadPassengers;

    _co.$logDebug(this.name, "ret = " + JSON.stringify(ret));

    return ret;
}

this.$howManyPassengers = function (mObj) {
    _co.$logInfo(this.name, ">>> howManyPassengers");
    var totalPassengers = 0;

    if (mObj.passengers) {
        totalPassengers = totalPassengers + mObj.passengers;
    }

    for (var ta = 0; ta < mObj.tasks.length; ta++) {
        var task = mObj.tasks[ta];
        if (task.passengers) {
            totalPassengers = totalPassengers + task.passengers;
        }
    }

    _co.$logDebug(this.name, "totalPassengers = " + totalPassengers);
    return totalPassengers;
}

this.$deleteBBSItem = function (itemId) {
    var deleted = false;

    var bbsData = _litf.litfVars.bbsData;

    for (var i = 0; i < bbsData.bbsItemsArr.length; i++) {
        var item = bbsData.bbsItemsArr[i];

        if (item.missionId === itemId) {
            bbsData.bbsItemsArr.splice(i, 1);
            deleted = true;
            break;
        }
    }

    if (deleted === true) {
        while (bbsData.selectedItem >= bbsData.bbsItemsArr.length) {
            bbsData.selectedItem = bbsData.selectedItem - 1;
        }
    }
}

// this.$actionDonate = function (choice, amount) {
this.$actionDonate = function (choiceStr, amount) {
        _co.$logInfo(this.name, ">>> actionDonate");

    var bbsData = _litf.litfVars.bbsData;

    // var choiceStr = this.$getChoices(bbsData.selectedItemObj.choices)[choice];

    if (player.credits < amount) {
        bbsData.selectedItemObj.response = choiceStr + "\nYou don't have enough cash.";
    } else {
        player.credits = player.credits - amount;
        bbsData.selectedItemObj.response = choiceStr + "\nThank you. All donations are gratefully received.";

        // Feedback reward

        if (amount === 0.1) {
            // No feedback reward
        } else if (amount === 1) {
            if (_co.$roll(1, 100) < 5) {
                _co.$setPlayerFeedback(1);
            }
        } else if (amount === 10) {
            if (_co.$roll(1, 100) < 20) {
                _co.$setPlayerFeedback(1);
            }
        } else if (amount === 100) {
            _co.$setPlayerFeedback(1);
        } else if (amount === 1000) {
            _co.$setPlayerFeedback(5);
        } else if (amount === 10000) {
            _co.$setPlayerFeedback(10);
        }
    }
}

this.$feedbackSuccessScore = function (mObj) {
    _co.$logInfo(this.name, ">>> feedbackSuccessScore");

    var score = 2;

    if (mObj.local === false) {
        if (mObj.moreMoneyObtained === false) {
            score = score + 1;
        }

        if (mObj.rewardAdvanceObtained === null) {
            score = score + 1;
        }
    }

    return score;
}

this.$feedbackFailureScore = function (mObj) {
    _co.$logInfo(this.name, ">>> feedbackFailureScore");

    var score = 2;

    if (mObj.local === false) {
        if (mObj.moreMoneyObtained === true) {
            score = score + 2;
        }

        if (mObj.rewardAdvanceObtained === "half") {
            score = score + 1;
        } else if (mObj.rewardAdvanceObtained === "full") {
            score = score + 2;
        }
    }

    return score;
}

this.$getMissionTask = function (mObj, taskIndex) {
    _co.$logInfo(this.name, ">>> getCurrentTask");

    var tasks = mObj.tasks;
    var tObj = {};

    for (var t = 0; t < tasks.length; t++) {
        var tItem = tasks[t];
        if (tItem.index === taskIndex) {
            tObj = tItem;
            break;
        }
    }

    _co.$logDebug(this.name, "task[" + taskIndex + "] = " + JSON.stringify(tObj));

    return tObj;
}

this.$getParcelMissionInfo = function (mObj) {
    var pmInfo = {
        start: -1,
        destination: -1,
        parcel: null,
        arrivalTime: -1,
        risk: 0
    };

    var currentTask = this.$getMissionTask(mObj, mObj.currentTask);
    var atMissionStart = false;

    if (mObj.parcel) {
        pmInfo.parcel = mObj.parcel;
        atMissionStart = true;
    } else {
        for (var ta = 0; ta < mObj.tasks.length; ta++) {
            var task = mObj.tasks[ta];
            if (task.parcel) {
                pmInfo.parcel = task.parcel;
                break;
            }
        }
    }

    if (atMissionStart === true) {
        pmInfo.start       = mObj.startingSystem.id;
        pmInfo.destination = currentTask.info.systems[0].id;
    } else {
        var nextTask = this.$getMissionTask(mObj, mObj.currentTask + 1);

        pmInfo.start       = currentTask.info.systems[0].id;
        pmInfo.destination = nextTask.info.systems[0].id;
    }

    pmInfo.arrivalTime = clock.seconds + mObj.timeAllowed + 5;

    if (mObj.risk) { pmInfo.risk = mObj.risk; }

    return pmInfo;
}

this.$createParcelMission = function (mObj) {
    var pmInfo = this.$getParcelMissionInfo(mObj);

    var newParcel = player.ship.addParcel("(BBS) " + pmInfo.parcel, pmInfo.start, pmInfo.destination, pmInfo.arrivalTime, 0, 0, pmInfo.risk);
    _co.$logDebug(this.name, "new parcel :: " + pmInfo.parcel + ", " + pmInfo.start + ", " + pmInfo.destination + ", " + pmInfo.arrivalTime + ", 0, 0, " + pmInfo.risk + " == " + newParcel);

    return newParcel;
}

this.$deleteParcelMission = function (mObj) {
    var parcel = null;

    if (mObj.parcel) {
        parcel = mObj.parcel;
    } else {
        for (var t = 0; t < mObj.tasks.length; t++) {
            var task = mObj.tasks[t];

            if (task.parcel) {
                parcel = task.parcel;
            }
        }
    }

    player.ship.removeParcel("(BBS) " + parcel);
    _co.$logDebug(this.name, "deleting parcel: " + parcel);
}

this.$getPassengerMissionInfo = function (mObj) {
    var pmInfo = {
        start: -1,
        destination: -1,
        passengerList: [],
        arrivalTime: -1,
        risk: 0
    };

    var currentTask = this.$getMissionTask(mObj, mObj.currentTask);
    var atMissionStart = false;
    var passengers = -1;

    if (mObj.passengers) {
        passengers = mObj.passengers;
        atMissionStart = true;
    } else {
        for (var ta = 0; ta < mObj.tasks.length; ta++) {
            var task = mObj.tasks[ta];
            if (task.passengers) {
                passengers = task.passengers;
                break;
            }
        }
    }

    if (atMissionStart === true) {
        pmInfo.start = mObj.startingSystem.id;
        pmInfo.destination = currentTask.info.systems[0].id;

        if (mObj.firstPassenger) {
            if (mObj.firstPassenger === 0) {
                pmInfo.passengerList.push(mObj.contractor);
            } else if (mObj.firstPassenger > 0) {
                var pTask = this.$getMissionTask(mObj, mObj.firstPassenger);
                pmInfo.passengerList.push(pTask.info.names[0]);
            }
            passengers--;
        }
    } else {
        var nextTask = this.$getMissionTask(mObj, mObj.currentTask + 1);

        pmInfo.start = currentTask.info.systems[0].id;
        pmInfo.destination = nextTask.info.systems[0].id;
    }

    pmInfo.arrivalTime = clock.seconds + mObj.timeAllowed + 5;

    if (mObj.risk) { pmInfo.risk = mObj.risk; }

    for (var p = 0; p < passengers; p++) {
        pmInfo.passengerList.push(randomName() + " " + randomName());
    }

    return pmInfo;
}

this.$createPassengerMission = function (mObj) {
    var pmInfo = this.$getPassengerMissionInfo(mObj);

    for (var pl = 0; pl < pmInfo.passengerList.length; pl++) {
        var newPassenger = player.ship.addPassenger("(BBS) " + pmInfo.passengerList[pl], pmInfo.start, pmInfo.destination, pmInfo.arrivalTime, 0, 0, pmInfo.risk);
        _co.$logDebug(this.name, "new passenger :: " + pmInfo.passengerList[pl] + ", " + pmInfo.start + ", " + pmInfo.destination + ", " + pmInfo.arrivalTime + ", 0, " + pmInfo.risk + " == " + newPassenger);
    }

    return pmInfo.passengerList;
}

this.$deletePassengerMission = function (mObj) {
    if (mObj.passengerList) {
        for (var pa = 0; pa < mObj.passengerList.length; pa++) {
            player.ship.removePassenger("(BBS) " + mObj.passengerList[pa]);
            _co.$logDebug(this.name, "deleting passenger " + mObj.passengerList[pa]);
        }
    }
}

this.$createCargoMission = function (cargoGoods) {
    _co.$logDebug(this.name, "Cargo mission for " + cargoGoods);
    player.ship.useSpecialCargo("(BBS) A shipment of " + cargoGoods);
}

this.$deleteCargoMission = function () {
    player.ship.removeAllCargo();
    _co.$logDebug(this.name, "deleting special cargo");
}

// Parse choices

this.$executeItemAction = function (mObj, choiceLabel) {
    _co.$logInfo(this.name, ">>> executeItemAction :: " + choiceLabel);

    var bbsData = _litf.litfVars.bbsData;
    var pFeedback = _litf.litfCommander.feedback;
    var minFeedback = 0;

    var choiceObj = null;

    if (mObj.groupId === 97) {
        // Dialog message item
        var stepObj = this.$getCurrentStep(mObj);
        choiceObj = _pa.$getChoiceObject(choiceLabel, stepObj.choices);
    } else {
        choiceObj = _pa.$getChoiceObject(choiceLabel, _md.scriptChoices);
    }

    var cText = choiceObj.text;

    var action = choiceObj.action;
    var ret = _pa.$parseActionScript(action);

    if (ret === "ok") {
        // No output
    } else if (ret.indexOf("error") === 0) {
        _co.$logSevere(this.name, "ERROR :: " + ret);
    } else if (ret === "go") {
        var newStepId = _litf.litfCommander.scriptVars.currentStepId;
        mObj.currentStep = newStepId;
        // _co.$logDebug(this.name, "go to step = " + mObj.currentStep);
    } else if (ret === "quit") {
        bbsData.bbsMode = 0;
    } else if (ret.indexOf("ret") === 0) {
        var retSplit = ret.split(" ");
        var cmd = retSplit[1];

        // Specific actions
        if (cmd === "donate") {
            var donation = parseFloat(retSplit[2]);
            this.$actionDonate(cText, donation);
        } else if (cmd === "missionstart") {
            this.$acceptMissionAction(mObj, cText);
            bbsData.bbsMode = 0;
        } else if (cmd === "missionnext") {
            var advance = this.$advanceTaskAction(mObj);
            if (advance) {
                bbsData.bbsMode = 0;
            }
        } else if (cmd === "missionend") {
            this.$completeMissionAction(mObj);
            bbsData.bbsMode = 0;
        } else if (cmd === "openstarchart") {
            _litf.litfVars.bbsData.starChartModeIdx = 0;
            _litf.litfVars.bbsData.starChartZoom = 1;
            _litf.litfVars.bbsData.starChartSystemCentre = 0;
            bbsData.bbsMode = 2;
        } else if (cmd === "resp") {
            var response = retSplit[2];

            if (response === "howmany") {
                var totPassengers = this.$howManyPassengers(mObj);
                if (totPassengers === 1) {
                    mObj.response = cText + "\nOnly one person.";
                } else if (totPassengers > 1) {
                    mObj.response = cText + "\nA group of " + totPassengers + " persons.";
                }
            } else if (response === "money") {
                mObj.response = cText + "\n" + mObj.responses.money;
            } else if (response === "problems") {
                mObj.response = cText + "\n" + mObj.responses.problems;
            } else if (response === "unaware") {
                var currentTask = this.$getMissionTask(mObj, mObj.currentTask);
                if (currentTask.unaware === false) {
                    mObj.response = cText + "\n" + mObj.responses.unaware[0];
                } else {
                    mObj.response = cText + "\n" + mObj.responses.unaware[1];
                }
            } else if (response === "moremoney") {
                minFeedback = mObj.moreMoneyMinFeedback ? mObj.moreMoneyMinFeedback : this.moreMoneyMinFeedbackDefault;
                if (pFeedback < minFeedback) {
                    mObj.response = cText + "\n" + mObj.reward + "cr is all I can pay.";
                } else if (mObj.moreMoneyObtained === false) {
                    var percentMore = (_co.$roll(0, 3) * 5) + 10;
                    var newReward = mObj.reward + (mObj.reward / 100 * percentMore);

                    mObj.reward = newReward;
                    mObj.response = cText + "\nOk. Considering your feedback I can increase the reward to " + mObj.reward + " credits.";
                    mObj.moreMoneyObtained = true;
                } else {
                    mObj.response = cText + "\nWe've agreed on payment of " + mObj.reward + " credits.";
                }
            } else if (response === "halfadvance") {
                minFeedback = mObj.halfAdvanceMinFeedback ? mObj.halfAdvanceMinFeedback : this.halfAdvanceMinFeedbackDefault;
                if (pFeedback < minFeedback) {
                    mObj.response = cText + "\nWhat sort of fool do you take me for?";
                } else if (mObj.rewardAdvanceObtained === null) {
                    mObj.response = cText + "\nOk. I'll pay half once you agree, and half on arrival.";
                    mObj.rewardAdvanceObtained = "half";
                } else {
                    mObj.response = cText + "\nWe've agreed on payment.";
                }
            } else if (response === "fulladvance") {
                minFeedback = mObj.fullAdvanceMinFeedback ? mObj.fullAdvanceMinFeedback : this.fullAdvanceMinFeedbackDefault;
                if (pFeedback < minFeedback) {
                    mObj.response = cText + "\nWhat sort of fool do you take me for?";
                } else if (mObj.rewardAdvanceObtained === null) {
                    mObj.response = cText + "\nOk. I'll pay the full reward once you agree.";
                    mObj.rewardAdvanceObtained = "full";
                } else {
                    mObj.response = cText + "\nWe've agreed on payment.";
                }
            }
        }
    }

    // var choiceStr = this.$getChoices(mObj.choices)[choice];
    // var pFeedback = _litf.litfCommander.feedback;
    // var minFeedback = 0;

    // if (choice.indexOf("_QUIT") > -1) {
    //     bbsData.bbsMode = 0;
    // } else if (choice === "10_OK") {
    //     this.$acceptMissionAction(mObj, choiceStr);
    //     bbsData.bbsMode = 0;
    // } else if (choice === "15_STARCHART") {
    //     _litf.litfVars.bbsData.starChartModeIdx = 0;
    //     _litf.litfVars.bbsData.starChartZoom = 1;
    //     _litf.litfVars.bbsData.starChartSystemCentre = 0;
    //     bbsData.bbsMode = 2;
    // } else if (choice === "20_HOWMANY") {
    //     var totPassengers = this.$howManyPassengers(mObj);
    //     if (totPassengers === 1) {
    //         mObj.response = choiceStr + "\nOnly one person.";
    //     } else if (totPassengers > 1) {
    //         mObj.response = choiceStr + "\nA group of " + totPassengers + " persons.";
    //     }
    // } else if (choice === "30_MONEY") {
    //     mObj.response = choiceStr + "\n" + mObj.responses.money;
    // } else if (choice === "40_PROBLEMS") {
    //     mObj.response = choiceStr + "\n" + mObj.responses.problems;
    // } else if (choice === "50_PERMIT") {
    //     mObj.response = choiceStr + "\nDon't be silly - you don't need a permit.";
    // } else if (choice === "50_UNAWARE") {
    //     var currentTask = this.$getMissionTask(mObj, mObj.currentTask);
    //     if (currentTask.unaware === false) {
    //         mObj.response = choiceStr + "\n" + mObj.responses.unaware[0];
    //     } else {
    //         mObj.response = choiceStr + "\n" + mObj.responses.unaware[1];
    //     }
    // } else if (choice === "60_MOREMONEY") {
    //     minFeedback = mObj.moreMoneyMinFeedback ? mObj.moreMoneyMinFeedback : this.moreMoneyMinFeedbackDefault;
    //     if (pFeedback < minFeedback) {
    //         mObj.response = choiceStr + "\n" + mObj.reward + "cr is all I can pay.";
    //     } else if (mObj.moreMoneyObtained === false) {
    //         var percentMore = (_co.$roll(0, 3) * 5) + 10;
    //         var newReward = mObj.reward + (mObj.reward / 100 * percentMore);

    //         mObj.reward = newReward;
    //         mObj.response = choiceStr + "\nOk. Considering your feedback I can increase the reward to " + mObj.reward + " credits.";
    //         mObj.moreMoneyObtained = true;
    //     } else {
    //         mObj.response = choiceStr + "\nWe've agreed on payment of " + mObj.reward + " credits.";
    //     }
    // } else if (choice === "70_HALFADVANCE") {
    //     minFeedback = mObj.halfAdvanceMinFeedback ? mObj.halfAdvanceMinFeedback : this.halfAdvanceMinFeedbackDefault;
    //     if (pFeedback < minFeedback) {
    //         mObj.response = choiceStr + "\nWhat sort of fool do you take me for?";
    //     } else if (mObj.rewardAdvanceObtained === null) {
    //         mObj.response = choiceStr + "\nOk. I'll pay half once you agree, and half on arrival.";
    //         mObj.rewardAdvanceObtained = "half";
    //     } else {
    //         mObj.response = choiceStr + "\nWe've agreed on payment.";
    //     }
    // } else if (choice === "80_FULLADVANCE") {
    //     minFeedback = mObj.fullAdvanceMinFeedback ? mObj.fullAdvanceMinFeedback : this.fullAdvanceMinFeedbackDefault;
    //     if (pFeedback < minFeedback) {
    //         mObj.response = choiceStr + "\nWhat sort of fool do you take me for?";
    //     } else if (mObj.rewardAdvanceObtained === null) {
    //         mObj.response = choiceStr + "\nOk. I'll pay the full reward once you agree.";
    //         mObj.rewardAdvanceObtained = "full";
    //     } else {
    //         mObj.response = choiceStr + "\nWe've agreed on payment.";
    //     }
    // } else if (choice === "01_DONATE01") {
    //     this.$actionDonate(choice, 0.1);
    // } else if (choice === "02_DONATE1") {
    //     this.$actionDonate(choice, 1);
    // } else if (choice === "03_DONATE10") {
    //     this.$actionDonate(choice, 10);
    // } else if (choice === "04_DONATE100") {
    //     this.$actionDonate(choice, 100);
    // } else if (choice === "05_DONATE1000") {
    //     this.$actionDonate(choice, 1000);
    // } else if (choice === "06_DONATE10000") {
    //     this.$actionDonate(choice, 10000);
    // } else if (mObj.choices === "dialog") {
    //     if (choice.indexOf("_GOTO_") > -1) {
    //         var choiceSplit = choice.split("_");
    //         mObj.currentStep = parseInt(choiceSplit[2]);
    //         // _co.$logDebug(this.name, "go to step = " + mObj.currentStep);
    //     } else if (choice.indexOf("_NEXTTASK") > -1) {
    //         var advance = this.$advanceTaskAction();
    //         if (advance) {
    //             bbsData.bbsMode = 0;
    //         }
    //     } else if (choice.indexOf("_COMPLETE") > -1) {
    //         this.$completeMissionAction();
    //         bbsData.bbsMode = 0;
    //     }
    // // } else if (mObj.choices === "search") {
    // }
}

// Display methods

this.$display = function () {
    var bbsData = _litf.litfVars.bbsData;

    // _co.$logInfo(this.name, ">>> display :: bbsMode = " + bbsData.bbsMode);

    if (bbsData.bbsMode === 0) {
        this.$showBBSListScreen();
    } else if (bbsData.bbsMode === 1) {
        this.$showBBSDetailScreen();
    } else if (bbsData.bbsMode === 2) {
        // TEST
        this.$showMissionStarChart();
    }
}

this.$showBBSListScreen = function () {
    var bbsData = _litf.litfVars.bbsData;
    var displayText = null;

    // BBS message list
    displayText = "(" + (bbsData.selectedItem + 1) + " / " + bbsData.bbsItemsArr.length + ")\n\n";

    this.$setPageListEdges();

    var lastItemIdx = bbsData.firstItemIdx + bbsData.rowsSize;
    if (lastItemIdx > bbsData.bbsItemsArr.length) {
        lastItemIdx = bbsData.bbsItemsArr.length;
    }

    for (var r = bbsData.firstItemIdx; r < lastItemIdx; r++) {
        var iObj = bbsData.bbsItemsArr[r];

        if (r === bbsData.selectedItem) {
            displayText += "> ";
        } else {
            displayText += "    ";
        }

        var iTitle = iObj.title;
        if (iTitle.length > 70) {
            iTitle = iTitle.substring(0, 70) + "...";
        }

        displayText += iTitle + "\n";
    }

    var curChoices = this.$definePageListChoices();

    var opts = {
        screenID: "litf_bbs_list_screen",
        title: system.name + " Bulletin Board",
        exitScreen: "GUI_SCREEN_INTERFACES",
        message: displayText,
        choices: curChoices,
        allowInterrupt: true,
        initialChoicesKey: bbsData.lastChoice
    };

    mission.runScreen(opts, this.$updateMissionList);

    setScreenBackground("bbs-bg.png");
}

this.$showBBSDetailScreen = function () {
    var bbsData = _litf.litfVars.bbsData;

    var displayText = null;
    var curChoices = null;

    // BBS item details
    var selItemObj = bbsData.selectedItemObj;

    displayText = "// " + selItemObj.title + "\n\n";

    _co.$logDebug(this.name, "mission details: " + JSON.stringify(selItemObj));

    if (selItemObj.groupId === 97) {
        // Dialog message item

        var stepObj = this.$getCurrentStep(selItemObj);

        // Build description for the current step
        displayText += stepObj.description + "\n";
        // displayText += _pa.$parseStepText(stepObj.text) + "\n";

        // Replace constants with strings
        displayText = _mi.$replaceKeys(displayText, selItemObj);

        // Build choice list for the current step
        // curChoices = stepObj.choice;
        curChoices = _pa.$buildChoiceArray(stepObj.choices);
    } else {
        var description = selItemObj.description;

        if (selItemObj.choices === "mission" || selItemObj.choices === "transport" || selItemObj.choices === "cargo") {
            for (var t = 0; t < selItemObj.tasks.length; t++) {
                var task = selItemObj.tasks[t];
                for (var s = 0; s < task.info.systems.length; s++) {
                    var sItem = task.info.systems[s];
                    if (sItem.distance > 0) {
                        description = description.replace(sItem.name, sItem.name + " (dist: " + sItem.distance + " ly)");
                    }
                }
            }
        }

        displayText += description + "\n";

        if (selItemObj.showCredits === true) {
            displayText += "\nYOUR CREDITS: " + player.credits;
        } else if (selItemObj.choices !== "hangup" && selItemObj.timeAllowed) {
            var etaTime = _co.$formatTime(selItemObj.timeAllowed, false);
            displayText += "\nDEADLINE: " + etaTime + ".";
        }

        var hangUp = false;

        if (typeof selItemObj.choices === 'object') {
            _co.$logDebug(this.name, "choices is an object");
            curChoices = selItemObj.choices;
        } else {
            _co.$logDebug(this.name, "choices is a string");
            curChoices = this.$getChoices(selItemObj.choices);

            if (selItemObj.choices === "hangup") {
                hangUp = true;
            }
        }

        if (hangUp === false) {
            // // Show systems name and distance from here
            // for (var t = 0; t < selItemObj.tasks.length; t++) {
            //     var task = selItemObj.tasks[t];
            //     if (!task.hideSystemData) {
            //         for (var s = 0; s < task.info.systems.length; s++) {
            //             var sItem = task.info.systems[s];
            //             if (sItem.distance > 0) {
            //                 displayText += "\nSYSTEM DATA: " + sItem.name + " (distance: " + sItem.distance + " ly)";
            //             }
            //         }
            //     }
            // }

            _co.$logDebug(this.name, "response = " + selItemObj.response);
            if (selItemObj.response && selItemObj.response !== null) {
                displayText += "\n\n> " + selItemObj.response;
            }
        }
    }

    var opts = {
        screenID: "litf_bbs_details_screen",
        title: system.name + " Bulletin Board",
        exitScreen: "GUI_SCREEN_INTERFACES",
        message: displayText,
        choices: curChoices
    };

    mission.runScreen(opts, this.$updateMissionDetails);

    // Frontier: Elite face backgrounds - disabled because I am afraid they are copyrighted.
    if (_litf.litfVars.config.disableMissionBackground && _litf.litfVars.config.disableMissionBackground === false) {
        if (selItemObj.faceBackground) {
            setScreenBackground(selItemObj.faceBackground);
        }
    } else {
        setScreenBackground("bbs-bg.png");
    }
}

this.$showMissionStarChart = function () {
    var bbsData = _litf.litfVars.bbsData;
    var selItemObj = bbsData.selectedItemObj;

    var markerColors = [ "cyanColor", "yellowColor", "pinkColor" ];

    var displayText = "";
    var curChoices = null;

    // Mark the selected mission associate systems

    bbsData.starChartMarkedSystems = [];

    var marker = null;
    var markerCounter = 0;

    for (var ta = 0; ta < selItemObj.tasks.length; ta++) {
        var systems = selItemObj.tasks[ta].info.systems;

        if (selItemObj.tasks[ta].typeId === 6) {
            // SEARCH mission
            var sysId = selItemObj.missing.posArr[0];
            marker = {
                system: sysId,
                name: "litf_mission_marker_" + sysId,
                markerColor: markerColors[markerCounter],
                markerScale: 1.5,
                markerShape: "MARKER_DIAMOND"
                // sysName: system.name,
                // sysDistance: system.distance
            };

            bbsData.starChartMarkedSystems.push(marker);
            mission.markSystem(marker);
            markerCounter++;
        } else {
            for (var sy = 0; sy < systems.length; sy++) {
                var system = systems[sy];

                if (system.id !== selItemObj.startingSystem.id) {
                    if (!selItemObj.tasks[ta].hideSystemData || selItemObj.tasks[ta].hideSystemData === false) {
                        marker = {
                            system: system.id,
                            name: "litf_mission_marker_" + system.id,
                            markerColor: markerColors[markerCounter],
                            markerScale: 1.5,
                            markerShape: "MARKER_DIAMOND",
                            sysName: system.name,
                            sysDistance: system.distance
                        };

                        bbsData.starChartMarkedSystems.push(marker);
                        mission.markSystem(marker);

                        markerCounter++;
                    }
                }
            }
        }
    }

    // Current star chart mode
    var scMode = starChartModes[bbsData.starChartModeIdx];

    // Empty rows for star chart
    var scRows = 18;
    if (scMode.indexOf("SHORT") > -1) { scRows += 2; }

    displayText = "";
    for (var nl = 0; nl < scRows; nl++) { displayText += "\n"; }

    // Show mission resume
    var missionDesc = selItemObj.tasks[0].description;

    // Show systems name and distance from here
    for (var ma = 0; ma < bbsData.starChartMarkedSystems.length; ma++) {
        marker = bbsData.starChartMarkedSystems[ma];

        var colorDesc = marker.markerColor.substring(0, marker.markerColor.indexOf("Color"));

        var sysInfo = marker.sysName + " (" + colorDesc + "; " + marker.sysDistance + " ly)";

        missionDesc = missionDesc.replace(marker.sysName, sysInfo);
    }

    displayText += missionDesc;
    // displayText += "\nSPECIAL BACKGROUND MODE: " + scMode;

    curChoices = {
        "01_CYCLEMODE": "Cycle chart mode (" + starChartModesDesc[bbsData.starChartModeIdx] + ").",
        // "02_CYCLEZOOM": "Cycle zoom level. (" + bbsData.starChartZoom + ")",
        // "03_CYCLESYSTEMS": "Cycle centered system (" + bbsData.starChartSystemCentre + ").",
        "99_RETURN": "Return to the mission screen."
    };

    //

    // var centerSystemId = bbsData.starChartMarkedSystems[bbsData.starChartSystemCentre].system;
    // var centerSystemInfo = System.infoForSystem(galaxyNumber, centerSystemId);
    // _co.$logDebug(this.name, "backgroundSpecial(" + scMode + ") customChartZoom(" + bbsData.starChartZoom + ") customChartCentreLY(" + centerSystemInfo.coordinates + ")");

    var opts = {
        screenID: "litf_bbs_mission_starchart_screen",
        title: "Mission Star Chart Map",
        backgroundSpecial: scMode,
        // customChartZoom: parseInt(bbsData.starChartZoom),
        // customChartCentreLY: centerSystemInfo.coordinates,
        // exitScreen: "GUI_SCREEN_INTERFACES",
        message: displayText,
        choices: curChoices
    };

    mission.runScreen(opts, this.$updateMissionStarChart);

    setScreenBackground("bbs-bg.png");
}

//

this.$acceptMissionAction = function (mObj, choiceStr) {
    var agree = true;

    var currentTask = this.$getMissionTask(mObj, mObj.currentTask);

    // Parcel Contracts
    if (mObj.parcel) {
        agree = this.$createParcelMission(mObj);
    }

    // Passengers contracts
    if (mObj.passengers) {
        var passengers = mObj.passengers; // Number of passengers
        var availableCabins = player.ship.passengerCapacity - player.ship.passengerCount;

        if (availableCabins < passengers) {
            if (passengers === 1) {
                if (mObj.firstPassenger && mObj.firstPassenger === 0) {
                    mObj.response = choiceStr + "\n" + expandMissionText("litf_transport_denied_01");
                } else {
                    mObj.response = choiceStr + "\n" + expandMissionText("litf_transport_denied_02")
                }
            } else {
                if (mObj.firstPassenger && mObj.firstPassenger === 0) {
                    mObj.response = choiceStr + "\n" + expandMissionText("litf_transport_denied_03");
                } else {
                    mObj.response = choiceStr + "\n" + expandMissionText("litf_transport_denied_04");
                }
            }
            agree = false;
        } else {
            // Create a contract for every passenger
            mObj.passengerList = this.$createPassengerMission(mObj);
        }
    } else if (currentTask.typeId === 4) {
        var pCapacity = player.ship.passengerCapacity
        if (pCapacity === 0) {
            mObj.response = choiceStr + "\n" + expandMissionText("litf_nocabins");
            agree = false;
        }
    }

    // Cargo contracts
    if (currentTask.typeId === 5 && mObj.cargoLoadingTask === 0) {
        if (player.ship.cargoSpaceUsed > 0) {
            mObj.response = choiceStr + "\n" + expandMissionText("litf_needs_empty_cargo");
            agree = false;
        } else {
            this.$createCargoMission(currentTask.info.goods[0]);
        }
    }

    var pFeedback = _litf.litfCommander.feedback;

    // Check max active missions limit
    if (agree === true) {
        var maxActiveMissions = parseInt(Math.abs(pFeedback) / 25) + 1;
        if (_litf.litfCommander.activeMissions.length === maxActiveMissions) {
            mObj.response = choiceStr + "\n" + expandMissionText("litf_no_more_mission_slots");
            agree = false;
        }
        _co.$logDebug(this.name, "active missions: " + _litf.litfCommander.activeMissions.length + " max: " + maxActiveMissions + " agree = " + agree);
    }

    //

    if (agree === true) {
        var advance = 0;

        // Accept mission
        if (mObj.rewardAdvanceObtained !== null) {
            if (mObj.rewardAdvanceObtained === "half") {
                advance = mObj.reward / 2;
                player.credits = player.credits + advance;
                mObj.reward = mObj.reward / 2;
            } else if (mObj.rewardAdvanceObtained === "full") {
                advance = mObj.reward;
                player.credits = player.credits + advance;
                mObj.reward = 0;
            }
        }

        if (mObj.missing) {
            // Activate SEARCH mission
            mObj.missing.step = 1;
        }

        // define reputation success and failure score
        mObj.feedbackSuccess = this.$feedbackSuccessScore(mObj);
        mObj.feedbackFailure = this.$feedbackFailureScore(mObj);

        //

        mObj.startTime = clock.seconds;

        _litf.litfCommander.activeMissions.push(mObj);

        this.$deleteBBSItem(mObj.missionId);

        _co.$updateStatistics("missionAccepted", mObj.groupId, 1);

        _co.addPlayerLog("Mission accepted.");
        if (advance > 0) {
            _co.addPlayerLog("Received " + advance + "cr as advance.");
        }

        _litf.litfVars.sounds["mission_agree"].play();
    }
}

this.$completeMissionAction = function (mObj) {
    _co.$logInfo(this.name, ">>> completeMissionAction");

    var pmObj = this.$getParentMissionObject(mObj); // Original active mission item
    _co.$logDebug(this.name, "pmObj = " + JSON.stringify(pmObj));

    var currentTask = this.$getMissionTask(pmObj, pmObj.currentTask);
    _co.$logDebug(this.name, "currentTask = " + JSON.stringify(currentTask));

    if (currentTask.typeId < 4) {
        // Remove parcel contract
        this.$deleteParcelMission(pmObj);
    } else if (currentTask.typeId === 4) {
        // Remove every passenger contract
        this.$deletePassengerMission(pmObj);
    } else if (currentTask.typeId === 5) {
        // Cargo mission
        this.$deleteCargoMission();
    }

    // Mission completed
    var consText = "Mission complete.";

    // Get credits award
    if (mObj.reward > 0) {
        player.credits = player.credits + mObj.reward;
        consText += " " + mObj.reward + "cr received.";
    }

    // Get feedback award
    if (mObj.groupId === 3) {
        _co.$setPlayerFeedback(-mObj.feedbackSuccess);
    } else {
        _co.$setPlayerFeedback(mObj.feedbackSuccess);
    }

    // Delete active mission
    this.$deleteActiveMission(mObj.parentMissionId);

    // Delete dialog item from BBS
    this.$deleteBBSItem(mObj.missionId);

    _co.$updateStatistics("missionCompleted", pmObj.groupId, 1);
    _co.$updateStatistics("feedbackGained", pmObj.groupId, mObj.feedbackSuccess);

    _co.addPlayerLog(consText);

    _co.addPlayerLog("Personal feedback change (+" + mObj.feedbackSuccess + ").");

    _litf.litfVars.sounds["mission_agree"].play();
}

this.$advanceTaskAction = function (mObj, choice) {
    var advance = true;

    var pmObj = this.$getParentMissionObject(mObj); // Original active mission item
    _co.$logDebug(this.name, "pmObj = " + JSON.stringify(pmObj));

    var currentTask = this.$getMissionTask(pmObj, pmObj.currentTask);
    _co.$logDebug(this.name, "currentTask = " + JSON.stringify(currentTask));

    // Parcel Contracts
    if (currentTask.parcel) {
        advance = this.$createParcelMission(pmObj);
    }

    if (currentTask.passengers) {
        var passengers = currentTask.passengers; // Number of passengers
        _co.$logDebug(this.name, "passengerCapacity = " + player.ship.passengerCapacity + " passengerCount = " + player.ship.passengerCount);

        var availableCabins = player.ship.passengerCapacity - player.ship.passengerCount;
        _co.$logDebug(this.name, "availableCabins = " + availableCabins + " for Passengers = " + passengers);

        if (availableCabins < passengers) {
            var choiceStr = mObj.steps[mObj.currentStep].choices[choice];
            if (passengers === 1) {
                mObj.response = choiceStr + "\n" + expandMissionText("litf_transport_denied_01");
            } else {
                mObj.response = choiceStr + "\n" + expandMissionText("litf_transport_denied_03");
            }

            advance = false;
        } else {
            // Create a contract for every passenger
            pmObj.passengerList = this.$createPassengerMission(pmObj);
        }
    }

    if (currentTask.typeId === 5 && pmObj.cargoLoadingTask === pmObj.currentTask) {
        // Cargo mission
        if (player.ship.cargoSpaceUsed > 0) {
            mObj.response = choiceStr + "\n" + expandMissionText("litf_needs_empty_cargo");
            advance = false;
        } else {
            this.$createCargoMission(currentTask.info.goods[0]);
        }
    }

    //

    if (advance === true) {
        this.$advanceActiveMissionTask(mObj.parentMissionId);

        // Delete dialog item from BBS
        this.$deleteBBSItem(mObj.missionId);

        _co.addPlayerLog("Mission advanced to next task.");

        _litf.litfVars.sounds["mission_agree"].play();
    }

    return advance;
}
