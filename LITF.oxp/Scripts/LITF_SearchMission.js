/* eslint-disable semi, no-multi-spaces, quotes, indent, no-tabs, yoda */
/* global worldScripts, clock, system, randomName, galaxyNumber */

"use strict";

this.name	     = "LITF_SearchMission";
this.author	     = "BeeTLe BeTHLeHeM";
this.copyright	 = "2018 BeeTLe BeTHLeHeM";
this.description = "Methods and data for the SEARCH mission templates";
this.version	 = "0.10.0";
this.licence	 = "CC BY-NC-SA 4.0";

var _litf = null;
var _co   = null;
var _mi   = null;
var _md   = null;

this.$init = function () {
    _litf = worldScripts.LITF;

    _co = _litf.LITFInstances.co;
    _mi = _litf.LITFInstances.mi;
    _md = _litf.LITFInstances.md;
}

this.$defineMissingPersonData = function (mpName) {
    _co.$logInfo(this.name, ">>> defineMissingPersonData");

    // step = 0, missing person, mission not accepted
    // step = 1, mission accepted, searching for person
    // step = 2, person found
    // step = 3, mission completed

    var mPerson = {
        name: mpName,
        step: 0,
        posArr: null,
        currentPos: 0, // (posArr idx)
        lastKnownPos: 0, // Last known (or hinted) position (posArr idx)
        escape: _co.$roll(1, 100) < 40, // Don't know if I'll use this (chance person has to run away when found)
        nextMove: clock.seconds + (_co.$roll(12, 48) * 3600), // Next system switch
        protocolCode: this.$generateProtocolCode()
    };

    mPerson.posArr = this.$defineMissingPersonTravel(-1);

    return mPerson;
}

this.$generateProtocolCode = function () {
    _co.$logInfo(this.name, ">>> generateProtocolCode");

    var pCode = "SC";
    var rand = 0;
    var c = 0;

    // SC0000PX0000

    for (c = 0; c < 4; c++) {
        rand = _co.$roll(48, 57);
        pCode += String.fromCharCode(rand);
    }

    pCode += "PX";

    for (c = 0; c < 4; c++) {
        rand = _co.$roll(48, 57);
        pCode += String.fromCharCode(rand);
    }

    _co.$logDebug(this.name, "protocol code = " + pCode);

    return pCode;
}

this.$defineMissingPersonTravel = function (startingSystem) {
    _co.$logInfo(this.name, ">>> defineMissingPersonTravel");

    var posArr = [];
    var jumps = 0;

    var firstSystem = -1;

    if (startingSystem !== -1) {
        firstSystem = startingSystem;
    } else {
        firstSystem = _co.$randomSystem(-1);
    }

    posArr.push(firstSystem.id);

    var enough = false;
    while (enough === false) {
        var startSystem = posArr[posArr.length - 1];
        var endSystem = null;

        var valid = false;
        while (valid === false) {
            valid = true;
            endSystem = _co.$randomSystem(-1);
            for (var s = 0; s < posArr.length; s++) {
                if (posArr[s].id === endSystem.id) {
                    valid = false;
                }
            }
        }

        _co.$logDebug(this.name, "startSystem[" + startSystem + "] endSystem[" + endSystem.id + "]");

        var systemRoute = _co.$getSystemRoute(startSystem, endSystem.id);
        jumps += systemRoute.route.length;

        _co.$logDebug(this.name, "jumps = " + jumps);

        for (var sr = 1; sr < systemRoute.route.length; sr++) {
            posArr.push(systemRoute.route[sr]);
        }

        if (jumps >= 10) {
            enough = true;
        }
    }

    return posArr;
}

this.$updateMissingPerson = function (mPerson) {
    var endMission = false;

    if (clock.seconds > mPerson.nextMove) {
        // Time to move on
        mPerson.currentPos = mPerson.currentPos + 1;

        if (mPerson.currentPos >= mPerson.posArr.length) {
            // The path has ended
            endMission = true;
        } else {
            mPerson.nextMove = clock.seconds + (_co.$roll(12, 48) * 3600);
        }
    }

    return endMission;
}

this.$checkSearchMissionBBSItems = function () {
    var smObjArr = [];

    var title = null;
    var nextDestinationId = -1;

    // Check for active missions
    if (_litf.litfCommander.activeMissions) {
        for (var am = 0; am < _litf.litfCommander.activeMissions.length; am++) {
            var amItem = _litf.litfCommander.activeMissions[am];
            var typeId = amItem.tasks[0].typeId;

            // Check for SEARCH active missions
            if (typeId === 6) {
                var mPerson = amItem.missing;

                if (mPerson.step === 1) {
                    // Searching for the person

                    // "Distance" between current system and the system the person actually is in
                    var playerLag = 9999; // An impossible value
                    for (var sy = 0; sy < mPerson.posArr.length; sy++) {
                        if (system.ID === mPerson.posArr[sy] && playerLag === 9999) {
                            playerLag = mPerson.currentPos - sy;

                            nextDestinationId = mPerson.posArr[sy + 1];
                        }
                    }

                    var mpObj = null;

                    if (playerLag === 0) {
                        // Person IS here

                    } else {
                        // items 101, 102, 103 - people to ask about the missing person

                        var valid = false;
                        while (!valid) {
                            var storeObj = _mi.$generateRandomItem(_md.adStore);

                            if (storeObj.groupId === 101 || storeObj.groupId === 102 || storeObj.groupId === 103) {
                                title = storeObj.title;
                                // description = storeObj.description;

                                // mpObj = JSON.parse(JSON.stringify(storeObj));
                                // mpObj.missionId = _co.$getGeneralId();
                                valid = true;
                            }
                        }

                        // Face
                        mpObj.faceBackground = _mi.$getFaceBackground(mpObj);

                        mpObj.missionId = -1;
                        mpObj.label = "12.0.0";
                        mpObj.title = title;
                        mpObj.contractor = randomName() + " " + randomName();

                        mpObj.step1 = [];

                        mpObj.step1.push(this.$addRandomItem(this.searchMissingDialog.step1Description));
                        mpObj.step1.concat(this.searchMissingDialog.step1Choices);
                        // this.$addAllItems(mpObj.step1, this.searchMissingDialog.step1Choices);

                        mpObj.step2 = [];

                        if (playerLag === 9999 || playerLag < 0) {
                            // System out of the person path OR the person is still not here
                            mpObj.step2.push(this.$addRandomItem(this.searchMissingDialog.step2DescriptionNo));
                            mpObj.step2.concat(this.searchMissingDialog.step2ChoicesNo);
                        } else {
                            if (playerLag > 1) {
                                // Person was here, recently (=== 1) on in the past (> 1)
                                mpObj.step2.push(this.$addRandomItem(this.searchMissingDialog.step2DescriptionYes));
                                mpObj.step2.concat(this.searchMissingDialog.step2Choices);
                            } else if (playerLag === 1) {
                                // The person was recently in the system
                                mpObj.step2.push(this.$addRandomItem(this.searchMissingDialog.step2DescriptionYesRecent));
                                mpObj.step2.concat(this.searchMissingDialog.step2Choices);
                            }

                            mpObj.step3 = [];

                            var hintDescription = this.$defineHint(this.$addRandomItem(this.searchMissingDialog.step3Description), nextDestinationId);
                            mpObj.step3.push(hintDescription);

                            mpObj.step3.concat(this.searchMissingDialog.step3Choices);
                        }
                    }

                    smObjArr.push(mpObj);
                }
            }
        }
    }

    return smObjArr;
}

this.$defineHint = function (hintText, nextDestId) {
    var fbRank = _litf.$getFeedbackRanking(this.litfCommander.feedback);
    var replace = null;

    var nextDestSystemInfo = System.infoForSystem(galaxyNumber, nextDestId);

    if (fbRank <= 0) {
        if (nextDestSystemInfo.government === 0) {
            replace = "an ";
        } else {
            replace = "a ";
        }
        replace += nextDestSystemInfo.governmentDescription + " system";
    } else if (fbRank === 1) {
        if (nextDestSystemInfo.economy < 4) {
            replace = "an industrial system";
        } else {
            replace = "an agricultural system";
        }
    } else if (fbRank === 2) {
        if (nextDestSystemInfo.techLevel < 5) {
            replace = "a low-tech system";
        } else if (nextDestSystemInfo.techLevel < 10) {
            replace = "an average-tech system";
        } else {
            replace = "an high-tech system";
        }
    } else if (fbRank >= 3) {
        replace = nextDestSystemInfo.name;
    }

    hintText = hintText.replace("#MP_HINT#", replace);

    return hintText;
}

this.$addRandomItem = function (arr) {
    var itemIdx = _co.$roll(0, arr.length - 1);
    return arr[itemIdx];
}

// this.$addAllItems = function (arrDest, arrSrc) {
//     for (var i = 0; i < arrSrc.length; i++) {
//         arrDest.push(arrSrc[i]);
//     }
// }

// this.$createMissingPersonHintsItem = function () {
//     var mPerson = _litf.litfVars.missingPerson;

//     if (mPerson.step === 1 || mPerson.step === 2) {

//     }
// }

this.reportMissingDialog = {
    title: "SEARCHING: The Station Pop-Registry is open to the public.",
    step1: [
        "Welcome to the Station Population Registry. We can provide info about persons actually located on this station. What do you need?",
        "01_GOTO_2:\"I need info about a missing person that should be in this system.\"",
        "99_QUIT:Hang up."
    ],
    step2: [
        "Before you submit the person name, you have to provide three pieces of info for verification. Are you ready?",
        "02_GOTO_3:\"Proceed.\"",
        "99_QUIT:\"Sorry I'm busy now\", and hang up."
    ],
    step3: [
        "Submit the name of the person who gave you the job:"
    ],
    step4: [
        "Submit the name of the system where you have accepted the job:"
    ],
    step5: [
        "Submit the protocol code provided by the contractor:"
    ],
    step6ko: [
        "Sorry, we found nothing using the info you submitted. You should check the info in your possess. You can retry again, but every failure will be detrimental for you feedback. Goodbye.",
        "99_QUIT:Hang up."
    ],
    step6ok: [
        "We received confirmation of your job. You are authorized to ask info about a person. What's his name?"
    ],
    step7ok: [
        "Yes, we confirm the person you asked for is actually in this station. We are transmitting a message to your contractor. Wait a moment, please.",
        "01_GOTO_8:Wait."
    ],
    step7ko: [
        "No, this person isn't actually in this station. He could be have left a few moments ago. You should hurry and check for hints for his next destination. Goodbye.",
        "99_QUIT:Hang up."
    ],
    step8: [
        "The contractor has accepted our message. You can consider your job completed. The contractor will pay you a final reward of ### credits. Goodbye and thanks for your service.",
        "01_COMPLETE:\"Ok, goodbye.\" and hang up."
    ]
}

this.searchMissingDialog = {
    step1Description: [
        "Hi, I'm #NAME0#. There's something I can do for you? I'm pretty busy at the moment.",
        "Hi, I'm #NAME0#. What can I do for you?",
        "Hello, I'm #NAME0#. What's up?",
        "I don't know you, What do you want?"
    ],
    step1Choices: [
        // "01_GOTO_2:\"I'm searching for a person, #MP_NAME#. Do you know if he has been here?\"",
        "01_GOTO_2:\"I'm looking for someone. Do you know or had contacts with #MP_NAME# lately?\"",
        "99_QUIT:Hang up."
    ],
    step2DescriptionNo: [
        "No, I'm sorry. I never heard that name until now. Goodbye."
    ],
    step2DescriptionYesRecent: [
        "Yes. I believe I saw him around here recently.",
        "Hm, I think a friend of mine talked to me about him.",
        "Yes I know him, but he isn't here anymore. He left."
    ],
    step2DescriptionYes: [
        "I think he was here in this system some time ago.",
        "Hm, the name ring a bell, but it's passed some time.",
        "Yes I think I know him but it was a while ago."
    ],
    step2ChoicesNo: [
        "99_QUIT\"Thank you anyway.\" and hang up."
    ],
    step2Choices: [
        "01_GOTO_3:\"Please, do you have some information that can be useful to me?\"",
        "99_QUIT\"Thank you anyway.\" and hang up."
    ],
    step3Description: [
        "He had to meet someone in #MP_HINT#.",
        "I heard someone was waiting for him in #MP_HINT#.",
        "He had to pick up a package in #MP_HINT#.",
        "Someone told me he had to bring a package in #MP_HINT#.",
        "He said he had to talk to someone in #MP_HINT#.",
        "I heard he wanted to visit #MP_HINT# soon.",
        "Someone told that he had to go to #MP_HINT# to settle something.",
        "He said that he needed to make a visit to #MP_HINT#.",
        "His next stop was #MP_HINT# - this is what I know.",
        "He had something to do in #MP_HINT#.",
        "Someone told me that he was leaving for #MP_HINT#.",
        "He was in a hurry, and his next destination was #MP_HINT#.",
        "I believe he once said that he wished to visit #MP_HINT#.",
        "He had some business in #MP_HINT# - or so I thought at the time."
    ],
    step3Choices: [
        "99_QUIT\"Thank you for the info.\" and hang up."
    ]
};
