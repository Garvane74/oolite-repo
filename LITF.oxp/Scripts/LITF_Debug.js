/* eslint-disable semi, no-multi-spaces, quotes, indent, no-tabs, yoda */
/* global worldScripts, player, mission */

"use strict";

this.name	     = "LITF_Debug";
this.author	     = "BeeTLe BeTHLeHeM";
this.copyright	 = "2018 BeeTLe BeTHLeHeM";
this.description = "Debug methods";
this.version	 = "0.10.0";
this.licence	 = "CC BY-NC-SA 4.0";

var _litf = null;
var _co   = null;
var _bbs  = null;
// var _sm   = null;

var debugData = null;

var $display = this.$display;

this.$init = function () {
    _litf = worldScripts.LITF;

    _co = _litf.LITFInstances.co;
    _bbs = _litf.LITFInstances.bbs;
    // _sm = _litf.LITFInstances.sm;

    debugData = {
        cursorRow: 0,
        lastChoice: "02_NEXT_ITEM",
        items: []
    }
}

this.$display = function () {
    var title = "Debug Console";

    debugData.items = [
        { id: "MONEY", text: "Give the player 100000 credits. (Player credits: " + player.credits + ")." },
        { id: "NAVIGATION", text: "Toggle Station Navigation interface (" + _litf.litfVars.debug.enableNavigation + ")." },
        { id: "LOG", text: "Change log level (Log level: " + _litf.litfVars.debug.logLevel + ")." },
        { id: "REFRESH", text: "Refresh BBS items list." },
        { id: "DELETE", text: "Cancel active missions (active: " + _litf.litfCommander.activeMissions.length + ")." },
        { id: "STATS", text: "Toggle Statistics Screen (" + _litf.litfVars.debug.enableStatsScreen + ")." },
        { id: "STARCHART", text: "Toggle StarChart Screen (" + _litf.litfVars.debug.enableStarChartScreen + ")." }
    ];

    var text = "";

    for (var i = 0; i < debugData.items.length; i++) {
        if (debugData.cursorRow === i) {
            text += "> ";
        } else {
            text += "  ";
        }

        text += debugData.items[i].text + "\n";
    }

    var choices = this.$debugChoices();

    var opts = {
        screenID: "litf_debug_screen",
        title: title,
        exitScreen: "GUI_SCREEN_INTERFACES",
        allowInterrupt: true,
        message: text,
        choices: choices,
        initialChoicesKey: debugData.lastChoice
    };

    mission.runScreen(opts, this.$executeChoice);
}

this.$executeChoice = function (choice) {
    _co.$logInfo(this.name, ">>> executeChoice :: " + choice);

    var choiceLabel = debugData.items[debugData.cursorRow].id;

    if (choice === "01_PREV_ITEM") {
        if (debugData.cursorRow > 0) {
            debugData.cursorRow = debugData.cursorRow - 1;
        }
    } else if (choice === "02_NEXT_ITEM") {
        if (debugData.cursorRow < debugData.items.length - 1) {
            debugData.cursorRow = debugData.cursorRow + 1;
        }
    } else if (choice === "03_SELECT_ITEM") {
        if (choiceLabel === "MONEY") {
            player.credits = player.credits + 100000;
        } else if (choiceLabel === "NAVIGATION") {
            _litf.litfVars.debug.enableNavigation = !_litf.litfVars.debug.enableNavigation;
        } else if (choiceLabel === "LOG") {
            var logLevel = _litf.litfVars.debug.logLevel + 1;
            if (logLevel > 3) { logLevel = 0; }
            _litf.litfVars.debug.logLevel = logLevel;
        } else if (choiceLabel === "REFRESH") {
            _bbs.$createBBSItems();
        } else if (choiceLabel === "DELETE") {
            _litf.litfCommander.activeMissions = [];
        } else if (choiceLabel === "STATS") {
            _litf.litfVars.debug.enableStatsScreen = !_litf.litfVars.debug.enableStatsScreen;
        } else if (choiceLabel === "STARCHART") {
            _litf.litfVars.debug.enableStarChartScreen = !_litf.litfVars.debug.enableStarChartScreen;
        } else {
            _co.$logDebug(this.name, "Debug flag unknown: " + choiceLabel);
        }
    }

    debugData.lastChoice = choice;

    $display();
}

this.$debugChoices = function () {
    var choices = {};

    if (debugData.cursorRow > 0) {
        choices["01_PREV_ITEM"] = { text: "Select previous item", color: "yellowColor" };
    } else {
        choices["01_PREV_ITEM"] = { text: "Select previous item", color: "darkGrayColor", unselectable: true };
    }

    if (debugData.cursorRow < debugData.items.length - 1) {
        choices["02_NEXT_ITEM"] = { text: "Select next item", color: "yellowColor" };
    } else {
        choices["02_NEXT_ITEM"] = { text: "Select next item", color: "darkGrayColor", unselectable: true };
    }

    choices["03_SELECT_ITEM"] = { text: "Activate", color: "yellowColor" };

    return choices;
}
