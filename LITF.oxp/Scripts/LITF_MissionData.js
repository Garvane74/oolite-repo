/* eslint-disable semi, no-multi-spaces, quotes, indent, no-tabs, yoda */

"use strict";

this.name = "LITF_MissionData";
this.author = "BeeTLe BeTHLeHeM";
this.copyright = "2018 BeeTLe BeTHLeHeM";
this.description = "Variables and arrays for BBS items and missions data";
this.version = "0.10.0";
this.licence = "CC BY-NC-SA 4.0";

//

// Mission types

this.missionTypes = [ "LOCAL", "CONTACT", "TRANSFER", "RETRIEVE", "TRANSPORT", "CARGO", "SEARCH", "INFO", "TOUR", "KILL" ];

//

this.debugStore = [
    // {
    //     groupId: 11,
    //     index: 0,
    //     local: true,
    //     title: "(TRAINING) This is a mission of type LOCAL",
    //     description: "TBD",
    //     choices: null
    // },
    {
        groupId: 11,
        index: 1,
        choices: "search",
        customResponses: {
            money: [ "This is a debug mission, don't forget it." ],
            problems: [ "No, but he could move to another system - you'll have to follow his traces, asking in the BBS and gathering hints." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 6,
                generate: { search: true },
                unaware: true
            }
        ]
    }
];

this.missionDialogStore2 = [
    // Generic -----------------------------------------------------------------
    {
        label: "G1",
        title: "REWARD: Commander #PLAYERNAME#, get your payment.",
        step1: {
            text: [ "AUTOMATED MESSAGE - Please accept this message to close the transaction." ],
            choices: {
                "COMPLETE": { index: 10, text: "Accept and hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "G2",
        title: "LOOK HERE: Commander #PLAYERNAME#.",
        step1: {
            text: [ "Hello Commander #PLAYERNAME#. Can we leave for our destination?" ],
            choices: {
                "NEXT": { index: 10, text: "\"Yes, we can leave now.\" and hang up.", action: "ret missionnext" },
                "QUIT": { index: 99, text: "\"Hold on while I make room.\" then hang up.", action: "quit" }
            }
        }
    },
    {
        label: "G3.1",
        title: "HANGAR: <Loading cargo on Commander #PLAYERNAME# ship.>",
        step1: {
            text: [ "We're going to load a shipment of #GOODS# on you ship, as agreed." ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, proceed.\"", action: "go 2" },
                "QUIT": { index: 99, text: "\"Hold on while I make room.\" then hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "We have loaded the cargo. You can leave when you want. Goodbye." ],
            choices: {
                "COMPLETE": { index: 10, text: "\"Thanks.\" then hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "G3.2",
        title: "HANGAR: <Offloading cargo from Commander #PLAYERNAME# ship.>",
        step1: {
            text: [ "AUTOMATED MESSAGE - Please accept this message to close the transaction." ],
            choices: {
                "COMPLETE": { index: 10, text: "Accept and hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "G4",
        title: "LOOK HERE: Waiting for Commander #PLAYERNAME#.",
        step1: {
            text: [ "Hello, I'm #NAME1#. Can we leave to our destination now?" ],
            choices: {
                "NEXT": { index: 10, text: "\"Yes, we can leave now.\" and hang up.", action: "ret missionnext" },
                "QUIT": { index: 99, text: "\"Hold on while I make room.\" then hang up.", action: "quit" }
            }
        }
    },
    {
        label: "G5",
        title: "GALCOP: Commander #PLAYERNAME#, get your payment.",
        step1: {
            text: [ "AUTOMATED MESSAGE - Please accept this message to close the transaction." ],
            choices: {
                "COMPLETE": { index: 10, text: "Accept and hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "G6",
        title: "CHURCH: Commander #PLAYERNAME#, get your payment.",
        step1: {
            text: [ "AUTOMATED MESSAGE - Please accept this message to close the transaction." ],
            choices: {
                "COMPLETE": { index: 10, text: "Accept and hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "G7",
        title: "DELUXOR: Commander #PLAYERNAME#, get your payment.",
        step1: {
            text: [ "AUTOMATED MESSAGE - Please accept this message to close the transaction." ],
            choices: {
                "COMPLETE": { index: 10, text: "Accept and hang up.", action: "ret missionend" }
            }
        }
    },
    // GalCop -----------------------------------------------------------------
    {
        label: "0.1.1",
        title: null,
        step1: {
            text: [ "Hi, I'm #NAME1#. There's something I can do for you? I'm pretty busy at the moment." ],
            choices: {
                "GOTO": { index: 10, text: "\"I come from #SYSTEM0# with some informations for you. GalCop is the sender.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Can you trasmit me these info?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Here they are.\" and send the info.", action: "go 3" }
            }
        },
        step3: {
            text: [ "Yes, I was awaiting this. Good service citizen. Thank you." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "0.2.1",
        title: null,
        step1: {
            text: [ "Hi, I'm #NAME1#. There's something I can do for you? I'm pretty busy at the moment." ],
            choices: {
                "GOTO": { index: 10, text: "\"I come from #SYSTEM0#, I have some equipment for you. GalCop is the sender.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Oh yes, I was awaiting that. Are you sending me the package?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, just sent through the station network.\"", action: "go 3" }
            }
        },
        step3: {
            text: [ "Good service citizen. Thank you." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "0.3.1",
        title: "MEETING: Waiting Commander #PLAYERNAME# for a chat.",
        step1: {
            text: [ "I suppose you are here to pick the data I gathered." ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, GalCop sent me. They said it's an urgent matter.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Yes. I have sent you the data. Hurry and bring them back. Farewell." ],
            choices: {
                "NEXT": { index: 10, text: "Take the data and hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "0.3.2",
        title: "GALCOP: Waiting for Commander #PLAYERNAME#.",
        step1: {
            text: [ "Citizen, give us the data you obtained." ],
            choices: {
                "GOTO": { index: 10, text: "\"I'm just sending it to you.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "The data isn't corrupted. Good service citizen. Thank you." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "0.4.1",
        title: "MEETING: Waiting Commander #PLAYERNAME# for a chat.",
        step1: {
            text: [ "Hello citizen. Listen to me and report to the GalCop officer who sent you." ],
            choices: {
                "GOTO": { index: 10, text: "\"Ok, I'm listening.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "...and that's all. Do your work. Goodbye." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "0.4.2",
        title: "GALCOP: Waiting for Commander #PLAYERNAME#.",
        step1: {
            text: [ "Citizen, give us the data you obtained." ],
            choices: {
                "GOTO": { index: 10, text: "Report the data.", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Good service citizen. Thank you." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "0.5.1",
        title: "MEETING: Waiting Commander #PLAYERNAME# for a chat.",
        step1: {
            text: [ "Hello citizen. You have to report the following info to agent #NAME2#." ],
            choices: {
                "GOTO": { index: 10, text: "\"Ok, I'm listening.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "...and that's all. Do your work. Goodbye." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "0.5.2",
        title: "GALCOP: Waiting for Commander #PLAYERNAME#.",
        step1: {
            text: [ "Citizen, you can give me the info you gathered." ],
            choices: {
                "GOTO": { index: 10, text: "Report the data.", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Good service citizen. Thank you." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "0.7.1",
        title: "GALCOP: Commander #PLAYERNAME#, waiting to proceed.",
        step1: {
            text: [ "Hello citizen, can we proceed with the transfer?" ],
            choices: {
                "NEXT": { index: 10, text: "\"Yes, we can leave right now.\" and hang up.", action: "ret missionnext" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        }
    },
    {
        label: "0.8.1",
        title: "GALCOP: Looking for Commander #PLAYERNAME#.",
        step1: {
            text: [ "Hello, I'm #NAME1#, can we proceed with the transfer?" ],
            choices: {
                "NEXT": { index: 10, text: "\"We can leave now.\" and hang up.", action: "ret missionnext" },
                "QUIT": { index: 99, text: "\"Hold on while I make room.\" then hang up.", action: "quit" }
            }
        }
    },
    {
        label: "0.12.1",
        title: "WAITING: Commander #PLAYERNAME#.",
        step1: {
            text: [ "Here you are. Let's get out of here NOW!" ],
            choices: {
                "NEXT": { index: 10, text: "\"Ok let's go.\" and hang up.", action: "ret missionnext" },
                "QUIT": { index: 99, text: "\"Hold on while I make room.\" then hang up.", action: "quit" }
            }
        }
    },
    // GalBook ----------------------------------------------------------------
    {
        label: "1.1.1",
        title: "LOOK HERE: Commander #PLAYERNAME#, where is my book?",
        step1: {
            text: [ "Here we are. Do you have my book?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, I have it right here.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Can you send it to me via the station network?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Done.\"", action: "go 3" }
            }
        },
        step3: {
            text: [ "At last! '#BOOK#! I looked forward to this moment! Take your payment! Goodbye!" ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "1.3.1",
        title: "DELIVER: I have a book for Commander #PLAYERNAME#.",
        step1: {
            text: [ "Hello. I'm #NAME1#. I have a book for you." ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, please, send it to me.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Done. Please, keep it safe. It's a valuable copy." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "1.3.2",
        title: "WAITING: Commander #PLAYERNAME# with my book.",
        step1: {
            text: [ "Here we are. Where's the book?" ],
            choices: {
                "GOTO": { index: 10, text: "Send the book.", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Ah, perfect. Very good conditions - near mint. Thank you." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "1.2.1",
        title: "DELIVER: I have a book for Commander #PLAYERNAME#.",
        step1: {
            text: [ "Hello. I'm #NAME1#. I have a book for you." ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, please, send it to me.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Done. Bring my salutes to #NAME0#. Goodbye." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "1.2.2",
        title: "WAITING: Commander #PLAYERNAME# with my book.",
        step1: {
            text: [ "You have returned at last. Where's the book?" ],
            choices: {
                "GOTO": { index: 10, text: "Send the book.", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Ah, perfect. Thank you." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    // GalChurch --------------------------------------------------------------
    {
        label: "2.2.1",
        title: null,
        step1: {
            text: [ "Hi, I'm #NAME1#. What do you want?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Hmm... the Church of #SYSTEM1# wants you to read this sacred text and join them.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Ah. I just needed something to read." ],
            choices: {
                "GOTO": { index: 10, text: "\"I'm sending you the text.\"", action: "go 3" }
            }
        },
        step3: {
            text: [ "Ok. Bye." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "2.3.1",
        title: "WAITING: Church envoy #PLAYERNAME#.",
        step1: {
            text: [ "Hello. I'm Acolyte #NAME1#. I will bring to your ship the holy alien artifacts for the transport." ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, thank you.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Done. My blessing to you and Acolyte #NAME2#. Goodbye." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "2.3.2",
        title: "WAITING: Gentle #PLAYERNAME# with our sacred materials.",
        step1: {
            text: [ "I thank you, #PLAYERNAME#, for your service." ],
            choices: {
                "GOTO": { index: 10, text: "\"You're welcome. You can pick up the artifacts.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Holy alien artifacts, to be precise. Thank you. The Church bless you." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "2.1.1",
        title: "WAITING: Church envoy #PLAYERNAME#.",
        step1: {
            text: [ "Hello. I'm Acolyte #NAME1#. Can I send you the materials for Acolyte #NAME0#?." ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, thank you.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Done. My blessing to you and Acolyte #NAME0#. Goodbye." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "2.1.2",
        title: "WAITING: Looking forward to encounter #PLAYERNAME# again.",
        step1: {
            text: [ "I thank you, #PLAYERNAME#, for your service." ],
            choices: {
                "GOTO": { index: 10, text: "\"You're welcome. Here are your materials.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Thank you. The church bless you." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "2.6.1",
        title: "CHURCH: Commander #PLAYERNAME#, ready to leave.",
        step1: {
            text: [ "Hello kindred soul, I'm Priest #NAME1#. Can we proceed with the transfer?" ],
            choices: {
                "NEXT": { index: 10, text: "\"We can leave now.\" and hang up.", action: "ret missionnext" },
                "QUIT": { index: 99, text: "\"Hold on while I make room.\" then hang up.", action: "quit" }
            }
        }
    },
    {
        label: "2.8.1",
        title: "CHURCH: Church followers eagerly waiting for Commander #PLAYERNAME#.",
        step1: {
            text: [ "Hello kindred soul, We are ready to start our pilgrimage." ],
            choices: {
                "NEXT": { index: 10, text: "\"We can leave now.\" and hang up.", action: "ret missionnext" },
                "QUIT": { index: 99, text: "\"Hold on while I make room.\" then hang up.", action: "quit" }
            }
        }
    },
    // GalShady ---------------------------------------------------------------
    {
        label: "3.1.1",
        title: "HELP: Dear friend #PLAYERNAME#, I'm waiting for you.",
        step1: {
            text: [ "Good day, friend. Do you have something for me?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, some... 'useful data'.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Perfect. It's the data I like most!" ],
            choices: {
                "GOTO": { index: 10, text: "\"Here you are.\"", action: "go 3" }
            }
        },
        step3: {
            text: [ "Thank you, friend." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "3.3.1",
        title: "HURRY: #PLAYERNAME#, take away this thing.",
        step1: {
            text: [ "Hi, I'm #NAME1#, and you must pick up this thing. Deliver it to #NAME2# in the #SYSTEM2#." ],
            choices: {
                "GOTO": { index: 10, text: "\"Ok... but what is it?\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "The thing? It's... a thing. It seems a box but... Doesn't matter. I almost forgot! It's a surprise! #NAME2# doesn't know of the thing." ],
            choices: {
                "GOTO": { index: 10, text: "\"Ah... ok.\"", action: "go 3" }
            }
        },
        step3: {
            text: [ "Do what you got to do. Goodbye." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "3.3.2",
        title: null,
        step1: {
            text: [ "Hello, #NAME2# here, what do you want?" ],
            choices: {
                "GOTO": { index: 10, text: "\"I have a th... box for you. A present.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "A present? Mh. Ok, send it to me." ],
            choices: {
                "GOTO": { index: 10, text: "\"Sent just now.\"", action: "go 3" }
            }
        },
        step3: {
            text: [ "Bye." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "3.2.1",
        title: null,
        step1: {
            text: [ "Hello, I'm #NAME1#. What can I do for you?" ],
            choices: {
                "GOTO": { index: 10, text: "\"#NAME0# wants some package back.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "#NAME0#? Hmm... yes I understand. Please wait a moment..." ],
            choices: {
                "GOTO": { index: 10, text: "Wait.", action: "go 3" }
            }
        },
        step3: {
            text: [ "I've sent you the package. Goodbye." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "3.2.2",
        title: "WAITING: #PLAYERNAME# with my package.",
        step1: {
            text: [ "You have brought my package, right?" ],
            choices: {
                "GOTO": { index: 10, text: "Send the package via the station network.", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Bye." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "3.4.1",
        title: "NOW: Commander #PLAYERNAME#, ready to leave.",
        step1: {
            text: [ "Let's get out of there." ],
            choices: {
                "NEXT": { index: 10, text: "\"Ok.\" then hang up.", action: "ret missionnext" },
                "QUIT": { index: 99, text: "\"Hold on while I make room.\" then hang up.", action: "quit" }
            }
        }
    },
    {
        label: "3.5.1",
        title: "WAITING: Commander #PLAYERNAME#, we are ready to leave.",
        step1: {
            text: [ "We can go now." ],
            choices: {
                "NEXT": { index: 10, text: "\"Ok.\" then hang up.", action: "ret missionnext" },
                "QUIT": { index: 99, text: "\"Hold on while I make room.\" then hang up.", action: "quit" }
            }
        }
    },
    {
        label: "3.9.1",
        title: "PASSAGE: Commander #PLAYERNAME#, I'm ready to leave.",
        step1: {
            text: [ "They send you to bring me to safety, right? Let's go now, I don't feel safe here." ],
            choices: {
                "NEXT": { index: 10, text: "\"Ok.\" then hang up.", action: "ret missionnext" },
                "QUIT": { index: 99, text: "\"Hold on while I make room.\" then hang up.", action: "quit" }
            }
        }
    },
    // GalMedical -------------------------------------------------------------
    {
        label: "4.1.1",
        title: "PSA: Medical team available here!",
        step1: {
            text: [ "Hello, I'm Doctor #NAME1#. There's something I can do for you?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Your colleagues from #SYSTEM0# want to know how goes the work.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Oh right. We're very busy and I forgot to update them. Thank you. I'll answer to them right now." ],
            choices: {
                "COMPLETE": { index: 10, text: "\"Perfect. Goodbye.\"", action: "ret missionend" }
            }
        }
    },
    {
        label: "4.2.1",
        title: "SCIENCE: Looking for professionals for medical research.",
        step1: {
            text: [ "Hello, I'm Doctor #NAME1#. There's something I can do for you?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Your colleagues from #SYSTEM0# want to know how goes the work.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Yes, I must update them on the stage of our research. It's going pretty well but the work is very intensive and leave us little time for other. Thanks for your reminding." ],
            choices: {
                "COMPLETE": { index: 10, text: "\"Ok then. Goodbye.\"", action: "ret missionend" }
            }
        }
    },
    {
        label: "4.5.1",
        title: "URGENT: Doctor #NAME1# looking for Commander #PLAYERNAME#.",
        step1: {
            text: [ "Hello. I'm Doctor #NAME1#. Are you here to take the donor blood?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, you can do it now.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Ok, done. Thank you. Please hurry." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "4.5.2",
        title: "HOSPITAL: Emergency!! Donor blood needed. Do your part.",
        step1: {
            text: [ "Do you have the donor blood we need?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, taken from #NAME1#. You can pick it up now.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "This is a relief. Thank you. Goodbye." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "4.4.1",
        title: "PICK-UP: I'm Doctor #NAME1# and I'm looking for Commander #PLAYERNAME#",
        step1: {
            text: [ "Hello. I'm Doctor #NAME1#. I have a box of documents for you." ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, please, send it to me.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Done. Thank you. Goodbye." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "4.4.2",
        title: "DOCUMENTS: Commander #PLAYERNAME# answer here when you return.",
        step1: {
            text: [ "Have you taken the documents I need?" ],
            choices: {
                "GOTO": { index: 10, text: "Send all the documents.", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Thank you." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "4.11.1",
        title: "PICK-UP: Commander #PLAYERNAME# for ship-ambulance job.",
        step1: {
            text: [ "Hello, we have the passenger. Can we bring him into your ship?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, please.\"", action: "go 2" },
                "QUIT": { index: 10, text: "\"Hold on while I make room.\" then hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Done. Please leave quickly. Goodbye." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "4.11.2",
        title: "HOSPITAL: Waiting for Commander #PLAYERNAME# with a sick passenger.",
        step1: {
            text: [ "We are bringing the passenger to the hospital. Thanks for your service." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    // GalCivic ---------------------------------------------------------------
    {
        label: "5.1.1",
        title: null,
        step1: {
            text: [ "Hello, I'm #NAME1#. Do you have a package for me?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, exactly.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Can you send it to me, please?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Just done.\"", action: "go 3" }
            }
        },
        step3: {
            text: [ "Thank you, friend." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "5.2.1",
        title: null,
        step1: {
            text: [ "Hi, I'm #NAME1#. Can I do something for you?" ],
            choices: {
                "GOTO": { index: 10, text: "\"I come from #SYSTEM0#. #NAME0# is worried about you and the family.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Oh! We had a lot of problem with the communication network here. We'll try to make contact again soon. Things aren't too bad. Thanks for your interest." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "5.3.1",
        title: "LOOK HERE: Commander #PLAYERNAME#, I have #ITEM# for you.",
        step1: {
            text: [ "Hello. I'm #NAME1#. Can I send you the stuff #NAME0# has forgotten here?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, please, send it to me.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Thank you. Goodbye." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "5.3.2",
        title: "ITEMS: Commander #PLAYERNAME#, bring back my stuff please!",
        step1: {
            text: [ "Where is my stuff?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Right here, I'm going to send it to you.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Thank you. I was desperate without." ],
            choices: {
                "COMPLETE": { index: 10, text: "(\"Go figure.\") Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "5.8.1",
        title: "HEY YOU: Commander #PLAYERNAME#, I'm waiting for you.",
        step1: {
            text: [ "Hi, I'm #NAME1#. Can we leave now? I have a bad feeling..." ],
            choices: {
                "NEXT": { index: 10, text: "\"Ok, let's leave.\" and hang up.", action: "ret missionnext" },
                "QUIT": { index: 99, text: "\"Hold on while I make room.\" then hang up.", action: "quit" }
            }
        }
    },
    // GalTourism missions ----------------------------------------------------
    {
        label: "6.1.1",
        title: "DELUXOR HOTELS: Exaggerate luxury for a deluxe holiday!",
        step1: {
            text: [ "Hi, this is the DeLuxor hotels reception. What can I do for you?" ],
            choices: {
                "GOTO": { index: 10, text: "\"I need to cancel a room reservation for #NAME0# from the #SYSTEM0# system.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Are you #NAME0#?" ],
            choices: {
                "GOTO": { index: 10, text: "\"No. #NAME0# has employed me to attend this because he can't contact you. I'm sending you the documents I was given.\"", action: "go 3" }
            }
        },
        step3: {
            text: [ "Hm. I see. We'll need a further confirmation from #NAME0#, though. Thanks, DeLuxor hotels wishes you a good day." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "6.2.1",
        title: "SPACEPORT: Baggage reclamation office open for service.",
        step1: {
            text: [ "Hi, I'm #NAME1#. Do you wish to collect your baggage?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Not mine, but #NAME2# baggage. Here's the documents.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Ok, I see. All right, you can collect the baggage. Good day." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "6.2.2",
        title: "WAITING: Commander #PLAYERNAME# and my baggage.",
        step1: {
            text: [ "Hello, I'm #NAME2#. Do you have my baggage?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, I've collected it from the #SYSTEM1# spaceport. I'm sending it to you.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Ooh thank you. I was very worried. Thanks again. Goodbye." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "6.3.1",
        title: "SPACEPORT: Baggage reclamation office open for service.",
        step1: {
            text: [ "Hi, I'm #NAME1#. Do you wish to collect your baggage?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Not mine, but #NAME0# baggage. Here's the documents.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Ok, I see. All right, you can collect the baggage. Good day." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "6.3.2",
        title: "WAITING: Commander #PLAYERNAME# and my baggage.",
        step1: {
            text: [ "Welcome back, Commander. Do you have my baggage?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, it's here. I'm sending it to you.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Thanks for your service. Goodbye." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    // GalNature --------------------------------------------------------------
    {
        label: "8.1.1",
        title: "BOTANICAL: Commander #PLAYERNAME#, plants specimens are ready for transport.",
        step1: {
            text: [ "I'm #NAME1#, a botanist. We have the specimens that you must return to #NAME0#." ],
            choices: {
                "GOTO": { index: 10, text: "\"Ok, bring them to my ship.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "We are going to the hangar. Hurry up." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "8.1.2",
        title: "BOTANICAL: Commander #PLAYERNAME#, answer here to deliver the specimens.",
        step1: {
            text: [ "Can we take the plants specimens now?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, you can take them.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Thank you. We'll provide better conditions for these plants. Goodbye." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    // GalCulture -------------------------------------------------------------
    {
        label: "9.1.1",
        title: "LOOK HERE: I'm waiting for Commander #PLAYERNAME#.",
        step1: {
            text: [ "Hello, I'm #NAME1# and you should have a painting for me." ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes. '#PAINTING#'. I'm sending it to you.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "I can't wait to put it in my collection! Thank you, Commander." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "9.2.1",
        title: "RELOCATION: Waiting for appointee Commander #PLAYERNAME#.",
        step1: {
            text: [ "Hi, I'm #NAME1#. I believe you must transport '#PAINTING#' to the art gallery in #SYSTEM2#, right?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, exactly. #NAME0# has sent me here.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Ok, I see. I'm taking the painting to your ship. Please be careful." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "9.2.2",
        title: "OPENING SOON: Art gallery currently preparing next big painting exposition.",
        step1: {
            text: [ "Hello, I'm #NAME2#, what can I do for you?" ],
            choices: {
                "GOTO": { index: 10, text: "\"I have a painting from #NAME1#, '#PAINTING#', for the gallery.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Oh yes, great! We are going to pick it up from your ship. Thanks for your work." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "9.3.1",
        title: "ART RESTORER: We'll give a new life to your old precious.",
        step1: {
            text: [ "Hi, I'm #NAME1#. Do you have something you want repaired?" ],
            choices: {
                "GOTO": { index: 10, text: "\"No, thanks. #NAME1# has sent me to pick up a painting: '#PAINTING#'.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Oh yes, one of my better works. I'll take it to your ship. Thanks and goodbye." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "9.3.2",
        title: "REMINDER: A painting from Commander #PLAYERNAME#",
        step1: {
            text: [ "Welcome back, Commander. Do you have my painting?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Yes, it's here. I'm sending it to you.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Ah, a very fine job. Thank you Commander. Goodbye." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    // GalGovernment ----------------------------------------------------------
    {
        label: "10.1.1",
        title: null,
        step1: {
            text: [ "Hi, I'm #NAME1#. What do you want?" ],
            choices: {
                "GOTO": { index: 10, text: "\"I come from #SYSTEM0#.\" Tell the given passcode.", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "I understand. I know I'm late with my report, but they always think everything is so easy... I'll send a detailed message very soon. No need to report back. Farewell." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "10.3.1",
        title: "DIPLOMACY: #SYSTEM0# consulate currently open to the public.",
        step1: {
            text: [ "Hello citizen. What can we do for you?" ],
            choices: {
                "GOTO": { index: 10, text: "\"I was appointed by #SYSTEM0# government to transfer some diplomatic documents to #SYSTEM2#.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Yes, we received communication. We are sending the documents to you. Thanks citizen. Goodbye." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "10.3.2",
        title: "DIPLOMACY: #SYSTEM0# consulate currently open to the public.",
        step1: {
            text: [ "Hello citizen. What can we do for you?" ],
            choices: {
                "GOTO": { index: 10, text: "\"I was appointed by #SYSTEM0# government to transfer some diplomatic documents from #SYSTEM1#.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Yes, we received communication." ],
            choices: {
                "GOTO": { index: 10, text: "\"I'm sending the documents to you right now.\"", action: "go 3" }
            }
        },
        step3: {
            text: [ "Thanks citizen. Goodbye." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "10.2.1",
        title: null,
        step1: {
            text: [ "I'm #NAME1#. Can I do something for you?" ],
            choices: {
                "GOTO": { index: 10, text: "\"The #SYSTEM0# government has sent me here.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "I understand. I'm sending you the documents. Keep them safe." ],
            choices: {
                "NEXT": { index: 10, text: "Hang up.", action: "ret missionnext" }
            }
        }
    },
    {
        label: "10.2.2",
        title: "GOVERNMENT: Front office open for Commander #PLAYERNAME#.",
        step1: {
            text: [ "The stolen dossiers?" ],
            choices: {
                "GOTO": { index: 10, text: "\"Here they are, I'm sending them to you.\"", action: "go 2" },
                "QUIT": { index: 99, text: "Hang up.", action: "quit" }
            }
        },
        step2: {
            text: [ "Good service citizen. Your government is proud of you." ],
            choices: {
                "COMPLETE": { index: 10, text: "Hang up.", action: "ret missionend" }
            }
        }
    },
    {
        label: "10.8.1",
        title: "DIPLOMATIC: Waiting for Commander #PLAYERNAME# for the trip back home.",
        step1: {
            text: [ "Hello citizen. We are escorting glorious officer #NAME1# to your ship. We can leave just after." ],
            choices: {
                "NEXT": { index: 10, text: "\"Ok.\" then hang up.", action: "ret missionnext" },
                "QUIT": { index: 99, text: "\"Hold on while I make room.\" then hang up.", action: "quit" }
            }
        }
    },
    {
        label: "10.9.1",
        title: "NOSTALGIC: Commander #PLAYERNAME#, let's get back home.",
        step1: {
            text: [ "Hello citizen. I'm #NAME1#. I'm ready to left for #SYSTEM0#, when you're ready." ],
            choices: {
                "NEXT": { index: 10, text: "\"Ok, let's go.\" then hang up.", action: "ret missionnext" },
                "QUIT": { index: 99, text: "\"Hold on while I make room.\" then hang up.", action: "quit" }
            }
        }
    }
];

this.missionDialogStore = [
    // Debug ------------------------------------------------------------------
    // {
    //     label: "11.1.1",
    //     title: null,
    //     step1: [
    //         "Hi, I'm #NAME1#. There's something I can do for you? I'm pretty busy at the moment.",
    //         "01_GOTO_2:\"I come from #SYSTEM0# with some informations for you. GalCop is the sender.\"",
    //         "99_QUIT:Hang up."
    //     ],
    //     step2: [
    //         "Can you trasmit me these info?",
    //         "01_GOTO_3:\"Here they are.\" and send the info."
    //     ],
    //     step3: [
    //         "Yes, I was awaiting this. Good service citizen. Thank you.",
    //         "01_COMPLETE:Hang up."
    //     ]
    // },
    // Generic ----------------------------------------------------------------
    {
        label: "G1",
        title: "REWARD: Commander #PLAYERNAME#, get your payment.",
        step1: [
            "<Please accept the transaction to close the deal.>",
            "01_COMPLETE:Accept and hang up."
        ]
    },
    {
        label: "G2",
        title: "LOOK HERE: Commander #PLAYERNAME#.",
        step1: [
            "Hello Commander #PLAYERNAME#. Can we leave for our destination?",
            "01_GOTO_2:\"Yes, we can leave now.\" and hang up.",
            "99_QUIT:\"Hold on while I make room.\" then hang up."
        ]
    },
    {
        label: "G3.1",
        title: "HANGAR: <Loading cargo on Commander #PLAYERNAME# ship.>",
        step1: [
            "We're going to load a shipment of #GOODS# on you ship, as agreed.",
            "01_GOTO_2:\"Yes, proceed.\"",
            "99_QUIT:\"Hold on while I make room.\" then hang up."
        ],
        step2: [
            "We have loaded the cargo. You can leave when you want. Goodbye.",
            "01_COMPLETE:Hang up."
        ]
    },
    {
        label: "G3.2",
        title: "HANGAR: <Offloading cargo from Commander #PLAYERNAME# ship.>",
        step1: [
            "-- Closing transaction --",
            "01_COMPLETE:Accept and hang up."
        ]
    },
    {
        label: "G4",
        title: "LOOK HERE: Waiting for Commander #PLAYERNAME#.",
        step1: [
            "Hello, I'm #NAME1#. Can we leave to our destination now?",
            "01_COMPLETE:\"Yes, we can leave now.\" and hang up.",
            "99_QUIT:\"Hold on while I make room.\" then hang up."
        ]
    },
    {
        label: "G5",
        title: "GALCOP: Commander #PLAYERNAME#, get your payment.",
        step1: [
            "<Please accept the transaction to close the deal.>",
            "01_COMPLETE:Accept and hang up."
        ]
    },
    {
        label: "G6",
        title: "CHURCH: Commander #PLAYERNAME#, get your payment.",
        step1: [
			"<Please accept the transaction to close the deal.>",
			"01_COMPLETE:Accept and hang up."
        ]
    },
    {
        label: "G7",
        title: "DELUXOR: Commander #PLAYERNAME#, get your payment.",
        step1: [
            "<Please accept the transaction to close the deal.>",
            "01_COMPLETE:Accept and hang up."
        ]
    },
    // GalCop -----------------------------------------------------------------
    {
        label: "0.1.1",
        title: null,
        step1: [
			"Hi, I'm #NAME1#. There's something I can do for you? I'm pretty busy at the moment.",
			"01_GOTO_2:\"I come from #SYSTEM0# with some informations for you. GalCop is the sender.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Can you trasmit me these info?",
			"01_GOTO_3:\"Here they are.\" and send the info."
		],
        step3: [
			"Yes, I was awaiting this. Good service citizen. Thank you.",
			"01_COMPLETE:Hang up."
        ]
    },
    {
        label: "0.2.1",
        title: null,
        step1: [
            "Hi, I'm #NAME1#. There's something I can do for you? I'm pretty busy at the moment.",
            "01_GOTO_2:\"I come from #SYSTEM0#, I have some equipment for you. GalCop is the sender.\"",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Oh yes, I was awaiting that. Are you sending me the package?",
            "01_GOTO_3:\"Yes, just sent through the station network.\""
        ],
        step3: [
            "Good service citizen. Thank you.",
            "01_COMPLETE:Hang up."
        ]
    },
    {
        label: "0.3.1",
        title: "MEETING: Waiting Commander #PLAYERNAME# for a chat.",
        step1: [
            "I suppose you are here to pick the data I gathered.",
            "01_GOTO_2:\"Yes, GalCop sent me. They said it's an urgent matter.\"",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Yes. I have sent you the data. Hurry and bring them back. Farewell.",
            "01_NEXTTASK:Take the data and hang up."
        ]
    },
    {
        label: "0.3.2",
        title: "GALCOP: Waiting for Commander #PLAYERNAME#.",
        step1: [
            "Citizen, give us the data you obtained.",
            "01_GOTO_2:\"I'm just sending it to you.\"",
            "99_QUIT:Hang up."
        ],
        step2: [
            "The data isn't corrupted. Good service citizen. Thank you.",
            "01_COMPLETE:Hang up."
        ]
    },
    {
        label: "0.4.1",
        title: "MEETING: Waiting Commander #PLAYERNAME# for a chat.",
        step1: [
            "Hello citizen. Listen to me and report to the GalCop officer who sent you.",
            "01_GOTO_2:\"Ok, I'm listening.\"",
            "99_QUIT:Hang up."
        ],
        step2: [
            "...and that's all. Do your work. Goodbye.",
            "01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "0.4.2",
        title: "GALCOP: Waiting for Commander #PLAYERNAME#.",
        step1: [
            "Citizen, give us the data you obtained.",
            "01_GOTO_2:Report the data.",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Good service citizen. Thank you.",
            "01_COMPLETE:Hang up."
        ]
    },
    {
        label: "0.5.1",
        title: "MEETING: Waiting Commander #PLAYERNAME# for a chat.",
        step1: [
            "Hello citizen. You have to report the following info to agent #NAME2#.",
            "01_GOTO_2:\"Ok, I'm listening.\"",
            "99_QUIT:Hang up."
        ],
        step2: [
            "...and that's all. Do your work. Goodbye.",
            "01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "0.5.2",
        title: "GALCOP: Waiting for Commander #PLAYERNAME#.",
        step1: [
            "Citizen, you can give me the info you gathered.",
            "01_GOTO_2:Report the data.",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Good service citizen. Thank you.",
            "01_COMPLETE:Hang up."
        ]
    },
    {
        label: "0.7.1",
        title: "GALCOP: Commander #PLAYERNAME#, waiting to proceed.",
        step1: [
            "Hello citizen, can we proceed with the transfer?",
            "01_NEXTTASK:\"Yes, we can leave right now.\" and hang up.",
            "99_QUIT:\"Hold on while I make room.\" then hang up."
        ]
    },
    {
        label: "0.8.1",
        title: "GALCOP: Looking for Commander #PLAYERNAME#.",
        step1: [
            "Hello, I'm #NAME1#, can we proceed with the transfer?",
            "01_NEXTTASK:\"We can leave now.\" and hang up.",
            "99_QUIT:\"Hold on while I make room.\" then hang up."
        ]
    },
    {
        label: "0.12.1",
        title: "WAITING: Commander #PLAYERNAME#.",
        step1: [
            "Here you are. Let's get out of here NOW!",
            "01_NEXTTASK:\"Ok let's go.\" and hang up.",
            "99_QUIT:\"Hold on while I make room.\" then hang up."
        ]
    },
    // GalBook ----------------------------------------------------------------
    {
        label: "1.1.1",
        title: "LOOK HERE: Commander #PLAYERNAME#, where is my book?",
        step1: [
            "Here we are. Do you have my book?",
            "01_GOTO_2:\"Yes, I have it right here.\"",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Can you send it to me via the station network?",
            "01_GOTO_3:\"Done.\""
        ],
        step3: [
            "At last! '#BOOK#! I looked forward to this moment! Take your payment! Goodbye!",
            "01_COMPLETE:Hang up."
        ]
    },
    {
        label: "1.3.1",
        title: "DELIVER: I have a book for Commander #PLAYERNAME#.",
        step1: [
            "Hello. I'm #NAME1#. I have a book for you.",
            "01_GOTO_2:\"Yes, please, send it to me.\"",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Done. Please, keep it safe. It's a valuable copy.",
            "01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "1.3.2",
        title: "WAITING: Commander #PLAYERNAME# with my book.",
        step1: [
            "Here we are. Where's the book?",
            "01_GOTO_2:Send the book.",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Ah, perfect. Very good conditions - near mint. Thank you.",
            "01_COMPLETE:Hang up."
        ]
    },
    {
        label: "1.2.1",
        title: "DELIVER: I have a book for Commander #PLAYERNAME#.",
        step1: [
            "Hello. I'm #NAME1#. I have a book for you.",
            "01_GOTO_2:\"Yes, please, send it to me.\"",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Done. Bring my salutes to #NAME0#. Goodbye.",
            "01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "1.2.2",
        title: "WAITING: Commander #PLAYERNAME# with my book.",
        step1: [
            "You have returned at last. Where's the book?",
            "01_GOTO_2:Send the book.",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Ah, perfect. Thank you.",
            "01_COMPLETE:Hang up."
        ]
    },

    // GalChurch --------------------------------------------------------------
    {
        label: "2.2.1",
        title: null,
        step1: [
			"Hi, I'm #NAME1#. What do you want?",
			"01_GOTO_2:\"Hmm... the Church of #SYSTEM1# wants you to read this sacred text and join them.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Ah.",
			"01_GOTO_3:\"I'm sending you the text.\""
		],
		step3: [
			"Ok. Bye.",
			"01_COMPLETE:Hang up."
        ]
    },
    {
        label: "2.3.1",
        title: "WAITING: Church envoy #PLAYERNAME#.",
        step1: [
			"Hello. I'm Acolyte #NAME1#. I will bring to your ship the holy alien artifacts for the transport.",
			"01_GOTO_2:\"Yes, thank you.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Done. My blessing to you and Acolyte #NAME2#. Goodbye.",
			"01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "2.3.2",
        title: "WAITING: Gentle #PLAYERNAME# with our sacred materials.",
        step1: [
			"I thank you, #PLAYERNAME#, for your service.",
			"01_GOTO_2:\"You're welcome. You can pick up the artifacts.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Holy alien artifacts, to be precise. Thank you. The Church bless you.",
			"01_COMPLETE:Hang up."
        ]
    },
    {
        label: "2.1.1",
        title: "WAITING: Church envoy #PLAYERNAME#.",
        step1: [
			"Hello. I'm Acolyte #NAME1#. Can I send you the materials for Acolyte #NAME0#?.",
			"01_GOTO_2:\"Yes, thank you.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Done. My blessing to you and Acolyte #NAME0#. Goodbye.",
			"01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "2.1.2",
        title: "WAITING: Looking forward to encounter #PLAYERNAME# again.",
        step1: [
			"I thank you, #PLAYERNAME#, for your service.",
			"01_GOTO_2:\"You're welcome. Here are your materials.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Thank you. The church bless you.",
			"01_COMPLETE:Hang up."
        ]
    },
    {
        label: "2.6.1",
        title: "CHURCH: Commander #PLAYERNAME#, ready to leave.",
        step1: [
			"Hello kindred soul, I'm Priest #NAME1#. Can we proceed with the transfer?",
			"01_NEXTTASK:\"We can leave now.\" and hang up.",
			"99_QUIT:\"Hold on while I make room.\" then hang up."
        ]
    },
    {
        label: "2.8.1",
        title: "CHURCH: Church followers eagerly waiting for Commander #PLAYERNAME#.",
        step1: [
			"Hello kindred soul, We are ready to start our pilgrimage.",
			"01_NEXTTASK:\"We can leave now.\" and hang up.",
			"99_QUIT:\"Hold on while I make room.\" then hang up."
        ]
    },

    // GalShady ---------------------------------------------------------------
    {
        label: "3.1.1",
        title: "HELP: Dear friend #PLAYERNAME#, I'm waiting for you.",
        step1: [
			"Good day, friend. Do you have something for me?",
			"01_GOTO_2:\"Yes, some... 'useful data'.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Perfect. It's the data I like most!",
			"01_GOTO_3:\"Here you are.\""
		],
		step3: [
			"Thank you, friend.",
			"01_COMPLETE:Hang up."
        ]
    },
    {
        label: "3.3.1",
        title: "HURRY: #PLAYERNAME#, take away this thing.",
        step1: [
			"Hi, I'm #NAME1#, and you must pick up this thing. Deliver it to #NAME2# in the #SYSTEM2#.",
			"01_GOTO_2:\"Ok... but what is it?\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"The thing? It's... a thing. It seems a box but... Doesn't matter. I almost forgot! It's a surprise! #NAME2# doesn't know of the thing.",
			"01_GOTO_3:\"Ah... ok.\""
		],
		step3: [
			"Do what you got to do. Goodbye.",
			"01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "3.3.2",
        title: null,
        step1: [
			"Hello, #NAME2# here, what do you want?",
			"01_GOTO_2:\"A th... box for you. A present.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"A present? Mh. Ok, send it to me.",
			"01_GOTO_3:\"Sent just now.\""
		],
		step3: [
			"Bye.",
			"01_COMPLETE:Hang up."
        ]
    },
    {
        label: "3.2.1",
        title: null,
        step1: [
			"Hello, I'm #NAME1#. What can I do for you?",
			"01_GOTO_2:\"#NAME0# wants some package back.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"#NAME0#? Hmm... yes I understand. Please wait a moment...",
			"01_GOTO_3:Wait."
		],
		step3: [
			"I've sent you the package. Goodbye.",
			"01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "3.2.2",
        title: "WAITING: #PLAYERNAME# with my package.",
        step1: [
			"You have brought my package, right?",
			"01_GOTO_2:Send the package via the station network.",
			"99_QUIT:Hang up."
		],
		step2: [
			"Bye.",
			"01_COMPLETE:Hang up."
        ]
    },
    {
        label: "3.4.1",
        title: "NOW: Commander #PLAYERNAME#, ready to leave.",
        step1: [
			"Let's get out of there.",
			"01_NEXTTASK:\"Ok.\" and hang up.",
			"99_QUIT:\"Hold on while I make room.\" then hang up."
        ]
    },
    {
        label: "3.5.1",
        title: "WAITING: Commander #PLAYERNAME#, we are ready to leave.",
        step1: [
			"We can go now.",
			"01_NEXTTASK:\"Ok.\" and hang up.",
			"99_QUIT:\"Hold on while I make room.\" then hang up."
        ]
    },
    {
        label: "3.9.1",
        title: "PASSAGE: Commander #PLAYERNAME#, I'm ready to leave.",
        step1: [
			"They send you to bring me to safety, right? Let's go now, I don't feel safe here.",
			"01_NEXTTASK:\"Ok.\" and hang up.",
			"99_QUIT:\"Hold on while I make room.\" then hang up."
        ]
    },

    // GalMedical -------------------------------------------------------------
    {
        label: "4.1.1",
        title: "PSA: Medical team available here!",
        step1: [
            "Hello, I'm Doctor #NAME1#. There's something I can do for you?",
            "01_GOTO_2:\"Your colleagues from #SYSTEM0# want to know how goes the work.\"",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Oh right. We're very busy and I forgot to update them. Thank you. I'll answer to them right now.",
            "01_COMPLETE:\"Perfect. Goodbye.\""
        ]
    },
    {
        label: "4.2.1",
        title: "SCIENCE: Looking for professionals for medical research.",
        step1: [
            "Hello, I'm Doctor #NAME1#. There's something I can do for you?",
            "01_GOTO_2:\"Your colleagues from #SYSTEM0# want to know how goes the work.\"",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Yes, I must update them on the stage of our research. It's going pretty well but the work is very intensive and leave us little time for other. Thanks for your reminding.",
            "01_COMPLETE:\"Ok then. Goodbye.\""
        ]
    },
    {
        label: "4.5.1",
        title: "URGENT: Doctor #NAME1# looking for Commander #PLAYERNAME#.",
        step1: [
            "Hello. I'm Doctor #NAME1#. Are you here to take the donor blood?",
            "01_GOTO_2:\"Yes, you can do it now.\"",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Ok, done. Thank you. Please hurry.",
            "01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "4.5.2",
        title: "HOSPITAL: Emergency!! Donor blood needed. Do your part.",
        step1: [
            "Do you have the donor blood we need?",
            "01_GOTO_2:\"Yes, taken from #NAME1#. You can pick it up now.\"",
            "99_QUIT:Hang up."
        ],
        step2: [
            "This is a relief. Thank you. Goodbye.",
            "01_COMPLETE:Hang up."
        ]
    },
    {
        label: "4.4.1",
        title: "PICK-UP: I'm Doctor #NAME1# and I'm looking for Commander #PLAYERNAME#",
        step1: [
            "Hello. I'm Doctor #NAME1#. I have a box of documents for you.",
            "01_GOTO_2:\"Yes, please, send it to me.\"",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Done. Thank you. Goodbye.",
            "01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "4.4.2",
        title: "DOCUMENTS: Commander #PLAYERNAME# answer here when you return.",
        step1: [
            "Have you taken the documents I need?",
            "01_GOTO_2:Send all the documents.",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Thank you.",
            "01_COMPLETE:Hang up."
        ]
    },
    {
        label: "4.11.1",
        title: "PICK-UP: Commander #PLAYERNAME# for ship-ambulance job.",
        step1: [
            "Hello, we have the passenger. Can we bring him into your ship?",
            "01_GOTO_2:\"Yes, please.\"",
            "99_QUIT:\"Hold on while I make room.\" then hang up."
        ],
        step2: [
            "Done. Please leave quickly. Goodbye.",
            "01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "4.11.2",
        title: "HOSPITAL: Waiting for Commander #PLAYERNAME# with a sick passenger.",
        step1: [
            "We are bringing the passenger to the hospital. Thanks for your service.",
            "01_COMPLETE:Hang up."
        ]
    },

    // GalCivic ---------------------------------------------------------------
    {
        label: "5.1.1",
        title: null,
        step1: [
			"Hello, I'm #NAME1#. Do you have a package for me?",
			"01_GOTO_2:\"Yes, exactly.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Can you send it to me, please?",
			"01_GOTO_3:\"Just done.\""
		],
		step3: [
			"Thank you, friend.",
			"01_COMPLETE:Hang up."
        ]
    },
    {
        label: "5.2.1",
        title: null,
        step1: [
			"Hi, I'm #NAME1#. Can I do something for you?",
			"01_GOTO_2:\"I come from #SYSTEM0#. #NAME0# is worried about you and the family.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Oh! We had a lot of problem with the communication network here. We'll try to make contact again soon. Things aren't too bad. Thanks for your interest.",
			"01_COMPLETE:Hang up."
        ]
    },
    {
        label: "5.3.1",
        title: "LOOK HERE: Commander #PLAYERNAME#, I have #ITEM# for you.",
        step1: [
			"Hello. I'm #NAME1#. Can I send you the stuff #NAME0# has forgotten here?",
			"01_GOTO_2:\"Yes, please, send it to me.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Thank you. Goodbye.",
			"01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "5.3.2",
        title: "ITEMS: Commander #PLAYERNAME#, bring back my stuff please!",
        step1: [
			"Where is my stuff?",
			"01_GOTO_2:\"Right here, I'll going to send it to you.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Thank you. I was desperate without.",
			"01_COMPLETE:(\"Go figure.\") Hang up."
        ]
    },
    {
        label: "5.8.1",
        title: "HEY YOU: Commander #PLAYERNAME#, I'm waiting for you.",
        step1: [
			"Hi, I'm #NAME1#. Can we leave now? I have a bad feeling...",
			"01_NEXTTASK:\"Ok, let's leave.\" and hang up.",
			"99_QUIT:\"Hold on while I make room.\" then hang up."
        ]
    },

    // GalTourism missions ----------------------------------------------------
    {
        label: "6.1.1",
        title: "DELUXOR HOTELS: Exaggerate luxury for a deluxe holiday!",
        step1: [
            "Hi, this is the DeLuxor hotels reception. What can I do for you?",
            "01_GOTO_2:\"I need to cancel a room reservation for #NAME0# from the #SYSTEM0# system.\"",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Are you #NAME0#?",
            "01_GOTO_3:\"No. #NAME0# has employed me to attend this because he can't contact you. I'm sending you the documents I was given.\""
        ],
        step3: [
            "Hm. I see. We'll need a further confirmation from #NAME0#, though. Thanks, DeLuxor hotels wishes you a good day.",
            "01_COMPLETE:Hang up."
        ]
    },
    {
        label: "6.2.1",
        title: "SPACEPORT: Baggage reclamation office open for service.",
        step1: [
            "Hi, I'm #NAME1#. Do you wish to collect your baggage?",
            "01_GOTO_2:\"Not mine, but #NAME2# baggage. Here's the documents.\"",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Ok, I see. All right, you can collect the baggage. Good day.",
            "01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "6.2.2",
        title: "WAITING: Commander #PLAYERNAME# and my baggage.",
        step1: [
            "Hello, I'm #NAME2#. Do you have my baggage?",
            "01_GOTO_2:\"Yes, I've collected it from the #SYSTEM1# spaceport. I'm sending it to you.\"",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Ooh thank you. I was very worried. Thanks again. Goodbye.",
            "01_COMPLETE:Hang up."
        ]
    },
    {
        label: "6.3.1",
        title: "SPACEPORT: Baggage reclamation office open for service.",
        step1: [
            "Hi, I'm #NAME1#. Do you wish to collect your baggage?",
            "01_GOTO_2:\"Not mine, but #NAME0# baggage. Here's the documents.\"",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Ok, I see. All right, you can collect the baggage. Good day.",
            "01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "6.3.2",
        title: "WAITING: Commander #PLAYERNAME# and my baggage.",
        step1: [
            "Welcome back, Commander. Do you have my baggage?",
            "01_GOTO_2:\"Yes, it's here. I'm sending it to you.\"",
            "99_QUIT:Hang up."
        ],
        step2: [
            "Thanks for your service. Goodbye.",
            "01_COMPLETE:Hang up."
        ]
    },

    // GalNature --------------------------------------------------------------
    {
        label: "8.1.1",
        title: "BOTANICAL: Commander #PLAYERNAME#, plants specimens are ready for transport.",
        step1: [
			"I'm #NAME1#, a botanist. We have the specimens that you must return to #NAME0#.",
			"01_GOTO_2:\"Ok, bring them to my ship.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"We are going to the hangar. Hurry up.",
			"01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "8.1.2",
        title: "BOTANICAL: Commander #PLAYERNAME#, answer here to deliver the specimens.",
        step1: [
			"Can we take the plants specimens now?",
			"01_GOTO_2:\"Yes, you can take them.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Thank you. We'll provide better conditions for these plants. Goodbye.",
			"01_COMPLETE:Hang up."
        ]
    },

    // GalCulture -------------------------------------------------------------
    {
        label: "9.1.1",
        title: "LOOK HERE: I'm waiting for Commander #PLAYERNAME#.",
        step1: [
			"Hello, I'm #NAME1# and you should have a painting for me.",
			"01_GOTO_2:\"Yes. '#PAINTING#'. I'm sending it to you.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"I can't wait to put it in my collection! Thank you, Commander.",
			"01_COMPLETE:Hang up."
        ]
    },
    {
        label: "9.2.1",
        title: "RELOCATION: Waiting for appointee Commander #PLAYERNAME#.",
        step1: [
			"Hi, I'm #NAME1#. I believe you must transport '#PAINTING#' to the art gallery in #SYSTEM2#, right?",
			"01_GOTO_2:\"Yes, exactly. #NAME0# has sent me here.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Ok, I see. I'm taking the painting to your ship. Please be careful.",
			"01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "9.2.2",
        title: "OPENING SOON: Art gallery currently preparing next big painting exposition.",
        step1: [
			"Hello, I'm #NAME2#, what can I do for you?",
			"01_GOTO_2:\"I have a painting from #NAME1#, '#PAINTING#', for the gallery.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Oh yes, great! We are going to pick it up from your ship. Thanks for your work.",
			"01_COMPLETE:Hang up."
        ]
    },
    {
        label: "9.3.1",
        title: "ART RESTORER: We'll give a new life to your old precious.",
        step1: [
			"Hi, I'm #NAME1#. Do you have something you want repaired?",
			"01_GOTO_2:\"No, thanks. #NAME1# has sent me to pick up a painting: '#PAINTING#'.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Oh yes, one of my better works. I'll take it to your ship. Thanks and goodbye.",
			"01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "9.3.2",
        title: "REMINDER: A painting from Commander #PLAYERNAME#",
        step1: [
			"Welcome back, Commander. Do you have my painting?",
			"01_GOTO_2:\"Yes, it's here. I'm sending it to you.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Ah, a very fine job. Thank you Commander. Goodbye.",
			"01_COMPLETE:Hang up."
        ]
    },

    // GalGovernment ----------------------------------------------------------
    {
        label: "10.1.1",
        title: null,
        step1: [
			"Hi, I'm #NAME1#. What do you want?",
			"01_GOTO_2:\"I come from #SYSTEM0#.\" Tell the given passcode.",
			"99_QUIT:Hang up."
		],
		step2: [
			"I understand. I know I'm late with my report, but they always think everything is so easy... I'll send a detailed message very soon. No need to report back. Farewell.",
			"01_COMPLETE:Hang up."
        ]
    },
    {
        label: "10.3.1",
        title: "DIPLOMACY: #SYSTEM0# consulate currently open to the public.",
        step1: [
			"Hello citizen. What can we do for you?",
			"01_GOTO_2:\"I was appointed by #SYSTEM0# government to transfer some diplomatic documents to #SYSTEM2#.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Yes, we received communication. We are sending the documents to you. Thanks citizen. Goodbye.",
			"01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "10.3.2",
        title: "DIPLOMACY: #SYSTEM0# consulate currently open to the public.",
        step1: [
			"Hello citizen. What can we do for you?",
			"01_GOTO_2:\"I was appointed by #SYSTEM0# government to transfer some diplomatic documents from #SYSTEM1#.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Yes, we received communication.",
			"01_GOTO_3:\"I'm sending the documents to you right now.\""
		],
		step3: [
			"Thanks citizen. Goodbye.",
			"01_COMPLETE:Hang up."
        ]
    },
    {
        label: "10.2.1",
        title: null,
        step1: [
			"I'm #NAME1#. Can I do something for you?",
			"01_GOTO_2:\"The #SYSTEM0# government has sent me here.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"I understand. I'm sending you the documents. Keep them safe.",
			"01_NEXTTASK:Hang up."
        ]
    },
    {
        label: "10.2.2",
        title: "GOVERNMENT: Front office open for Commander #PLAYERNAME#.",
        step1: [
			"The stolen dossiers?",
			"01_GOTO_2:\"Here they are, I'm sending them to you.\"",
			"99_QUIT:Hang up."
		],
		step2: [
			"Good service citizen. Your government is proud of you.",
			"01_COMPLETE:Hang up."
        ]
    },
    {
        label: "10.8.1",
        title: "DIPLOMATIC: Waiting for Commander #PLAYERNAME# for the trip back home.",
        step1: [
			"Hello citizen. We are escorting glorious officer #NAME1# to your ship. We can leave just after.",
			"01_NEXTTASK:\"Ok.\" and hang up.",
			"99_QUIT:\"Hold on while I make room.\" then hang up."
		]
    },
    {
        label: "10.9.1",
        title: "NOSTALGIC: Commander #PLAYERNAME#, let's get back home.",
        step1: [
			"Hello citizen. I'm #NAME1#. I'm ready to left for #SYSTEM0#, when you're ready.",
			"01_NEXTTASK:\"Ok, let's go.\" and hang up.",
			"99_QUIT:\"Hold on while I make room.\" then hang up."
        ]
    }
];

this.missionStore = [
    // GalCop Missions --------------------------------------------------------
    // CONTACT
    {
        groupId: 0,
        index: 1,
        choices: "mission",
        parcel: "A GalCop information drive",
        customResponses: {
            money: [ "Just the ordinary GalCop payment." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 1,
                generate: { systems: 1, names: 1 },
                unaware: true
            }
        ]
    },
    {
        groupId: 0,
        index: 2,
        choices: "mission",
        parcel: "A GalCop equipment kit",
        customResponses: {
            money: [ "Just the ordinary GalCop payment." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 1,
                generate: { systems: 1, names: 1 },
                unaware: true
            }
        ]
    },
    // TRANSFER
    {
        groupId: 0,
        index: 5,
        choices: "mission",
        customResponses: {
            money: [ "Just the ordinary GalCop payment." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 2,
                generate: { systems: 1, names: 1 },
                parcel: "A GalCop information drive"
            },
            {
                index: 2,
                typeId: 2,
                generate: { systems: 1, names: 1 }
            }
        ]
    },
    // RETRIEVE
    {
        groupId: 0,
        index: 3,
        choices: "mission",
        customResponses: {
            money: [ "Just the ordinary GalCop payment." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 3,
                generate: { systems: 1, names: 1 },
                parcel: "A GalCop information drive",
                urgent: true
            },
            {
                index: 2,
                typeId: 3,
                generate: {
                    retrieve: true
                }
            }
        ]
    },
    {
        groupId: 0,
        index: 4,
        choices: "mission",
        customResponses: {
            money: [ "Just the ordinary GalCop payment." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 3,
                generate: { systems: 1, names: 1 },
                parcel: "A GalCop information drive",
                urgent: true
            },
            {
                index: 2,
                typeId: 3,
                generate: {
                    retrieve: true
                }
            }
        ]
    },
    // TRANSPORT
    {
        groupId: 0,
        index: 6,
        choices: "transport",
        passengers: [ 1 ],
        firstPassenger: 0,
        customResponses: {
            money: [ "Just the ordinary GalCop payment." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "G5"
            }
        ]
    },
    {
        groupId: 0,
        index: 7,
        choices: "transport",
        customResponses: {
            money: [ "Just the ordinary GalCop payment." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                passengers: [ 1 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "G5"
            }
        ]
    },
    {
        groupId: 0,
        index: 8,
        choices: "transport",
        customResponses: {
            money: [ "Just the ordinary GalCop payment." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                passengers: [ 1 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: {
                    retrieve: true
                },
                dialogMission: "G5"
            }
        ]
    },
    {
        groupId: 0,
        index: 9,
        choices: "transport",
        passengers: [ 1 ],
        risk: 1,
        customResponses: {
            money: [ "Just the ordinary GalCop payment." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "G5"
            }
        ]
    },
    {
        groupId: 0,
        index: 10,
        choices: "transport",
        risk: 1,
        customResponses: {
            money: [ "Just the ordinary GalCop payment." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                dialogMission: "0.8.1",
                passengers: [ 1 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "G5"
            }
        ]
    },
    {
        groupId: 0,
        index: 12,
        choices: "transport",
        risk: 2,
        customResponses: {
            money: [ "This is an emergency situation." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                urgent: true,
                passengers: [ 1 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: {
                    retrieve: true
                },
                urgent: true,
                dialogMission: "G5"
            }
        ]
    },
    {
        groupId: 0,
        index: 13,
        choices: "transport",
        risk: 1,
        customResponses: {
            money: [ "This is an emergency situation." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                dialogMission: "0.7.1",
                urgent: true,
                passengers: [ 1 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: {
                    retrieve: true
                },
                urgent: true,
                dialogMission: "G5"
            }
        ]
    },
    // CARGO
    {
        groupId: 0,
        index: 11,
        choices: "cargo",
        cargoLoadingTask: 0,
        risk: 1,
        customResponses: {
            money: [ "Just the ordinary GalCop payment." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.2"
            }
        ]
    },
    // GalBook Missions -------------------------------------------------------
    // CONTACT
    {
        groupId: 1,
        index: 1,
        choices: "mission",
        parcel: "A copy of '#BOOK#'",
        customResponses: {
            money: [ "These are old books - ancient, I could say." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 1,
                generate: { systems: 1, names: 1, books: 1 }
            }
        ]
    },
    // TRANSFER
    {
        groupId: 1,
        index: 3,
        choices: "mission",
        customResponses: {
            money: [ "These are old books - ancient, I could say." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 2,
                generate: { systems: 1, names: 1, books: 1 },
                parcel: "A copy of '#BOOK#'"
            },
            {
                index: 2,
                typeId: 2,
                generate: { systems: 1, names: 1 }
            }
        ]
    },
    // RETRIEVE
    {
        groupId: 1,
        index: 2,
        choices: "mission",
        customResponses: {
            money: [ "These are old books - ancient, I could say." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 3,
                generate: { systems: 1, names: 1, books: 1 },
                parcel: "A copy of '#BOOK#'"
            },
            {
                index: 2,
                typeId: 3,
                generate: {
                    retrieve: true
                }
            }
        ]
    },
    // CARGO
    {
        groupId: 1,
        index: 4,
        choices: "cargo",
        cargoLoadingTask: 0,
        customResponses: {
            money: [ "We're talking about valuable books." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.2"
            }
        ]
    },
    // GalChurch Missions -----------------------------------------------------
    // CONTACT
    {
        groupId: 2,
        index: 2,
        choices: "mission",
        parcel: "A sacred text",
        customResponses: {
            money: [ "The Church is generous." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 1,
                generate: { systems: 1, names: 1 },
                unaware: true
            }
        ]
    },
    // TRANSFER
    {
        groupId: 2,
        index: 3,
        choices: "mission",
        customResponses: {
            money: [ "The Church is generous." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 2,
                generate: { systems: 1, names: 1 },
                parcel: "Some alien artifacts"
            },
            {
                index: 2,
                typeId: 2,
                generate: { systems: 1, names: 1 }
            }
        ]
    },
    // RETRIEVE
    {
        groupId: 2,
        index: 1,
        choices: "mission",
        customResponses: {
            money: [ "The Church is generous." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 3,
                generate: { systems: 1, names: 1 },
                parcel: "Boxes of religious materials"
            },
            {
                index: 2,
                typeId: 3,
                generate: {
                    retrieve: true
                }
            }
        ]
    },
    // TRANSPORT
    {
        groupId: 2,
        index: 4,
        choices: "transport",
        passengers: [ 1 ],
        firstPassenger: 1,
        customResponses: {
            money: [ "The Church is generous." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                dialogMission: "G6"
            }
        ]
    },
    {
        groupId: 2,
        index: 5,
        choices: "transport",
        passengers: [ 3, 7 ],
        customResponses: {
            money: [ "The Church is generous." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "G6"
            }
        ]
    },
    {
        groupId: 2,
        index: 6,
        choices: "transport",
        customResponses: {
            money: [ "The Church is generous." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                passengers: [ 1 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "G6"
            }
        ]
    },
    {
        groupId: 2,
        index: 7,
        choices: "transport",
        customResponses: {
            money: [ "The Church is generous." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                dialogMission: "2.6.1",
                passengers: [ 1 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "G6"
            }
        ]
    },
    {
        groupId: 2,
        index: 8,
        choices: "transport",
        customResponses: {
            money: [ "The Church is generous." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "2.8.1",
                passengers: [ 3, 7 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "G6"
            }
        ]
    },
    // GalShady Missions ------------------------------------------------------
    // CONTACT
    {
        groupId: 3,
        index: 1,
        choices: "mission",
        parcel: "A unlabeled data drive",
        tasks: [
            {
                index: 1,
                typeId: 1,
                generate: { systems: 1, names: 1 },
                urgent: true
            }
        ]
    },
    // TRANSFER
    {
        groupId: 3,
        index: 3,
        choices: "mission",
        customResponses: {
            money: [ "It's better if we do it fast." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 2,
                generate: { systems: 1, names: 1 },
                parcel: "A strange package",
                urgent: true
            },
            {
                index: 2,
                typeId: 2,
                hideSystemData: true,
                generate: { systems: 1, names: 1 },
                unaware: true
            }
        ]
    },
    // RETRIEVE
    {
        groupId: 3,
        index: 2,
        choices: "mission",
        customResponses: {
            money: [ "I must have that package back." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 3,
                generate: { systems: 1, names: 1 },
                parcel: "A somewhat trivial package",
                unaware: true
            },
            {
                index: 2,
                typeId: 3,
                generate: {
                    retrieve: true
                }
            }
        ]
    },
    // TRANSPORT
    {
        groupId: 3,
        index: 4,
        choices: "transport",
        risk: 2,
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                urgent: true,
                passengers: [ 1 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: { systems: 1 },
                urgent: true,
                dialogMission: "G1"
            }
        ]
    },
    {
        groupId: 3,
        index: 5,
        choices: "transport",
        risk: 1,
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1 },
                urgent: true,
                passengers: [ 3, 7 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: { systems: 1 },
                urgent: true,
                dialogMission: "G1"
            }
        ]
    },
    {
        groupId: 3,
        index: 9,
        choices: "transport",
        risk: 1,
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                passengers: [ 1 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: {
                    retrieve: true
                },
                dialogMission: "G1"
            }
        ]
    },
    // CARGO
    {
        groupId: 3,
        index: 6,
        choices: "cargo",
        cargoLoadingTask: 0,
        risk: 2,
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.2"
            }
        ]
    },
    {
        groupId: 3,
        index: 7,
        choices: "cargo",
        cargoLoadingTask: 1,
        risk: 1,
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.1",
                urgent: true
            },
            {
                index: 1,
                typeId: 5,
                generate: { retrieve: true },
                dialogMission: "G3.2",
                urgent: true
            }
        ]
    },
    {
        groupId: 3,
        index: 8,
        choices: "cargo",
        cargoLoadingTask: 1,
        customResponses: {
            money: [ "Commercial stuff, we can say." ]
        },
        risk: 1,
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.1"
            },
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1 },
                dialogMission: "G3.2"
            }
        ]
    },
    // GalMedical missions ----------------------------------------------------
    // CONTACT
    {
        groupId: 4,
        index: 1,
        choices: "mission",
        parcel: "A holomessage drive",
        customResponses: {
            money: [ "Communication with our teams is essential." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 1,
                generate: { systems: 1, names: 1 },
                unaware: true
            }
        ]
    },
    {
        groupId: 4,
        index: 2,
        choices: "mission",
        parcel: "A holomessage drive",
        customResponses: {
            money: [ "Communication with our teams is essential." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 1,
                generate: { systems: 1, names: 1 },
                unaware: true
            }
        ]
    },
    {
        groupId: 4,
        index: 3,
        choices: "mission",
        customResponses: {
            money: [ "Communication with our teams is essential." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 1,
                generate: { systems: 1, names: 1 },
                dialogMission: "4.1.1",
                unaware: true
            }
        ]
    },
    // TRANSFER
    {
        groupId: 4,
        index: 5,
        choices: "mission",
        customResponses: {
            money: [ "It's an urgent matter." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 2,
                generate: { systems: 1, names: 1 },
                parcel: "Special donor blood bags",
                urgent: true
            },
            {
                index: 2,
                typeId: 2,
                generate: { systems: 1, names: 1 },
                urgent: true
            }
        ]
    },
    // RETRIEVE
    {
        groupId: 4,
        index: 4,
        choices: "mission",
        customResponses: {
            money: [ "It's nothing special, really." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 3,
                generate: { systems: 1, names: 1 },
                parcel: "Boxes full of medical papers"
            },
            {
                index: 2,
                typeId: 3,
                generate: {
                    retrieve: true
                }
            }
        ]
    },
    // TRANSPORT
    {
        groupId: 4,
        index: 6,
        choices: "transport",
        passengers: [ 1 ],
        firstPassenger: 0,
        customResponses: {
            money: [ "It's an urgent matter." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1 },
                urgent: true,
                dialogMission: "G1"
            }
        ]
    },
    {
        groupId: 4,
        index: 7,
        choices: "transport",
        passengers: [ 3, 7 ],
        firstPassenger: 0,
        customResponses: {
            money: [ "The soon we go, the better." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "G1"
            }
        ]
    },
    {
        groupId: 4,
        index: 8,
        choices: "transport",
        passengers: [ 3, 7 ],
        firstPassenger: 0,
        customResponses: {
            money: [ "The soon we go, the better." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "G1"
            }
        ]
    },
    {
        groupId: 4,
        index: 11,
        choices: "transport",
        failureDead: true,
        customResponses: {
            money: [ "It's an emergency." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1, races: 1 },
                urgent: true,
                passengers: [ 1 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: {
                    retrieve: true
                },
                urgent: true
            }
        ]
    },
    {
        groupId: 4,
        index: 12,
        choices: "transport",
        passengers: [ 1 ],
        firstPassenger: 1,
        customResponses: {
            money: [ "It's an emergency." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, races: 1 },
                urgent: true,
                dialogMission: "4.11.2"
            }
        ]
    },
    {
        groupId: 4,
        index: 13,
        choices: "transport",
        customResponses: {
            money: [ "Nothing special, really." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                dialogMission: "G4",
                passengers: [ 1 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: {
                    retrieve: true
                },
                dialogMission: "G1"
            }
        ]
    },
    {
        groupId: 4,
        index: 14,
        choices: "transport",
        customResponses: {
            money: [ "The team has completed its work, and needs to get back home." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "G4",
                passengers: [ 3, 7 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: {
                    retrieve: true
                },
                dialogMission: "G1"
            }
        ]
    },
    {
        groupId: 4,
        index: 15,
        choices: "transport",
        customResponses: {
            money: [ "The team has completed its studies and we need the data." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "G4",
                passengers: [ 3, 7 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: {
                    retrieve: true
                },
                dialogMission: "G1"
            }
        ]
    },
    // CARGO
    {
        groupId: 4,
        index: 9,
        choices: "cargo",
        cargoLoadingTask: 0,
        customResponses: {
            money: [ "People lives depend on this." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.2"
            }
        ]
    },
    {
        groupId: 4,
        index: 10,
        choices: "cargo",
        cargoLoadingTask: 1,
        customResponses: {
            money: [ "Timing is essential." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                urgent: true,
                dialogMission: "G3.1"
            },
            {
                index: 1,
                typeId: 5,
                generate: { retrieve: true },
                urgent: true,
                dialogMission: "G3.2"
            }
        ]
    },
    // GalCivic missions ------------------------------------------------------
    // CONTACT
    {
        groupId: 5,
        index: 1,
        choices: "mission",
        parcel: "A package",
        customResponses: {
            money: [ "[litf_missionResponseMoneyParcel]" ]
        },
        tasks: [
            {
                index: 1,
                typeId: 1,
                generate: { systems: 1, names: 1 }
            }
        ]
    },
    {
        groupId: 5,
        index: 2,
        choices: "mission",
        parcel: "A holomessage player",
        tasks: [
            {
                index: 1,
                typeId: 1,
                generate: { systems: 1, names: 1 },
                unaware: true
            }
        ]
    },
    // TRANSFER

    // RETRIEVE
    {
        groupId: 5,
        index: 3,
        choices: "mission",
        customResponses: {
            money: [ "Sentimental value." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 3,
                generate: { systems: 1, names: 1, items: 1 },
                parcel: "#ITEM#"
            },
            {
                index: 2,
                typeId: 3,
                generate: {
                    retrieve: true
                }
            }
        ]
    },
    // TRANSPORT
    {
        groupId: 5,
        index: 4,
        choices: "transport",
        passengers: [ 1 ],
        firstPassenger: 0,
        customResponses: {
            money: [ "[litf_missionResponseMoneyPassenger]" ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "G1"
            }
        ]
    },
    {
        groupId: 5,
        index: 5,
        choices: "transport",
        passengers: [ 3, 7 ],
        firstPassenger: 0,
        customResponses: {
            money: [ "[litf_missionResponseMoneyGroup]" ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "G1"
            }
        ]
    },
    {
        groupId: 5,
        index: 8,
        choices: "transport",
        risk: 1,
        customResponses: {
            money: [ "I hope this guarantee a safe passage." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                passengers: [ 1 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "G1"
            }
        ]
    },
    {
        groupId: 5,
        index: 9,
        choices: "transport",
        risk: 1,
        customResponses: {
            money: [ "I hope this guarantee a safe passage." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                dialogMission: "5.8.1",
                passengers: [ 1 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: {
                    retrieve: true
                },
                dialogMission: "G1"
            }
        ]
    },
    // CARGO
    {
        groupId: 5,
        index: 6,
        choices: "cargo",
        cargoLoadingTask: 0,
        customResponses: {
            money: [ "Nothing special, really." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.2"
            }
        ]
    },
    {
        groupId: 5,
        index: 7,
        choices: "cargo",
        cargoLoadingTask: 1,
        customResponses: {
            money: [ "I don't want to lose more credits." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.1"
            },
            {
                index: 1,
                typeId: 5,
                generate: { retrieve: true },
                dialogMission: "G3.2"
            }
        ]
    },
    // GalTourism missions ----------------------------------------------------
    // CONTACT
    {
        groupId: 6,
        index: 1,
        choices: "mission",
        parcel: "A folder of documents",
        customResponses: {
            money: [ "We want to attend this matter without wasting other time." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 1,
                generate: { systems: 1, names: 1 },
                unaware: true
            }
        ]
    },
    // TRANSFER
    {
        groupId: 6,
        index: 2,
        choices: "mission",
        customResponses: {
            money: [ "My friend wants to recover his things as soon as possible." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 2,
                generate: { systems: 1, names: 1 },
                parcel: "A bunch of baggages and trolleys",
                unaware: true
            },
            {
                index: 2,
                typeId: 2,
                generate: { systems: 1, names: 1 }
            }
        ]
    },
    // RETRIEVE
    {
        groupId: 6,
        index: 3,
        choices: "mission",
        customResponses: {
            money: [ "I want to recover my things as soon as possible." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 3,
                generate: { systems: 1, names: 1 },
                parcel: "A bunch of baggages and trolleys",
                unaware: true
            },
            {
                index: 2,
                typeId: 3,
                generate: {
                    retrieve: true
                }
            }
        ]
    },
    // TRANSPORT
    {
        groupId: 6,
        index: 4,
        choices: "transport",
        passengers: [ 3, 7 ],
        firstPassenger: 0,
        customResponses: {
            money: [ "[litf_missionResponseMoneyGroup]" ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "G7"
            }
        ]
    },
    // CARGO
    {
        groupId: 6,
        index: 5,
        choices: "cargo",
        cargoLoadingTask: 0,
        customResponses: {
            money: [ "This stuff goes to a DeLuxor hotel." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.2"
            }
        ]
    },
    {
        groupId: 6,
        index: 6,
        choices: "cargo",
        cargoLoadingTask: 1,
        customResponses: {
            money: [ "We need this shipment for a DeLuxor hotel." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.1"
            },
            {
                index: 1,
                typeId: 5,
                generate: { retrieve: true },
                dialogMission: "G3.2"
            }
        ]
    },
    // GalShopping missions ---------------------------------------------------
    // CONTACT

    // TRANSFER

    // RETRIEVE

    // TRANSPORT

    // CARGO
    {
        groupId: 7,
        index: 1,
        choices: "cargo",
        cargoLoadingTask: 0,
        customResponses: {
            money: [ "It's a simple commercial trip." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.2"
            }
        ]
    },
    {
        groupId: 7,
        index: 2,
        choices: "cargo",
        cargoLoadingTask: 1,
        customResponses: {
            money: [ "It's a simple commercial trip." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.1"
            },
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1 },
                dialogMission: "G3.2"
            }
        ]
    },
    {
        groupId: 7,
        index: 3,
        choices: "cargo",
        cargoLoadingTask: 1,
        customResponses: {
            money: [ "It's a simple commercial trip." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.1"
            },
            {
                index: 1,
                typeId: 5,
                generate: { retrieve: true },
                dialogMission: "G3.2"
            }
        ]
    },
    // GalNature missions -----------------------------------------------------
    // CONTACT

    // TRANSFER

    // RETRIEVE
    {
        groupId: 8,
        index: 1,
        choices: "mission",
        customResponses: {
            money: [ "It's an important matter for us." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 3,
                generate: { systems: 1, names: 1 },
                parcel: "A insulated box of plants specimen",
                dialogMission: "8.1.1"
            },
            {
                index: 2,
                typeId: 3,
                generate: {
                    retrieve: true
                },
                dialogMission: "8.1.2"
            }
        ]
    },
    // TRANSPORT
    {
        groupId: 8,
        index: 2,
        choices: "transport",
        passengers: [ 1 ],
        firstPassenger: 0,
        customResponses: {
            money: [ "I'm completing a thesis in [litf_missionResponseMoneyNatureJob]." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                dialogMission: "G1"
            }
        ]
    },
    {
        groupId: 8,
        index: 3,
        choices: "transport",
        customResponses: {
            money: [ "He's a [litf_missionResponseMoneyNatureJob] expert." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                dialogMission: "G2",
                passengers: [ 1 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "G1"
            }
        ]
    },
    {
        groupId: 8,
        index: 4,
        choices: "transport",
        customResponses: {
            money: [ "He's a [litf_missionResponseMoneyNatureJob] scholar." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                dialogMission: "G2",
                passengers: [ 1 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: {
                    retrieve: true
                },
                dialogMission: "G1"
            }
        ]
    },
    // CARGO
    {
        groupId: 8,
        index: 5,
        choices: "cargo",
        cargoLoadingTask: 0,
        customResponses: {
            money: [ "Normal scientific stuff." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.2"
            }
        ]
    },
    {
        groupId: 8,
        index: 6,
        choices: "cargo",
        cargoLoadingTask: 1,
        customResponses: {
            money: [ "Normal scientific stuff." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.1"
            },
            {
                index: 1,
                typeId: 5,
                generate: { retrieve: true },
                dialogMission: "G3.2"
            }
        ]
    },
    // GalCulture missions ----------------------------------------------------
    // CONTACT
    {
        groupId: 9,
        index: 1,
        choices: "mission",
        parcel: "The '#PAINTING# painting",
        customResponses: {
            money: [ "It's a really good painting." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 1,
                generate: { systems: 1, names: 1, paintings: 1 }
            }
        ]
    },
    // TRANSFER
    {
        groupId: 9,
        index: 2,
        choices: "mission",
        customResponses: {
            money: [ "It's a really good painting." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 2,
                generate: { systems: 1, names: 1, paintings: 1 },
                parcel: "The '#PAINTING# painting"
            },
            {
                index: 2,
                typeId: 2,
                generate: { systems: 1, names: 1 }
            }
        ]
    },
    // RETRIEVE
    {
        groupId: 9,
        index: 3,
        choices: "mission",
        customResponses: {
            money: [ "It's a really good painting." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 3,
                generate: { systems: 1, names: 1, paintings: 1 },
                parcel: "The '#PAINTING# painting",
                unaware: true
            },
            {
                index: 2,
                typeId: 3,
                generate: {
                    retrieve: true
                }
            }
        ]
    },
    // TRANSPORT
    {
        groupId: 9,
        index: 4,
        choices: "transport",
        passengers: [ 1 ],
        firstPassenger: 1,
        customResponses: {
            money: [ "I'm [litf_missionResponseMoneyArtist]" ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                dialogMission: "G1"
            }
        ]
    },
    {
        groupId: 9,
        index: 5,
        choices: "transport",
        customResponses: {
            money: [ "He's [litf_missionResponseMoneyArtist]." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                dialogMission: "G2",
                passengers: [ 1 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: { systems: 1 },
                dialogMission: "G1"
            }
        ]
    },
    // CARGO
    {
        groupId: 9,
        index: 6,
        choices: "cargo",
        cargoLoadingTask: 0,
        customResponses: {
            money: [ "Nothing special, really." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.2"
            }
        ]
    },
    {
        groupId: 9,
        index: 7,
        choices: "cargo",
        cargoLoadingTask: 1,
        customResponses: {
            money: [ "We need the equipment." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.1"
            },
            {
                index: 1,
                typeId: 5,
                generate: { retrieve: true },
                dialogMission: "G3.2"
            }
        ]
    },

    // GalGovernment missions -------------------------------------------------
    // CONTACT
    {
        groupId: 10,
        index: 1,
        choices: "mission",
        parcel: "An encrypted data drive",
        customResponses: {
            money: [ "It's the regulatory government pay." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 1,
                generate: { systems: 1, names: 1 },
                unaware: true
            }
        ]
    },
    // TRANSFER
    {
        groupId: 10,
        index: 3,
        choices: "mission",
        customResponses: {
            money: [ "It's the regulatory government pay." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 2,
                generate: { systems: 1, names: 1 },
                parcel: "A folder of diplomatic papers"
            },
            {
                index: 2,
                typeId: 2,
                generate: { systems: 1, names: 1 }
            }
        ]
    },
    // RETRIEVE
    {
        groupId: 10,
        index: 2,
        choices: "mission",
        customResponses: {
            money: [ "No questions." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 3,
                generate: { systems: 1, names: 1 },
                parcel: "A box full of government dossiers",
                unaware: true
            },
            {
                index: 2,
                typeId: 3,
                generate: {
                    retrieve: true
                }
            }
        ]
    },
    // TRANSPORT
    {
        groupId: 10,
        index: 4,
        choices: "transport",
        passengers: [ 3, 7 ],
        firstPassenger: 1,
        risk: 1,
        customResponses: {
            money: [ "No questions." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                dialogMission: "G1"
            }
        ]
    },
    {
        groupId: 10,
        index: 5,
        choices: "transport",
        passengers: [ 1 ],
        firstPassenger: 1,
        risk: 1,
        customResponses: {
            money: [ "No questions." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                dialogMission: "G1"
            }
        ]
    },
    {
        groupId: 10,
        index: 8,
        choices: "transport",
        risk: 1,
        customResponses: {
            money: [ "No questions." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1 },
                passengers: [ 3, 7 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: {
                    retrieve: true
                },
                dialogMission: "G1"
            }
        ]
    },
    {
        groupId: 10,
        index: 9,
        choices: "transport",
        risk: 1,
        customResponses: {
            money: [ "No questions." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 4,
                generate: { systems: 1, names: 1 },
                passengers: [ 1 ]
            },
            {
                index: 2,
                typeId: 4,
                generate: {
                    retrieve: true
                },
                dialogMission: "G1"
            }
        ]
    },
    // CARGO
    {
        groupId: 10,
        index: 6,
        choices: "cargo",
        cargoLoadingTask: 0,
        customResponses: {
            money: [ "No questions." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.2"
            }
        ]
    },
    {
        groupId: 10,
        index: 7,
        choices: "cargo",
        cargoLoadingTask: 1,
        customResponses: {
            money: [ "No questions." ]
        },
        tasks: [
            {
                index: 1,
                typeId: 5,
                generate: { systems: 1, goods: 1 },
                dialogMission: "G3.1"
            },
            {
                index: 1,
                typeId: 5,
                generate: { retrieve: true },
                dialogMission: "G3.2"
            }
        ]
    }
];

// ----------------------------------------------------------------------------

// Dialog messages - BBS items

this.dialogStore = [
    { groupId: 99, index: 0, title: "LOOK HERE: #NAME1#! I'm awaiting your arrival!", description: "Hello, I'm sorry but you're not the person I'm waiting for. Goodbye.", choices: "hangup" },
    { groupId: 99, index: 0, title: "NOTICE: Please #NAME1# contact me ASAP for completing your work.", description: "Hello, I'm sorry but you're not the person I'm waiting for. Goodbye.", choices: "hangup" },
    { groupId: 99, index: 0, title: "HEY COMMANDER: Are you #NAME1#? I'm awaiting for you!", description: "Hello, I'm sorry but you're not the person I'm waiting for. Goodbye.", choices: "hangup" },
    { groupId: 99, index: 0, title: "ANSWER ME: #NAME1#.", description: "Hello, I'm sorry but you're not the person I'm waiting for. Goodbye.", choices: "hangup" },
    { groupId: 99, index: 0, title: "WAITING: I must to talk to #NAME1#.", description: "Hello, I'm sorry but you're not the person I'm waiting for. Goodbye.", choices: "hangup" },
    { groupId: 99, index: 0, title: "NOTICE: XXXXX Read here #NAME1# XXXXX.", description: "Hello, I'm sorry but you're not the person I'm waiting for. Goodbye.", choices: "hangup" },
    { groupId: 99, index: 0, title: "HERE: I'm waiting for #NAME1#. Where are you?", description: "Hello, I'm sorry but you're not the person I'm waiting for. Goodbye.", choices: "hangup" },
    { groupId: 99, index: 0, title: "HURRY: #NAME1#. Time is running out.", description: "Hello, I'm sorry but you're not the person I'm waiting for. Goodbye.", choices: "hangup" }
];

// Charities - BBS Items

this.charityStore = [
    { groupId: 98, index: 0, title: "FEELING GENEROUS? '#CHARITY#' is here for your unwanted money!", description: "Please select an amount to give to '#CHARITY#'.", showCredits: true, choices: "charity" },
    { groupId: 98, index: 0, title: "GIVE MONEY: Make a good action. Support '#CHARITY#' now!", description: "Please select an amount to give to '#CHARITY#'.", showCredits: true, choices: "charity" },
    { groupId: 98, index: 0, title: "DONATE: '#CHARITY#'. We accept your money. Thanks.", description: "Please select an amount to give to '#CHARITY#'.", showCredits: true, choices: "charity" },
    { groupId: 98, index: 0, title: "COMPASSION: Give something to '#CHARITY#'.", description: "Please select an amount to give to '#CHARITY#'.", showCredits: true, choices: "charity" },
    { groupId: 98, index: 0, title: "SINCERITY: '#CHARITY#'. We need money.", description: "Please select an amount to give to '#CHARITY#'.", showCredits: true, choices: "charity" }
];

// Ads - BBS Items

this.adStore = [
    // 101 - Generic messages and spam
    { groupId: 101, index: 0, title: "NEED HOME: I'm searching for a low-cost apartment. Contact me. Rent only.", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    { groupId: 101, index: 0, title: "FREE ROOM: Want to share my room with another person - expenses too.", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    { groupId: 101, index: 0, title: "CURIOUS: Do you want to see a real Thargoid? I can help you with that.", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    { groupId: 101, index: 0, title: "FORGIVE ME: #NAME1#, are you reading this? Please, please answer me!", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    { groupId: 101, index: 0, title: "LONELY? Do you want to share a bottle of #LIQUOR#? I'm here if you want.", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    { groupId: 101, index: 0, title: "HELLO: Hello to everybody! Have a good day!", description: "" },
    { groupId: 101, index: 0, title: "BREATHE: It's important. Don't underestimate gases.", description: "" },
    { groupId: 101, index: 0, title: "HELP: Stasis field. My friend is inside. How to turn off?", description: "" },
    { groupId: 101, index: 0, title: "WARNING: USERS WITH FAKE NAMES WILL BE BANNED FOREVER!", description: "" },
    { groupId: 101, index: 0, title: "IMPORTANT: If you are a time traveler or have the technology I need your help!", description: "" },
    { groupId: 101, index: 0, title: "IT WORKS: Write this message 10 times or you'll get bad luck in 10 days.", description: "" },
    { groupId: 101, index: 0, title: "HINT: How #NAME0# Saved 3000cr Using This Simple Trick", description: "" },
    { groupId: 101, index: 0, title: "WARNING: Look for a doctor if you feel a sudden loneliness feelings or vertigo.", description: "" },
    { groupId: 101, index: 0, title: "JUNK: There should be some cleaning up there.", description: "" },
    { groupId: 101, index: 0, title: "PLEASE: Close the windows. Black space is depressing.", description: "" },
    { groupId: 101, index: 0, title: "PETITION: Mandatory Grav-0 courses for everyone.", description: "" },
    // 102 - Items messages
    { groupId: 102, index: 0, title: "FOR SALE: I want to give away #ITEM#. Come and pick up.", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    { groupId: 102, index: 0, title: "NEED HELP: Someone has seen #ITEM# around here?", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    { groupId: 102, index: 0, title: "WANTED: Searching for #ITEM#. Contact me if you have info.", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    { groupId: 102, index: 0, title: "LOST ITEM: Need replacement for #ITEM#. Can you help me?", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    { groupId: 102, index: 0, title: "SELLING: Interested in #ITEM#. Contact me.", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    { groupId: 102, index: 0, title: "THEFT: Someone stole #ITEM# from my apartment.", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    // 103 - Job messages
    { groupId: 103, index: 0, title: "NEED WORK? Open offer for #JOB#. Good position, decent pay. Please contact me.", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    { groupId: 103, index: 0, title: "AVAILABLE JOB: We have a free position for #JOB#. Are you interested?", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    { groupId: 103, index: 0, title: "NOW HIRING: Unexpected events force us to search for #JOB#. Contact us if you're available.", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    { groupId: 103, index: 0, title: "JOB HERE: Awesome position for #JOB#. Don't miss this opportunity!", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    { groupId: 103, index: 0, title: "AVAILABLE: I'm #JOB#. Currently unemployed, searching for small jobs. Contact me.", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    { groupId: 103, index: 0, title: "JOB WANTED: Need job to feed disabled spouse and starving family.", description: "" },
    { groupId: 103, index: 0, title: "UNEMPLOYED CREW: Need work on a starship. Anything considered.", description: "" },
    { groupId: 103, index: 0, title: "NEED WORK: Recently fired for external causes. Looking for any job available.", description: "" },
    { groupId: 103, index: 0, title: "VACANCY REQUIRED: as a crew member on a starship.", description: "" },
    { groupId: 103, index: 0, title: "WANTED: Position on a reputable starship.", description: "" },
    // 104 - Racial messages
    { groupId: 104, index: 0, title: "EVOLUTION: It's time for the #RACE# to change diet?", description: "" },
    { groupId: 104, index: 0, title: "QUESTION: Isn't a #RACE# entitled to the sweat of its brow?", description: "" },
    { groupId: 104, index: 0, title: "CONTEST: Vote the most beautiful #RACE# of the system!!", description: "" },
    { groupId: 104, index: 0, title: "DANGER: Birth race in decline for #RACE#?", description: "" },
    { groupId: 104, index: 0, title: "POLL: What people think when talking about #RACE#?", description: "" },
    { groupId: 104, index: 0, title: "DISCOVER: 42 Life-Changing Style Tips Every #RACE# Should Know", description: "" },
    { groupId: 104, index: 0, title: "INCREDIBLE: This #RACE# will change your life.", description: "" },
    { groupId: 104, index: 0, title: "CONSIDER: Not every #RACE# has the same opinion.", description: "" },
    { groupId: 104, index: 0, title: "DISCOVER: The truth behind the success of this #RACE#.", description: "" }
];

// Government Specific BBS Items

// 0 - Anarchy, 1 - Feudal, 2 - Multigovernmental, 3 - Dictatorship, 4 - Communist, 5 - Confederacy, 6 - Democracy, 7 - Corporate

this.governmentStore = [
    // Anarchy Specifics
    { groupId: 105, gov: [ 0 ], index: 0, title: "R3M3MB3R: Property is theft!!!", description: "" },
    { groupId: 105, gov: [ 0 ], index: 0, title: "W4R: State bigger health system.", description: "" },
    { groupId: 105, gov: [ 0 ], index: 0, title: "V0T3: Bigger cages! Longer chains!", description: "" },
    { groupId: 105, gov: [ 0 ], index: 0, title: "TH1NK: We are the crisis!", description: "" },
    { groupId: 105, gov: [ 0 ], index: 0, title: "P30PL3: N07 PR0F175!!!", description: "" },
    { groupId: 105, gov: [ 0 ], index: 0, title: "TH3M: Whom the system work for???", description: "" },
    { groupId: 105, gov: [ 0 ], index: 0, title: "****: G4LC0P!", description: "" },
    { groupId: 105, gov: [ 0 ], index: 0, title: "DEBATE: Someone interested - not screaming slogans?", description: "" },
    { groupId: 105, gov: [ 0 ], index: 0, title: "PROPOSAL: Organize something. Use your imagination.", description: "" },
    { groupId: 105, gov: [ 0 ], index: 0, title: "WAHT: Who enabled strong language censorship HERE? **** ****!!!", description: "" },
    { groupId: 105, gov: [ 0 ], index: 0, title: "HINT: How to cope with GalCop officials.", description: "" },
    { groupId: 105, gov: [ 0 ], index: 0, title: "GUIDE: Ten ways to avoid uncomfortable space encounters.", description: "" },
    { groupId: 105, gov: [ 0 ], index: 0, title: "OPEN: Alternative botanical growing laboratory course.", description: "" },
    { groupId: 105, gov: [ 0 ], index: 0, title: "MARKET: Updated listing for unwanted merchandising.", description: "" },
    { groupId: 105, gov: [ 0 ], index: 0, title: "SOCIAL: Learning to lie in a believable way.", description: "" },
    { groupId: 105, gov: [ 0 ], index: 0, title: "HELP: A friend of mine has been arrested.", description: "" },
    { groupId: 105, gov: [ 0 ], index: 0, title: "NET: New and interesting contacts. Free and open.", description: "" },
    { groupId: 105, gov: [ 0 ], index: 0, title: "DARK-BBS: It doesn't exists.", description: "" },
    // Feudal Specifics
    { groupId: 105, gov: [ 1 ], index: 0, title: "EMPIRE: Your effort grows our strength.", description: "" },
    { groupId: 105, gov: [ 1 ], index: 0, title: "HAIL: For the glory of the King!", description: "" },
    { groupId: 105, gov: [ 1 ], index: 0, title: "LORD: Blessed are the meeks.", description: "" },
    { groupId: 105, gov: [ 1 ], index: 0, title: "HERESY: Pagan rituals are BANNED from the Empire!", description: "" },
    { groupId: 105, gov: [ 1 ], index: 0, title: "TAXES: Remember the dates, pay your dues.", description: "" },
    { groupId: 105, gov: [ 1 ], index: 0, title: "KNIGHTS: No more invaders in OUR sacred lands!", description: "" },
    { groupId: 105, gov: [ 1 ], index: 0, title: "PEASANT: You work the land, we protect the land.", description: "" },
    { groupId: 105, gov: [ 1 ], index: 0, title: "TOURNAMENT: Join the festivities and celebrate our englightened monarch.", description: "" },
    // Dictatorship Specifics
    { groupId: 105, gov: [ 3 ], index: 0, title: "WAKE UP: End corruption and immorality.", description: "" },
    { groupId: 105, gov: [ 3 ], index: 0, title: "TRAITORS: They don't love our system!", description: "" },
    { groupId: 105, gov: [ 3 ], index: 0, title: "YOUTH: Join the Revolutionary Forces.", description: "" },
    { groupId: 105, gov: [ 3 ], index: 0, title: "TRUST: Have faith in your government officials!", description: "" },
    { groupId: 105, gov: [ 3 ], index: 0, title: "THINK: What can you do for your system?", description: "" },
    { groupId: 105, gov: [ 3 ], index: 0, title: "WORK: Give a future to your children.", description: "" },
    { groupId: 105, gov: [ 3 ], index: 0, title: "ENEMIES: They could be among us.", description: "" },
    { groupId: 105, gov: [ 3 ], index: 0, title: "PRIDE: You live in the cradle of civilization!", description: "" },
    { groupId: 105, gov: [ 3 ], index: 0, title: "LOOKING FOR: Evicted from communal sector. Searching arrangement.", description: "" },
    { groupId: 105, gov: [ 3 ], index: 0, title: "MEETING: A civil debate about minor local issues.", description: "" },
    { groupId: 105, gov: [ 3 ], index: 0, title: "RAID: Last night. Someone heard something?", description: "" },
    { groupId: 105, gov: [ 3 ], index: 0, title: "MISSING: I can't find a person anymore.", description: "" },
    { groupId: 105, gov: [ 3 ], index: 0, title: "CONFORM: There's no need to dissent.", description: "" },
    { groupId: 105, gov: [ 3 ], index: 0, title: "MISSING: Someone has seen...", description: "" },
    { groupId: 105, gov: [ 3 ], index: 0, title: "NO PROBLEM: They say. You're happy and it's all okay.", description: "" },
    // Communist Specifics
    { groupId: 105, gov: [ 4 ], index: 0, title: "KOMM: To have more we have to produce more!", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "KOMM: For the Industrial Plan!", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "KOMM: Long live to the Party!", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "KOMM: All power to the Party. Peace to the People. Land to the peasants.", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "KOMM: With great labor we will fulfill the plan.", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "KOMM: All workers choose the Party!", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "KOMM: We grow under the sun of our system.", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "KOMM: Follow the true path, comrades!", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "KOMM: Come to us on the collective farm, comrade!", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "KOMM: Proletarians of all countries, unite!", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "KOMM: Have you signed up with the volunteers?", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "WORK: Reaching the monthly production level.", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "WORK: Are we allowed to start a strike?", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "WORK: Who control the industry?", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "WORK: Requiring new machinery - existing one break too often.", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "SOCIAL: Weekly political debates.", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "SOCIAL: Capitalism is bad. You should know.", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "SOCIAL: How much space for innovation?", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "SOCIAL: Discussion on global economic models.", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "POLICE: I heard a lot of noise in the near apartments...", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "MISSING: I can't find a person anymore.", description: "" },
    { groupId: 105, gov: [ 4 ], index: 0, title: "MISSING: Someone has seen...", description: "" },
    // Corporate Specifics
    { groupId: 105, gov: [ 7 ], index: 0, title: "AD: You should do it now!", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "AD: Imagine something original!", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "AD: Buy it, you deserve it!", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "AD: There aren't things credits can't buy!", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "AD: All is designed for your comfort!", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "AD: Tastes so good! You can't live without!", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "AD: Have a break! Do shopping!", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "AD: Buy, break, replace!", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "AD: Buy it, you need it!", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "AD: You're never spending enough!", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "AD: BUY, CONSUME, REPEAT.", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "AD: Work until expensive become cheap!", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "HELP: Credit expired on my credit card. What can I do?", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "POLL: The best debit card on the market today.", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "ASKING: How to do more overtime without risking your life.", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "THERAPY: I cannot stop working. Too stressed.", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "MEDS: Ten products to ease your psychological discomfort.", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "MARKET: Global Funds Daily Updates.", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "INVESTMENT: What is better? Long term.", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "POLL: Can We Guess Your Favourite Product Based On Random Questions?", description: "" },
    // Various
    { groupId: 105, gov: [ 0, 1, 2, 3, 4, 5, 6, 7 ], index: 0, title: "SEARCHING: Ask the Station Pop-Registry for info about a resident.", description: "We're sorry but at the moment we can't process public queries.", choices: "hangup" },
    // { groupId: 105, gov: [ 2, 5, 6, 7 ], index: 0, title: "SEARCHING: Contact the Missing People Data System to report about findings.", description: "" },
    { groupId: 105, gov: [ 0, 2, 5, 6, 7 ], index: 0, title: "ANSWER NOW: You have just won 999999 credits!!! Talk to me to get your money!!!", description: "" },
    { groupId: 105, gov: [ 0, 2, 5, 6, 7 ], index: 0, title: "IMPORTANT: You could be entitled up to 15340 credits in compensation.", description: "" },
    { groupId: 105, gov: [ 2, 5, 6, 7 ], index: 0, title: "SPACEPORT: Baggage reclamation office open for service.", description: "" },
    { groupId: 105, gov: [ 2, 5, 6, 7 ], index: 0, title: "DELUXOR HOTELS: Exaggerate luxury for a deluxe holiday!", description: "" },
    { groupId: 105, gov: [ 2, 5, 6, 7 ], index: 0, title: "HOSPITAL: Donor blood needed. Do your part.", description: "" },
    { groupId: 105, gov: [ 2, 5, 6 ], index: 0, title: "ART GALLERY: We are closed, but stay updated for our next events!", description: "" },
    { groupId: 105, gov: [ 0, 6, 7 ], index: 0, title: "CITIZEN: Do you trust your government?", description: "" },
    { groupId: 105, gov: [ 1, 2, 5, 7 ], index: 0, title: "PETITION: Ban Low Quality #GOODS# - they're hurting the economy!", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    { groupId: 105, gov: [ 1, 2, 5 ], index: 0, title: "CONCERNED: Don't trust every #GOODS# Vendor. Stay alert.", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    { groupId: 105, gov: [ 1, 2, 5, 7 ], index: 0, title: "PROTEST: Lower taxes for #GOODS#!", description: "Hi, I'm #NAME0#. I'm sorry but this ad is reserved to residents only.", choices: "hangup" },
    { groupId: 105, gov: [ 2, 5, 6, 7 ], index: 0, title: "SOON: The new #MOVIE# movie from the acclaimed director #NAME1#.", description: "Good day. If you're interested in this event, we suggest to ask for more information at the local cultural center. Thanks for your attention.", choices: "hangup" },
    { groupId: 105, gov: [ 2, 5, 6, 7 ], index: 0, title: "IN STORES NOW: The last masterpiece in #MUSIC# by #NAME1#.", description: "Good day. If you're interested in this event, we suggest to ask for more information at the local cultural center. Thanks for your attention.", choices: "hangup" },
    { groupId: 105, gov: [ 2, 5, 6 ], index: 0, title: "ADMIRE: The last collection of #ART# paintings by #NAME1#, now in tour across the galaxy.", description: "Good day. If you're interested in this event, we suggest to ask for more information at the local cultural center. Thanks for your attention.", choices: "hangup" },
    { groupId: 105, gov: [ 0, 2, 3, 4, 5, 6 ], index: 0, title: "ENJOY: Come at the bar, and try our #LIQUOR#! Unforgettable experiences guaranteed!", description: "" },
    { groupId: 105, gov: [ 0, 2, 3, 4, 5, 6 ], index: 0, title: "THE BEST: We have the best #LIQUOR# in the system. Why don't you try it?", description: "" },
    { groupId: 105, gov: [ 2, 5, 6, 7 ], index: 0, title: "COOPER CAFE': Come and taste: we serve a damn good coffee!", description: "" },
    { groupId: 105, gov: [ 6, 7 ], index: 0, title: "WEYLAND-YUTANI CORPORATION: Building better worlds.", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "ARES MACROTECHNOLGY: Making the world a safer place.", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "RENRAKU: Today's solutions to tomorrow's problems.", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "WUXING: We're behind everything you do.", description: "" },
    { groupId: 105, gov: [ 5, 6, 7 ], index: 0, title: "OMNICORP: We got the future under control.", description: "" },
    { groupId: 105, gov: [ 5, 6, 7 ], index: 0, title: "TYRELL CORP: More Human Than Human.", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "AURITECH SURVEILLANCE: We're keeping an eye out, for you.", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "WORLDCOM: For the Greater Good.", description: "" },
    { groupId: 105, gov: [ 2, 5, 6, 7 ], index: 0, title: "CENTRAL SERVICES: We do the work! You do the pleasure.", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "RENAUTAS: Doing good is good business.", description: "" },
    { groupId: 105, gov: [ 5, 6, 7 ], index: 0, title: "CYBERDYNE SYSTEMS: We are the future.", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "YOYODINE: The future begins tomorrow.", description: "" },
    { groupId: 105, gov: [ 2, 5, 6, 7 ], index: 0, title: "SOYLENT CORPORATION: Today is Soylent Green day!", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "MULTI-NATIONAL UNITED: Paving the way to unity.", description: "" },
    { groupId: 105, gov: [ 2, 5, 6, 7 ], index: 0, title: "ECORP: Together we can change the world.", description: "" },
    { groupId: 105, gov: [ 7 ], index: 0, title: "MASSIVE DYNAMIC: What do we do? What don't we do.", description: "" }
]

this.scriptChoices = {
    "DONATE01": { index: 10, text: "Donate 0.1 credits.", action: "ret donate 0.1" },
    "DONATE02": { index: 20, text: "Donate 1 credits.", action: "ret donate 1" },
    "DONATE03": { index: 30, text: "Donate 10 credits.", action: "ret donate 10" },
    "DONATE04": { index: 40, text: "Donate 100 credits.", action: "ret donate 100" },
    "DONATE05": { index: 50, text: "Donate 1000 credits.", action: "ret donate 1000" },
    "DONATE06": { index: 60, text: "Donate 10000 credits.", action: "ret donate 10000" },
    "OK": { index: 10, text: "Ok - agreed.", action: "ret missionstart" },
    "STARCHART": { index: 15, text: "Examine the Star Chart Map.", action: "ret openstarchart" },
    "HOWMANY": { index: 20, text: "How many of you are there?", action: "ret resp howmany" },
    "MONEY": { index: 30, text: "Why so much money?", action: "ret resp money" },
    "PROBLEMS": { index: 40, text: "Will there be any problems?", action: "ret resp problems" },
    "UNAWARE": { index: 50, text: "Do the recipient will know of my arrival?", action: "ret resp unaware" },
    "MOREMONEY": { index: 60, text: "I want more money.", action: "ret resp moremoney" },
    "HALFADVANCE": { index: 70, text: "I want half the money now.", action: "ret resp halfadvance" },
    "FULLADVANCE": { index: 80, text: "I want all the money now.", action: "ret resp fulladvance" },
    "QUIT": { index: 99, text: "Hang up.", action: "quit" }
};
