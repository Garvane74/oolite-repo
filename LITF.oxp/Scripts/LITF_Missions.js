/* eslint-disable semi, no-multi-spaces, quotes, indent, no-tabs, yoda */
/* global worldScripts, randomName, system, player, expandDescription, randomInhabitantsDescription, expandMissionText */

"use strict";

this.name        = "LITF_Missions";
this.author      = "BeeTLe BeTHLeHeM";
this.copyright   = "2018 BeeTLe BeTHLeHeM";
this.description = "Mission generation methods";
this.version     = "0.10.0";
this.licence     = "CC BY-NC-SA 4.0";

// Active groups index, by system government type

this.groupsByGov = [
    [3, 5], // Anarchy
    [2, 3, 5], // Feudal
    [0, 1, 2, 4, 5, 6, 7, 8, 9], // Multigovernmental
    [2, 5, 10], // Dictatorship
    [5, 10], // Communist
    [0, 1, 2, 4, 5, 6, 7, 8, 9], // Confederacy
    [0, 1, 2, 4, 5, 6, 7, 8, 9], // Democracy
    [3, 4, 6, 7] // Corporate
];

var _litf = null;
var _co   = null;
var _md   = null;
var _bbs  = null;
var _sm   = null;
var _pa   = null;

this.$init = function () {
    _litf = worldScripts.LITF;

    _co   = _litf.LITFInstances.co;
    _md   = _litf.LITFInstances.md;
    _bbs  = _litf.LITFInstances.bbs;
    _sm   = _litf.LITFInstances.sm;
    _pa   = _litf.LITFInstances.pa;
}

// Main method
this.$generateMission = function (activeGroupsArr, debugMissionIdx) {
    _co.$logInfo(this.name, ">>> generateMission");

    var storeItem = null; // Store object
    var mObj      = null; // Mission object
    var passArr   = null; // Passengers array

    if (activeGroupsArr[0] === "debug") {
        // Get debug mission object
        for (var m = 0; m < _md.debugStore.length; m++) {
            storeItem = _md.debugStore[m];
            if (storeItem.index === debugMissionIdx) {
                _co.$logDebug(this.name, "Clone store object");
                mObj = JSON.parse(JSON.stringify(storeItem));
                break;
            }
        }
    } else {
        // Get mission object

        var counter = 0;
        while (counter < 50) {
            counter++;

            var mIdx = _co.$roll(0, _md.missionStore.length - 1);
            storeItem = _md.missionStore[mIdx];

            if (activeGroupsArr.indexOf(storeItem.groupId) > -1) {
                _co.$logDebug(this.name, "Clone store object");
                mObj = JSON.parse(JSON.stringify(storeItem));
                break;
            }
        }

        if (mObj === null) {
            return null;
        }
    }

    // Define mission details
    _co.$logDebug(this.name, "Define mission details :: mObj = " + JSON.stringify(mObj));

    var pFeedback = _litf.litfCommander.feedback;
    var maxSystemDistance = 10 + (Math.abs(pFeedback) / 5);

    mObj.missionId      = _co.$getGeneralId(); // Unique id
    mObj.contractor     = randomName() + " " + randomName();
    mObj.startingSystem = { index: 0, id: system.ID, name: System.systemNameForID(system.ID), distance: 0 };
    mObj.rewardAdvanceObtained = null; // Player has obtained a reward advance
    mObj.moreMoneyObtained     = false; // Player has obtained a reward increase

    // Face
    mObj.faceBackground = this.$getFaceBackground(mObj);

    // Defaults
    if (!mObj.local) {  mObj.local = false; }
    if (!mObj.showCredits) { mObj.showCredits = false; }
    if (!mObj.noReward) { mObj.noReward = false; }
    if (!mObj.failureDead) { mObj.failureDead = false; }
    if (!mObj.currentTask) { mObj.currentTask = 1; }

    // Feedback checks
    _co.$logDebug(this.name, "Feedback checks");

    if (mObj.groupId === 3) {
        // GalShady
        var maxFeedback = mObj.maxFeedback ? mObj.maxFeedback : this.maxFeedbackDefault;
        if (pFeedback > maxFeedback) {
            mObj.description = "You smell too clean for this. Return when your hands will be dirtier.";
            mObj.choices = "hangup";
        }
    } else {
        // Not GalShady
        var minFeedback = mObj.minFeedback ? mObj.minFeedback : this.minFeedbackDefault;
        if (pFeedback < minFeedback) {
            mObj.description = "Hello, thanks for contacting us, but we want someone with a higher feedback rating.";
            mObj.choices = "hangup";
        }
    }

    // Define number of passengers (at the start of the mission, if any)
    if (mObj.passengers) {
        passArr = mObj.passengers;
        if (passArr.length === 1) {
            mObj.passengers = passArr[0]; // Fixed number
        } else if (passArr.length === 2) {
            mObj.passengers = _co.$roll(passArr[0], passArr[1]); // Random number
        }
    }

    // Define tasks

    var ta = 0;
    var task = {};

    var maxRisk = 0;

    if (mObj.risk) { maxRisk = mObj.risk; }

    var generatedSystems = [];

    for (ta = 0; ta < mObj.tasks.length; ta++) {
        task = mObj.tasks[ta];

        // Defaults
        if (!task.unaware) { task.unaware = false; }
        if (!task.urgent) { task.urgent = false; }
        // if (!task.risk) { task.risk = 0; }

        // Define number of passengers (during a task)
        if (task.passengers) {
            passArr = task.passengers;
            if (passArr.length === 1) {
                task.passengers = passArr[0]; // Fixed number
            } else if (passArr.length === 2) {
                task.passengers = _co.$roll(passArr[0], passArr[1]); // Random number
            }
        }

        // Procedural generation
        var generate = task.generate;
        task.info = this.$generateInfo(generate, mObj.groupId, maxSystemDistance, generatedSystems);

        // if (task.info.systems === null) {
        //     _co.$logDebug(this.name, "-- WARNING -- Error generating random systems! Aborting mission generation!");
        //     return null;
        // } else {
            for (var s = 0; s < task.info.systems.length; s++) {
                generatedSystems.push(task.info.systems[s]);
            }

            // Special generation properties
            if (generate.retrieve && generate.retrieve === true) {
                task.info.systems.push(mObj.startingSystem);
                task.info.names.push(mObj.contractor);
            } else if (generate.search && generate.search === true) {
                // task.info.systems.push(_co.$randomSystem(-1));
                task.info.names.push(randomName() + " " + randomName());
                _co.$logDebug(this.name, "generate.search");
            }
        // }
    }

    if (mObj.tasks[0].typeId === 6) {
        // SEARCH mission

        _co.$logDebug(this.name, "missing person name = " + mObj.tasks[0].info.names[0]);

        // Define missing persons travel
        mObj.missing = _sm.$defineMissingPersonData(mObj.tasks[0].info.names[0]);

        mObj.description = mObj.description.replace("#PROTOCOLCODE#", mObj.missing.protocolCode);

        _co.$logDebug(this.name, "missing = " + JSON.stringify(mObj.missing));

        // Minimum reward
        mObj.reward = 500;

        // No dangers
        mObj.risk = 0;
    } else {
        // Time for completing the mission
        mObj.timeAllowed = this.$missionAllocatedTime(mObj);
        if (mObj.timeAllowed === 0) {
            _co.$logDebug(this.name, "-- WARNING -- Error generating mission deadline! Aborting mission generation!");
            return null;
        }

        // Mission reward
        mObj.reward = this.$missionReward(mObj);

        // Tweak mission risk factor is payment is high
        if (mObj.reward > 1000) {
            if (!mObj.risk || mObj.risk < 1) {
                mObj.risk = 1;
            }
        } else if (mObj.reward > 3000) {
            if (!mObj.risk || mObj.risk < 2) {
                mObj.risk = 2;
            }
        }

        if (maxRisk < mObj.risk) { maxRisk = mObj.risk; }
    }

    // Responses
    _co.$logDebug(this.name, "Responses");

    mObj.responses = {};
    mObj.responses.howmany = "There is only me here - what do you mean?";
    mObj.responses.money = "Sorry, that's none of your business.";
    mObj.responses.problems = "No.";

    // Custom responses
    _co.$logDebug(this.name, "Custom responses");

    var crIdx = 0;
    if (mObj.customResponses) {
        _co.$logDebug(this.name, "customResponses esiste");
        if (mObj.customResponses.money) {
            var money = mObj.customResponses.money;

            if (money[0].indexOf("[") === 0) {
                mObj.responses.money = expandDescription(money[0]);
            } else {
                crIdx = _co.$roll(0, money.length - 1);
                mObj.responses.money = money[crIdx];
            }
            _co.$logDebug(this.name, "responses.money = " + mObj.responses.money);
        } else if (mObj.customResponses.problems) {
            var problems = mObj.customResponses.problems;

            if (problems[0].indexOf("[") === 0) {
                mObj.responses.problems = expandDescription(problems[0]);
            } else {
                crIdx = _co.$roll(0, problems.length - 1);
                mObj.responses.problems = problems[crIdx];
            }
        }
    }

    if (maxRisk === 1) {
        mObj.responses.problems = expandDescription("[litf_missionResponseRisk01]");
    } else if (maxRisk === 2) {
        mObj.responses.problems = expandDescription("[litf_missionResponseRisk02]");
    }

    mObj.responses.unaware = [ expandDescription("[litf_missionResponseUnawareFalse]"), expandDescription("[litf_missionResponseUnawareTrue]") ];

    //

    var mt = "litf_mission_" + mObj.groupId + "." + mObj.index + ".";

    mObj.title = expandMissionText(mt + "title");
    mObj.description = expandMissionText(mt + "desc");
    mObj.title = this.$replaceKeys(mObj.title, mObj);
    mObj.description = this.$replaceKeys(mObj.description, mObj);

    if (mObj.parcel) {
        mObj.parcel = this.$replaceKeys(mObj.parcel, mObj);
        mObj.parcel = mObj.parcel.charAt(0).toUpperCase() + mObj.parcel.substring(1);
    }

    for (ta = 0; ta < mObj.tasks.length; ta++) {
        task = mObj.tasks[ta];

        task.description = expandMissionText(mt + "t" + (ta + 1) + ".desc");
        task.description = this.$replaceKeys(task.description, mObj);

        if (task.parcel) {
            task.parcel = this.$replaceKeys(task.parcel, mObj);
            task.parcel = task.parcel.charAt(0).toUpperCase() + task.parcel.substring(1);
        }
    }

    _co.$logDebug(this.name, "mission object " + JSON.stringify(mObj));

    return mObj;
}

this.$getFaceBackground = function (mObj) {
    var roll = 0;
    var filename = null;

    if (mObj.groupId === 0) {
        // GalCop
        roll = _co.$roll(1, 5);
        filename = (roll < 10 ? "0" : "") + roll + "-galcop.png";
    } else if (mObj.groupId === 3) {
        // GalShady
        roll = _co.$roll(1, 5);
        filename = (roll < 10 ? "0" : "") + roll + "-galshady.png";
    } else if (mObj.groupId === 10) {
        // GalGovernment
        roll = _co.$roll(1, 5);
        filename = (roll < 10 ? "0" : "") + roll + "-galgov.png";
    // } else if (mObj.choices === "hangup") {
    //     filename = "connrfsd.png";
    } else {
        roll = _co.$roll(1, 21);
        filename = (roll < 10 ? "0" : "") + roll + "-casual.png";
    }

    _co.$logInfo(this.name, ">>> getFaceBackground :: " + filename);

    return filename;
}

this.$generateDialogMission = function (amItem) {
    var pwObj = this.$getDialogMission(amItem);

    _co.$logDebug(this.name, "replaceKeys -> title");
    pwObj.title = this.$replaceKeys(pwObj.title, amItem);

    for (var s = 0; s < pwObj.steps.length; s++) {
        var step = pwObj.steps[s];

        _co.$logDebug(this.name, "replaceKeys -> description");
        step.description = this.$replaceKeys(step.description, amItem);

        var choice = step.choice;
        var cKeys = _co.$getObjectKeys(choice);
        for (var ch = 0; ch < cKeys.length; ch++) {
            _co.$logDebug(this.name, "replaceKeys -> choice");
            choice[cKeys[ch]] = this.$replaceKeys(choice[cKeys[ch]], amItem);
        }
    }

    pwObj.parentMissionId = amItem.missionId; // original mission reference id
    pwObj.missionId = _co.$getGeneralId();

    pwObj.reward = amItem.reward;  // mission reward
    pwObj.feedbackSuccess = amItem.feedbackSuccess; // feedback reward

    return pwObj;
}

this.$getDialogMission = function (mItem) {
    _co.$logInfo(this.name, ">>> getDialogMission");

    var pwObj = null;

    var cTask = _bbs.$getMissionTask(mItem, mItem.currentTask);

    var pwLabel = mItem.groupId + "." + mItem.index + "." + cTask.index;
    if (cTask.dialogMission) { pwLabel = cTask.dialogMission; }

    // var pwLabel = cTask.dialogMission;
    _co.$logDebug(this.name, "pwLabel = " + pwLabel);
    // _co.$logDebug(this.name, "missionDialogStore length = " + _md.missionDialogStore.length);

    // for (var pw = 0; pw < _md.missionDialogStore.length; pw++) {
    //     var pwItem = _md.missionDialogStore[pw];

    for (var pw = 0; pw < _md.missionDialogStore2.length; pw++) {
        var pwItem = _md.missionDialogStore2[pw];

        _co.$logDebug(this.name, "pwItem[" + pw + "] " + JSON.stringify(pwItem));

        if (pwItem.label === pwLabel) {
            pwObj = {
                title: pwItem.title,
                steps: []
            };

            var stepCounter = 1;
            while (stepCounter !== -1) {
                var stepLabel = "step" + stepCounter;
                if (!pwItem[stepLabel]) {
                    _co.$logDebug(this.name, "step " + stepCounter + " does not exists!");
                    stepCounter = -1;
                } else {
                    var stepData = pwItem[stepLabel];

                    var stepObj = {
                        index: stepCounter,
                        // description: stepData[0],
                        // choice: {}
                        description: _pa.$parseStepText(stepData.text),
                        choices: _pa.$parseChoices(stepData.choices)
                    };

                    // for (var sd = 1; sd < stepData.length; sd++) {
                    //     var separator = stepData[sd].indexOf(":");

                    //     var choiceKey = stepData[sd].substring(0, separator);
                    //     var choiceText = stepData[sd].substring(separator + 1);

                    //     stepObj.choice[choiceKey] = choiceText;
                    // }

                    pwObj.steps.push(stepObj);

                    stepCounter++;
                }
            }

            // pwObj = JSON.parse(JSON.stringify(pwItem));

            // These properties are always the same
            pwObj.groupId = 97;
            pwObj.typeId = 0;
            pwObj.index = 0;
            pwObj.currentStep = 1;
            pwObj.choices = "dialog";

            break;
        }
    }

    // Use the title already present or generate a random one for the dialog item

    if (pwObj.title === null) {
        var idx = -1;
        var tObj = null;

        if (cTask.unaware === false) {
            idx = _co.$roll(0, _md.dialogStore.length - 1);
            tObj = _md.dialogStore[idx];
        } else {
            var valid = false;
            while (valid === false) {
                idx = _co.$roll(0, _md.adStore.length - 1);
                tObj = _md.adStore[idx];
                if (tObj.groupId === 101) {
                    valid = true;
                }
            }
        }

        pwObj.title = tObj.title;
        pwObj.title = pwObj.title.replace("#NAME1#", "#PLAYERNAME#");
    }

    _co.$logDebug(this.name, "dialogMission = " + JSON.stringify(pwObj));

    return pwObj;
}

this.$generateInfo = function (gObj, groupId, maxSystemDistance, generatedSystems) {
    _co.$logInfo(this.name, ">>> generateInfo");

    var info = {
        systems: [],
        names: [],
        books: [],
        charities: [],
        items: [],
        jobs: [],
        goods: [],
        races: [],
        liquors: [],
        movies: [],
        music: [],
        art: [],
        paintings: []
    };

    var i = 0;
    var gkeyArr = _co.$getObjectKeys(gObj);

    for (var gk = 0; gk < gkeyArr.length; gk++) {
        var gkey = gkeyArr[gk];
        if (gkey === "systems") {
            for (i = 0; i < gObj[gkey]; i++) {
                var valid = false;
                var rsys = null;
                var counter = 0;
                while (valid === false && counter < 100) {
                    rsys = _co.$randomSystem(maxSystemDistance);

                    valid = true;
                    for (var sy = 0; sy < info.systems.length; sy++) {
                        // _co.$logDebug(this.name, "rsys.id = " + rsys.id + " info.systems[" + sy + "].name = " + info.systems[sy].id);
                        // _co.$logDebug(this.name, "rsys.name = " + rsys.id + " info.systems[" + sy + "].name = " + info.systems[sy].name);
                        if (info.systems[sy].id === rsys.id) {
                            valid = false;
                        }
                    }

                    for (var gs = 0; gs < generatedSystems.length; gs++) {
                        if (generatedSystems[gs].id === rsys.id) {
                            valid = false;
                        }
                    }

                    counter++;
                }

                if (counter < 100) {
                    info.systems.push(rsys);
                } else {
                    info.systems = null;
                }
            }
        } else if (gkey === "names") {
            for (i = 0; i < gObj[gkey]; i++) {
                info.names.push(randomName() + " " + randomName());
            }
        } else if (gkey === "books") {
            for (i = 0; i < gObj[gkey]; i++) {
                info.books.push(expandDescription("[litf_Books]"));
            }
        } else if (gkey === "charities") {
            for (i = 0; i < gObj[gkey]; i++) {
                info.charities.push(expandDescription("[litf_Charity]"));
            }
        } else if (gkey === "items") {
            for (i = 0; i < gObj[gkey]; i++) {
                info.items.push(expandDescription("[litf_Items]"));
            }
        } else if (gkey === "jobs") {
            for (i = 0; i < gObj[gkey]; i++) {
                info.jobs.push(expandDescription("[litf_Jobs]"));
            }
        } else if (gkey === "goods") {
            for (i = 0; i < gObj[gkey]; i++) {
                var goodArrByGroup = _co.commodityGroups[groupId];
                var goodIdx = _co.$roll(0, goodArrByGroup.length - 1);
                info.goods.push(goodArrByGroup[goodIdx]);
            }
        } else if (gkey === "races") {
            for (i = 0; i < gObj[gkey]; i++) {
                info.races.push(randomInhabitantsDescription(true));
            }
        } else if (gkey === "liquors") {
            for (i = 0; i < gObj[gkey]; i++) {
                info.liquors.push(expandDescription("[litf_Liquors]"));
            }
        } else if (gkey === "movies") {
            for (i = 0; i < gObj[gkey]; i++) {
                info.movies.push(expandDescription("[litf_Movie1]") + " " + expandDescription("[litf_Movie2]") + " " + expandDescription("[litf_Movie3]"));
            }
        } else if (gkey === "music") {
            for (i = 0; i < gObj[gkey]; i++) {
                info.music.push(expandDescription("[litf_Music1]") + " " + expandDescription("[litf_Music2]") + " " + expandDescription("[litf_Music3]"));
            }
        } else if (gkey === "art") {
            for (i = 0; i < gObj[gkey]; i++) {
                info.art.push(expandDescription("[litf_Art1]") + " " + expandDescription("[litf_Art2]"));
            }
        } else if (gkey === "paintings") {
            for (i = 0; i < gObj[gkey]; i++) {
                info.paintings.push(expandDescription("[litf_Painting1]") + " " + expandDescription("[litf_Painting2]"));
            }
        }
    }

    return info;
}

this.$generateCount = function (obj) {
    _co.$logInfo(this.name, ">>> generateCount");

    var generate = {
        systems: 0,
        names: 0,
        books: 0,
        charities: 0,
        items: 0,
        jobs: 0,
        goods: 0,
        races: 0,
        liquors: 0,
        movies: 0,
        music: 0,
        art: 0,
        paintings: 0
    };

    if (obj.title.indexOf("#SYSTEM1#") > -1 || obj.description.indexOf("#SYSTEM1#") > -1) {
        generate.systems = generate.systems + 1;
    }

    if (obj.title.indexOf("#NAME1#") > -1 || obj.description.indexOf("#NAME1#") > -1) {
        generate.names = generate.names + 1;
    }

    if (obj.title.indexOf("#BOOK#") > -1 || obj.description.indexOf("#BOOK#") > -1) {
        generate.books = generate.books + 1;
    }

    if (obj.title.indexOf("#CHARITY#") > -1 || obj.description.indexOf("#CHARITY#") > -1) {
        generate.charities = generate.charities + 1;
    }

    if (obj.title.indexOf("#ITEM#") > -1 || obj.description.indexOf("#ITEM#") > -1) {
        generate.items = generate.items + 1;
    }

    if (obj.title.indexOf("#JOB#") > -1 || obj.description.indexOf("#JOB#") > -1) {
        generate.jobs = generate.jobs + 1;
    }

    if (obj.title.indexOf("#GOODS#") > -1 || obj.description.indexOf("#GOODS#") > -1) {
        generate.goods = generate.goods + 1;
    }

    if (obj.title.indexOf("#RACE#") > -1 || obj.description.indexOf("#RACE#") > -1) {
        generate.races = generate.races + 1;
    }

    if (obj.title.indexOf("#LIQUOR#") > -1 || obj.description.indexOf("#LIQUOR#") > -1) {
        generate.liquors = generate.liquors + 1;
    }

    if (obj.title.indexOf("#MOVIE#") > -1 || obj.description.indexOf("#MOVIE#") > -1) {
        generate.movies = generate.movies + 1;
    }

    if (obj.title.indexOf("#MUSIC#") > -1 || obj.description.indexOf("#MUSIC#") > -1) {
        generate.music = generate.music + 1;
    }

    if (obj.title.indexOf("#ART#") > -1 || obj.description.indexOf("#ART#") > -1) {
        generate.art = generate.art + 1;
    }

    if (obj.title.indexOf("#PAINTING#") > -1 || obj.description.indexOf("#PAINTING#") > -1) {
        generate.paintings = generate.paintings + 1;
    }

    return generate;
}

this.$generateGovernmentItem = function () {
    _co.$logInfo(this.name, ">>> generateGovernmentItem");

    var storeObj = null;

    var stationData = _litf.litfVars.stationParams;
    var government = stationData.government;

    var valid = false;
    while (valid === false) {
        var idx = _co.$roll(0, _md.governmentStore.length - 1);
        storeObj = _md.governmentStore[idx];

        valid = true;
        if (storeObj.gov.indexOf(government) === -1) {
            valid = false;
        }
    }

    // Clone store object
    _co.$logDebug(this.name, "Clone store object");
    var obj = JSON.parse(JSON.stringify(storeObj));

    obj.missionId = -1;

    // Procedural generation
    var generate = this.$generateCount(obj);

    obj.tasks = [{}];

    // Procedural generation
    obj.tasks[0].info = this.$generateInfo(generate, 10, 20, []);

    //

    obj.title = this.$replaceKeys(obj.title, obj);

    return obj;
}

this.$generateRandomItem = function (store) {
    _co.$logInfo(this.name, ">>> generateRandomItem");

    var pFeedback = _litf.litfCommander.feedback;
    var maxSystemDistance = 10 + (Math.abs(pFeedback) / 5);

    var idx = _co.$roll(0, store.length - 1);
    var storeObj = store[idx];

    // Clone store object
    _co.$logDebug(this.name, "Clone store object");
    var obj = JSON.parse(JSON.stringify(storeObj));

    obj.missionId = -1;
    obj.contractor = randomName() + " " + randomName();

    // Face
    obj.faceBackground = this.$getFaceBackground(obj);

    // Procedural generation
    var generate = this.$generateCount(obj);

    obj.tasks = [{}];

    // Procedural generation
    obj.tasks[0].info = this.$generateInfo(generate, 12, maxSystemDistance, []);

    //

    obj.title = this.$replaceKeys(obj.title, obj);
    obj.description = this.$replaceKeys(obj.description, obj);

    _co.$logDebug(this.name, "obj.title = " + obj.title);

    return obj;
}

this.$replaceKeys = function (text, mObj) {
    _co.$logInfo(this.name, ">>> replaceKeys");

    while (text.indexOf("#SYSTEM0#") > -1) { text = text.replace("#SYSTEM0#", mObj.startingSystem.name); }
    while (text.indexOf("#SYSTEM1#") > -1) { text = text.replace("#SYSTEM1#", mObj.tasks[0].info.systems[0].name); }
    while (text.indexOf("#SYSTEM2#") > -1) { text = text.replace("#SYSTEM2#", mObj.tasks[1].info.systems[0].name); }

    while (text.indexOf("#NAME0#") > -1) { text = text.replace("#NAME0#", mObj.contractor); }
    while (text.indexOf("#NAME1#") > -1) { text = text.replace("#NAME1#", mObj.tasks[0].info.names[0]); }
    while (text.indexOf("#NAME2#") > -1) { text = text.replace("#NAME2#", mObj.tasks[1].info.names[0]); }

    while (text.indexOf("#PLAYERNAME#") > -1) { text = text.replace("#PLAYERNAME#", player.name); }

    while (text.indexOf("#REWARD#") > -1) { text = text.replace("#REWARD#", mObj.reward); }

    while (text.indexOf("#BOOK#") > -1) { text = text.replace("#BOOK#", mObj.tasks[0].info.books[0]); }
    while (text.indexOf("#CHARITY#") > -1) { text = text.replace("#CHARITY#", mObj.tasks[0].info.charities[0]); }
    while (text.indexOf("#ITEM#") > -1) { text = text.replace("#ITEM#", mObj.tasks[0].info.items[0]); }
    while (text.indexOf("#JOB#") > -1) { text = text.replace("#JOB#", mObj.tasks[0].info.jobs[0]); }
    while (text.indexOf("#GOODS#") > -1) { text = text.replace("#GOODS#", mObj.tasks[0].info.goods[0]); }
    while (text.indexOf("#RACE#") > -1) { text = text.replace("#RACE#", mObj.tasks[0].info.races[0]); }
    while (text.indexOf("#LIQUOR#") > -1) { text = text.replace("#LIQUOR#", mObj.tasks[0].info.liquors[0]); }
    while (text.indexOf("#MOVIE#") > -1) { text = text.replace("#MOVIE#", mObj.tasks[0].info.movies[0]); }
    while (text.indexOf("#MUSIC#") > -1) { text = text.replace("#MUSIC#", mObj.tasks[0].info.music[0]); }
    while (text.indexOf("#ART#") > -1) { text = text.replace("#ART#", mObj.tasks[0].info.art[0]); }
    while (text.indexOf("#PAINTING#") > -1) { text = text.replace("#PAINTING#", mObj.tasks[0].info.paintings[0]); }

    while (text.indexOf("#PASSENGERS#") > -1) { text = text.replace("#PASSENGERS#", _bbs.$howManyPassengers(mObj)); }

    while (text.indexOf("#MP_NAME#") > -1) { text = text.replace("#MP_NAME#", mObj.missing.name); }
    while (text.indexOf("#MP_START#") > -1) { text = text.replace("#MP_START#", System.systemNameForID(mObj.missing.posArr[0])); }
    while (text.indexOf("#MP_CURRENT#") > -1) { text = text.replace("#MP_CURRENT#", System.systemNameForID(mObj.missing.posArr[mObj.missing.currentPos])); }
    while (text.indexOf("#MP_NEXT#") > -1) { text = text.replace("#MP_NEXT#", System.systemNameForID(mObj.missing.posArr[mObj.missing.currentPos + 1])); }
    while (text.indexOf("#MP_KNOWN#") > -1) { text = text.replace("#MP_KNOWN#", System.systemNameForID(mObj.missing.posArr[mObj.missing.lastKnownPos])); }

    return text;
}

this.$missionAllocatedTime = function (mObj) {
    _co.$logInfo(this.name, ">>> missionAllocatedTime");

    var time = 0;
    var totalTime = 0;
    var route = null;
    var multi = 0;

    if (mObj.local === false) {
        var tasks = mObj.tasks.length;

        if (tasks === 1) {
            route = _co.$getSystemRoute(mObj.startingSystem.id, mObj.tasks[0].info.systems[0].id);

            if (mObj.tasks[0].urgent === true) {
                multi = 1.2;
            } else {
                multi = 1.5
            }
            totalTime = route.time * multi;
        } else if (tasks > 1) {
            for (var ta = 0; ta < mObj.tasks.length; ta++) {
                if (ta === 0) {
                    route = _co.$getSystemRoute(mObj.startingSystem.id, mObj.tasks[0].info.systems[0].id);
                } else {
                    route = _co.$getSystemRoute(mObj.tasks[ta - 1].info.systems[0].id, mObj.tasks[ta].info.systems[0].id);
                }

                if (mObj.tasks[ta].urgent === true) {
                    multi = 1 + parseInt(_co.$roll(1, 3) / 10);
                } else {
                    multi = 1 + parseInt(_co.$roll(3, 6) / 10);
                }

                totalTime = totalTime + (route.time * multi);
            }
        }
        time = totalTime * 3600; // seconds
    } else {
        time = 0;
    }

    return Math.round(time);
}

this.$missionReward = function (mObj) {
    _co.$logInfo(this.name, ">>> missionReward");

    var baseReward = 50;

    var passengers = 0;
    var urgentTask = false;
    var risk = 0;

    var reward = 0;
    var partialReward = 0;
    var multi = 1;

    // Missions without rewards
    if (mObj.noReward === true) {
        return -1;
    }

    // Local missions
    if (mObj.local === true) {
        _co.$logDebug(this.name, "local missions not implemented.")
        return 0;
    }

    // Non-local missions

    var taskNum = mObj.tasks.length;
    _co.$logDebug(this.name, "tasks = " + taskNum);

    var absFeedback = Math.abs(_litf.litfCommander.feedback); // Absolute value player feedback
    multi = parseInt(absFeedback / 30);

    var groupMissionCompleted = _litf.litfCommander.statistics.missionCompleted[mObj.groupId];
    if (groupMissionCompleted > 0 && groupMissionCompleted <= 50) {
        multi += parseInt(groupMissionCompleted / 10);
    }

    //

    if (taskNum === 1) {
        if (mObj.tasks[0].typeId === 5) {
            var cargoGoods = mObj.tasks[0].info.goods[0];
            var scPrice = _co.$getSubCommodityPrice(cargoGoods);
            var cargoTotal = _co.$roll(1, player.ship.cargoSpaceAvailable);

            var subPrice = parseInt((scPrice * cargoTotal) / 3);
            baseReward = baseReward + subPrice;

            _co.$logDebug(this.name, "cargoGoods[" + cargoGoods + "] scPrice[" + scPrice + "] cargoTotal[" + cargoTotal + "] subPrice = " + subPrice);
        }

        if (mObj.passengers) { passengers = mObj.passengers; }
        if (passengers > 2) { multi = multi + 1; } // Small groups
        if (passengers > 4) { multi = multi + 1; } // Large groups

        if (mObj.tasks[0].urgent) { urgentTask = mObj.tasks[0].urgent; }
        if (urgentTask === true) { multi = multi + 1; }

        if (mObj.risk) { risk = mObj.risk; }
        multi = multi + risk;

        partialReward = baseReward * multi;
    } else {
        for (var ta = 0; ta < taskNum; ta++) {
            var task = mObj.tasks[ta];

            multi = parseInt(absFeedback / 10);
            if (mObj.risk) { risk = mObj.risk; }
            multi = multi + risk;

            passengers = 0;
            if (task.passengers) { passengers = task.passengers; }
            if (passengers > 2) { multi = multi + 1; } // Small groups
            if (passengers > 4) { multi = multi + 1; } // Large groups

            urgentTask = false;
            if (task.urgent) { urgentTask = task.urgent; }
            if (urgentTask === true) { multi = multi + 1; }

            partialReward = partialReward + (baseReward * multi);
        }
    }

    var missionTimeHours = parseInt(mObj.timeAllowed / 3600);
    var multiTime = 11 - parseInt(missionTimeHours / 10);
    if (multiTime < 1) { multiTime = 1; }
    var timeReward = multiTime * 100;
    reward = partialReward + timeReward;

    // // var pricePerHour = 10;
    // reward = partialReward + (missionTimeHours * pricePerHour);
    _co.$logDebug(this.name, "reward = " + reward);

    // Adjust by system economy
    var econDesc = system.economyDescription;
    if (econDesc.indexOf("Rich") !== -1) {
        reward = parseInt(reward / 100 * 110);
    } else if (econDesc.indexOf("Poor") !== -1) {
        reward = parseInt(reward / 100 * 75);
    } else if (econDesc.indexOf("Mainly")) {
        reward = parseInt(reward / 100 * 90);
    }

    return reward;
}
