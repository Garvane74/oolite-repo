/* eslint-disable semi, no-multi-spaces, quotes, indent, no-tabs, yoda */
/* global worldScripts, system, missionVariables, player, mission, clock, SoundSource, setScreenBackground */

"use strict";

this.name	     = "LITF";
this.author	     = "BeeTLe BeTHLeHeM";
this.copyright	 = "2018 BeeTLe BeTHLeHeM";
this.description = "Main OXP file";
this.version	 = "0.10.0";
this.licence	 = "CC BY-NC-SA 4.0";

this.LITFInstances = {
    co: null
};

var _co  = null;
var _bbs = null;
var _sm  = null;

this.startUp = function () {
    this.$initInstances();

    _co  = this.LITFInstances.co;
    _bbs = this.LITFInstances.bbs;
    _sm  = this.LITFInstances.sm;

    //

    this.$initOXPVariables();

    _co.$logInfo(this.name, ">>> startup - [LIFE IN THE FRONTIER v" + this.version + "]");

    this.litfVars.config.debug = true;
    _co.$logInfo(this.name, "debug = " + this.litfVars.config.debug);

    this.$checkOtherOXP();

    // Display Current Course OXP
    if (this.litfVars.oxp.DisplayCurrentCourse) {
        this.litfVars.oxp.DisplayCurrentCourse._screenIDList.push("litf_bbs_mission_starchart_screen");
    }

    // Starting up
    if (this.litfVars.starting === true) {
        _co.$logDebug(this.name, "creating initial BBS items");
        _bbs.$createBBSItems();

        this.litfVars.starting = false;
    }

    player.ship.hudHidden = true;

    this.$LITFinterface();
}

this.$initInstances = function () {
    this.LITFInstances.bbs = worldScripts.LITF_BBS;
    this.LITFInstances.co = worldScripts.LITF_Common;
    this.LITFInstances.md = worldScripts.LITF_MissionData;
    this.LITFInstances.mi = worldScripts.LITF_Missions;
    this.LITFInstances.pa = worldScripts.LITF_Parser;
    this.LITFInstances.sm = worldScripts.LITF_SearchMission;

    this.LITFInstances.co.$init();
    this.LITFInstances.mi.$init();
    this.LITFInstances.sm.$init();
    this.LITFInstances.bbs.$init();
    this.LITFInstances.pa.$init();
}

this.playerWillSaveGame = function () {
    _co.$logInfo(this.name, ">>> playerWillSaveGame");

    // Store variables in savegame
    missionVariables.litfCommander = JSON.stringify(this.litfCommander);
    missionVariables.litfVars = JSON.stringify(this.litfVars);
}

this.shipWillDockWithStation = function (station) {
    _co.$logInfo(this.name, ">>> shipWillDockWithStation");

    this.$storeActiveMissionSupportData(false);
}

this.shipWillLaunchFromStation = function (station) {
    _co.$logInfo(this.name, ">>> shipWillLaunchFromStation");

    this.$restoreActiveMissionSupportData();

    player.ship.hudHidden = false;
}

this.shipDockedWithStation = function () {
    _co.$logInfo(this.name, ">>> shipDockedWithStation");

    // Player has just docked with a station

    this.$storeActiveMissionSupportData(true);

    // Store system data for BBS generation - only if player has changed system
    if (this.litfVars.stationParams.id !== system.ID) {
        var stationParams = {
            systemId: system.ID,
            government: system.government,
            economy: system.economy,
            techLevel: system.techLevel
        };

        this.litfVars.stationParams = stationParams;
    }

    this.$checkExpiredMissions();

    // BBS item generation
    if (this.litfVars.refreshBBS === true) {
        _bbs.$createBBSItems();
        this.litfVars.refreshBBS = false;
        this.litfVars.bbsItems = this.litfVars.bbsData.bbsItemsArr;
    } else {
        this.litfVars.bbsData.bbsItemsArr = this.litfVars.bbsItems;
    }

    player.ship.hudHidden = true;

    this.$LITFinterface();
}

this.shipExitedWitchspace = function () {
    _co.$logInfo(this.name, ">>> shipExitedWitchspace");

    // Player has just exited a witchspace jump

    // New messages when changing system
    this.litfVars.refreshBBS = true;
    this.litfVars.bbsItems = null;

    // Console message if there is a mission to complete in the present system.
    if (this.litfCommander.activeMissions.length > 0) {
        var missionsHere = 0;

        for (var m = 0; m < this.litfCommander.activeMissions.length; m++) {
            var mObj = this.litfCommander.activeMissions[m];

            var mSystems = _bbs.$getMissionTask(mObj, mObj.currentTask).info.systems;
            _co.$logDebug(this.name, "system.ID = " + system.ID + " mSystems = " + JSON.stringify(mSystems));

            for (var s = 0; s < mSystems.length; s++) {
                if (mSystems[s].id === system.ID) {
                    missionsHere = missionsHere + 1;
                }
            }
        }

        _co.$logDebug(this.name, missionsHere + " tasks can be completed in this system.");

        if (missionsHere > 0) {
            var pl = missionsHere > 1 ? "s" : "";
            player.commsMessage("You have " + missionsHere + " active BBS mission" + pl + " in this system.");
        }
    }
}

this.playerEnteredNewGalaxy = function (galaxyNumber) {
    _co.$logInfo(this.name, ">>> playerEnteredNewGalaxy");

    // Player changed galaxy

    // Re-Initializing

    this.litfCommander = {
        feedback: 0,
        activeMissions: [],
        busyCargo: false,
        busyPassenger: false,
        busyParcel: false,
        statistics: {
            missionAccepted: [],
            missionCompleted: [],
            missionFailed: [],
            feedbackGained: [],
            feedbackLost: []
            // creditsGained: []
        }
    };

    for (var g = 0; g < _co.groupsLabels.length; g++) {
        this.litfCommander.statistics.missionAccepted.push(0);
        this.litfCommander.statistics.missionCompleted.push(0);
        this.litfCommander.statistics.missionFailed.push(0);
        this.litfCommander.statistics.feedbackGained.push(0);
        this.litfCommander.statistics.feedbackLost.push(0);
        // this.litfCommander.statistics.creditsGained.push(0);
    }
}

// Add OXP interfaces to station screen (F4)
this.$LITFinterface = function () {
    _co.$logInfo(this.name, ">>> LITFinterface");

	player.ship.dockedStation.setInterface("LITF_bbs",
		{
            title: "Login to BBS",
			category: "Activity",
			summary: "Connect to local Bulletin Board System",
			callback: this.$accessBBS.bind(this)
		}
    );

	player.ship.dockedStation.setInterface("LITF_profile",
		{
			title: "Your online status",
			category: "Activity",
			summary: "Check your online feedback and accepted missions.",
			callback: this.$displayOnlineStatus.bind(this)
		}
	);

    if (this.litfVars.config.debug === true) {
        player.ship.dockedStation.setInterface("LITF_debugscreen",
            {
                title: "*** DEBUG CONSOLE ***",
                category: "Activity",
                summary: "Stuff for development and tests.",
                callback: this.$debugScreen.bind(this)
            }
        );
    }

    if (this.litfVars.debug.enableNavigation === true) {
        player.ship.dockedStation.setInterface("LITF_navigation",
            {
                title: "Disembark from your ship",
                category: "Activity",
                summary: "Go roaming through the station.",
                callback: this.$navigateStation.bind(this)
            }
        );
    }
}

this.$debugScreen = function () {
    _co.$logInfo(this.name, ">>> debugScreen");

    var _deb = worldScripts.LITF_Debug;

    _deb.$init();
    _deb.$display();
}

this.$navigateStation = function () {
    _co.$logInfo(this.name, ">>> navigateStation");

    var _nav = worldScripts.LITF_Station;

    _nav.$init();
    _nav.$start();
}

// Open the BBS message list screen
this.$accessBBS = function () {
    _co.$logInfo(this.name, ">>> accessBBS");

    this.$checkExpiredMissions();

    var _bbs = worldScripts.LITF_BBS;

    this.litfVars.enterBBSFlag = true;
    this.litfVars.bbsData.lastChoice = "02_NEXT_ITEM";

    this.litfVars.bbsData.firstItemIdx = 0;
    this.litfVars.bbsData.selectedItem = 0;

    _bbs.$run();
}

// Open the Online Profile screen
this.$displayOnlineStatus = function () {
    _co.$logInfo(this.name, ">>> displayOnlineStatus");

    var displayText = null;
    var opts = null;
    var choices = null;

    if (this.litfVars.statusPage === 0) {
        this.$checkExpiredMissions();

        var fbRank = _co.$getFeedbackRanking(this.litfCommander.feedback);

        displayText = "NAME: " + player.name;
        displayText += "                                        ";
        displayText += "FEEDBACK: " + fbRank.label + " (" + this.litfCommander.feedback + ")";
        displayText += "\n\n";

        var maxActiveMissions = parseInt(Math.abs(this.litfCommander.feedback) / 25) + 1;

        displayText += "-- ACTIVE MISSIONS (" + this.litfCommander.activeMissions.length + " / " + maxActiveMissions + ") --";
        displayText += "\n\n";

        if (this.litfCommander.activeMissions.length === 0) {
            displayText += ">> No active missions at the moment <<";
        } else {
            for (var m = 0; m < this.litfCommander.activeMissions.length; m++) {
                var mObj = this.litfCommander.activeMissions[m];

                var missionText = "> ";

                if (this.litfVars.config.debug === true) {
                    missionText += "(" + mObj.groupId + "." + mObj.tasks[0].typeId + "." + mObj.index + ") ";
                }

                missionText += _bbs.$getMissionTask(mObj, mObj.currentTask).description;

                if (mObj.timeAllowed) {
                    var deadline = parseInt(mObj.startTime) + parseInt(mObj.timeAllowed);
                    var timeText = _co.$formatTime(deadline, true);
                    missionText += " Mission expires in " + timeText + ".";
                }

                if (mObj.reward && mObj.reward > 0) {
                    missionText += " Payment: " + mObj.reward + " credits.";
                }

                displayText += missionText + "\n\n";
            }
        }

        choices = {
            "80_LOGPAGE": "Go to the Logs Screen"
            // "99_QUIT": "Exit the profile page."
        };

        if (this.litfVars.debug.enableStatsScreen === true) {
            choices["90_STATSPAGE"] = "Go to the Statistics screen.";
        }

        opts = {
            title: "Online Profile Data",
            exitScreen: "GUI_SCREEN_INTERFACES",
            message: displayText,
            allowInterrupt: true,
            choices: choices
        };
    } else if (this.litfVars.statusPage === 1) {
        displayText = "";

        for (var g = 0; g < _co.groupsLabels.length; g++) {
            displayText += _co.groupsLabels[g] + ": ";

            displayText += "Accepted: " + this.litfCommander.statistics.missionAccepted[g] + ", ";
            displayText += "Completed: " + this.litfCommander.statistics.missionCompleted[g] + ", ";
            displayText += "Failed: " + this.litfCommander.statistics.missionFailed[g] + ", ";

            displayText += " Feedback: ";
            if (this.litfCommander.statistics.feedbackGained[g] > 0) {
                displayText += "+" + this.litfCommander.statistics.feedbackGained[g];
            } else {
                displayText += this.litfCommander.statistics.feedbackGained[g];
            }

            displayText += " / ";
            if (this.litfCommander.statistics.feedbackLost[g] > 0) {
                displayText += "-" + this.litfCommander.statistics.feedbackLost[g];
            } else {
                displayText += this.litfCommander.statistics.feedbackLost[g];
            }

            displayText += "\n";
        }

        choices = {
            "10_PROFILEPAGE": "Go to the Profile screen."
            // "99_QUIT": "Exit the profile page."
        };

        opts = {
            screenID: "litf_stats_screen",
            title: "Online Statistics",
            exitScreen: "GUI_SCREEN_INTERFACES",
            allowInterrupt: true,
            message: displayText,
            choices: choices
        };
    } else if (this.litfVars.statusPage === 2) {
        displayText = "";

        var logItems = 10;

        if (this.litfCommander.bbsLogs.length < logItems) { logItems = this.litfCommander.bbsLogs.length; }

        if (logItems === 0) {
            displayText = "> There's nothing here."
        } else {
            for (var l = 0; l < logItems; l++) {
                displayText += this.litfCommander.bbsLogs[l] + "\n";
            }
        }

        choices = {
            "10_PROFILEPAGE": "Go to the Profile screen."
            // "99_QUIT": "Exit the profile page."
        };

        opts = {
            screenID: "litf_logs_screen",
            title: "Online Logs",
            exitScreen: "GUI_SCREEN_INTERFACES",
            allowInterrupt: true,
            message: displayText,
            choices: choices
        };
    }

    mission.runScreen(opts, this.$executeStatusAction);

    setScreenBackground("bbs-bg.png");
}

this.$executeStatusAction = function (choice) {
    if (choice === "99_QUIT") {
        this.litfVars.statusPage = 0;
        this.$LITFinterface();
        return;
    }

    if (choice === "10_PROFILEPAGE") {
        this.litfVars.statusPage = 0;
    } else if (choice === "80_LOGPAGE") {
        this.litfVars.statusPage = 2;
    } else if (choice === "90_STATSPAGE") {
        this.litfVars.statusPage = 1;
    }

    this.$displayOnlineStatus();
}

// Check expired missions and delete them
this.$checkExpiredMissions = function () {
    var failed = [];

    if (this.litfCommander.activeMissions) {
        for (var am = 0; am < this.litfCommander.activeMissions.length; am++) {
            var amItem = this.litfCommander.activeMissions[am];

            if (amItem.timeAllowed) {
                var deadline = parseInt(amItem.startTime) + parseInt(amItem.timeAllowed) - clock.seconds;
                if (deadline < 0) {
                    failed.push(amItem.missionId);
                }
            } else {
                if (amItem.missing) {
                    // SEARCH mission
                    if (_sm.$updateMissingPerson(amItem.missing) === true) {
                        failed.push(amItem.missionId);
                    }
                }
            }
        }

        _co.$logDebug(this.name, "failed = " + JSON.stringify(failed));

        if (failed.length > 0) {
            var pl = null;

            pl = failed.length > 1 ? "s" : "";
            _co.addPlayerLog("Time expired: " + failed.length + " mission" + pl + " failed.");
            _co.$logDebug(this.name, "Time expired: " + failed.length + " mission" + pl + " failed.");

            // var removedPassengers = 0;
            // var deadPassengers = 0;

            var feedbackLost = 0;

            // Delete failed missions from active missions list
            for (var fm = 0; fm < failed.length; fm++) {
                var ret = _bbs.$deleteActiveMission(failed[fm]);

                if (ret.cargo !== null) {
                    // _bbs.$deleteCargoMission();
                    _co.addPlayerLog("A shipment of " + ret.cargo + " has been removed from your ship.");
                }

                if (ret.parcel !== null) {
                    // _bbs.$deleteParcelMission({ parcel: ret.parcel });
                    _co.addPlayerLog(ret.parcel + " has been removed from your ship.");
                }

                _co.$updateStatistics("missionFailed", ret.groupId, 1);

                // Get feedback penalty
                if (ret.groupId !== 3) {
                    _co.$setPlayerFeedback(-ret.feedback.lost);
                    feedbackLost += ret.feedback.lost;
                }

                if (ret.removedPassengers > 0) {
                    pl = ret.removedPassengers > 1 ? "s" : "";
                    _co.addPlayerLog(ret.removedPassengers + " passenger" + pl + " have left your ship!");

                    // Get feedback penalty (1 point per passenger)
                    if (ret.groupId !== 3) {
                        _co.$setPlayerFeedback(-ret.removedPassengers);
                        feedbackLost += ret.removedPassengers;
                    }
                }

                if (ret.deadPassengers > 0) {
                    pl = ret.deadPassengers > 1 ? "s" : "";
                    _co.addPlayerLog(ret.deadPassengers + " dead passenger" + pl + " has been removed from your ship!");

                    // Get feedback penalty (3 point per passenger)
                    if (ret.groupId !== 3) {
                        _co.$setPlayerFeedback(-ret.deadPassengers * 3);
                        feedbackLost += ret.removedPassengers * 3;
                    }
                }

                _co.$updateStatistics("feedbackLost", ret.groupId, -feedbackLost);

                _co.addPlayerLog("Personal feedback change (-" + feedbackLost + ").");
            }

            this.litfVars.sounds["mission_fail"].play();
        }
    }
}

this.$initOXPVariables = function () {
    // OXP player commander data

    if (missionVariables.litfCommander) {
        // Loading from savegame
        this.litfCommander = JSON.parse(missionVariables.litfCommander);
    } else {
        // Initializing
        this.litfCommander = {
            feedback: 0,
            activeMissions: [],
            busyCargo: false,
            busyPassenger: false,
            busyParcel: false,
            bbsLogs: [],
            statistics: {
                missionAccepted: [],
                missionCompleted: [],
                missionFailed: [],
                feedbackGained: [],
                feedbackLost: []
                // creditsGained: []
            },
            scriptVars: {
                currentStepId: -1,
                prevStepId: []
            }
        };

        for (var g = 0; g < _co.groupsLabels.length; g++) {
            this.litfCommander.statistics.missionAccepted.push(0);
            this.litfCommander.statistics.missionCompleted.push(0);
            this.litfCommander.statistics.missionFailed.push(0);
            this.litfCommander.statistics.feedbackGained.push(0);
            this.litfCommander.statistics.feedbackLost.push(0);
            // this.litfCommander.statistics.creditsGained.push(0);
        }
    }

    // OXP variables and flags

    if (missionVariables.litfVars) {
        // Loading from savegame
        this.litfVars = JSON.parse(missionVariables.litfVars);
    } else {
        this.litfVars = {
            starting: true,
            config: {
                debug: false,
                disableMissionBackground: true
            },
            sounds: {},
            debug: {
                enableNavigation: false,
                enableStatsScreen: false,
                enableStarChartScreen: false,
                logLevel: 1
            },
            stationParams: {
                systemId: system.ID,
                government: system.government,
                economy: system.economy,
                techLevel: system.techLevel
            },
            refreshBBS: true,
            bbsItems: null,
            enterBBSFlag: false,
            generalId: 1,
            bbsData: {
                bbsItemsArr: [],
                checkDuplicatesArr: [],
                selectedItem: 0,
                lastChoice: "02_NEXT_ITEM",
                firstItemIdx: 0,
                totalItems: 0,
                rowsSize: 18,
                bbsMode: 0, // 0 - item list, 1 - item details
                selectedItemObj: null,
                starChartMarkedSystems: [],
                starChartModeIdx: 0,
                starChartZoom: 1,
                starChartSystemCentre: 0
            },
            statusPage: 0,
            oxp: {}
        };

        // Fetch sounds

        var ssInstance = null;

        ssInstance = new SoundSource();
        ssInstance.sound = "Sounds/misscomp.ogg";
        this.litfVars.sounds["mission_agree"] = ssInstance;

        // this.litfVars.sounds["mission_task"] = ssInstance;

        // this.litfVars.sounds["mission_complete"] = ssInstance;

        ssInstance = new SoundSource();
        ssInstance.sound = "Sounds/missfail.ogg";
        this.litfVars.sounds["mission_fail"] = ssInstance;
    }
}

this.$checkOtherOXP = function () {
    _co.$logInfo(this.name, ">>> checkOtherOXP");

    // Check OXPs main files (look into the OXZs) and set associate flags

    if (worldScripts.DisplayCurrentCourse) {
        this.litfVars.oxp.DisplayCurrentCourse = worldScripts.DisplayCurrentCourse;
        _co.$logDebug(this.name, "* Display Current Course is installed")
    }
}

this.$restoreActiveMissionSupportData = function () {
    _co.$logInfo(this.name, ">>> restoreActiveMissionSupportData");

    if (this.litfCommander.activeMissionsTempStorage) {
        for (var am = 0; am < this.litfCommander.activeMissions.length; am++) {
            var amItem = this.litfCommander.activeMissions[am];

            var tObj = null;
            for (var tm = 0; tm < this.litfCommander.activeMissionsTempStorage.length; tm++) {
                var tmItem = this.litfCommander.activeMissionsTempStorage[tm];
                if (tmItem.missionId === amItem.misionId) {
                    tObj = tmItem;
                }
            }

            if (tObj === null) {
                _co.$logDebug(this.name, "Something wrong, no temporary mission for mission " + JSON.stringify(amItem));
            } else {
                var typeId = tObj.missionType;

                if (typeId === 5) {
                    // Cargo mission
                    _bbs.$createCargoMission(tObj.cargo);
                } else if (typeId === 4) {
                    // Passenger mission
                    _bbs.$createPassengerMission(amItem);
                } else {
                    // Parcel mission
                    _bbs.$createParcelMission(amItem);
                }
            }
        }
    }

    this.litfVars.activeMissionsTempStorage = [];
}

this.$storeActiveMissionSupportData = function (docked) {
    _co.$logInfo(this.name, ">>> storeActiveMissionSupportData");

    var tempArr = [];

    if (this.litfCommander.activeMissionsTempStorage) {
        tempArr = this.litfCommander.activeMissionsTempStorage;
    }

    // Temporarily removes vanilla "support" missions
    if (this.litfCommander.activeMissions) {
        for (var am = 0; am < this.litfCommander.activeMissions.length; am++) {
            var amItem = this.litfCommander.activeMissions[am];
            var typeId = amItem.tasks[0].typeId;

            var tObj = {
                missionId: amItem.missionId,
                missionType: typeId,
                parcel: null,
                passengerList: null,
                cargo: null
            };

            if (docked === true) {
                if (typeId === 5) {
                    // Cargo mission
                    tObj.cargo = amItem.tasks[0].info.goods[0];
                    _bbs.$deleteCargoMission();
                }
            } else {
                if (typeId === 4) {
                    // Passenger mission
                    tObj.passengerList = amItem.passengerList; // I'm not using it
                    _bbs.$deletePassengerMission(tObj);
                } else {
                    // Parcel mission

                    // I'm not using it
                    if (amItem.parcel) {
                        tObj.parcel = amItem.parcel;
                    } else {
                        for (var t = 0; t < amItem.tasks.length; t++) {
                            var task = amItem.tasks[t];
                            if (task.parcel) {
                                tObj.parcel = task.parcel;
                            }
                        }
                    }
                    _bbs.$deleteParcelMission(tObj);
                }
            }

            tempArr.push(tObj);
        }

        this.litfCommander.activeMissionsTempStorage = tempArr;
    }
}
