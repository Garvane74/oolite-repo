
this.$start = function() {
    _litf.litfCommander.vars = {
        currentStep: null,
        lastStepId: null
    };
}

///////////////////////////////////////////////////////////////////////////////

$scope.getPage = function(pageId) {
    console.log(">> getPage " + pageId);

    var page = null;

    for (var i = 0; i < $scope.data.story.length; i++) {
        var item = $scope.data.story[i];
        if (item.id == pageId) {
            page = item;
            break;
        }
    }

    return page;
}

$scope.resolveAction = function(script) {
    console.log(">> resolveAction");

    $scope.checkGameTurn();

    var instructions = script.split(";");
    for (var i = 0; i < instructions.length; i++) {
        var instr = instructions[i];

        var parms = instr.split(" ");
        var keyword = parms[0];

        if (keyword == "add") {
            var v_name = parms[1];
            var v_val = 0;

            if (parms[2] == "rand") {
                v_val = $scope.random100();
            } else {
                v_val = parseInt(parms[2]);
            }

            if ($scope.data.vars[v_name] != undefined) {
                $scope.data.vars[v_name] = $scope.data.vars[v_name] + v_val;
                console.log("add :: " + v_name + " = " + $scope.data.vars[v_name]);
            } else {
                console.log("la variabile " + v_name + " non esiste!");
            }
        } else if (keyword == "sub") {
            var v_name = parms[1];
            var v_val = 0;

            if (parms[2] == "rand") {
                v_val = $scope.random100();
            } else {
                v_val = parseInt(parms[2]);
            }

            if ($scope.data.vars[v_name] != undefined) {
                $scope.data.vars[v_name] = $scope.data.vars[v_name] - v_val;
                console.log("sub :: " + v_name + " = " + $scope.data.vars[v_name]);
            } else {
                console.log("la variabile " + v_name + " non esiste!");
            }
        } else if (keyword == "set") {
            var v_name = parms[1].substring(1);
            var v_val = 0;

            if (parms[2] == "rand") {
                v_val = $scope.random100();
            } else {
                v_val = parseInt(parms[2]);
            }

            if ($scope.data.vars[v_name] != undefined) {
                $scope.data.vars[v_name] = v_val;
                console.log("set :: " + v_name + " = " + $scope.data.vars[v_name]);
            } else {
                console.log("la variabile " + v_name + " non esiste!");
            }
        } else if (keyword == "go") {
            var goId = parseInt(parms[1]);
            console.log("go " + goId);

            $scope.goPage(goId);
            return;
        } else if (keyword == "back") {
            var goId = $scope.data.previousPageId;
            console.log("back (go " + goId + ")");

            $scope.goPage(goId);
            return;
        } else if (keyword == "end") {
            // Fine della partita
            console.log("end");
            localStorage.setItem("lastGame", JSON.stringify($scope.data));
            Utils.go("gameover");
        }
    }
}

$scope.random100 = function() {
    return (Math.random() * 100);
}

$scope.evalVal = function(value) {
    if (value.indexOf("$") == 0) {
        // variabile
        var v_name = value.substring(1);
        return $scope.data.vars[v_name];
    } else if (value == "rand") {
        // Numero casuale da 0 a 100
        return $scope.random100();
    } else {
        // numero
        return parseInt(value);
    }
}

$scope.resolveScript = function(script) {
    console.log(">> resolveScript :: " + script);

    var result = true;

    var instructions = script.split(";");
    for (var i = 0; i < instructions.length; i++) {
        var instr = instructions[i];
        console.log("instr = " + instr);

        var parms = instr.split(" ");
        var keyword = parms[0];

        if (keyword.indexOf("if") == 0) {
            var v_a = $scope.evalVal(parms[1]);
            var v_b = $scope.evalVal(parms[2]);
            
            if (keyword == "if" && v_a != v_b) {
                result = false;
            } else if (keyword == "if>" && v_a <= v_b) {
                result = false;
            } else if (keyword == "if<" && v_a >= v_b) {
                result = false;
            } else if (keyword == "if>=" && v_a < v_b) {
                result = false;
            } else if (keyword == "if<=" && v_a > v_b) {
                result = false;
            } else if (keyword == "if!=" && v_a == v_b) {
                result = false;
            }
        }
    }

    console.log("<< " + result);
    return result;
}

$scope.storePreviousPage = function(pageId, forgetPageFlag) {
    if (forgetPageFlag == undefined || forgetPageFlag == null || forgetPageFlag == false) {
        // Memorizza l'id della pagina precedente nell'array delle pagine visitate
        var alreadyVisited = Utils.contains($scope.data.visited, $scope.data.currentPageId);            
        if (alreadyVisited == false && pageId != $scope.data.currentPageId) {
            $scope.data.visited.push($scope.data.currentPageId);
        }

        // Conserva id dell'ultima pagina visitata
        $scope.data.previousPageId = $scope.data.currentPageId;
    }
}

$scope.goPage = function(pageId) {
    console.log(">> goPage(" + pageId + ")");

    var pageData = $scope.getPage(pageId);

    // Ritorna in cima alla pagina
    $ionicScrollDelegate.scrollTop(true);            

    // Gestione pagina precedente
    $scope.storePreviousPage(pageId, pageData.forgetPage);

    $scope.data.currentPageId = pageId;

    $scope.checksManagement(pageData);

    var displayText = $scope.textManagement(pageData, pageId);

    var displayChoices = $scope.choiceManagement(pageData);

    // Dati sulla pagina attuale

    $scope.currentPage = {
        title: pageData.title,
        image: pageData.image,
        text: displayText,
        choices: displayChoices
    };
}

$scope.checksManagement = function(pageData) {
    var checks = $scope.data.common.checks;
    //console.log("checks = " + JSON.stringify(checks));

    for (var c = 0; c < checks.length; c++) {
        var checkItem = checks[c];

        var action = textItem.substring(textItem.indexOf("]") + 1);
        var condition = textItem.substring(1, textItem.indexOf("]"));

        var result = $scope.resolveScript(condition);
        if (result == true) {
            $scope.resolveAction(action);
        }
    }
}

$scope.textManagement = function(pageData, pageId) {
    // Analizza testo e sceglie quali visualizzare

    var textArr = pageData.text;
    if (pageData.short != undefined && Utils.contains($scope.data.visited, pageId) == true) {
        textArr = pageData.short;
    }

    var displayText = "";

    for (var t = 0; t < textArr.length; t++) {
        var textItem = textArr[t];
        if (textItem.indexOf("[") == 0) {
            // Testo con condizione

            var text = textItem.substring(textItem.indexOf("]") + 1);
            var script = textItem.substring(1, textItem.indexOf("]"));
            console.log("script = " + script);

            var result = $scope.resolveScript(script);
            if (result == true) {
                displayText += "<p>" + text + "</p>";
            }
        } else {
            displayText += "<p>" + textItem + "</p>";
        }
    }

    // Aggiunge il testo comune ad ogni pagina

    textArr = $scope.data.common.text;

    for (var t = 0; t < textArr.length; t++) {
        var textItem = textArr[t];
        if (textItem.indexOf("[") == 0) {
            // Testo con condizione

            var text = textItem.substring(textItem.indexOf("]") + 1);
            var script = textItem.substring(1, textItem.indexOf("]"));

            var result = $scope.resolveScript(script);
            if (result == true) {
                displayText += "<p>" + text + "</p>";
            }
        } else {
            displayText += "<p>" + textItem + "</p>";
        }
    }

    // Sostituisce le stringhe variabili con i rispettivi valori

    var replace = true;
    while (replace == true) {
        var beginvar = displayText.indexOf("[[");
        if (beginvar == -1) {
            replace = false;
        } else {
            var endvar = displayText.indexOf("]]", beginvar + 2);
            
            var v_name = displayText.substring(beginvar + 2, endvar);
            var v_val = $scope.data.vars[v_name];
            
            var v_replace = "[[" + v_name + "]]";

            displayText = displayText.replace(v_replace, v_val);
        }
    }

    return displayText;
}

$scope.choiceManagement = function(pageData) {
    // Analizza le scelte e sceglie quali visualizzare

    var choiceArr = pageData.choices;
    var displayChoices = [];
    for (var c = 0; c < choiceArr.length; c++) {
        var choiceItem = choiceArr[c];
        var result = false;

        if (choiceItem.requisite != undefined) {
            result = $scope.resolveScript(choiceItem.requisite);
        } else {
            result = true;
        }

        if (result == true) {
            displayChoices.push(choiceItem);
        }
    }

    //  Scelte comuni ad ogni pagina

    choiceArr = $scope.data.common.choices;
    console.log("choices = " + JSON.stringify(choiceArr));
    for (var c = 0; c < choiceArr.length; c++) {
        var choiceItem = choiceArr[c];
        var result = false;

        if (choiceItem.requisite != undefined) {
            result = $scope.resolveScript(choiceItem.requisite);
        } else {
            result = true;
        }

        if (result == true) {
            displayChoices.push(choiceItem);
        }
    }

    return displayChoices;            
}
