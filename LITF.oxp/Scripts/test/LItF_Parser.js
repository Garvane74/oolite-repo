/* eslint-disable semi, no-multi-spaces, quotes, indent, no-tabs, yoda */
/* global worldScripts, player, system */

"use strict";

this.name	     = "LITF_Parser";
this.author	     = "BeeTLe BeTHLeHeM";
this.copyright	 = "2018 BeeTLe BeTHLeHeM";
this.description = "Dialog Scripting Parser";
this.version	 = "0.10.0";
this.licence	 = "CC BY-NC-SA 4.0";

var _litf = null;
var _co   = null;

this.$init = function () {
    _litf = worldScripts.LITF;

    _co = _litf.LITFInstances.co;
}

this.$parseStepText = function (textArr) {
    var text = "";

    for (var t = 0; t < textArr.length; t++) {
        var display = true;

        var openBracketPos = textArr[t].indexOf("[");
        if (openBracketPos === 0) {
            var closeBracketPos = textArr[t].indexOf("]");
            var condition = textArr[t].substring(1, closeBracketPos);

            display = this.$parseConditionScript(condition);

            if (display === true) {
                text += textArr[t].substring(closeBracketPos + 1);
            }
        } else {
            text += textArr[t];
        }
    }

    return text;
}

// This method takes an array of choice objects and build a choice array for the game
this.$buildChoiceArray = function (choices) {
    _co.$logInfo(this.name, ">>> buildChoiceArray");

    var choiceArr = {};

    var cKeysArr = _co.$getObjectKeys(choices);

    for (var c = 0; c < cKeysArr.length; c++) {
        var cItem = choices[cKeysArr[c]];

        var cKey = cItem.index + "_" + cKeysArr[c];
        var cValue = cItem.text; // + "|" + cItem.action;

        choiceArr[cKey] = cValue;
    }

    return choiceArr;
}

this.$getChoiceObject = function (label, catalog) {
    _co.$logInfo(this.name, ">>> getChoiceObject :: " + label);

    var lSplit = label.split("_");
    var key = lSplit[1];

    _co.$logDebug(this.name, "key = " + key + " >> " + JSON.stringify(catalog));

    var cObj = catalog[key];
    return cObj;
}

// Parse a string containing multiple actions
this.$parseActionScript = function (actionScript) {
    _co.$logInfo(this.name, ">>> parseActionScript :: " + actionScript);

    var ret = "ok";

    var actionArr = actionScript.split(";");
    _co.$logDebug(this.name, "actions = " + actionArr.length);

    for (var a = 0; a < actionArr.length; a++) {
        ret = this.$parseAction(actionArr[a]);
    }

    return ret;
}

// Parse a single action
this.$parseAction = function (action) {
    _co.$logInfo(this.name, ">>> parseAction :: " + action);

    var scriptVars = _litf.litfCommander.scriptVars;

    var svar, parm;

    var ret = "ok";

    var asData = action.split(" ");
    var keyword = asData[0];

    if (keyword === "ife" || keyword === "ifg" || keyword === "ifl" || keyword === "ifn") {
        action = this.resolveCondition(action);
        asData = action.split(" ");
        keyword = asData[0];
    }

    if (keyword === "add") {
        svar = asData[1];
        parm = this.$parseVar(asData[2]);

        if (scriptVars[svar]) {
            scriptVars[svar] = scriptVars[svar] + parm;
            _co.$logDebug(this.name, "add :: " + svar + " + " + parm + " = " + scriptVars[svar]);
        } else {
            _co.$logSevere(this.name, "ERROR :: Script variable " + svar + " doesn't exist!!");
        }
    } else if (keyword === "sub") {
        svar = asData[1];
        parm = this.$parseVar(asData[2]);

        if (scriptVars[svar]) {
            scriptVars[svar] = scriptVars[svar] - parm;
            _co.$logDebug(this.name, "sub :: " + svar + " - " + parm + " = " + scriptVars[svar]);
        } else {
            _co.$logSevere(this.name, "ERROR :: Script variable " + svar + " doesn't exist!!");
        }
    } else if (keyword === "mul") {
        svar = asData[1];
        parm = this.$parseVar(asData[2]);

        if (scriptVars[svar]) {
            scriptVars[svar] = scriptVars[svar] * parm;
            _co.$logDebug(this.name, "mul :: " + svar + " * " + parm + " = " + scriptVars[svar]);
        } else {
            _co.$logSevere(this.name, "ERROR :: Script variable " + svar + " doesn't exist!!");
        }
    } else if (keyword === "set") {
        svar = asData[1];
        parm = this.$parseVar(asData[2]);

        scriptVars[svar] = parm;
        _co.$logDebug(this.name, "set :: " + svar + " = " + parm);
    } else if (keyword === "go") {
        parm = this.$parseVar(asData[1]);

        scriptVars.prevStepId.push(scriptVars.currentStepId);
        scriptVars.currentStepId = parm;
        ret = "go";
    } else if (keyword === "back") {
        if (scriptVars.prevStepId.length > 0) {
            scriptVars.currentStepId = scriptVars.prevStepId[scriptVars.prevStepId.length - 1];
            scriptVars.prevStepId.splice(scriptVars.prevStepId.length - 1, 1);
            ret = "go";
        } else {
            ret = "error-" + keyword;
        }
    } else if (keyword === "quit") {
        scriptVars.currentStepId = -1;
        scriptVars.prevStepId = [];

        ret = "quit";
    } else if (keyword === "ret") {
        ret = action;
    }

    _co.$logDebug(this.name, "ret = " + ret);

    return ret;
}

// Parse a string containing multiple conditions
this.$parseConditionScript = function (conditionScript) {
    _co.$logInfo(this.name, ">>> parseConditionScript :: " + conditionScript);

    var result = true;

    var conditionArr = conditionScript.split(";");
    _co.$logDebug(this.name, "conditions = " + conditionArr.length);

    for (var c = 0; c < conditionArr.length; c++) {
        if (result === true) {
            result = this.$parseCondition(conditionArr[c]);
        }
    }

    _co.$logDebug(this.name, "result = " + result);

    return result;
}

// Parse a single condition
this.$parseCondition = function (condition) {
    _co.$logInfo(this.name, ">>> parseCondition :: " + condition);

    var result = false;

    var ifParm = condition[0].trim().split(" "); // 0 - if keyword; 1 - parm1; 2 - parm2
    var parm1 = this.$parseVar(ifParm[1]);
    var parm2 = this.$parseVar(ifParm[2]);

    if (ifParm[0] === "ife" && parm1 === parm2) {
        result = true;
    } else if (ifParm[0] === "ifg" && parm1 > parm2) {
        result = true;
    } else if (ifParm[0] === "ifl" && parm1 < parm2) {
        result = true;
    } else if (ifParm[0] === "ifn" && parm1 !== parm2) {
        result = true;
    }

    _co.$logDebug(this.name, "result = " + result);

    return result;
}

// Return the action linked to the condition result
this.$resolveCondition = function (actionScript) {
    _co.$logInfo(this.name, ">>> resolveCondition :: " + actionScript);

    var action = null;

    var ifSplit = actionScript.split("?"); // 0 - if condition; 1 - results

    var result = this.$parseCondition(ifSplit[0]);

    var resSplit = ifSplit[1].split(":"); // 0 - true result; 1 - false result

    if (result === true) {
        action = resSplit[0].trim();
    } else {
        action = resSplit[1].trim();
    }

    _co.$logDebug(this.name, "result = " + result + " >>> " + action);

    return action;
}

// Valida le opzioni in un oggetto, con o senza requisiti
this.$parseChoices = function (choices) {
    var validChoices = {};

    var cKeys = _co.$getObjectKeys(choices);

    for (var k = 0; k < cKeys.length; k++) {
        var key = cKeys[k];
        var choice = choices[key];

        if (choice.requisite) {
            var result = this.$parseConditionScript(choice.requisite);
            if (result) {
                validChoices[key] = choice;
            }
        } else {
            validChoices[key] = choice;
        }
    }

    return validChoices;
}

this.$parseVar = function (pVar) {
    var value = 0;

    pVar = pVar.trim();

    if (pVar === "%roll100") {
        // Random number (1-100)
        value = _co.$roll(1, 100);
    } else if (pVar === "%money") {
        // Player money
        value = player.credits;
    } else if (pVar === "%feedback") {
        // Player feedback
        value = _litf.litfCommander.feedback;
    } else if (pVar === "%systemid") {
        // Current system id
        value = system.ID;
    } else if (pVar === "%government%") {
        // Current system government
        value = system.government;
    } else if (pVar === "%techlevel") {
        // Current system TL
        value = system.techLevel;
    } else if (pVar === "%economy") {
        // Current system economy
        value = system.economy;
    } else if (_litf.litfCommander.scriptVars[pVar]) {
        // Variable value
        value = _litf.litfCommander.scriptVars[pVar];
    } else {
        // Constant value
        value = parseInt(pVar);
    }

    return value;
}
