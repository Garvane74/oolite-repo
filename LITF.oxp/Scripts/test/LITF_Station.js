/* eslint-disable semi, no-multi-spaces, quotes, indent, no-tabs, yoda */
/* global worldScripts, mission, setScreenBackground, system */

"use strict";

this.name	     = "LITF_Station";
this.author	     = "BeeTLe BeTHLeHeM";
this.copyright	 = "2018 BeeTLe BeTHLeHeM";
this.description = "Experimental Station Navigation";
this.version	 = "0.10.0";
this.licence	 = "CC BY-NC-SA 4.0";

var _litf = null;
var _co   = null;
var _mi   = null;

var $goPlace = this.$goPlace;

this.litfNavigation = null;

this.$init = function () {
    _litf = worldScripts.LITF;

    _co = _litf.LITFInstances.co;
    _mi = _litf.LITFInstances.mi;

    //

    var stationData = _litf.litfVars.stationParams;
    var sGroups = _mi.groupsByGov[stationData.government];

    this.litfNavigation = {
        currentLocation: null,
        lastLocation: null,
        stationMapTemplate: this.testStationMap,
        systemTL: system.techLevel,
        stationData: stationData,
        stationGroups: sGroups
    };
}

this.$start = function () {
	this.$goPlace("hangar");
};

this.$goPlace = function (location) {
    var title = "Roaming Around the Station";

    this.litfNavigation.lastLocation = this.litfNavigation.currentLocation;
    this.litfNavigation.currentLocation = location;

    var locationData = this.$getLocation(location);

    var text = "LOCATION: " + locationData.name + "\n\n" + locationData.description;

    var choices = this.$checkChoices(locationData.choices);

    var opts = {
        screenID: "litf_navigation_screen",
        title: title,
        exitScreen: "GUI_SCREEN_INTERFACES",
        message: text,
        choices: choices
    };

    mission.runScreen(opts, this.$executeChoice);

    if (locationData.background) {
        // setScreenBackground(locationData.background);
        setScreenBackground({ name: "images/" + locationData.background, height: 480 });
    }
};

this.$checkChoices = function (choiceArr) {
    var choices = {};
    var value = -1;

    for (var c = 0; c < choiceArr.length; c++) {
        var cObj = choiceArr[c];

        var valid = true;
        if (cObj.requires) {
            var crSplit = cObj.requires.split(",");
            for (var cr = 0; cr < crSplit.length; cr++) {
                var requires = crSplit[cr];
                var rSplit = requires.split(" ");

                if (rSplit[0] === "TL") {
                    if (rSplit[1] === "low") {
                        if (this.litfNavigation.systemTL > 5) {
                            valid = false;
                        }
                    } else if (rSplit[1] === "notlow") {
                        if (this.litfNavigation.systemTL < 6) {
                            valid = false;
                        }
                    } else if (rSplit[1] === "medium") {
                        if (this.litfNavigation.systemTL < 6 || this.litfNavigation.systemTL > 9) {
                            valid = false;
                        }
                    } else if (rSplit[1] === "high") {
                        if (this.litfNavigation.systemTL < 10) {
                            valid = false;
                        }
                    }
                } else if (rSplit[0] === "group") {
                    value = parseInt(rSplit[1]);

                    if (this.litfNavigation.stationGroups.indexOf(value) === -1) {
                        valid = false;
                    }
                }
            }
        }

        if (valid === true) {
            choices[cObj.label] = cObj.text;
        }
    }

    return choices;
}

this.$getLocation = function (locationId) {
    locationId = locationId.toLowerCase();

    _co.$logInfo(this.name, ">>> getLocation : " + locationId);

    var stationMap = this.litfNavigation.stationMapTemplate;
    for (var l = 0; l < stationMap.locations.length; l++) {
        var loc = stationMap.locations[l];
        _co.$logDebug(this.name, "[" + l + "] " + loc.id);
        if (loc.id === locationId) {
            return loc;
        }
    }

    return null;
}

this.$executeChoice = function (choice) {
    var cSplit = choice.split("_"); // choice index _ command _ parameters...

    var command = cSplit[1];
    if (command === "QUIT") {
        _litf.$LITFinterface();
    } else if (command === "GOTO") {
        $goPlace(cSplit[2]);
    }
}

// -----------------------------------------------------------------------------

this.testStationMap = {
    locations: [
        {
            id: "hangar",
            name: "Hangar",
            description: "You see your ship, parked. There are other ships, in different conditions. You see a few of them being examined by the station droids, floating slowly around the hull detecting breaches.",
            background: "bg_hangar.png",
            choices: [
                { label: "01_QUIT", text: "Embark your ship." },
                { label: "02_GOTO_DOCKS", text: "Go to the Docks." }
            ]
        },
        {
            id: "docks",
            name: "Docks",
            description: "There's a lot of movement. You see droids moving crates and containers around, loading and offloading ships.",
            // background: "bg_docks.png",
            choices: [
                { label: "01_GOTO_LIFT", text: "Go to the Station Lift" },
                { label: "02_GOTO_HANGAR", text: "Enter the Hangar." },
                { label: "03_GOTO_STORAGE", text: "Enter the Storage Area.", requires: "TL medium" },
                { label: "04_GOTO_REPAIR", text: "Enter the Maintenance Area.", requires: "TL high" }
            ]
        },
        {
            id: "storage",
            name: "Storage Area",
            description: "You see an endless row of containers of various colors and size.",
            // background: "bg_storage.png",
            choices: [
                { label: "01_GOTO_DOCKS", text: "Go to the Docks." }
            ]
        },
        {
            id: "repair",
            name: "Maintenance Area",
            description: "Several crews of people are repairing different ships.",
            // background: "bg_repair.png",
            choices: [
                { label: "01_GOTO_DOCKS", text: "Go to the Docks." }
            ]
        },
        {
            id: "lift",
            name: "Station Lift Tube",
            description: "The lift tube has several cabins going up and down to different levels. People come and go, alone and in groups. There's a continuous activity here, and a constant background noise.",
            // background: "bg_lift.png",
            choices: [
                { label: "01_GOTO_DOCKS", text: "Take a cabin to the Docks Level." },
                { label: "02_GOTO_AGRINDUSTRIAL", text: "Take a cabin to the AgrIndustrial Level.", requires: "group 10" },
                { label: "03_GOTO_CIVIC", text: "Take a cabin to the Civic Level." },
                { label: "04_GOTO_ENTERTAINMENT", text: "Take a cabin to the Entertainment Level." },
                { label: "05_GOTO_TOURISM", text: "Take a cabin to the Tourism Level.", requires: "group 6" },
                { label: "06_GOTO_ADMIN", text: "Take a cabin to the Administrative Level." }
            ]
        },
        {
            id: "civic",
            name: "Civic Level Hub",
            description: "You're at the entrance of the civic level, where the station permanent and semi-permanent population resides outside of work.",
            // background: "bg_civic.png",
            choices: [
                { label: "01_GOTO_LIFT", text: "Go to the Station Lift" },
                { label: "02_GOTO_HIVES", text: "Go to the Common Hives", requires: "TL low" },
                { label: "03_GOTO_APARTMENTS", text: "Go to the Apartments Complex", requires: "TL notlow" },
                { label: "04_GOTO_LIBRARY", text: "Go to the Public Library", requires: "group 1" },
                { label: "05_GOTO_SQUARE", text: "Go to the Public Square", requires: "group 5" },
                { label: "06_GOTO_MEGAMALL", text: "Go to the MEGAMall", requires: "group 7,TL notlow" }
            ]
        }
    ]
}
