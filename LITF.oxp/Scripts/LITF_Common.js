/* eslint-disable semi, no-multi-spaces, quotes, indent, no-tabs, yoda */
/* global worldScripts, player, log, system, clock, galaxyNumber */

"use strict";

this.name        = "LITF_Common";
this.author      = "BeeTLe BeTHLeHeM";
this.copyright   = "2018 BeeTLe BeTHLeHeM";
this.description = "Library of common methods";
this.version     = "0.10.0";
this.licence     = "CC BY-NC-SA 4.0";

// Groups labels

this.groupsLabels = ["GalCop", "GalBook", "GalChurch", "GalShady", "GalMedical", "GalCivic", "GalTourism", "GalShopping", "GalNature", "GalCulture", "GalGovernment", "GalDebug"];

// Sub-commodity and associated commodity (for reference price)

this.commodityReference = {
	"food": [ "Grain", "Generic Foods", "Food Dispensers", "Medicines", "Farm Animals", "Colture Plants", "Junk Foods" ],
	"textiles": [ "Textiles" ],
	"machinery": [ "Factory Equipment", "Mining Equipment", "Robo-Servants", "Robo-Workers", "Robo-Fighters", "Robo-Pleasures", "Robo-Medics" ],
	"gem_stones": [ "Industrial Gems" ],
	"luxuries": [ "Luxury Foods", "Electric Pets", "Artwork", "Games", "Movies", "Holographics", "Home Entertainment", "Books", "Furniture", "Paleo-Arts", "Archeo-Sims", "Relics", "Religious Texts", "Wood", "Dream Cards", "Hacked Dream Cards" ],
	"firearms": [ "Weaponry", "Combat Drones", "Recon Drones" ],
	"platinum": [],
	"slaves": [ "Slaves", "Black Market Organs", "Altered Embryos", "Counterfeit DNA Sequences" ],
	"furs": [ "Furs" ],
	"radioactives": [ "Plutonium", "Uranium", "Advanced Fuels", "Radium", "Thorium", "Hazardous Waste" ],
	"alien_items": [ "Holy Symbols", "Prayer Books", "Fossils", "Stolen Cargos" ],
	"alloys": [ "Construction Materials", "Pre-Fabs", "Space Salvage", "Plastics", "Synthetics" ],
	"liquor_wines": [ "Liquors" ],
	"narcotics": [ "Brilliance", "Tobacco", "Cheap Spice", "Unlicensed Pharmaceuticals", "Contaminated Products" ],
	"minerals": [ "Iron", "Tungsten", "Nickel", "Quartz" ],
	"computers": [ "Communication Equipment", "Computers", "Home Appliances", "Medical Equipment", "Music Equipment", "Industrial Capacitors", "Artificial Limbs", "Artificial Organs", "Neural Interfaces", "Spy Microchips" ],
	"gold": [ "Gold" ]
};

// Sub-commodity list by group

this.commodityGroups = [
	[ "Weaponry", "Robo-Servants", "Communication Equipment", "Combat Drones", "Recon Drones" ], // GalCop
	[ "Books" ], // GalBook
	[ "Relics", "Religious Texts", "Prayer Books", "Holy Symbols" ], // GalChurch
	[ "Plutonium", "Uranium", "Radium", "Thorium", "Slaves", "Brilliance", "Tobacco", "Cheap Spice", "Liquors", "Weaponry", "Industrial Gems", "Gold", "Platinum", "Robo-Fighters", "Robo-Pleasures", "Hacked Dream Cards", "Space Salvage", "Combat Drones", "Recon Drones", "Black Market Organs", "Altered Embryos", "Counterfeit DNA Sequences", "Stolen Cargos", "Spy Microchips", "Unlicensed Pharmaceuticals", "Hazardous Waste", "Contaminated Products" ], // GalShady
	[ "Medicines", "Robo-Medics", "Medical Equipment", "Artificial Limbs", "Artificial Organs", "Neural Interfaces" ], // GalMedical
	[ "Generic Foods", "Luxury Foods", "Textiles", "Liquors", "Electric Pets", "Games", "Holographics", "Home Entertainment", "Furs", "Furniture", "Robo-Servants" ], // GalCivic
	[ "Plants", "Robo-Servants", "Luxury Foods", "Electric Pets", "Movies", "Holographics", "Furniture", "Liquors", "Tobacco" ], // GalTourism
	[ "Junk Foods", "Grain", "Food Dispensers", "Textiles", "Liquors", "Luxury Foods", "Wood", "Computers", "Home Appliances", "Furs", "Dream Cards" ], // GalShopping
	[ "Farm Animals", "Colture Plants", "Paleo-Arts", "Archeo-Sims" ], // GalNature
	[ "Paleo-Arts", "Archeo-Sims", "Fossils", "Artwork", "Movies", "Holographics", "Music Equipment" ], // GalCulture,
	[ "Generic Foods", "Factory Equipment", "Mining Equipment", "Weaponry", "Construction Materials", "Communication Equipment" ], // GalGovernment
	[ "Books", "Relics", "Liquors", "Medicines", "Electric Pets", "Computers" ], // GalDebug
	[ "Grain", "Generic Foods", "Food Dispensers", "Medicines", "Farm Animals", "Colture Plants", "Junk Foods", "Textiles", "Factory Equipment", "Mining Equipment", "Robo-Servants", "Robo-Workers", "Robo-Fighters", "Robo-Pleasures", "Robo-Medics", "Industrial Gems", "Luxury Foods", "Electric Pets", "Artwork", "Games", "Movies", "Holographics", "Home Entertainment", "Books", "Furniture", "Paleo-Arts", "Archeo-Sims", "Relics", "Religious Texts", "Wood", "Dream Cards", "Hacked Dream Cards", "Weaponry", "Slaves", "Furs", "Plutonium", "Uranium", "Radium", "Thorium", "Advanced Fuels", "Holy Symbols", "Prayer Books", "Fossils", "Construction Materials", "Pre-Fabs", "Space Salvage", "Synthetics", "Plastics", "Liquors", "Brilliance", "Tobacco", "Spice", "Iron", "Tungsten", "Nickel", "Quartz", "Communication Equipment", "Computers", "Home Appliances", "Medical Equipment", "Music Equipment", "Gold", "Industrial Capacitors" ] // EVERYTHING
];

// System governments labels

this.govLabels = ["Anarchy", "Feudal", "Multigovernmental", "Dictatorship", "Communist", "Confederacy", "Democracy", "Corporate"];

var _litf = null;

this.$init = function () {
	_litf = worldScripts.LITF;
}

// Print a text message on the Oolite log file
this.$logSevere = function (file, msg) {
	this.$printLog(file, "[SEVERE] " + msg, 3);
}

this.$logDebug = function (file, msg) {
	this.$printLog(file, "[DEBUG] " + msg, 2);
}

this.$logInfo = function (file, msg) {
	this.$printLog(file, "[INFO] " + msg, 1);
}

// this.$printLog = function (msg, level) {
this.$printLog = function (file, msg, level) {
	if (!_litf.litfVars) { return; }
	if (_litf.litfVars.debug.logLevel === 0) { return; }
	if (_litf.litfVars.debug.logLevel > level) { return; }

	log(file, msg);
}

//

this.addPlayerLog = function (msg) {
	var pLog = clock.clockString + " [" + system.name + "] " + msg;
	_litf.litfCommander.bbsLogs.push(pLog);
}

this.$decodeGroup = function (groupId) {
	var groupLabel = this.groupsLabels[groupId];
	return groupLabel;
}

this.$encodeGroup = function (groupLabel) {
	var groupId = this.groupsLabels.indexOf(groupLabel);
	return groupId;
}

// Replace all occurences of a string in a text
this.$replaceAll = function (find, replace, str) {
	this.$logInfo(this.name, ">>> replaceAll");

	return str.split(find).join(replace);
};

// Generates a random number between two limits
this.$roll = function (min, max) {
	// this.$logInfo(this.name, ">>> roll");

	if (min > max) {
		var buf = min;
		min = max;
		max = buf;
	}

	var rv = Math.floor(Math.random() * (max - min + 1)) + min;
	return rv;
};

// Choose a random system in the input range, that it isn't the current player system
this.$randomSystem = function (maxSystemDistance) {
	this.$logInfo(this.name, ">>> randomSystem :: " + maxSystemDistance);

	var valid = false;
	var counter = 0;

	var sysObj    = null; // System object

	while (valid === false) {
		var rand = Math.floor(Math.random() * 256);

		var distance = this.$getSystemRoute(system.ID, rand).distance;

		if (maxSystemDistance === -1 || distance < maxSystemDistance) {
			var sName = System.systemNameForID(rand);
			if (sName !== system.name) {
				sysObj = {
					id: rand,
					name: sName,
					distance: distance.toFixed(2)
				};
				valid = true;
			}
		}

		counter++;

		if (counter > 50 && maxSystemDistance !== -1) {
			counter = 0;

			maxSystemDistance += 0.5;

			if (maxSystemDistance > 20) {
				this.$logDebug(this.name, "enlarging random extraction to every system in the galaxy");
				maxSystemDistance = -1;
			} else {
				this.$logDebug(this.name, "enlarging random extraction to " + maxSystemDistance);
			}
		}
	}

	this.$logDebug(this.name, "randomSystem << " + JSON.stringify(sysObj));

	return sysObj;
};

// Check if a specific time has still to come or not
this.$isTimeExpired = function (startingTime, duration) {
	this.$logInfo(this.name, ">>> isTimeExpired");

	var expired = false;

	var deadline = startingTime + duration;

	if (clock.seconds > deadline) {
		expired = true;
	}

	return expired;
}

// Calculate time from "now" to the specified time, and format the resulting text
this.$formatTime = function (time, remaining) {
	this.$logInfo(this.name, ">>> formatTime");

	if (remaining === true) {
		time = time - clock.seconds;
	}

	var dataTime = this.$secondsData(time);

	var formattedTime = "";

	formattedTime += dataTime.onlyHours + " hours, ";
	formattedTime += dataTime.minutes + " minutes, ";
	formattedTime += dataTime.seconds + " seconds"

    return formattedTime;
}

// Convert a value in seconds in day/hours/only hours/minute/seconds
this.$secondsData = function (seconds) {
	this.$logInfo(this.name, ">>> secondsData");

	var minute = 60;
    var hour = 60 * minute;
    var day = 24 * hour;

    var dd = 0;
    var hh = 0;
    var mm = 0;

	while (seconds > day) {
        dd++;
        seconds -= day;
    }

    while (seconds > hour) {
        hh++;
        seconds -= hour;
    }

    while (seconds > minute) {
        mm++;
        seconds -= minute;
    }

	var obj = {
		days: dd,
		hours: hh,
		onlyHours: hh + (dd * 24),
		minutes: mm,
		seconds: Math.round(seconds)
	}

	this.$logDebug(this.name, "obj = " + JSON.stringify(obj));

	return obj;
}

// Fisher-Yates Shuffle algorithm for arrays
this.$shuffleArray = function (array) {
	this.$logInfo(this.name, ">>> shuffleArray");

	var currentIndex = array.length;
	var temporaryValue;
	var randomIndex;

	// While there remain elements to shuffle...
	while (0 !== currentIndex) {
		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;

		// And swap it with the current element.
		temporaryValue      = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex]  = temporaryValue;
	}

	return array;
}

this.$getSubCommodityPrice = function (subCommodity) {
	var scBasePrice = -1;
	var market = player.ship.dockedStation.market;
	var crKeys = this.$getObjectKeys(this.commodityReference);
	for (var k = 0; k < crKeys.length; k++) {
		var scArr = this.commodityReference[crKeys[k]];
		if (scArr.indexOf(subCommodity) !== -1) {
			scBasePrice = market[crKeys[k]].price;
			break;
		}
	}
	return scBasePrice;
}

// Returns an array with all the keys in an associative object
this.$getObjectKeys = function (object) {
	this.$logInfo(this.name, ">>> getObjectKeys");

	var keys = [];
	for (var key in object) {
		if (object.hasOwnProperty(key)) {
			keys.push(key);
		}
	}

	return keys;
}

// Get a literal ranking from a feedback value
this.$getFeedbackRanking = function (feedback) {
	this.$logInfo(this.name, ">>> getFeedbackRanking");

	var rankData = {
		label: null,
		idx: 0
	};

	if (feedback < -100) {
		rankData.label = "Outcast"; // -101 or less
		rankData.idx = -4;
	} else if (feedback < -50) {
		rankData.label = "Criminal"; // -51 to -100
		rankData.idx = -3;
	} else if (feedback < -25) {
		rankData.label = "Unlawful"; // -26 to -50
		rankData.idx = -2;
	} else if (feedback < -10) {
		rankData.label = "Untrusted"; // -11 to -25
		rankData.idx = -1;
	} else if (feedback < 10) {
		rankData.label = "Neutral"; // 9 to -10
		rankData.idx = 0;
	} else if (feedback < 25) {
		rankData.label = "Appreciated"; // 24 to 10
		rankData.idx = 1;
	} else if (feedback < 50) {
		rankData.label = "Helpful"; // 49 to 25
		rankData.idx = 2;
	} else if (feedback < 100) {
		rankData.label = "Trusted"; // 99 to 50
		rankData.idx = 3;
	} else {
		rankData.label = "Renowned"; // 100 or more
		rankData.idx = 4;
	}

	return rankData;
}

// Information about a route between two systems
this.$getSystemRoute = function (system1, system2) {
	var infoS1 = System.infoForSystem(galaxyNumber, system1);
	var infoS2 = System.infoForSystem(galaxyNumber, system2);

	var route = infoS1.routeToSystem(infoS2); // route, distance, time
	return route;
}

// Get the general id and increase its value to avoi duplicates
this.$getGeneralId = function () {
	this.$logInfo(this.name, ">>> getGeneralId");

	var generalId = _litf.litfVars.generalId;

	_litf.litfVars.generalId = _litf.litfVars.generalId + 1;

	return generalId;
}

this.$setPlayerFeedback = function (add) {
	this.$logInfo(this.name, ">>> setPlayerFeedback");

	var pFeedback = _litf.litfCommander.feedback;

	pFeedback = pFeedback + add;

	// Check limits

	if (pFeedback > 150) { pFeedback = 150; }
	if (pFeedback < -150) { pFeedback = -150; }

	_litf.litfCommander.feedback = pFeedback;
}

this.$updateStatistics = function (property, groupId, value) {
	this.$logInfo(this.name, ">>> updateStatistics :: " + property + "[" + groupId + "] << " + value);

	var sProperty = _litf.litfCommander.statistics[property];
	_litf.litfCommander.statistics[property][groupId] = sProperty[groupId] + value;
}

this.$padLeft = function (padNumber, padLength, padChar) {
	var numStr = "" + padNumber;

	while (numStr.length < padLength) {
		numStr = padChar + numStr;
	}

	return numStr;
}

// Debug //
this.$exportMissionData = function () {
	this.$logInfo(this.name, ">>> exportMissionData");

	var _md = worldScripts.MissionData;

    var csv = "";
    var separator = "#";

    var k, m, mObj;

    var keyArr = [ "groupId", "typeId" ];

    // First reading - get every key
    for (m = 0; m < _md.missionStore.length; m++) {
        mObj = _md.missionStore[m];
        var mKeys = this.$getObjectKeys(mObj);

        for (k = 0; k < mKeys.length; k++) {
            if (keyArr.indexOf(mKeys[k]) === -1) {
                keyArr.push(mKeys[k]);
            }
        }
    }

    // Header row
    for (k = 0; k < keyArr.length; k++) {
        csv += keyArr[k];
        if (k < keyArr.length - 1) { csv += separator; }
    }
    csv += "\n";

    // Second reading - all the data
    for (m = 0; m < _md.missionStore.length; m++) {
        mObj = _md.missionStore[m];

        // var mRef = _co.$getMissionReference(mObj.type);
        csv += mObj.type.groupId + separator + mObj.type.typeId + separator;

        for (k = 0; k < keyArr.length; k++) {
            var value = mObj[keyArr[k]];
            if (value) {
                csv += value;
            }
            if (k < keyArr.length - 1) { csv += separator; }
        }
        csv += "\n";
    }

    this.$logInfo(this.name, "--------------------------------------------------\n\n" + csv + "\n\n--------------------------------------------------");
}
