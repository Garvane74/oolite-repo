# Oolite - Life in the Frontier - REVIVAL

## A little self-referential introduction

A few years ago I started developing an OXP for Oolite that promised more action for the player when docked to a station. In particular, the OXP allowed the player to disembark his own ship and walk through several different areas of the station with a mechanic similar to a CYOA game.

The project was ambitious, as I wanted to integrate it with new mission templates, RPG mechanics and so on. On top of this, I suddenly received a lot of (real) work on my shoulder, a project that kept me super-busy for almost a year and caused me a lot of stress. I abandoned the OXP idea and vanished from the forums (shame on me).

Some time ago I've seen my unfinished OXP on the Oolite expansion manager list and I felt ashamed - I was not proud of the state of my work. So I promised to start again, and complete it.

And here I am.

## Life in the Frontier - REVIVAL

Several concepts are changed since the old, original version of the OXP. I decided to follow a modular approach because I wish to lower the general complexity, and I splitted the project in three phases.

**PHASE 1**

- This phase revolve around providing the player a number of procedural missions to complete, varied in scope and description, to simulate a system BBS similar to the one in Frontier:Elite. The BBS will contain various items, missions, ads and flavour text, that will change depending on the system government type.
- The player will navigate the list and eventually open a details screen for some items. IE the detail screen for a mission allow the player to get more info or accept the missions - think again to Frontier:Elite.
- There will be different mission templates.
- Completing missions will increase the player BBS feedback, and in turn this will allow for more rewards in missions, with destinations far away from the starting system.
- Missions will have a deadline - failing a deadline the player will suffer a feedback penalty.
- A status screen will allow the player to see his feedback and the current active missions.

**PHASE 2**

- In this phase a new mission template will appear: LOCAL. People will ask the player to complete tasks on the station.
- The player will have the chance to explore the station to complete tasks.
- Every station will have a specific list of components, depending on government, TL and industrial level.
- When the player complete or fail a mission, this could have consequences: major events will be reported in the BBS via news items, and in turn this events could generate new missions for the player.
- The final goal of this phase is the generation of scripted or semi-scripted missions built on a sequence of tasks, based on the phase 1 missions and the LOCAL missions.

**PHASE 3**

- Introduction of player properties - like RPG attributes, to represent his physical and mental condition.
- Missions or major events could influence attributes, or affix statuses to the player.
- Special places on stations can cure affixes, or attach new ones to the player.
- Introduction of a simple player inventory.
- Markets on stations to buy or sell items from inventory.
- Player reputation with various groups (defined in Phase 1).
- Interface reorganization to display all the necessary infos.
- Final integration with the previous phases features.
