"use strict";

this.name   = "LITF_Mission";
this.author = "BeeTLe BeTHLeHeM";
this.copyright  = "2015 BeeTLe BeTHLeHeM";
this.description= "Life In The Frontier - Mission";
this.version    = "0.8";
this.licence    = "CC BY-NC-SA 4.0";

///////////////////////////////////////

this.personalEffects = [ "a box full of ties", "a couple of suitcases full of clothes", "150 pairs of socks", "a huge decorated and colorful rug", "a bunch of towels", "a box full of shoes", "a dozen of tennis rackets", "a fishing rod complete with accessories", "a case with various liquors bottles", "an old typewriter missing some keys", "a mailbag full of magazines", "a little case containing a number of family pictures", "an old digital watch", "a box full of CDs", "a case containing cables, a power supply, an old cassette player and a black keyboard with rubber keys" ]; 

this.$createPackageMission = function(mType, mLocation) {
    
    if (mType == "TRANSPORT") {

        if (mLocation == "LOUNGE") {
            // TODO Create transport-package mission for passengers lounge            
        }
        
    }

};



this.$randomSystem = function() {
    var valid = 0;
    var sName = "";
    while (valid == 0) {
        var rand = Math.floor(Math.random() * 256);
        sName = System.systemNameForID(rand);
        if (sName != system.name) { valid = 1;}
    }

    return sName;
};


this.$randomSystemAtMinDistance = function(minDist) {
    var valid = 0;
    var sName = "";
    while (valid == 0) {
        var rand = Math.floor(Math.random() * 256);
        sName = System.systemNameForID(rand);
        if (sName != system.name) {
            var dist = this.$getTravelDistance(sName);
            if (dist >= minDist) { 
                valid = 1;
            }
        }
    }

    return sName;
};


this.$getTravelDistance = function(destSystemName) {
    var _co = worldScripts.LITF_Common;
        
    var systemNow = system.ID;
    var systemDest = System.systemIDForName(destSystemName);

    var route = System.infoForSystem(galaxyNumber, systemNow).routeToSystem(System.infoForSystem(galaxyNumber, systemDest));

    var distance = _co.$formatNumber(route.distance);

    return distance;
};


this.$getTravelTime = function(destSystemName) {
    var _co = worldScripts.LITF_Common;
    
    var systemNow = system.ID;
    var systemDest = System.systemIDForName(destSystemName);

    var route = System.infoForSystem(galaxyNumber, systemNow).routeToSystem(System.infoForSystem(galaxyNumber, systemDest));

    var time = _co.$formatNumber(route.time);

    return time;
};
