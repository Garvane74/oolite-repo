"use strict";

this.name	= "LITF_Navigation";
this.author	= "BeeTLe BeTHLeHeM";
this.copyright	= "2015 BeeTLe BeTHLeHeM";
this.description= "Life In The Frontier - Navigation";
this.version	= "0.7.1";
this.licence	= "CC BY-NC-SA 4.0";

///////////////////////////////////////

var LITF_placeLabel = "";

this.LITF_lastPlaceLabel = "";

this.$startNavigation = function() {
	$goPlace("area1");
};


this.$statusScreen = function() {
    var _co = worldScripts.LITF_Common;

    var sText = this.$setStatusText();
    var sChoices = this.$setStatusChoices();

    mission.runScreen(
        { 
            title:"COMMANDER STATUS", 
            message:sText, 
            choices:sChoices 
        }, 
        this.$statusScreenChoice
    );
};

this.$setStatusText = function() {
    var _rpge = worldScripts.LITF_RPGElements;
    var _co = worldScripts.LITF_Common;
    var _litf = worldScripts.LITF;

    var msText = "[COMMANDER RESUME]\n\n";
    
    msText += "Commander " + player.name + ", for the people on the station you're " + _rpge.$getReputationFromValue(_litf.LITF_cmdStats.reputation) + ". ";
    msText += "You consider yourself " + _rpge.$getStanceFromValue(_litf.LITF_cmdStats.stance) + ". ";
    msText += "Your physical shape is " + _rpge.$getStrengthFromValue(_litf.LITF_cmdStats.strength) + ". ";
    msText += "You currently have " + _co.$formatNumber(player.credits) + "cr. ";
    
    if (_litf.LITF_flags.getDrunk == 1) {
        msText += "\n* You're drunk. Hiccups, a bit of nausea and the world swinging left and right.";
    }

    var hDesc = _rpge.$getHealthFromValue(_litf.LITF_cmdStats.health);
    if (hDesc != "") { 
        msText += "\n* " + hDesc; 
    }

    var sDesc = _rpge.$getSicknessFromValue(_litf.LITF_cmdStats.sickness);
    if (sDesc != "") { 
        msText += "\n* " + sDesc; 
    }
    
    msText += "\n";
    
    // Universal Fitness Badge status
    
    var fitBadge = _litf.LITF_flags.fitBadge;
    if (fitBadge > -1) {
        var expired = _co.$checkTimeForExpiration(fitBadge);
        if (expired == 1) {
            msText += "\nYou own an expired Universal Fitness Badge.";
        } else {
            var remain = _co.$formatRemainingTime(fitBadge);
            
            msText += "\nYou own a Universal Fitness Badge: it will expire in " + remain + ".";
        }
    }

    msText += "\n";

    // XXX Contractors and missions
    /*
    if (this.LITF_contractorData != undefined && this.LITF_contractorData != "") {
        var cData = this.LITF_contractorData;

        // Destination system ID, NPC name, mission type, max time
        var cSplit = cData.split("|");

        var remainTime = parseInt(cSplit[3]) - clock.seconds;
        var formattedTime = this.$formatTime(remainTime);

        msText += "\nCONTRACTOR: Meet " + cSplit[1] + " at " + System.systemNameForID(parseInt(cSplit[0])) 
            + " for a job. (" + formattedTime + " before expiring).";
    }

    if (this.LITF_missionData != undefined && this.LITF_missionData != "") {
        var mData = this.LITF_missionData;

        // cargo, pay, systemDestId, maxTime, missionType, systemStartId    
        var mSplit = mData.split("|");

        var remainTime = parseInt(mSplit[3]) - clock.seconds;
        var formattedTime = this.$formatTime(remainTime);

        var cType = mSplit[4].toUpperCase() + " MISSION";

        msText += "\n" + cType + ": for " + mSplit[1] + "cr deliver " + mSplit[0] 
            + " to " + System.systemNameForID(parseInt(mSplit[2])) + " within " + formattedTime + ".";
    }
    */

    //msText += "\n";

    return msText;
};


this.$setStatusChoices = function() {
    var msChoiceList = {};
    msChoiceList["BACK"] = "Back to the Hangar";
    return msChoiceList;
};


this.$statusScreenChoice = function(choice) {
    switch (choice) {
        case "BACK":
            worldScripts.LITF_Navigation.$goPlace("area1");
            break;
    }
};


this.$goPlace = function(pLabel) {
	var _co = worldScripts.LITF_Common;
	var _ec = worldScripts.LITF_EventsCatalog;
	var _rpge = worldScripts.LITF_RPGElements;
	var _litf = worldScripts.LITF;

	// The commander is dying!
	if (_litf.LITF_cmdStats.health >= 5 || _litf.LITF_cmdStats.sickness >= 5) {
		if (pLabel.indexOf("area") == 0) {
			var passoutchance = Math.floor(Math.random() * 100) + 1;
			// The commander pass out!
			if (passoutchance < 50) {
				pLabel = "passout0";
			}
		}
	}

	if (pLabel == "area1" && _litf.LITF_flags.defacedShip == 1) {
		pLabel = "area1_defacedShip";
	}

    LITF_lastPlaceLabel = LITF_placeLabel;
	LITF_placeLabel = pLabel;
	_co.$log("[goPlace] " + LITF_placeLabel);

	var pTitle = "AROUND THE SPACE STATION";

	var LITF_place = eval("this." + pLabel + ";");
	//_co.$log("[goPlace] text = " + LITF_place.text);

	var placeText = "[ " + _rpge.$getReputationFromValue(_litf.LITF_cmdStats.reputation) + " " + player.name + "  |  " + _co.$formatNumber(player.credits) + "cr  |  Wounds: " + _rpge.LITF_healthShort[_litf.LITF_cmdStats.health] + "  |  Health: " + _rpge.LITF_sicknessShort[_litf.LITF_cmdStats.sickness] + " ]\n\n";

    placeText += this.buildLocationText(LITF_place);

	placeText += this.$addRandomHealthSnippet();

	placeText += this.$addRandomStatusSnippet();

	placeText = _co.$replaceConstants(placeText);

    //_co.$log("[goPlace] placeText = " + placeText);

	var placeChoices = _co.$extractChoices(LITF_place);

	mission.runScreen(
		{ 
			title: pTitle, 
			message: placeText, 
			choices: placeChoices 
		},
		this.$selectChoice
	);

	//

	_co.$screenBackground(LITF_place.background);
};

this.buildLocationText = function(pData) {
    var _co = worldScripts.LITF_Common;

    var textCounter = 0;
    var valid = 1;

    var placeText = "";

    var staticText = pData.text;
    if (staticText) {
        // text - static text
        //_co.$log("[buildLocationText] staticText = " + staticText);
        placeText = staticText;
    } else {
        // text0, text1 ... - dynamic text

        while (valid == 1) {
            var t = "text" + textCounter;
            var text = eval("pData." + t + ";");
    
            if (text) {
                _co.$log("[buildLocationText] indexed text " + t + " = " + text);
                var tSplit = text.split("|");
                if (tSplit.length == 1) {
                    // No conditions - always valid, always shown
                    placeText += text;
                } else {
                    // Needs to validate condition
                    var validate = 1;
                    var LITF_items = tSplit[0].split(",");
                    for (var i = 0; i < LITF_items.length; i++) {
                    
                        if (_co.$checkRequirements(LITF_items[i]) == 0) { validate = 0; }
                        _co.$log("[validateChoice] " + LITF_items[i] + " = " + validate);
                    }

                    if (validate == 1) { placeText += tSplit[1]; }
                }
                
                textCounter++;            
            } else {
                valid = 0;
            }
        }
    }
    
    //_co.$log("[buildLocationText] placeText = " + placeText);

    return placeText;
};

this.$addRandomStatusSnippet = function() {
	var _litf = worldScripts.LITF;

    var text = "";
	var chance = Math.floor(Math.random() * 100);

	if (_litf.LITF_flags.getDrunk == 1) {
		if (chance < 10) {
			text = "\n\nYou produce a undignified belch.";
		} else if (chance < 20) {
			text = "\n\nYou stumble on your feet.";
		} else if (chance < 30) {
			text = "\n\nYou hiccup.";
		}
	}

	return text;
};


this.$addRandomHealthSnippet = function() {
	var _litf = worldScripts.LITF;

    var text = "";
	var chance = Math.floor(Math.random() * 100);

	var health = _litf.LITF_cmdStats.health;
	var sickness = _litf.LITF_cmdStats.sickness;

	if (health > 0 && chance < 10) {
		switch (health) {
			case 1: 
				text = "\n\nYou feel your skin slightly itching.";
				break;
			case 2:
				text = "\n\nYou feel a bit of pain while you walk.";
				break;
			case 3:
				text = "\n\nYou feel pain even standing still.";
				break;
			case 4:
				text = "\n\nYou feel great pain, that makes you moan in anguish.";
				break;
			case 5: 
				text = "\n\nYou can't walk straight, trying your best to not fall over. The pain is unbearable.";
				break;
		}
	} else if (sickness > 0 && chance < 20) {
		switch (sickness) {
			case 1: 
				text = "\n\nYou cough a couple of times.";
				break;
			case 2:
				text = "\n\nYou sneeze and feel suddenly cold for a while.";
				break;
			case 3:
				text = "\n\nYou feel nauseated.";
				break;
			case 4:
				text = "\n\nYou feel incredibly tired.";
				break;
			case 5: 
				text = "\n\nYour head spin, you can't walk straight.";
				break;
		}
	}

	return text;
};


this.$selectChoice = function(choice) {
	var _co = worldScripts.LITF_Common;

	_co.$log("[selectChoice] choice = " + choice);

	if (choice == null || choice == "null") { return; }

	var action = choice.replace('c', 'a');
	_co.$log("[selectChoice] action = " + action);

	// Retrieve the associate actions

	var actionData = eval("worldScripts.LITF_Navigation." + LITF_placeLabel + "." + action);

    _co.$executeActions(actionData);

	/*
    var actionsSplit = actionData.split("|");

	// Iterate, parse and execute the actions
	for (var i = 0; i < actionsSplit.length; i++) {
		var action = actionsSplit[i];

        _co.$executeAction(action);
	}
	*/
};


///////////////////////////////////////

this.passout0 = {
	text: "Suddenly you experience a strong vertigo, your legs become unstable and your body get permeated by a glacial coldness. You fall on the floor, uncapable to say anything, and faint while around you can still hear a far echo of worried voices that call for help... after that, darkness.",
	c1: "...",
	a1: "time:3600,14400|paytreatment:health|paytreatment:sickness|goNav:passout1"
};

this.passout1 = {
	text: "Time passes... until you come round, starting to perceive the world around, chaotic and confuse... the lights blinds you, but you can barely move. Someone is near you, and you need to focus completely on $GENDERPOSS$ to understand the words...\n\n'...we managed to get back from the edge of death, basically. In these cases law force us to apply a complete healing process, and confusion aside, you should get well about in twenty minutes. When you feel you can walk again, come to the reception.'\n\nThat was a doctor, and you're in the Medical Center - now you recognize it. You take a deep breath and wait. The world is starting to stabilize...",
	background: "litf_bg_medcenter.png",
	c1: "...",
	a1: "time:600,1200|goNav:passout2"
};

this.passout2 = {
	text: "After a while, against your conjectures, you really feel you can stand up and walk. You're starting to feel better.\n\nYou wear your clothes, and exit the room to the Medical Center reception.",
	background: "litf_bg_medcenter.png",
	c1: "Let's hear the news...",
	a1: "goNav:passout3"
};

this.passout3 = {
	text: "The receptionist smiles as usual. She says that the healing process gone well, and that you are healthy enough again.\n\nAfter this, the receptionist reminds you that the cures weren't free. The price for the process has already been withdrawn from your account, and on top of this there is an additional 200cr for the emergency procedure. Don't worry if you haven't enough money: the station bank system will automatically adjust your bounty for debts insolvence. You can clean your bounty using the Galcop terminal at the precinct.\n\n'Good day, commander.'",
	background: "litf_bg_medcenter.png",
	c1: "Yes, really a good day...",
	a1: "reputation:-15|goNav:area2_2"
};

///////////////////////////////////////

this.area1 = {
    text: "You're in the hangar where your ship is parked. There are other ships, in different conditions. You see a few of them being examined by the station droids, floating slowly around the hull searching for breaches.",
    background: "litf_bg_hangar.png",
    c1: "Go to the Docks",
    a1: "randomAndGo:lobby1",
    c2: "Embark your ship",
    a2: "endNav:0",
    c3: "See your status",
    a3: "showStatus:1"
};

/*
this.lobby1 = {
    text: "You're in the Docks.\n\nYou see droids moving crates and containers off and on ships. The activity is unrelenting.\n\nFrom here:\n- You can go to your ship in the Hangar\n- You can enter the Cargo Area\n- You can take the Lift and go to the Main Concourse, the Recreation Bridge or the Galcop Precinct.\n\n$LOBBYSNIPPET$",
    background: "litf_bg_docks.png",
    c1: "Enter the Hangar",
    a1: "randomAndGo:area1",
    c2: "Enter the Cargo Area",
    a2: "randomAndGo:area1_2",
    c3: "Lift >> Main Concourse",
    a3: "time:120|goNav:lobby2",
    c4: "Lift >> Recreation Bridge",
    a4: "time:240|goNav:lobby3",
    c5: "Lift >> Galcop Precinct",
    a5: "time:360|goNav:lobby4"
};
*/

this.lobby1 = {
    text: "You're in the Docks.\n\nYou see droids moving crates and containers off and on ships. The activity is unrelenting.\n\n$LOBBYSNIPPET$",
    background: "litf_bg_docks.png",
    c1: "Look at the station map",
    //a1: "goNav:map1",
    a1: "goNav:map",
    c2: "Enter the Hangar",
    a2: "randomAndGo:area1",
    c3: "Enter the Cargo Area",
    a3: "randomAndGo:area1_2",
    c4: "Lift >> Main Concourse",
    a4: "time:120|goNav:lobby2",
    c5: "Lift >> Recreation Bridge",
    a5: "time:240|goNav:lobby3",
    c6: "Lift >> Galcop Precinct",
    a6: "time:360|goNav:lobby4"
};



this.area1_defacedShip = {
    text: "You're in the Docks Area.\n\nThere's people going around, and droids moving crates and containers off and on ships. The activity is unrelenting.\n\nYou can see your ship from here, but it's not exactly like you remembered. It seems someone has painted over the hull a series of colorful insults and raunchy words. A worker suggests you use the Area droids to clean the ship before departing - those graffiti could grant you a fine for public indecence.\n\nYou're annoyed but take control of the droids and erase the insults - a task that takes you an hour...",
    c1: "Damn kids...",
    a1: "time:3600|flag_defacedShip:0|goNav:area1"
};


this.area1_2 = {
    text: "You're in the Cargo Area.\n\nDroids transports here all the cargo unloaded from ships, and get back to the ships the containers to load into them. There are dozen of containers of various colors, and the commodities are being put into or removed from them, waiting to be transferred to Storage.\n\nFrom here you can visit the Storage section, or return to the Docks.",
    background: "litf_bg_cargoarea.png",
    c1: "Go to the Docks",
    a1: "randomAndGo:lobby1",
    c2: "Visit the Storage section",
    a2: "goNav:storage0"
};

/*
this.lobby2 = {
    text: "You're in the Main Concourse lobby.\n\nFrom here:\n- You can go to one of three sections of the Concourse:\n  Section A: Passengers Lounge, Hotel\n  Section B: Medical Center, Church of the Cosmic Wanderer, Bookshop\n  Section C: ## DATA NOT FOUND ##\n- You can take the Lift and go to the Docks, the Recreation Bridge or the Galcop Precinct.\n\n$LOBBYSNIPPET$",
    c1: "Enter the Concourse-A section",
    a1: "randomAndGo:area2_1",
    c2: "Enter the Concourse-B section",
    a2: "randomAndGo:area2_2",
    c3: "Enter the Concourse-C section",
    a3: "randomAndGo:area2_3",
    c4: "Lift >> Docks",
    a4: "time:120,goNav:lobby1",
    c5: "Lift >> Recreation Bridge",
    a5: "time:120|goNav:lobby3",
    c6: "Lift >> Galcop Precinct",
    a6: "time:240|goNav:lobby4"
};
*/

this.lobby2 = {
    text: "You're in the Main Concourse lobby.\n\n$LOBBYSNIPPET$",
    c1: "Look at the station map",
    //a1: "goNav:map2",
    a1: "goNav:map",
    c2: "Enter the Concourse-A section",
    a2: "randomAndGo:area2_1",
    c3: "Enter the Concourse-B section",
    a3: "randomAndGo:area2_2",
    c4: "Enter the Concourse-C section",
    a4: "randomAndGo:area2_3",
    c5: "Lift >> Docks",
    a5: "time:120|goNav:lobby1",
    c6: "Lift >> Recreation Bridge",
    a6: "time:120|goNav:lobby3",
    c7: "Lift >> Galcop Precinct",
    a7: "time:240|goNav:lobby4"
};

this.area2_1 = {
    text: "You're in the Concourse-A section.\n\nYou see groups of people walking between the shops and the large decorated plaza.\n\nFrom here you can visit the Passengers Lounge, the Hotel, go to the lobby or to the other Concourse sections.",
    background: "litf_bg_mainconcourse1.png",
    c1: "Exit the Main Concourse",
    a1: "randomAndGo:lobby2",
    c2: "Visit the Passengers Lounge",
    a2: "goNav:plounge0",
    c3: "Visit the Hotel",
    a3: "goNav:hotel0",
    c4: "Enter the Concourse-B section",
    a4: "randomAndGo:area2_2",
    c5: "Enter the Concourse-C section",
    a5: "randomAndGo:area2_3"
};


this.area2_2 = {
    text: "You're in the Concourse-B section.\n\nYou see many people talking to each other. There's a big fountain with benches around.\n\nFrom here you can visit the Church of the Cosmic Wanderer, the Bookshop or the Medical Center for your health-related issues, go to the lobby or to the other Concourse sections.",
    background: "litf_bg_mainconcourse2.png",
    c1: "Exit the Main Concourse",
    a1: "randomAndGo:lobby2",
    c2: "Sit on a bench near the fountain",
    a2: "goNav:area2_4",
    c3: "Visit the Church of the Cosmic Wanderer",
    a3: "goNav:church0",
    c4: "Visit the Bookshop",
    a4: "goNav:bookshop0",
    c5: "Visit the Medical Center",
    a5: "goNav:med0",
    c6: "Enter the Concourse-A section",
    a6: "randomAndGo:area2_1",
    c7: "Enter the Concourse-C section",
    a7: "randomAndGo:area2_3"
};


this.area2_3 = {
    text: "You're in the Concourse-C section.\n\nThere are people working in the gardens. The food processing is automated, and you smell a sweet mix of odours from the facilities.\n\nFrom here you can go to the lobby or to the other Concourse sections.",
    background: "litf_bg_hpgardens.png",
    c1: "Exit the Main Concourse",
    a1: "randomAndGo:lobby2",
    c2: "Enter the Concourse-A section",
    a2: "randomAndGo:area2_1",
    c3: "Enter the Concourse-B section",
    a3: "randomAndGo:area2_2"
};


this.area2_4 = {
    text: "You sit on a bench. The water noise behind you is relaxing, almost hypnotic. You start to zone out, your thoughts going astray... you should rest here for a while...",
    c1: "Better if I go, I don't want to fall asleep here",
    a1: "goNav:area2_2",
    c2: "Stay still. Let's wait some time here",
    a2: "time:1800|goNav:area2_4"
};

/*
this.lobby3 = {
    text: "You're in the Recreation Bridge lobby.\n\nFrom here:\n- You can go to the level sections:\n  Delight Avenue: Local Bar, Wild Worlds Virtual Tour, Museum\n  Olympian Square: Fit Center\n  Mecenate Court: Art Gallery, Opera House, Cinema Hall\n- You can take the Lift and go to the Docks, the Main Concourse or the Galcop Precinct.\n\n$LOBBYSNIPPET$",
    c1: "Enter the Delight Avenue",
    a1: "randomAndGo:area3_1",
    c2: "Enter the Olympian Square",
    a2: "randomAndGo:area3_2",
    c3: "Enter the Mecenate Court",
    a3: "randomAndGo:area3_3",
    c4: "Lift >> Docks",
    a4: "time:240,goNav:lobby1",
    c5: "Lift >> Main Concourse",
    a5: "time:120|goNav:lobby2",
    c6: "Lift >> Galcop Precinct",
    a6: "time:240|goNav:lobby4"
};
*/

this.lobby3 = {
    text: "You're in the Recreation Bridge lobby.\n\n$LOBBYSNIPPET$",
    c1: "Look at the station map",
    //a1: "goNav:map3",
    a1: "goNav:map",
    c2: "Enter the Delight Avenue",
    a2: "randomAndGo:area3_1",
    c3: "Enter the Olympian Square",
    a3: "randomAndGo:area3_2",
    c4: "Enter the Mecenate Court",
    a4: "randomAndGo:area3_3",
    c5: "Lift >> Docks",
    a5: "time:240|goNav:lobby1",
    c6: "Lift >> Main Concourse",
    a6: "time:120|goNav:lobby2",
    c7: "Lift >> Galcop Precinct",
    a7: "time:240|goNav:lobby4"
};

this.area3_1 = {
    text: "You're in Delight Avenue.\n\nPeople joke and laugh, there are expensive clothes around here. You can hear music playing in background.\n\nFrom here you can visit the local Bar, the Museum or the Wild Worlds Virtual Tour, go to the lobby, to Olympian Square or to Mecenate Court.",
    background: "litf_bg_entcorridor.png",
    c1: "Exit the Recreation Bridge",
    a1: "randomAndGo:lobby3",
    //r2: "flag_getDrunk=0",
    c2: "Visit the local Bar",
    a2: "if:flag_getDrunk=0?goNav:bar0;goNav:bar0_1",
    c3: "Visit the museum",
    a3: "goNav:museum0",
    c4: "Visit the Wild Worlds Virtual Tour",
    a4: "goNav:wwvt0",
    c5: "Enter Olympian Square",
    a5: "randomAndGo:area3_2",
    c6: "Enter Mecenate Court",
    a6: "randomAndGo:area3_3"
};


this.area3_2 = {
    text: "You're in Olympian Square.\n\nYou see people with colourful tracksuit, ready to exercise or race against other competitors. The place is filled with different sports areas and with gleeful audiences. Various holograms shows different activities and disciplines.\n\nFrom here you can visit the Fit Center, go to the lobby, to Delight Avenue or to Mecenate Court.",
    background: "litf_bg_entcorridor.png",
    c1: "Exit the Recreation Bridge",
    a1: "randomAndGo:lobby3",
    c2: "Visit the Fit Center",
    a2: "goNav:fit0",
    c3: "Enter Delight Avenue",
    a3: "randomAndGo:area3_1",
    c4: "Enter Mecenate Court",
    a4: "randomAndGo:area3_3"
};


this.area3_3 = {
    text: "You're in Mecenate Court.\n\nThis place is decorated with columns and galleries sculpted in ancient styles. Exotic vines climb the walls in elaborated patterns. In the wide central court you see several people discussing and debating a variety of topics.\n\nFrom here you can visit the Art Gallery, the Opera House or the Cinema Hall, go to the lobby, to Delight Avenue or to Olympian Square.",
    background: "litf_bg_entcorridor.png",
    c1: "Exit the Recreation Bridge",
    a1: "randomAndGo:lobby3",
    r2: "flag_artGallery=0",
    c2: "Visit the Art Gallery",
    a2: "goNav:art0",
    r3: "flag_operaHouse=0",
    c3: "Visit the Opera House",
    a3: "goNav:opera01,opera02,opera03",
    r4: "flag_cinemaHall=0",
    c4: "Visit the Cinema Hall",
    a4: "goNav:cinema0",
    c5: "Enter Delight Avenue",
    a5: "randomAndGo:area3_1",
    c6: "Enter Olympian Square",
    a6: "randomAndGo:area3_2",
};

/*
this.lobby4 = {
    text: "You're in the Galcop Precinct.\n\nFrom here:\n- You can go to the Galcop Facilities\n- You can take the Lift and go to the Docks, the Main Concourse or the Recreation Bridge.\n\n$LOBBYSNIPPET$",
    c1: "Enter the Galcop Facilities",
    a1: "randomAndGo:area4_1",
    c2: "Lift >> Docks",
    a2: "time:360|goNav:lobby1",
    c3: "Lift >> Main Concourse",
    a3: "time:240|goNav:lobby2",
    c4: "Lift >> Recreation Bridge",
    a4: "time:120|goNav:lobby3",
};
*/

this.lobby4 = {
    text: "You're in the Galcop Precinct.\n\n$LOBBYSNIPPET$",
    c1: "Look at the station map",
    //a1: "goNav:map4",
    a1: "goNav:map",
    c2: "Enter the Galcop Facilities",
    a2: "randomAndGo:area4_1",
    c3: "Lift >> Docks",
    a3: "time:360|goNav:lobby1",
    c4: "Lift >> Main Concourse",
    a4: "time:240|goNav:lobby2",
    c5: "Lift >> Recreation Bridge",
    a5: "time:120|goNav:lobby3",
};


this.area4_1 = {
    text: "You're in the Galcop Facilities.\n\nYou see agents responding to calls and going on patrols. A number of desks is busy with reports from station visitors.\n\nImmediately into the Facilities you see a row of Galcop terminals.\n\nFrom here you can enter the Mission Briefing Room, the Detainment Zone and the Pilot Course Room, or exit to the Precinct.",
	c1: "Exit the Galcop Facilities",
	a1: "goNav:lobby4",
	c2: "Go to one of the Galcop terminals",
	a2: "goNav:area4_2",
	c3: "Enter the Mission Briefing Room",
	a3: "goNav:area4_3",
	c4: "Enter the Detainment Zone",
	a4: "goNav:area4_4",
	c5: "Enter the Social Services Zone",
	a5: "goNav:area4_5",
	c6: "Enter the Pilot Course Room",
	a6: "goNav:area4_6"
};

this.area4_2 = {
	text0: "WELCOME TO GALCOP ANONYMOUS REHABILITATION TERMINAL\n\nPUT YOU RIGHT PALM ON THE SENSOR\n\nVERIFICATION COMPLETE\n\nHELLO, COMMANDER,",
	text1: "bounty<1|\n\nYOUR CRIMINAL RECORD IS CLEAN.",
	text2: "bounty>0|\n\nYOU CAN CLEAN YOUR CRIMINAL RECORD FOR $BOUNTYFINE$.",
	text3: "\n\nGOOD DAY COMMANDER",
	background: "litf_bg_terminal.png",
	c1: "Good day, terminal",
	a1: "goNav:area4_1",
    r2: "bounty>0,bountyfine=1",
	c2: "I want to pay and clean my criminal record.",
	a2: "paybountyfine:1|goNav:area4_2"
};

this.area4_3 = {
    text: "The door to the briefing room is closed. An officer tells you that only Galcop associates can join the mission briefings.",
    c1: "Go away",
    a1: "goNav:area4_1"
};

this.area4_4 = {
    text: "You go to the detainment zone gate, but an officer stops before you can enter: 'Sorry, but your legal status in currently under examination. Therefore you are interdicted to this area access, unless you can produce a legitimate and proper payment for this transgression. I believe 30cr should be enough.'",
    r1: "credits>=30",
    c1: "Pay the 'fee'",
    a1: "credits:-30|goNav:area4_41",
    c2: "Go away",
    a2: "goNav:area4_1"
};

this.area4_41 = {
    text: "You are inside a big hall, full of metallic cells in a grid scheme, on multiple floors. You can hear the prisoners complaining and swearing. Galcop drones patrol the area.",
    c1: "Exit",
    a1: "goNav:area4_1"
};

this.area4_5 = {
    text0: "You enter the room, and a galcop android scans your legal status.",
    text1: "bounty=0|'Your criminal record is clean, but you can contribute to the services if you want, as a volunteer.'",
    text2: "bounty>0|'If you want you can endure a session of work to decrease your bounty, as a volunteer.'",
    //r1: "bounty>0",
    c1: "Ok, I want to endure a session",
    a1: "time:14400|reputation:2|goNav:area4_51,area4_51,area4_51,area4_52",
    c2: "Go away",
    a2: "goNav:area4_1"
};

this.area4_51 = {
    text: "You spend the session working on the back alleys of the station, cleaning floors, fixing lights, repairing blocked doors, supervising little service droids. It isn't a fun job, and at the end of the session you're tired, but you have the feeling of doing something useful.",
    c1: "I'll stop for now",
    a1: "bounty:-20|goNav:area4_1"
};

this.area4_52 = {
    text: "During the session you feel tired and careless, and as a result you get into an accident and wound yourself. Nothing to worry, you get bandaged and treated readily. But your session end here, and the associated bonus will be reduced.",
    c1: "I should be more careful",
    a1: "bounty:-5|health:1,2",
};

this.area4_6 = {
    text: "The door to the pilot course hall is closed. An officer tells you the courses are suspended for an undefined time.",
    c1: "Go away",
    a1: "goNav:area4_1"
};

/////////////////// STATION MAP ///////////////////

this.map = {
    text: " ",
    background: "litf_bg_stationmap.png",
    c1: "Back to the lobby",
    a1: "goNav:back"
};

/*
this.map1 = {
    text: "\n>>",
    background: "litf_bg_stationmap.png",
    c1: "Back to the lobby",
    a1: "goNav:lobby1"
};

this.map2 = {
    text: "\n\n\n\n\n\n>>",
    background: "litf_bg_stationmap.png",
    c1: "Back to the lobby",
    a1: "goNav:lobby2"
};

this.map3 = {
    text: "\n\n\n\n\n\n\n\n\n\n>>",
    background: "litf_bg_stationmap.png",
    c1: "Back to the lobby",
    a1: "goNav:lobby3"
};

this.map4 = {
    text: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n>>",
    background: "litf_bg_stationmap.png",
    c1: "Back to the lobby",
    a1: "goNav:lobby4"
};
*/

/////////////////// SPECIAL LOCATION ///////////////////

this.bar0 = {
	text: "The place is full of pilots. You don't see free seats at the tables, so you decide to sit at the counter. Everyone is pretty chatty: you hear talks of all kinds, public or private, and the usual jokers who take the opportunity to have a nice drink with friends.",
	background: "litf_bg_localbar.png",
    r1: "flag_drinkChallenge=0",
	c1: "A group at a table repeat loud: 'CHALLENGE! CHALLENGE!' Maybe I could check?",
	a1: "goNav:bar1",
    r2: "flag_chatBartender=0",
	c2: "I could talk a bit with the bartender",
	a2: "goNav:bar2",
    r3: "flag_offerToCounter=0,credits>=150",
	c3: "I could offer a drink to everyone at the counter (should cost no more than 150cr)",
	a3: "goNav:bar3",
	c4: "Exit the Bar",
	a4: "time:300|goNav:area3_1"
};

this.bar0_1 = {
    text: "Suddenly, a brawny bouncer blocks you right at the entrance of the Bar. He's staring right into your eyes. 'We don't like cheaters or drunkard here, and you don't seem a cheater to me. Get lost', he says to you with a deep, serious voice.",
    c1: "I think I'll follow your advice",
    a1: "goNav:area3_1"
};

//

this.bar1 = {
	text: "You sit in front of a $SPECIES_S$ that explains to you the rules of the challenge. 'A liquor is chosen. I drink a glass. You drink a glass. Again. Until one crashes. The bet is 50cr. You're in?",
	background: "litf_bg_localbar.png",
    r1: "credits>=50",
	c1: "Yes, you can count me in",
	a1: "credits:-50|goNav:bar11",
	c2: "Not tonight, thanks",
	a2: "goNav:bar0"
};

this.bar11 = {
	text: "Today the liquor is the $LIQUOR$. 'First glass is mine'. The $SPECIES_S$ raise the glass and drink the content in one gulp.",
	background: "litf_bg_localbar.png",
	c1: "It's my turn",
	a1: "goNav:bar12"
};

this.bar12 = {
	text: "You raise your glass and drink. It burns, but you can stand it.",
	background: "litf_bg_localbar.png",
	c1: "It's your turn",
	a1: "goNav:bar111"
};

this.bar111 = {
	text: "'NEXT ROUND! You're not a newbie, I see!'\nThe $SPECIES_S$ raise the glass and drink the content in one gulp.",
	background: "litf_bg_localbar.png",
	c1: "It's my turn",
	a1: "goNav:bar112"
};

this.bar112 = {
	text: "You raise your glass and drink...",
	background: "litf_bg_localbar.png",
	c1: "It's your turn",
	a1: "goNav:bar1111"
};

this.bar1111 = {
	text: "'NEXT ROUND!'\nThe $SPECIES_S$ raise the glass and drink the content in one gulp.",
	background: "litf_bg_localbar.png",
	c1: "It's my turn...",
	a1: "goNav:bar1112,bar1112,bar1112,bar1112,bar1113,bar1113"
};

this.bar1112 = {
	text: "You raise your glass and drink...",
	background: "litf_bg_localbar.png",
	c1: "It's your turn!",
	a1: "goNav:bar1111,bar1111,bar1111,bar1111,bar1114,bar1114"
};

this.bar1113 = {
	text: "You focus on the glass... but now you see two glasses on the table. No, three glasses... dancing glasses. You move your hand but you can't take them, they always slip away. Then table becomes bigger and someone turn off the lights.\n\nYou awake in the street, outside the bar. Your rival pats you on the shoulder: 'You're not bad. But I won. Thanks for the money.'",
	c1: "...burp",
	a1: "flag_drinkChallenge:1|flag_getDrunk:1|time:3600|goNav:area3_1"
};

this.bar1114 = {
	text: "'NEXT ROUND!'\nThat said, the $SPECIES_S$ collapses on the table! You raise one arm for victory while the other pilots at the table applaud you. The bet is yours. Too bad you're drunk, after all, because you can not keep your balance. The owner goes with you to the exit and gently throw you out. 'You can go back when you learn how to walk alone!'. The door closes but you can hear the laughters from inside.",
	c1: "It's ok. I won!",
	a1: "flag_drinkChallenge:1|flag_getDrunk:1|credits:100|time:1800|goNav:area3_1"
};

//

this.bar2 = {
	text: "The $SPECIES_S$ bartender pours a glass of liquor, and you start chatting. After a bit, the bartender tells you: $GOSSIP$.",
	background: "litf_bg_localbar.png",
	c1: "Ok, I think it's enough for now",
	a1: "time:300|goNav:bar0"
};

//

this.bar3 = {
	text: "The customers at the counter rejoice and take their drinks. Many begin to sing. After everyone has been drinking his glass, another commander stands up and offers another round. And you have to start over!",
	background: "litf_bg_localbar.png",
	c1: "Let's go, another round!",
	a1: "credits:-50,-150|reputation:5|goNav:bar31",
	c2: "Ok, I'm out, I don't want to go too far...",
	a2: "flag_offerToCounter:0|time:600|goNav:bar0"
};

this.bar31 = {
	text: "You have just finished your glass, when another commander stands up and offer another round! Screams of joy and new songs! The party goes on!",
	background: "litf_bg_localbar.png",
	c1: "Let's go, another round!",
	a1: "reputation:3|goNav:bar311,bar313",
	c2: "Ok, I'm out, I don't want to go too far...",
	a2: "flag_offerToCounter:0|time:1200|goNav:bar0"
};

this.bar311 = {
	text: "And this glass is empty too! You try to say something when someone offers a new round! The glasses go around the counter, and you find one in your hands. This things could never end...",
	background: "litf_bg_localbar.png",
	c1: "Let's go, another round...",
	a1: "reputation:2|goNav:bar31,bar313",
	c2: "Ok, I'm out, I don't want to go too far...",
	a2: "flag_offerToCounter:0|time:2400|goNav:bar0"
};

this.bar313 = {
	text: "You say: 'Another round!', but there isn't another round. And there isn't the counter anymore. You open your eyes and you're on the street. The bar is closed since an hour at least. You decide to keep sitting in your place for a while, just to clear your mind.",
	c1: "Five minutes. Five minutes and then I go...",
	a1: "reputation:-15|flag_getDrunk:1|time:14400|goNav:area3_1"
};

////

this.art0 = {
	text: "The art gallery is open to the public, and there is a good attendance. You see people conversing about the present exhibit, comparing different painting schools. You go to the entrance and read the following announcement:\n\nTHE ART GALLERY IS PROUD TO PRESENT\n\n[[ The masters of $ARTGENRE$ ]]\n\nFOR AN INDEFINITE AND EXTREMELY VARIABLE AMOUNT OF TIME\n\nAdmission price: 10cr",
    r1: "credits>=10",
	c1: "Let's take a look",
	a1: "credits:-10|flag_artGallery:1|goNav:art11,art12,art13",
	c2: "Exit the Art Gallery",
	a2: "goNav:area3_3"
};

this.art11 = {
	text: "The display was better than expected. You have been amazed by certain shapes and combinations of colors and light effects. Not bad. Really.",
	c1: "Exit the Art Gallery",
	a1: "time:3600|goNav:area3_3"
};

this.art12 = {
	text: "At the end of the visit you understand that the only impression left on you is an irresistible boring one.",
	c1: "Exit the Art Gallery",
	a1: "time:1200|goNav:area3_3"
};

this.art13 = {
	text: "'What's this vileness! This is an insult to Art in every form and conception!' You can't stand looking at these horrible monstrosity for another second!",
	c1: "Exit the Art Gallery",
	a1: "time:600|goNav:area3_3"
};

////

this.cinema0 = {
	text: "A robo-clerk is showing the following announcement:\n\nTONIGHT AT THE CINEMA HALL\n\nAn extended retrospective on the $MOVIEGENRE$ genre\n\n\Three movies and a debate on the history and the importance of the genre in the review of the late artistic and technologic developments.\n\nPrice for admission: 30cr",
    r1: "credits>=30",
	c1: "I'm curious to see what sort of movies are these",
	a1: "credits:-30|flag_cinemaHall:1|goNav:cinema11,cinema12,cinema13",
	c2: "Exit the Cinema Hall",
	a2: "goNav:area3_3"
};

this.cinema11 = {
	text: "The movies were great and the debate was interesting. All in all it was a good show.",
	c1: "Exit the Cinema Hall",
	a1: "time:7200,14400|reputation:1,2|chancesick:10|goNav:area3_3"
};

this.cinema12 = {
	text: "You liked the movies but at the end you felt pretty tired, so you exited the building before the debate.",
	c1: "Exit the Cinema Hall",
	a1: "time:6000,13200|chancesick:10|goNav:area3_3"
};

this.cinema13 = {
	text: "You don't know what were you expecting from this show, but it hasn't been a good idea. You decided to walk out, since nobody will ever give back to you the time you passed in the Hall...",
	c1: "Exit the Cinema Hall",
	a1: "time:3600,7200|chancesick:10|goNav:area3_3"
};

////

this.opera01 = {
	text: "Near the entrance you read the following announcement:\n\nTONIGHT THE OPERA HOUSE PRESENTS\n\nThe exclusive $MUSICGENRE$ singer that will play a selection of $GENDERPOSS$ best hits.\n\nAdmission price: 20cr",
    r1: "credits>=20",
	c1: "That's a genre of music that I didn't know existed",
	a1: "credits:-20|flag_operaHouse:1|goNav:opera11,opera12,opera13",
	c2: "Exit the Opera House",
	a2: "goNav:area3_3"
};

this.opera02 = {
	text: "Near the entrance you read the following announcement:\n\nTONIGHT THE OPERA HOUSE PRESENTS\n\nThe exclusive $MUSICGENRE$ musician that will play a selection of $GENDERPOSS$ best hits.\n\nAdmission price: 20cr",
	
	r1: "credits>=20",
	c1: "That's a genre of music that I didn't know existed",
	a1: "credits:-20|flag_operaHouse:1|goNav:opera11,opera12,opera13",
	
	c2: "Exit the Opera House",
	a2: "goNav:area3_3"
};

this.opera03 = {
	text: "Near the entrance you read the following announcement:\n\nTONIGHT THE OPERA HOUSE PRESENTS\n\nThe exclusive $MUSICGENRE$ band, ready to awe the audience with the tracks from their latest album\n\nAdmission price: 20cr",
	
	r1: "credits>=20",
	c1: "That's a genre of music that I didn't know existed",
	a1: "credits:-20|flag_operaHouse:1|goNav:opera11,opera12,opera13",
	
	c2: "Exit the Opera House",
	a2: "goNav:area3_3"
};

this.opera11 = {
	text: "The performance was a success. When you exit the show, your ears are still ringing. It was a riveting experience, that allowed you to explore unknown musical landscapes. You won't forget this concert for some time.",
	c1: "Exit the Opera House",
	a1: "time:3600,10800|reputation:1,2|chancesick:10|goNav:area3_3"
};

this.opera12 = {
	text: "When you exit the show, you fail to grasp the appeal of the exhibition. You keep a vague memory of strange melodies and awkward composition, nothing worthy remembering.",
	c1: "Exit the Opera House",
	a1: "time:3600,10800|chancesick:10|goNav:area3_3"
};

this.opera13 = {
	text: "'LET-ME-PASS WHAT-IS-THIS-AUDIO-TORTURE MY-EARS-ARE-BLEEDING I-HAVE-TO-GET-OUT-OF-HERE!'",
	c1: "Exit the Opera House",
	a1: "time:300,1200|chancesick:10|goNav:area3_3"
};

////

this.med0 = {
	text: "The place is clean and illuminated. Everything is white. The receptionist scans you for a moment before talking: 'Hello Commander, we can provide you services to rid of your physiological annoyances.'",
	background: "litf_bg_medcenter.png",

    //r1: "health>0",
	c1: "Wounds Treatment and Tissue Repairing",
	a1: "if:health>0?goNav:med1;goNav:med1_1",

    //r2: "sickness>0",
	c2: "Virus and germs Nano disinfestation",
	a2: "if:sickness>0?goNav:med2;goNav:med1_1",

    //r3: "flag_getDrunk=1",
	c3: "Quick alcoholic purge (can generate allergic reactions)",
	a3: "if:flag_getDrunk=1?goNav:med3;goNav:med3_1",

    r4: "tech=hitech,maxStrength<200",
    c4: "Recovering Therapy",
    a4: "goNav:med4",

	c5: "Exit the Medical Center",
	a5: "goNav:area2_2"
};

this.med1 = {
	text0: "tech=lowtech|'Technology on this station allow us to heal only light wounds. Price for treatment is 100cr.'",
	text1: "tech=avgtech|'Technology on this station allow us to heal serious wounds. Price for treatment is 150cr.'",
	text2: "tech=hitech|'We can heal wounds of every type and severity. Price for treatment is 250cr.'",
	background: "litf_bg_medcenter.png",

    r1: "paytreatment=1",
	c1: "I need the treatment, now",
	a1: "paytreatment:health|goNav:med0",

	c2: "Thanks, I've changed my mind",
	a2: "goNav:med0"
};

this.med1_1 = {
    text: "After a second the receptionist says: 'Our analysis confirms that you have no need for our treatment. You're already enough healthy. Please come back when your conditions will be more severe. We hope to see you soon. Goodbye.'",
    background: "litf_bg_medcenter.png",
    c1: "I don't know if I want to see you soon too...",
    a1: "goNav:med0"
};

this.med2 = {
	text0: "tech=lowtech|'Technology on this station allow us to heal only light sickness. Price for treatment is 100cr.'",
	text1: "tech=avgtech|'Technology on this station allow us to heal serious sickness. Price for treatment is 150cr.''",
	text2: "tech=hitech|'We can heal sickness of every type and severity. Price for treatment is 250cr.'",
	background: "litf_bg_medcenter.png",

    r1: "paytreatment=1",
	c1: "I need the treatment, now",
	a1: "paytreatment:sickness|goNav:med0",

	c2: "Thanks, I've changed my mind",
	a2: "goNav:med0"
};

this.med3 = {
	text: "'The treatment is free. Please enter the purging machine and stay still untile the process is terminated.'\n\nYou enter the capsule and pass the worst three minutes of the day. But the results are worthy of the annoyance - you feel your mind clear and ready.",
	background: "litf_bg_medcenter.png",
	c1: "I really needed this",
	a1: "flag_getDrunk:0|chancesick:10|goNav:med0"
};

this.med3_1 = {
    text: "After a second the receptionist says: 'I'm sorry to inform you that this treatment isn't applicable to sober people. You should come back when you're drunk to be allowed to endure the treatment. We hope to see you soon. Goodbye.'",
    c1: "I'll be back when I'll be drunk, then",
    a1: "goNav:med0"
};

this.med4 = {
    text: "'This treatment allow to recover the body from damage endured in time, following prolungated work or efforts of various nature. The price for treatment is 400cr. The process will last about two hours.'",
    background: "litf_bg_medcenter.png",
    r1: "credits>=400",
    c1: "Yes, you can proceed",
    a1: "maxStrength:250|time:7200|credits:-400|goNav:med0",
    c2: "I don't think this is the right time for this...",
    a2: "goNav:med0"
};

////

this.hotel0 = {
    text: "In the main hall you can see the robo-receptionist. Little three-wheeled droids pick and transport baggages to the guest rooms, crossing each other at high speed without crashing - it's almost a miracle.\n\nThe robo-receptionist says: 'Welcome, guest. I'm glad to announce that we have available rooms. You can spend 6 hours in a room for 60cr in advance. A modest price, in my opinion. Do you want to take a room?'",
    c1: "Yes, please, I want to get some rest",
    a1: "if:credits>=60?goNav:hotel2;goNav:hotel1",
    c2: "No, thanks",
    a2: "goNav:area2_1"
};

this.hotel1 = {
    text: "The robo-receptionist scans you. 'I'm sorry, but I have to cancel the booking process, as I see that you haven't the required money for the payment. Good day.' After this, it turns to another guest and continue working.",
    c1: "I guess they don't give credit",
    a1: "goNav:hotel0"
};

this.hotel2 = {
    text: "The robo-receptionist instructs a droid to take you to your room. The droid seems somewhat disappointed from your lack of baggages, but runs forward to the nearest elevator. After a brief walk, the droids stop at the door of the $ROOMNUM$, and emits a high-pitched sound before going away.\n\nYou enter the room. It's clean, of an average quality that doesn't impress you. But the bed is comfortable and the bathroom has hot water. A video terminal offers you a wide selection of movies or local transmissions.",
    c1: "These 6 hours won't be a problem",
    a1: "credits:-60|time:21600|goNav:hotel3"
};

this.hotel3 = {
    text: "You hear a buzzing sound. Your booking time is ended. You leave the room and get back outside. You feel relieved and relaxed.",
    c1: "It has been a nice distraction",
    a1: "goNav:area2_1"
}; 

////

this.wwvt0 = {
    text: "The WWVT bulding resemble an amusement park, filled with neon signs and holo-videos of strange planets with extreme climates. There are pictures of big fanged predators, and plants and trees of different variety and color. A guide guarantees an authentic immersive experience, a unique virtual 4 hour safari. All of this for only 5cr.",
    c1: "Let's try this virtual experience",
    a1: "if:credits>=5?goNav:wwvt2;goNav:wwvt1",
    c2: "I'm not in the mood for simulated dangerous situations",
    a2: "goNav:area3_1"
};

this.wwvt1 = {
    text: "The ticket-droid can't give you the ticket - you haven't the 5cr, as required. No money, no entry.",
    c1: "Maybe another time",
    a1: "goNav:area3_1"
};

this.wwvt2 = {
    text: "You join a group of people, and the guide leads you into a great empty hall. 'Some of you have already seen or visited an holographic ambient, but I assure you this is something more. Stay ready, let's begin this voyage.'\nThe lights go out, and you notice shapes and colours gradually appearing out of thin air.",
    c1: "It's like another place is becoming real... with us in it",
    a1: "credits:-5|goNav:wwvt2_1,wwvt2_2,wwvt2_3,wwvt2_4"
};

this.wwvt2_1 = {
    text: "The tour brings you on a glacial planet, covered with ice and wind-blown. You see enormous mountains and deep white abysses, furious blizzard filled of spiky ice shards, and slow armored creatures that walks without pause on the white plains. Around them other little quick creatures jump and run, sometime digging holes in the ice. The big-armored creature use a long and flexible tail to fish into the holes, and the little quick ones hide under it when the blizzard comes.",
    c1: "Really an unusual experience",
    a1: "time:14400|goNav:area3_1"
};

this.wwvt2_2 = {
    text: "The tour brings you on a desert planet. Everything still, there's no wind, no clouds in the sky, all you see is an endless sea of yellowish sand. Only after a while you notice little creatures running in the sand, almost perfectly concealed by their colors. You don't understand if they're fleeing from someone, until a huge horned head surface from the sand with a loud roar. The little runners change quickly directions and run away from the creature, picking little straws of a purple plant in the process.",
    c1: "It was like I was really there",  
    a1: "time:14400|goNav:area3_1"
};

this.wwvt2_3 = {
    text: "The tour brings you on a oceanic planet. You're in the middle of an endless sea. In the waters you see a school of thousand fishes that forms a rotating circle. Other fishes, of various size, shape another smaller circle. This strange and hypnotic ritual get interrupted by a couple of big predators that starts hunting the fishes.",
    c1: "Impressing...",
    a1: "time:14400|goNav:area3_1"
};

this.wwvt2_4 = {
    text: "The tour brings you on a jungle planet. Everything is covered in grass, with huge plants and the tallest tree you have ever seen. There are hundreds of animal calls, a symphony that walks, jump, crawl and climb the trees. The light of two suns shines over life and death circling each other, a complex and complete ecosystem untouched since thousand of years.",
    c1: "It was an awesome experience, I have to admit it",  
    a1: "time:14400|goNav:area3_1"
};

////

this.storage0 = {
    text0: "You walk towards the passage to the Storage section, where a group of workers is chatting. When they see you, they block the passage.\n",
    text1: "stance>-25|One of them makes a step forward and talk directly to you: 'Hey, this area is for authorized personnel only. This means you can't walk there, at least you get a permission, and we are the ones who can get you a one-time permission for 25cr.'",
    text2: "stance<=-25|One of them makes a step forward, but another worker stop $GENDEROBJ$, look at you and says: 'The boys told me this is a friend. This commander can pass.'",
    r1: "stance>-25",
    c1: "Pay and enter the Storage section",
    a1: "if:credits>=25?credits:-25§goNav:storage2;goNav:storage1",
    r2: "stance<=25",
    c2: "Visit the Storage section",
    a2: "goNav:storage2",
    c3: "Go back to the Cargo Area",
    a3: "goNav:area1_2"  
};

this.storage1 = {
    text: "Where is the money? This isn't enough! Get lost before we lock you in a crate of trumbles!",
    c1: "Ok ok there's no need to be so angry",
    a1: "goNav:area1_2"
};
 
this.storage2 = {
    text: "You're in the Storage section.\n\nThe cargo from ships come here once it is removed from containers. It's basically a huge warehouse filled of box and crates of various shapes and size, that creates walls and corridors and rooms.\n\nFrom here you can return to the Cargo Area.",
    background: "litf_bg_storagearea.png",
    c1: "Exit to the Cargo Area",
    a1: "goNav:area1_2"
};

////

this.plounge0 = {
    text: "In the lounge, dozens of persons are fiddling with terminals trying to get a passage out of the station. A number of monitors fixed to the walls display local and extra-system news. Other screens shows data about current departing and incoming passenger flights. You can use a terminal to see if there are available contracts.",
    c1: "Unfortunately there are no suitable offers",
    a1: "goNav:area2_1"
};

////

this.church0 = {
    text: "The hall has a strange configuration: you can't say for sure, but it seems that the walls change and slides modifying the shape of the building. On the walls you see pictures from several systems. The wall opposite to the entrance shows a hooded man in an embracing pose, surrounded by shining stars. In front of this sacred picture there's an altar, and a series of benches. A soothing melody plays in the background. Everything is silent.",
    c1: "This place is a little weird. Maybe I'll explore it another time",
    a1: "goNav:area2_2"
};

////

this.bookshop0 = {
    text: "You enter into an old-style shop built with some exotic type of wood. There are many shelves full of books, volumes, tomes of every size and colour: on the cover you read titles in various languages. You see piles of books on the ground and in the corner. It's impossible to estimate the exact size of the shop - this place is like a little maze.",
    c1: "There's too much dust here. I need a breath of fresh air",
    a1: "goNav:area2_2" 
};

////

this.museum0 = {
    text: "A number of slightly fluorescent signs guide you along the suggested path, allowing you to view and examine the collection of ancient and alien artifacts kept inside safe glasses box. Everyone whispers, fearing to disturb the tranquillity of the halls.",
    c1: "Visit the museum",
    a1: "time:1800|goNav:museum1"
};

this.museum1 = {
    text: "At the end of your visit, you notice you felt impressed $MUSEUMPIECE$.\n\nYou reach the exit, where a stand ask the visitors to contribute to the museum maintenance. You can make an offer, if you want.",
    c1: "Give nothing",
    a1: "go:",
    
    r2: "credits>=1",
    c2: "Give 1cr",
    a2: "reputation:1|credits:-1|goNav:area3_1",
    
    r3: "credits>=10",
    c3: "Give 10cr",
    a3: "reputation:5|credits:-10|goNav:area3_1",
    
    r4: "credits>=100",
    c4: "Give 100cr",
    a4: "reputation:50|credits:-100|goNav:area3_1",
    
    r5: "credits>=1000",
    c5: "Give 1000cr",
    a5: "reputation:500|credits:-1000|goNav:area3_1",
};    

////

/*
this.fit0 = {
    text: "You walk to the reception, where a droid informs you that an unspecified malfunction prevents the Universal Fitness Badge validation. The center is closed until the situation will be fixed.",
    c1: "That's a pity. I'll try again in the future",
    a1: "goNav:area3_2"
};
*/

this.fit0 = {
    text0: "You walk to the reception, where a droids greets you.\n",
    text1: "checkFitBadge=0|The droid tries to scan something and then says: 'I see you aren't in possession of an Universal Fitness Badge. You need one to spend time in Fit Centers like this. You get a monthly subscription with it, for 15cr.'",
    //text2: "checkFitBadge=1|'I see that your subscription is expired. Do you want to renew it for 10cr?'",
    text2: "checkFitBadge=1|The droid scans the badge and then says: 'Sorry, it seems the expiration date is passed. You need to renew your subscription to continue using the Fit Center machinery. It will cost you 10cr for another month.'",
    text3: "checkFitBadge=2|The droid scans the badge and then says: 'Your subscription is still valid. You can proceed to the Health Validation Room. Good day",
    
    r1: "checkFitBadge=0",
    c1: "Ok, I'll subscribe",
    a1: "goNav:fit1_1",

    r2: "checkFitBadge=1",
    c2: "Ok, I'll renew the subscription",
    a2: "goNav:fit1_2",

    r3: "checkFitBadge=2",
    c3: "Enter the Health Validation Room",
    a3: "goNav:fit2",

    c4: "Maybe another time",
    a4: "goNav:area3_2"
};

this.fit1_1 = {
    text0: "credits>=15|'Here it is your new badge. You have free access to any Fit Center for an entire month. You can proceed to the Health Validation Room now. Good day.'",
    text1: "credits<15|'It seems that you don't own the required money for buying the Universal Fitness Badge. I suggest you to return with the required sum. Good day.'",

    r1: "credits>=15",
    c1: "Enter the Health Validation Room",
    a1: "credits:-15|writeTime:fitBadge_2592000|goNav:fit2",
    
    c2: "Thanks for the badge. See you later",
    a2: "credits:-15|writeTime:fitBadge_2592000|goNav:area3_2"
};

this.fit1_2 = {
    text0: "credits>=10|'I have just renewed you subscription for another month. You can proceed to the Health Validation Room now. Good day.'",
    text1: "credits<10|'It seems that you don't own the required money for buying the Universal Fitness Badge. I suggest you to return with the required sum. Good day.'",

    r1: "credits>=10",
    c1: "Enter the Health Validation Room",
    a1: "credits:-10|writeTime:fitBadge_2592000|goNav:fit2",

    c2: "Thanks for renewing my subscription. See you later",
    a2: "credits:-10|writeTime:fitBadge_2592000|goNav:area3_2"
};

this.fit2 = {
    text0: "A couple of robo-docs makes some examination while producing a series of bips in various tones. After a couple of minutes they give you their response:\n",
    text1: "health>0|'Negative: You're wounded. You can't access the gym in this conditions.'\n",
    text2: "sickness>0|'Negative: You're sick. You can't access the gym in this conditions.'\n",
    text3: "maxstrength<100|'Negative: Your body has endured too much damage.  You can't access the gym in this conditions. We suggest you get a recovering therapy from a Medical Center - this feature is available only on hi-tech systems.'",
    text4: "health=0,sickness=0,maxstrength>=100|'Positive: you can access the gym. Good day.'",

    r1: "health=0,sickness=0,maxstrength>=100",
    c1: "Enter the gym",
    a1: "goNav:fit3",
    
    c2: "I changed idea. Goodbye",
    a2: "goNav:area3_2"    
};

this.fit3 = {
    text: "This building is comprised of various floors, and is full of gym machinery for every type of exercise and every type of alien race.",
    c1: "Start exercising",
    a1: "if:fitDamageChance<=50?goNav:fit3_1;fit3_2",

    c2: "I changed idea. Maybe another time",
    a2: "goNav:area3_2"
};

this.fit3_1 = {    
    text: "At the end of your training period you have a shower. You feel tired but somewhat relaxed, and your body feels a little more tonic.",
    c1: "Exit the Fit Center",
    a1: "time:3600|strength:10,40|checkFitDamage:1|writeTime:lastFitSession_0|goNav:area3_2"
};

this.fit3_2 = {
    text: "While you're exercising, you feel an acute pain that makes you cringe and almost scream. Other people in the gym run to you, and call for help. Preliminary medical scans show that you hurt yourself: you can't continue exercising until you get healed.",
    c1: "Exit the Fit Center",
    a1: "time:1800|fitDamage:1|writeTime:lastFitSession_0|goNav:area3_2"
};