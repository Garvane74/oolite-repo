"use strict";

this.name	= "LITF_RPGElements";
this.author	= "BeeTLe BeTHLeHeM";
this.copyright	= "2015 BeeTLe BeTHLeHeM";
this.description= "Life In The Frontier - RPG Elements";
this.version	= "0.7.1";
this.licence	= "CC BY-NC-SA 4.0";

///////////////////////////////////////

this.LITF_reputationDesc = [ "Unknown", "Barely Known", "Recognized", "Well-Known", "Largely Known", "Widely Known", "Prestigious", "Famous", "Popular", "Acclaimed", "Renowned", "Legendary" ];

this.LITF_strengthDesc = [ "Very poor", "Poor", "Average", "Good", "Excellent" ];

this.LITF_healthShort = [ "None", "Slight", "Medium", "Serious", "Severe", "Critical" ];
this.LITF_sicknessShort = [ "Excellent", "Good", "Average", "Weak", "Severe", "Critical" ];

///////////////////////////////////////

this.$getReputationLevel = function(rep) {
	var repLev = 0;

	while (rep > (Math.pow(2, repLev) * 200)) {
		repLev++;
	}

	return repLev;
};


this.$getReputationFromValue = function(rep) {
	var repLev = this.$getReputationLevel(rep);
	return this.LITF_reputationDesc[repLev];
};


this.$getStrengthLevel = function(str) {
	var strLev = 0;

	while (str > (Math.pow(2, strLev) * 100)) {
		strLev++;
	}

	return strLev;
};


this.$getStrengthFromValue = function(str) {
	var strLev = this.$getStrengthLevel(str);
	return this.LITF_strengthDesc[strLev];
};


this.$getStanceFromValue = function(sta) {
	var desc = "";

	if (sta == 0) { desc = "absolutely neutral"; }

	if (sta < 5 && sta > -5) { desc = "generally neutral"; }

	if (sta >= 5) { desc = "sympathetic with GalCop"; }
	if (sta >= 25) { desc = "friendly with GalCop"; }
	if (sta >= 75) { desc = "loyal with GalCop"; }
	if (sta >= 200) { desc = "sworn to act along the rules of GalCop"; }

	if (sta <= -5) { desc = "sympathetic with criminals"; }
	if (sta <= -25) { desc = "friendly with criminals"; }
	if (sta <= -75) { desc = "loyal with criminals"; }
	if (sta <= -200) { desc = "respectful of the laws of the criminal underworld"; }

	return desc;
};


this.$getHealthFromValue = function(hea) {
	var heaDesc = "";

	switch (hea) {
		case 0: 
			heaDesc = "";
			break;
		case 1: 
			heaDesc = "You have some bruises and scratches.";
			break;
		case 2:
			heaDesc = "You are slightly wounded.";
			break;
		case 3:
			heaDesc = "You are wounded.";
			break;
		case 4:
			heaDesc = "You are severely wounded.";
			break;
		case 5: 
			heaDesc = "Your health condition is critical!";
			break;
	}

	return heaDesc;
};


this.$getSicknessFromValue = function(sic) {
	var sicDesc = "";

	switch (sic) {
		case 0: 
			sicDesc = "";
			break;
		case 1: 
			sicDesc = "You feel uneasy.";
			break;
		case 2:
			sicDesc = "You cough, sneeze and feel chills all over your body.";
			break;
		case 3:
			sicDesc = "You feel a strong nausea.";
			break;
		case 4:
			sicDesc = "You feel tired and can't think straight.";
			break;
		case 5: 
			sicDesc = "You feel burning inside. You're delirious.";
			break;
	}

	return sicDesc;
};
