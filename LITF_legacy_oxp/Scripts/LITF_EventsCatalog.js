"use strict";

this.name	= "LITF_EventsCatalog";
this.author	= "BeeTLe BeTHLeHeM";
this.copyright	= "2015 BeeTLe BeTHLeHeM";
this.description= "Life In The Frontier - Events Catalog";
this.version	= "0.7.1";
this.licence	= "CC BY-NC-SA 4.0";

/*
/////////////////////////////////////// MINOR CARGO ARRAYS

// low reputation (0-2)
this.minorCargo0 = [ "Beryllium", "Cobalt", "Copper", "Oxygen", "Water", "High Temperature Alloy", "Scrap Metal", "Fertilizers", "Wood", "Tungsten" ];

// average reputation (3-5)
this.minorCargo1 = [ "Engine Components", "Ship Hull Panels", "Superconductors", "Polymers", "Construction Machinery", "Mining Machinery", "Robot Workers", "Factory Equipment", "Plant Seeds", "Pesticides", "Reciclying Tanks", "Atmospheric Scrubbers", "Laser Drills", "Waste Recyclers" ];

// high reputation (6-8)
this.minorCargo2 = [ "Pets", "Artwork", "Games", "Movies", "Holographics", "Home Appliances", "Home Entertainment", "Robot Servants", "Books", "Recycled Goods", "Musical Instruments", "Clothing" ];

// very high reputation (9-11)
this.minorCargo3 = [ "Hydroponics", "Optronics", "Pharmaceuticals", "Robosurgeons", "Classical Instruments", "Museum Exhibits", "Rare artwork" ];

/////////////////////////////////////// CONSTANTS ARRAYS

this.arrCommodity = [ "food", "textiles", "radioactives", "slaves", "liquor_wines", "luxuries", "narcotics", "computers", "alloys", "firearms", "furs", "minerals","gold", "platinum", "gem stones", "alien items" ];

this.genderSyntax = [ "He|him|his", "She|her|her" ];

this.arrLiquors = [ "Trumble Bubble", "Thargoid Slingshot", "Witch Red Wine", "Injector Thunder", "Shadow Rum", "Thyme Hopper", "Double Halo Talon", "Ginger Blitzer", "Cosmic Fluff", "Perfect Storm", "Radioactive Phantom", "Perfect Scooper", "Vodka Square Wave", "Milky Engine Twister", "Imaginary Alien", "Gin Octopoid Walk", "Sherry Robo Punch", "Shield Light Ale", "Glowing Asteroid", "Gleaming Q-Whiskey" ];

this.arrCharity = [ "Cosmic Cosmo Peace", "Piracy Rehabilitation Program", "Save the Usleian Tree Ant", "Trumble Protection League", "Save the Star-Jellies - Ban Astromining!", "Historical Buildings Fund", "Save Our Species" ];

this.arrGossip = [ 
	"'You shouldn't talk with those dirty beggars. I've heard they're all undercover government agents. Don't let them touch you!'",
	"'My mother said me that if you point your ship far from any star, to the darker area of space, look at your reflection on the windscreen and say 'Witchspace' three times, something horrible will happen...'",
	"'You should be careful what you drink. I've heard of a new liquor that they are selling now. You put a finger in it, only the bones will remain.'",
	"'Once I heard a pilot told me of an another pilot stranded in space after a collision with a luxury liner. The owner of the liner offered to pay damages, and gave 50Kcr to the pilot! It was some billionaire...'",
	"'I know someone who has seen Thargoid ships many times. And he said me this: there's one ship, different from the others, near impossible to encounter, but still you can see, on certain transparent sections, thousands of people in cryopods... This story always makes me freak out...'",
	"'You must remain on the upper sectors. Don't go down. Lower station sector are very dangerous. Chemical companies discharge every type of waste down there. You can't imagine what crazy type of mutants can live in that stuff...'",
	"'He was carrying this guy for a business meeting, and when he arrived the berth was empty! Nothing! He never find him! How crazy is that!'",
	"'It was in $SYSTEM$... so he was far from the planet, but it was so sleepy he decided to stop the ship behind some asteroid, sensors on, and take a nap. And while he sleeps, he feels something licking his hand. He awake but there was noone. Noone!'",
	"'Oh yes I've seen it. It's a trumble snuff movie. Yeah I know the cousin of a trader that I met some time ago that had an entire collection of those.'",
	"'Clown droids? I hate clown droids! They should be packed and thrown into the sun. Did you know who is responsible for the majority of droid murders? Clown droids!'",
	"'You can't believe me, but I tell you that this station is haunted. I know it, because I know one that worked on its construction. And you know one thing? He says that under the main level there is a sort of gap, and it was filled with cemetery soil. Do you understand now?'",
	"'I don't believe in ghosts. I think they're all holograms. Or maybe it's the contrary. Frankly I don't know. This is a little bit creepy, don't you think?'",
	"'They say that you buy on odd day and sell on even day. And every fourth day you buy on organic, and every eighth day you buy on artificial. So do you have to get a software to define every date of launch and the length of every travel. They say you can become rich in two weeks with this method.'",
	"'I received this mail yesterday, and it says I have to re-send it to other five persons in five days, or a virus will destroy every system on my ship. What I have to do? Alien and androids count?'",
	"'Every time I look this strange human beings I remember the stories my grandmother told me when I was an egg. You know, the Gurzulla. Small and large, two eyes and two arms, all covered in hair. Do you remember? It was disgusting!'",
	"'If someone with red clothes asks you for a passenger trip towards $SYSTEM$, go away as fast as you can. That person is dead. I tell you, it happened to me.'",
	"'You can remove ice crystals with warm water and vinegar.'",
	"'The friend of a friend told me that is going to be an economic change because of certain turmoils on the planet. I don't know for sure, but my friend told me to monitor $COMMODITY$ for a while.'",
	"'The news are fake. Don't follow the news. When you listen to a broadcast, understand it as the exact contrary. Do you understand?'",
	"'Remember, don't give your password to ANYBODY!'",
	"'Going around the station you could find contractors that needs pilot to transport wares or people.'",
	"'If you want to spend some time you could go to the Entertainment Area.'",
	"'Recently Galcop has installed a terminal in its precinct to clean criminal records for a price.'"
];

this.arrMuseumPieces = [
    "by a collection of near identical statuines coming from systems very far from each other, all representing some sort of wise creature giving knowledge.",
    "by a strange rock, worked and detailed with weird angle and perspectives that made you head hurt.",
    "by a metallic throne surrounded with curious appendages, that could be blades or tentacles, that you swear were moving when you don't looked directly at them.",
    "by a set of various spheres, probabily representing a solar system, moving without external influence in round patterns.",
    "by an ancient document inscripted in an exotic species of papyrus, documenting the epic history of a civilization disappeared eons ago.",
    "by a set of mechanical figurines roughly molded as fighters that repeatedly lead a charge against another set of opponents. The little play develops every time in a different manner.",
    "by a pile of bright gears, bars, knobs and levers, presumedly belonging to some strange machinery built by a race known as the 'Replicators'.",
    "by a collection of giant leaves where small colonies of insects draw shapes and pictograms.",
    "by a series of ancient weapons built from a volcanic formation that still preserve a slow and reddish glow.",
    "by a pair of dolls carved from animal bones, whose eyes seems to follow as you move in front of them."
];


this.arrLobbySnippet = [ 
    "Strangely, there's no one in sight right now. Unusual and a bit eerie.",
	"There are few $SPECIES_P$, they don't speak and look at the other visitors.",
	"There is a family of $SPECIES_P$ with a pair of very noisy kid.",
	"Not so far from you a couple of $SPECIES_P$ is bickering.",
	"Little groups of people whisper to the others.",
	"An engineering squad is fixing some circuits under a wall panel.",
	"A group of college $SPECIES_P$ jokes and make stupid moves before going into exaggerated laughs. They're probably drunk.",
	"The place is crowded. There's many people now.", 
	"The place is crowded. Someone is complaining the station management for the delays.",
	"A gently voice remind every visitor to spend as much as possible.",
	"A gently voice remind every visitor to stay on the station as much as possible.",
    "A loud buzz interrupts a gently voice reminding something that nobody understand.",
	"An annoying jingle plays in background.",
	"A lonely $SPECIES_S$ walks away looking around suspiciously.",
	"There are few groups of people, and everyone is talking loud for some reason."
];

this.arrArt1 = [
	"ASCII", "Abstract", "Baroque", "Computer", "Conceptual", "Dada", "Digital", "Fantastic", "Figurative", "Graffiti", "Gothic", "Humanistic", "Kinetic", "Magic", "Metaphysical", "Modular", "Objective", "Psychedelic", "Renaissance", "Shock", "Space", "Street", "Transgressive", "Underground", "Xenormorphic"	
];

this.arrArt2 = [
	"Expressionism", "Illusionism", "Aestheticism", "Realism", "Impressionism", "Anti-realism", "Art nouveau", "Blobism", "Modernism", "Post-modernism", "Cubism", "Deconstructivism", "Futurism", "Hypermodernism", "Hyperrealism", "Letterism", "Mannerism", "Maximalism", "Minimalism", "Neoclassicism", "Photoshoppism", "Primitivism", "Rococo", "Surrealism"
];

this.arrMusic1 = [
	"Proto", "Post", "Retro", "Classic", "Contemporary", "Alternative", "Neotraditional", "Traditional", "Elevator", "Futuristic", "Ancestral", "Commercial", "Anti", "Avant-garde", "Underground", "Acid", "Orchestral", "Mainstream", "Dance", "Indie", "Melodic"
];

this.arrMusic2 = [
	"Heavy", "Experimental", "Lo-fi", "Progressive", "Instrumental", "Gospel", "Piano", "Swamp", "Comedy", "Parody", "Religious", "Ambient", "Drone", "Dark", "Soft", "Space", "Trip", "Dream", "Cyber", "Robot", "Lyrical"
];

this.arrMusic3 = [
	"Metal", "Hip hop", "Jazz", "Rap", "Pop", "Noise", "Electronica", "Blues", "Rock", "Country", "Punk", "Calypso", "Reggae", "Ska", "Chiptune", "Disco", "Funk", "House", "Trance", "Industrial", "Poetry"
];

this.arrMovie1 = [
	"Animal", "Biker", "Comic-Book", "Conspiracy", "Erotic", "Martial Ar", "Political", "Surfing", "Techno", "Classic", "Family", "Procedural", "Diary", "Ehtnic", "Medical", "Ghost", "Gothic", "Vampire", "Werewolf", "Atomic", "Cyberpunk"
];

this.arrMovie2 = [
	"Action", "Noir", "Hard-boiled", "Crime", "Shakespearean", "Historical", "Horror", "Musical", "Science Fiction ", "War", "Western", "Slasher", "Terror", "Post-Apocalyptic", "Time Travel"
];

this.arrMovie3 = [
	"Thriller", "Epic", "Adventure", "Survival", "Comedy", "Mockumentary", "Drama", "Tragedy", "Ballet", "Concert"
];
*/

/////////////////////////////////////// RANDOM EVENTS

this.rand1 = {
	text: "A $SPECIES_S$ beggar approaches you asking for some credits. $GENDERSUBJ$ is covered in dirt, pale in the face, and $GENDERPOSS$ hands tremble. It could be an elaborate act, but it isn't easy to say. 'Credits for food! Five days no eat!', $GENDERSUBJ$ begs.",
    
    //r1: "credits>=5",
	c1: "Give 5 credits to the beggar",
	//a1: "credits:-5|reputation:5|go:rand1_1",
	a1: "if:credits>=5?go:rand1_1;go:rand1_2",
	
	//r2: "credits>=1",
	c2: "Give 1 credit to the beggar",
	//a2: "credits:-1|reputation:1|go:rand1_1",
	a2: "if:credits>=1?go:rand1_3;go:rand1_2",
	
	c3: "Move on and ignore the beggar",
	a3: "go:rand1_4"
};
 
this.rand1_1 = {
	text: "The beggar take the credits, displaying a gap-toothed smile. $GENDERSUBJ$ nods to you and walks away searching for another benefactor.",
	c1: "Goodbye",
	a1: "credits:-5|end:600"
};
	
this.rand1_2 = {
    text: "'Your words are generous but your wallet isn't!'.\nThe beggar laughs out loud, while exiting through a side corridor. You can still hear echoes of $GENDERPOS$ laughter.",
    c1: "At least I have still something",
    a1: "end:600"
};	

this.rand1_3 = {
    text: "The beggar take the credit saying 'A credit a day!' and laughing out loud. $GENDERSUBJ$ nods to you and walks away searching for another benefactor.",
    c1: "Goodbye",
    a1: "credits:-1|end:600"
};
	
this.rand1_4 = {
	text: "The beggar angrily shouts and you, then walks away stuttering something unintelligible.",
	c1: "You look at $GENDEROBJ$ for a while before going back to your business",
	a1: "end:600"
};

//

this.rand2 = {
	text: "A $SPECIES_S$ pass beside you: before you have time to do anything $GENDERSUBJ$ points $GENDERPOSS$ finger at you and says: $GOSSIP$.\nThen $GENDERSUBJ$ disappears in the crowd, talking to $GENDEROBJ$self, leaving you baffled.",
	c1: "The universe is full of weird people...", 
	a1: "end:300"
};

//

this.rand3 = {
	text: "A deployment of guards blocks your way. Behind them, bulkheads are slowly closing, accompanied by flashing lights. Together with other people you try to look beyond the massive grey walls, but you manage to notice only a reddish flickering of flames.\n\nWalking away from the deployment, you overhear a couple of $SPECIES_P$ talking about the incident: 'Another fire. Frankly I don't know if they are really incident anymore...'",
	c1: "Nothing to see here...", 
	a1: "end:300"
};

//

this.rand4 = {
	text: "A deployment of guards blocks your way. Behind them, bulkheads are slowly closing, accompanied by flashing lights. Together with other people you try to look beyond the massive grey walls, but you manage to notice only the reddish flickering of flames.",
	c1: "Luckily I wasn't inside there",
	a1: "end:300"
};

//

this.rand5 = {
	text: "From the shadows of a tight alley an unknown $SPECIES_S$ introduce $GENDEROBJ$self as a renowned seller of prodigious elixirs. $GENDERSUBJ$ shows you an ampoule full of a dark liquid, to $GENDERPOSS$ extreme delight, while proclaming: 'Today there's an exceptional offer! The Water of Life, straight from the sacred temple in the huge swamps of the remote jungle planets! And is yours only for 50cr. You would be a fool not to buy it!'",
	//requires: "credits>=50",
	c1: "Awesome! But I think I will pass.",
	a1: "go:rand5_1",
	c2: "Mmh. I feel rather tired lately. Give me this Water of Life, let's try it!",
    a2: "if:credits>=50?go:rand5_2;go:rand5_3",
	//a2: "credits:-50|go:rand5_2,rand5_2,rand5_3,rand5_4,rand5_5"
};
	
this.rand5_1 = {
	text: "The seller tries to convince you a little more, but seeing you're resolute, runs back to the shadows waiting for the next lucky acquirer.",
	c1: "Never trust $SPECIES_S$ sellers in the shadows.", 
	a1: "end:600"
};

this.rand5_2 = {
    text: "'Good! Good! Here's this awesome elixir, in change of a miserable amount of money!'\nThe seller gives you a little bottle filled with a cloudy and dense liquid, before quickly disappearing in the shadows.",
    c1: "Let's taste this stuff",
    a1: "credits:-50|go:rand5_4,rand5_5,rand5_6,rand5_7"
};

this.rand5_3 = {
    text: "'Aha! You haven't the money! And without money, no elixir! I'm sorry, I can't give this for free... See you another time!'\nThe seller moves away and disappears in the shadows before you can say anything.",
    c1: "Pretty evasive...",
    a1: "end:300"
};

this.rand5_4 = {
	text: "You drink the dark liquid, feeling its fuzzy taste into the throat. Obviously it's a soft drink, nothing less, nothing more...",
	c1: "Damned swindler!", 
	a1: "end:600"
};

this.rand5_5 = {
	text: "You drink the dark liquid, feeling its fuzzy taste into the throat. You don't recognize the flavour, but immediately start searching for water to wash your mouth.",
	c1: "Damned swindler! It was disgusting!", 
	a1: "sickness:1|end:600"
};

this.rand5_6 = {
	text: "You drink the dark liquid, feeling its fuzzy taste into the throat. For a couple of minute your body is filled with a strange warmth that makes you dizzy and feverish. You have to rest against a wall to not fall down. When the feeling is vanished, you feel considerably better than before.",
	c1: "Maybe it was a medicine... in a sense.", 
	a1: "sickness:-1|end:800"
};

this.rand5_7 = {
	text: "You drink the dark liquid, feeling its fuzzy taste into the throat. It tastes like a soft drink but after a moment the world start to go round and round, until everything become black...\n\nYou awake in the same place, and you feel better, but how many time is passed?",
	c1: "Anyway, better running than walking!", 
	a1: "sickness:-2|end:3600,18000"
};

//
	
this.rand6 = {
	text: "A group of $SPECIES_S$ partygoers proceeds randomly through the amused crowd, laughing and drinking from a large number of bottles. Someone screams a name, someone else answer and laugh again, but you can't recognize the words. They walk and dance and zigzag without an intentional pattern. Their clothes flaps and swings around them in a swirl of vivid colours while they cross the concourse.",
	c1: "Watch the group stagger along their drunken path", 
	a1: "end:300"
};

//

this.rand7 = {
	text: "'Police! Stop right away!'\n\nYou see a $SPECIES_S$ running for $GENDERPOSS$ life, stumbling and jumping between walls and worried people. Behind $GENDEROBJ$, few meters away, a cop is on a relentless chase, weapon in hand - there's no line of sight to attempt a shot. Suddenly you notice that the fugitive is coming your way, $GENDERPOSS$ mind focused on escaping.",
	c1: "You try jumping on the fugitive and stop $GENDEROBJ$ until the cop arrives",
	a1: "go:rand7_1,rand7_2",
	c2: "You don't disturb the fugitive, but with nonchalance try to delay the cop",
	a2: "go:rand7_3,rand7_4",
	c3: "You sidestep and don't intervene in the situation.",
	a3: "go:rand7_5"
};
	
this.rand7_1 = { 
	text: "You quickly prepare to jump, but at the last moment the fugitive realizes your intentions and execute a sudden swerve. You grasp the air and fall down on the floor with an heavy thud. After a few seconds the cop passes and glance in your direction.",
	c1: "You stand up massaging an elbow. At least you tried.", 
	a1: "stance:2|reputation:-2|end:900"
};
	
this.rand7_2 = { 
	text: "You quickly prepare to jump on the unaware fugitive. You grasp $GENDEROBJ$ shoulder and fall down together on the floor. After a brief fight the cop arrives and stun $GENDEROBJ$, and the $SPECIES_S$ starts suffering an intense and disabilitating pain. The cop look at you and says: 'Excellent work. I think that for the help provided to the police forces you have full right to an adequate reward.' $GENDERSUBJ$ reaches for $GENDERPOSS$ communicator, discover your name from the station database and write something on a tiny keypad. 'Done. Good day sir.'",
	c1: "You feel strangely proud of yourself", 
	a1: "stance:5|reputation:15|credits:50|end:900"
};
	
this.rand7_3 = { 
	text: "Feigning a certain clumsiness you begin to apologize to the cop. $GENDERSUBJ$ finally manages to push you away to continue the pursuit, but has lost valuable time. The fugitive is nowhere to be seen.",
	c1: "Before the cop start suspecting something, you discreetly walk away.",
	a1: "stance:-2|end:900"
};
	
this.rand7_4 = { 
	text: "Feigning a certain clumsiness you begin to apologize to the cop. $GENDERSUBJ$ finally manages to push you away to continue the pursuit, but has lost valuable time. The fugitive is nowhere to be seen.\n\nBefore you can walk away, the cop blocks you - and $GENDERSUBJ$'s not happy. 'Visitor! You are guilty of voluntarily obstructing an officer in the course of $GENDERPOSS$ duty! This is not an offense tha will pass unobserved!",
	c1: "There's no much you can do at this point. Go away as soon as you can.", 
	a1: "stance:-10|bounty:5|end:1200"
};
	
this.rand7_5 = {
	text: "The duo continues running, the cop screaming again $GENDERPOSS$ warnings. $GENDERPOSS$ voice fade in the distance.",
	c1: "Better leave to the cops this stuff.", 
	a1: "end:600"
};

//

this.rand8 = {
	text: "You see a $SPECIES_S$ with dark clothes stealing the wallet of a tourist. In the meanwhile, the thief turns $GENDERPOSS$ head, looks at you and put a finger on $GENDERPOSS$ lips: 'Stay quiet.' Nobody else seems to have realised what is happening.",
	c1: "Call the police!",
	a1: "go:rand8_1",
	c2: "Nothing of my business here...",
	a2: "go:rand8_2,rand8_2,rand8_2,rand8_3"
};

this.rand8_1 = {
	text: "You immediately start calling help to stop the thief, but the $SPECIES_S$ is too nimble to be stopped by the crowd. $GENDERSUBJ$ slide and run away, not before casting a glance of contempt towards you.",
	c1: "I've done my good deed for today",
	a1: "stance:3|reputation:2|end:600"
};
	
this.rand8_2 = {
	text: "You continue to look at the thief, as $GENDERSUBJ$ takes the wallet and put it in one of $GENDERPOSS$ various pockets. $GENDERSUBJ$ smiles at you and a second later you can't see $GENDEROBJ$ anymore.",
	c1: "Money come, money go", 
	a1: "stance:-2|end:600"
};
	
this.rand8_3 = {
	text: "You continue to look the thief, as $GENDERSUBJ$ take the wallet and hide it between the folds of $GENDERPOSS$ clothes. $GENDERSUBJ$ smiles and calmly walks to you as if nothing is happened. Passing beside you $GENDERSUBJ$ puts something in your pocket. But when you turn your head $GENDERSUBJ$'s not there anymore.",
	c1: "I don't believe it's a good idea to return this money to its owner...", 
	a1: "stance:-5|credits:10,80|end:900"
};

//

this.rand9 = { 
	text: "While you're resting the $SPECIES_S$ commander near you says: 'I beg your pardon, I am ashamed but I have another way to put it: me and my crew have docked on this station and we have no way to lift off again. You can hardly believe it, but we are stranded - and this is not exactly a lonely asteroid in the middle of space. I repeat, I am ashamed but I need to go away from here, it's a week and we need to go back home or we will have to start begging for food.' $GENDERSUBJ$ clears $GENDERPOSS$ throat: 'So I have to ask you: would you be so gentle to give us a few resources to save us from this discomfortable woe?'",
	//requires: "fuel>=5",
	
	r1: "fuel>=5",
	c1: "Fellow commanders have to help each other, isn't it?",
	a1: "go:rand9_1",
	
	c2: "I'm sorry, but I'm in dire conditions too. Unfortunately I have nothing to spare",
	a2: "go:rand9_2"
};
	
this.rand9_1 = {
	text: "The commander rejoice at your generosity. 'I won't ask too much! Only the strict necessary. 5t of fuel. Nothing more. And you will have my gratitude, and the gratitude of my crew and their families!'",
	c1: "Don't say a word. I'll order the transfer now.",
	a1: "reputation:50|fuel:-5|end:600",
	c2: "Oh, 5t? Really? Mmm... really sorry... but I haven't so much!",
	a2: "go:rand9_2"
};
	
this.rand9_2 = {
	text: "The commander face turns to a gloomy mask. 'I understand. I'm sorry, I was too much insolent. But you know, when one see a glimpse of hope in the dark... Forget it all about it. Good day, commander'. $GENDERSUBJ$ stands up and slowly walks away.",
	c1: "I'm really sorry. Maybe another time...",
	a1: "end:300",
};

//

this.rand10 = { 
	text: "While you're resting the $SPECIES_S$ commander near you says: 'I beg your pardon, I am ashamed but I have another way to put it: me and my crew have docked on this station and we have no way to lift off again. You can hardly believe it, but we are stranded - and this is not exactly a lonely asteroid in the middle of space. I repeat, I am ashamed but I need to go away from here, it's a week and we need to go back home or we will have to start begging for food.' $GENDERSUBJ$ clears $GENDERPOSS$ throat: 'So I have to ask you: would you be so gentle to give us a few resources to save us from this discomfortable woe?'",
	//requires: "credits>=100",
	
	r1: "credits>=100",
	c1: "Fellow commanders have to help each other, isn't it?",
	a1: "go:rand10_1",
	
	c2: "I'm sorry, but I'm in dire conditions too. Unfortunately I have nothing to spare",
	a2: "go:rand10_2"
};
	
this.rand10_1 = { 
	text: "The commander rejoice at your generosity. 'I won't ask too much! 100cr will be enough to depart from this place. Nothing more. And you will have my gratitude, of my crew and their families!'",
	c1: "Don't say a word. I'll order the transfer now.",
	a1: "reputation:50|credits:-100|end:600",
	c2: "Oh, 100cr? Really? Mmm... really sorry... but I haven't so much!",
	a2: "go:rand10_2"
};
	
this.rand10_2 = { 
	text: "The commander face turns to a gloomy mask. 'I understand. I'm sorry, I was too much insolent. But you know, when one see a glimpse of hope in the dark... Forget it all about it. Good day, commander'. $GENDERSUBJ$ stands up and slowly walks away.",
	c1: "I'm really sorry. Maybe another time...",
	a1: "end:300",
};

//

this.rand11 = {
	text: "A group of $SPECIES_P$ approaches you and ask for a donation to their '$CHARITY$' charity.",
	requires: "credits>=1",
	r1: "credits>=1",
	c1: "Donate 1cr",
	a1: "reputation:1|credits:-1|go:rand11_1",
    
    r2: "credits>=10",
	c2: "Donate 10cr",
	a2: "reputation:10|credits:-10|go:rand11_1",
    
    r3: "credits>=100",
	c3: "Donate 100cr",
	a3: "reputation:50|credits:-100|go:rand11_2",
    
    r4: "credits>=1000",
	c4: "Donate 1000cr",
	a4: "reputation:100|credits:-1000|go:rand11_2",
    
    r5: "credits>=10000",
	c5: "Donate 10000cr",
	a5: "reputation:1000|credits:-10000|go:rand11_3",
    
	c6: "Sorry, I have nothing to give",
	a6: "go:rand11_4"
};

this.rand11_1 = {
	text: "They smile at you: 'Thanks for the donation!'",
	c1: "It was nothing, really",
	a1: "end:300"
};

this.rand11_2 = {
	text: "They didn't expected it: 'Thanks! That's a big sum of money! Good day to you sir!'",
	c1: "It was nothing, really",
	a1: "end:300"
};

this.rand11_3 = {
	text: "They stare at you, jaw dropped, petrified from astonishment: '...Really!? We almost can't believe it! You're fantastic!' Someone hugs you, and someone kisses you, but in the euphoric confusion you can't say who's who.",
	c1: "It was... nothing... really",
	a1: "end:300"
};

this.rand11_4 = {
	text: "They smile at you. 'Remember our cause!'",
	c1: "I'll remember (at least for a few minutes)",
	a1: "end:180"
};

//

this.rand12 = {
	text: "Next to you, two bulky $SPECIES_P$ start fighting over a trivial matter. They seem willing to come to blows just to prove who is right. The place is pretty crowded so this could be a really bad thing.",
	
	c1: "If they're so stupid they want to handle their issues in this way, be it so, I'll leave them alone",
	a1: "go:rand12_1",
	
	c2: "Better to stop them. Other people could get involved in the fight.",
	a2: "go:rand12_2,rand12_3",
};

this.rand12_1 = {
	text: "As you walk away you listen to the $SPECIES_P$ talking with louder voices, like thunders rumbling. After seconds a brawl quickly develops. People start screaming and running. As you approach an elevator, you can see the cops arriving to the scene to stop the fights.",
	c1: "I couldn't possibly avoid all this chaos.",
	a1: "end:600"
};


this.rand12_2 = {
	text: "The $SPECIES_P$ look at you while you do your peacemaker speech, and after a glimpse of understanding between them, they decide to beat YOU.",

	c1: "Hey I'm coming in peace...",
	a1: "go:rand12_4",

    r2: "strength>=80",
	c2: "I didn't want to resort to violence, but I see no choice...",
	a2: "if:strength>=120?go:rand12_5;go:rand12_4"
};

this.rand12_3 = {
	text: "The $SPECIES_P$ look at you while you do your peacemaker speech, and in minutes they calm down and start reasoning. The discussion ends with a handshake, and they give you a couple of strong pats on the back that leave you breathless.",
	c1: "And fortunately they were in a good mood...",
	a1: "reputation:10|end:900"
};

this.rand12_4 = {
	text: "They are too strong for you. You try your best, but it's not enough. They left you lying on the floor and leave together trading compliments.",
	c1: "We could say I win. But I need to lay down for a while...",
	a1: "reputation:5|health:1,2|end:3600"
};

this.rand12_5 = {
	text: "Surprised, the aliens can't coordinate their attacks. Your punch knocks out one of them, and other, suddenly panicked, runs away.",

	c1: "I suppose I could call it a day.",
	a1: "reputation:5|end:900",

    r2: "stance<0",
	c2: "I suppose I deserve some sort of loot as prize...",
	a2: "credits:10,200|stance:-8|end:1200"
};

//

this.rand13 = {
	text: "You walk for awhile but nothing seems to attract your interest. This place seems really boring.",
	c1: "Even here, nothing to see",
	a1: "end:600"
};

//

this.rand14 = {
	text: "The corridors you explore are empty. The feeling is pretty unsettling. Maybe you went in the wrong direction?",
	c1: "I think I took a wrong turn, I'll backtrack",
	a1: "end:1200"
};

//

this.rand15 = {
	text: "This part of the station is less populated. You see groups of $SPECIES_P$ chatting. They look at you when you pass near them. They're not smiling.",
	c1: "Maybe I won't pass too close to them",
	a1: "end:600"
};

//

this.rand16 = {
	text: "You had entered the wrong corridor, and now you must walk through conducts, alleys, crossroads, climb ladders and use secondary elevators. You can't understand precisely where you are. You're lost. After asking several people you manage to reach your destination. You sight with relief.",
	c1: "What a nightmare!",
	a1: "end:1800"
};

//

this.rand17 = {
	text: "The atmosphere is very relaxed, but also dull. People walk slowly, their chat has a distinct monotonous sound. There's also a strange smell in the air. You feel dizzy for a moment.",
	c1: "I hope this will pass soon",
	a1: "end:300"
};

//

this.rand18 = {
	text: "A sexy and suave $SPECIES_S$, from the corner of an alley, asks you with a mellow voice if you want some company. 'Are 80cr enough to pay for a little bit of relax, away from you stressful life?'",
	requires: "credits>=80",

	c1: "Thanks for the offer, but I'm not so stressed...",
	a1: "end:300",

    //r2: "credits>=80",
	c2: "Yes, maybe I need this right now",
	//a2: "credits:-80|time:7200|go:rand18_1",
	a2: "if:credits>=80?go:rand18_1;go:rand18_2",

    //r3: "credits>=160",
	c3: "Let's make it 160cr and spend a little more time together",
	//a3: "credits:-160|time:14400|go:rand18_2"
    a3: "if:credits>=160?go:rand18_3;go:rand18_2",
};

this.rand18_1 = {
	text: "You spend time with your charming partner. A very relaxing experience, as promised.",
	c1: "Time to go back on the street",
	a1: "credits:-80|sickness:0,1|end:0"
};

this.rand18_2 = {
    text: "'Are you joking! You have no money for this! And I don't do work for charity!'\nThat said, the $SPECIES_S$ walks away swearing against you.",
    c1: "I think she likes me",
    a1: "end:300"
};

this.rand18_3 = {
	text: "You spend so much time with your charming partner. A very fun and relaxing experience, maybe more than advertised.",
	c1: "Unfortuntaley it's time to go back on the street...",
	a1: "credits:-160|sickness:0,2|end:0"
};

//

this.rand19 = {
	text: "You're walking through a massive crowd, when you feel a rather strange movement around you. When you find a free corner you check your clothes, and discover that someone has mugged part of your credits...",
	requires: "credits>=200",
	c1: "Still they haven't taken all",
	a1: "credits:-10,-100|end:600"
};

//

this.rand20 = {
	text: "A drunken $SPECIES_S$ asks you if you can bring $GENDEROBJ$ home. Problem is: you can't understand where $GENDERSUBJ$ lives. $GENDERSUBJ$ tries to give indication but it's more confusing than before.",
	c1: "Let's try another time. Which way did you came?",
	a1: "go:rand20_1",
	c2: "I give up",
	a2: "end:900"
};

this.rand20_1 = {
    text: "You accompany the $SPECIES_S$ from corridor to corridor, until you stop at the center of an intersection. Your companion looks around aimlessly. After a minute or two, he points a seemingly random direction.",
    c1: "I hope this is it",
    a1: "time:300|go:rand20_2,rand20_2,rand20_2,rand20_3",
    c2: "No, I can't stand it anymore",
    a2: "end:900"
};

this.rand20_2 = {
    text: "The $SPECIES_S$ seems more confident, but you continue walking without a coherent destination. This thing is going to last forever.",
    c1: "Ok, only another corridor and then I give up",
    a1: "time:300|go:rand20_2,rand20_2,rand20_2,rand20_4",
    c2: "I'm sorry but I have no more time... good luck and good bye",
    a2: "end:900"
};

this.rand20_3 = {
    text: "Finally you arrive to the door of a humble apartment. The $SPECIES_S$ is very happy and gives you a strong handshake. He thanks you for an entire minute before leaving.",
    c1: "Thanks...",
    a1: "end:300"
};

this.rand20_4 = {
    text: "Finally you arrive to the door of a humble apartment. The $SPECIES_S$ is very happy and gives you a strong handshake. He thanks you for an entire minute before leaving. Before entering the apartment, the drunkard throw in your hands some credits: 'I apologize for the hassle', mumbles.",
    c1: "Thanks...",
    a1: "credits:50,100|end:300"
};

//

this.rand21 = {
	text: "As you walk you encounter an emergency medical camp. A man in bio-suit is telling people that an alien flu is just arrived on the station. There are already some cases, they are treating it. There's no reason to panic, but everyone should be more careful from now on.",
	requires: "sickness<2",
	c1: "I'm already careful. I don't need any more hints",
	a1: "sickness:0,1|end:300",
	c2: "Oh yeah perfect, better to stay at large.",
	a2: "sickness:0,1|end:300"
};

//

this.rand22 = {
	text: "You stop to listen, together with other persons, to a robo-bard at a corner of the street. It is telling	a story of brave pilots, daredevil bounty hounters, of fearful pioneers on desolate colonies, and greedy explorers on haunted derelicts, accompanied by an ancient and partially ruined song. The robo-bard moves and emit lights to underline the most dangerous moments of the story, and the audience is silent in awe.",
	requires: "gov=feudal",

    r1: "credits>=5",
	c1: "Aye bard, your stories are well worth 5cr",
	a1: "credits:-5|end:1200",

    r2: "credits>=1",
	c2: "Aye bard, your stories are well worth 1cr",
	a2: "credits:-1|end:1200",

	c3: "Aye bard, good story, I'll give you my approval",
	a3: "end:1200"
};

this.rand23 = {
	text: "A group of $SPECIES_S$ hunters cross through the station riding their mounts - not without some difficulty, but noone laughs or joke at them. Meanwhile, the hunters servants unkindly push the people away from the path of their masters.\n\nListening to the hunters chatting you understand that it's better for you if you don't understand the game they're searching for...",
	requires: "gov=feudal",
	c1: "I want to stay as far as possible from those creepies",
	a1: "end:300"
};

this.rand24 = {
	text: "You discover a semidestroyed droid among the trash. It is still functioning, endlessly repeating words that you barely understand. It could be a name, or anything else, really. You don't know why the droid is repeating these words.",
	requires: "gov=anarchy-feudal-multigovernmental-dictatorship|tech=lowtech",
	c1: "Probably it's only some glitch in the software",
	a1: "end:120",
};

this.rand25 = {
	text: "'Hello visitor!'\n\nA commercial droid waves to you and recommend to visit the station market, wishing you can do better purchases.\nAfter some seconds, and a lot of ticking and clicking, he suggest you to invest in $COMMODITY$.\n\nOn a side of the robot you notice the near-erased logo of a group of market brokers.",
	requires: "gov=corporate",
	c1: "I don't think it is completely unbiased in its opinions...",
	a1: "end:180"
};

this.rand26 = {
	text: "As you walk with other people, you're pushed away. You help a tourist that was falling over, and turn your head to see a $SPECIES_S$ supercelebrity, accompanied by $GENDERPOSS$ entourage and a little army of bodyguards. After them, another little army of reporters follows, photographs and photo-drones. There are flashes, there are questions, there is some scuffle. When the group is passed, you can resume your walk.",
	requires: "gov=democracy-corporate",
	c1: "I can't stand these VIPs",
	a1: "end:300"
};

this.rand27 = {
	text: "Suddenly the lights go out. The station switches to emergency lights. A voice in the intercomm starts repeating: 'THERE HAS BEEN A FAILURE ON THE ELECTRIC POWERGRID. WE SUGGEST VISITORS TO DO NOT PANIC. THE LIGHTS WILL BE RESTORED AS SOON AS POSSIBLE.'\n\nAfter a few minutes, the ligths go back.",
	requires: "tech=lowtech",
	c1: "Ok, everything's normal",
	a1: "end:180"
};

this.rand28 = {
	text: "Suddenly you notice a noise similar to a waterfall, and you see puddles of water on the floor. A voice in the intercomm starts repeating: 'THERE HAS BEEN A FAILURE IN THE WATER SYSTEM. WE SUGGEST VISITORS TO DO NOT PANIC. THE MALFUNCTIONING WILL BE FIXED AS SOON AS POSSIBLE.'\n\nAfter a few minutes, the waterfall noises stop.",
	requires: "tech=lowtech",
	c1: "Ok, everything's normal, I suppose",
	a1: "end:180"
};

this.rand29 = {
	text: "Suddenly you notice a strange smell in the air, and you see some people passing out. A voice in the intercomm starts repeating: 'THERE HAS BEEN A FAILURE IN THE VENTILATION SYSTEM. WE SUGGEST VISITORS TO DO NOT PANIC. THE MALFUNCTIONING WILL BE FIXED AS SOON AS POSSIBLE.'\n\nAfter a few minutes, the air return fresh.",
	requires: "tech=lowtech",
	c1: "Ok, everything is normal again... fortunately",
	a1: "end:180"
};

this.rand30 = {
	text: "As you walk in a quiet part of the area, you see some kids 'tagging' a corridor wall with colorful and luminescent spray paint. You can't recognize a precise shape or element, every color and line seems to mold and fuse into the others, like living creatures.",
	requires: "flag_defacedShip=0",
	c1: "This is a real mess. I'll call the security to stop this defacement!",
	a1: "go:rand30_1,rand30_2",
	c2: "It's none of my concern. And these corridors need a repaint, after all...",
	a2: "stance:-2|end:120"
};

this.rand30_1 = {
	text: "You call a Galsec official, that readily come to the place. The kids have run away, but you can point at the wall painted. The official nods gravely and thanks you for your report.",
	c1: "It was my duty",
	a1: "stance:2|end:240"
};

this.rand30_2 = {
	text: "You call a Galsec official, that readily come to the place. The kids have run away, but you can point at the wall painted. The official nods gravely and thanks you for your report.",
	c1: "It was my duty",
	a1: "stance:2|flag_defacedShip:1|end:240"
};
