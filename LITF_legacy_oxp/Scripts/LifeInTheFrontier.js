"use strict";

this.name	= "LITF";
this.author	= "BeeTLe BeTHLeHeM";
this.copyright	= "2015 BeeTLe BeTHLeHeM";
this.description= "An attempt to provide more roleplaying environment on space stations: the player can get a casual event, that could bring an advantage or a risk, a reward or a trouble.";
this.version	= "0.7.1";
this.licence	= "CC BY-NC-SA 4.0";

///////////////////////////////////////

this.startUp = function() {
	var _co = worldScripts.LITF_Common;

	this.LITF_cmdStats = {};
	this.LITF_flags = {};
	
	this.LITF_missionAvail = {};

    // Clean obsolete variables //
    if (missionVariables.LITF_init) { missionVariables.LITF_init = null; }

    // Clean inactive variables //
    if (missionVariables.LITF_lastGymDay) { missionVariables.LITF_lastGymDay = null; }
    if (missionVariables.LITF_enableContractor) { missionVariables.LITF_enableContractor = null; }
    if (missionVariables.LITF_enableMission) { missionVariables.LITF_enableMission = null; }
    if (missionVariables.LITF_contractorData) { missionVariables.LITF_contractorData = null; }
    if (missionVariables.LITF_missionData) { missionVariables.LITF_missionData = null; }

    // Init legit variables //

    // Commander Stats
    this.$loadCommanderStats();

    // Flags
    this.$loadOXPFlags();

    // Missions data
    if (missionVariables.LITF_packageMissionData) {
        this.LITF_packageMissionData = missionVariables.LITF_packageMissionData;
    } else {
        this.LITF_packageMissionData = null;
    }
    
    /*
    if (missionVariables.LITF_cargoMissionData) {
        this.LITF_cargoMissionData = missionVariables.LITF_cargoMissionData;
    } else {
        this.LITF_cargoMissionData = null;
    }

    if (missionVariables.LITF_passengerMissionData) {
        this.LITF_passengerMissionData = missionVariables.LITF_passengerMissionData;
    } else {
        this.LITF_passengerMissionData = null;
    }
    */

    ////////

	// Store OXP inner variables
	this.LITF_varStore = {};
	
	// Settings //
	this.LITF_logger = 0;

	this.setInterface();

    _co.$log("[startUp] [[[ LIFE IN THE FRONTIER v" + this.version + " ]]]");
};


this.$loadOXPFlags = function() {
    var mvOXPFlagsArr = missionVariables.LITF_flagArr;

    if (!mvOXPFlagsArr || mvOXPFlagsArr.indexOf("map:") == -1) {
        this.LITF_flags = {
            getDrunk: 0,
            drinkChallenge: 0,
            chatBartender: 0,
            offerToCounter: 0,
            artGallery: 0,
            cinemaHall: 0,
            operaHouse: 0,
            defacedShip: 0,
            fitBadge: -1,
            lastFitSession: -1,
            fitDamageChance: 0,
            maxStrength: 2000
        };
    } else {
        mvOXPFlagsArr = mvOXPFlagsArr.substring(4);
        var OXPFlagsMap = mvOXPFlagsArr.split(","); // array of items
        for (var c = 0; c < OXPFlagsMap.length; c++) {
            var OXPFlagsItem = OXPFlagsMap[c].split("="); // param=value
            
            var vartype = OXPFlagsItem[0].substring(0, 1);
            var varname = OXPFlagsItem[0].substring(1);

            if (vartype = "S") {
                this.LITF_flags[varname] = OXPFlagsItem[1];
            } else if (vartype = "N") {
                this.LITF_flags[varname] = parseInt(OXPFlagsItem[1]);
            }
        }
    }
};


this.$loadCommanderStats = function() {
    var mvCmdStatsArr = missionVariables.LITF_cmdStatsArr;
    
    if (!mvCmdStatsArr || mvCmdStatsArr.indexOf("map:") == -1) {
        this.LITF_cmdStats = {
            reputation: 0,
            health: 0,
            sickness: 0,
            strength: 200,
            stance: 0
        };
    } else {
        mvCmdStatsArr = mvCmdStatsArr.substring(4);
        var cmdStatsMap = mvCmdStatsArr.split(","); // array of items
        for (var c = 0; c < cmdStatsMap.length; c++) {
            var cmdStatsItem = cmdStatsMap[c].split("="); // param=value
            
            this.LITF_cmdStats[cmdStatsItem[0]] = parseInt(cmdStatsItem[1]);
        }
    }
};


this.$saveCommanderStats = function() {
    var cmdStats = "map:reputation=" + this.LITF_cmdStats.reputation 
        + ",health=" + this.LITF_cmdStats.health 
        + ",sickness=" + this.LITF_cmdStats.sickness 
        + ",strength=" + this.LITF_cmdStats.strength 
        + ",stance=" + this.LITF_cmdStats.stance;
        
    missionVariables.LITF_cmdStatsArr = cmdStats;
};


this.$saveOXPFlags = function() {
    var flags = "map:"
        +  "NgetDrunk=" + this.LITF_flags.getDrunk 
        + ",NdrinkChallenge=" + this.LITF_flags.drinkChallenge 
        + ",NchatBartender=" + this.LITF_flags.chatBartender 
        + ",NofferToCounter=" + this.LITF_flags.offerToCounter 
        + ",NartGallery=" + this.LITF_flags.artGallery 
        + ",NcinemaHall=" + this.LITF_flags.cinemaHall 
        + ",NoperaHouse=" + this.LITF_flags.operaHouse 
        + ",NdefacedShip=" + this.LITF_flags.defacedShip
        + ",NfitBadge=" + this.LITF_flags.fitBadge
        + ",NlastFitSession=" + this.LITF_flags.lastFitSession
        + ",NfitDamageChance=" + this.LITF_flags.fitDamageChance
        + ",NmaxStrength=" + this.LITF_flags.maxStrength;
        
    missionVariables.LITF_flagsArr = flags;
};


this.playerWillSaveGame = function() {
	// Store variables in savegame

	// Commander stats
    this.$saveCommanderStats();

	// Flags
	this.$saveOXPFlags();
	
	// Missions data
	missionVariables.LITF_packageMissionData = this.LITF_packageMissionData;

    /*
    missionVariables.LITF_cargoMissionData = this.LITF_cargoMissionData;
    missionVariables.LITF_passengerMissionData = this.LITF_passengerMissionData;
    */
};


this.shipWillDockWithStation = function(station) {
    var _mis = worldScripts.LITF_Mission;
        
    // Init station variables
    if (this.LITF_varStore["ARTGENRE"]) { this.LITF_varStore["ARTGENRE"] = null; }
	if (this.LITF_varStore["MUSICGENRE"]) { this.LITF_varStore["MUSICGENRE"] = null; }
	if (this.LITF_varStore["MOVIEGENRE"]) { this.LITF_varStore["MOVIEGENRE"] = null; }

	// Init station flags
	this.LITF_flags.getDrunk = 0;
	this.LITF_flags.drinkChallenge = 0;
	this.LITF_flags.chatBartender = 0;
	this.LITF_flags.offerToCounter = 0;
	this.LITF_flags.artGallery = 0;
	this.LITF_flags.cinemaHall = 0;
	this.LITF_flags.operaHouse = 0;
    this.LITF_flags.defacedShip = 0;
    
    // Init mission availability
    this.LITF_missionAvail = {};

    this.LITF_missionAvail["LOUNGE"] = _mis.$createPackageMission("TRANSPORT", "LOUNGE"); // Passengers lounge
    
    /*
    this.LITF_missionAvail["WWVT"] = null; // Wild World Virtual Tour
    this.LITF_missionAvail["OPERA"] = null; // Opera house
    */
};


this.shipDockedWithStation = function() {
	this.$healthManagement();
	
	this.$checkLastFitSession();

	this.setInterface();
};


this.setInterface = function() {
	player.ship.dockedStation.setInterface("LITF_hangarScreen", 
		{
			title: "Disembark from your ship",
			category: "Activity",
			summary: "Visit the station",
			callback: this.$enterStation.bind(this) 
		}
	);
};


///////////////////////////////////////


this.$checkLastFitSession = function() {
    var lfs = this.LITF_flags.lastFitSession;
    if (lfs > -1) {
        var diff = clock.seconds - lfs;
        if (diff > 604800) {
            // More than a week from the last fit session
            this.LITF_cmdStats.strength -= _co.$randomValue(5, 10);
            if (this.LITF_cmdStats.strength < 0) { this.LITF_cmdStats.strength = 0; }
        }
    }
};


this.$healthManagement = function() {
	var chance;

	// Rule 1 - If player sickness > 1, every time he docks on a station 
	//          there's a 10% chance of worsening (+1).

	chance = Math.floor(Math.random() * 100) + 1;
	if (this.LITF_cmdStats.sickness > 1 && chance < 10) {
		this.LITF_cmdStats.sickness += 1;
	}

	// Rule 2 - If player sickness = 1, every time he docks on a station 
	//          there's a 10% chance of healing (sickness = 0)

	chance = Math.floor(Math.random() * 100) + 1;
	if (this.LITF_cmdStats.sickness == 1 && chance < 10) {
		this.LITF_cmdStats.sickness = 0;
	}

	// Rule 3 - If player health = 1, every time he docks on a station 
	//          there's a 10% chance of healing (health = 0)

	chance = Math.floor(Math.random() * 100) + 1;
	if (this.LITF_cmdStats.health == 1 && chance < 10) {
		this.LITF_cmdStats.health = 0;
	}

	// Rule 4 - If player health > 1, every time he docks on a station 
	//          there's a 10% chance of worsening (+1).

	chance = Math.floor(Math.random() * 100) + 1;
	if (this.LITF_cmdStats.health > 1 && chance < 10) {
		this.LITF_cmdStats.health += 1;
	}

	// Rule 5 - If player health > 2 and sickness < 2, every time he docks on a station 
	//          there's a 10% chance of generating infections (sickness +1)

	chance = Math.floor(Math.random() * 100) + 1;
	if (this.LITF_cmdStats.health > 2 && this.LITF_cmdStats.sickness < 2 && chance < 10) {
		this.LITF_cmdStats.sickness += 1;
	}

	// Rule 6 - If player health > 2 or player sickness > 2 
	//          there's a chance of 10% of reducing physical shape 

	chance = Math.floor(Math.random() * 100) + 1;
	if ((this.LITF_cmdStats.health > 2 || this.LITF_cmdStats.sickness > 2) && chance < 10) {
		var decay = Math.floor(Math.random() * 10) + 1;

		this.LITF_cmdStats.strength -= decay;
		if (this.LITF_cmdStats.strength < 10) {
			this.LITF_cmdStats.strength = 10;
		}
	}
};


this.$enterStation = function() {
    var _nav = worldScripts.LITF_Navigation; 
    _nav.$startNavigation();
};

