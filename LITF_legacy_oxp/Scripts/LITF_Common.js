"use strict";

this.name	= "LITF_Common";
this.author	= "BeeTLe BeTHLeHeM";
this.copyright	= "2015 BeeTLe BeTHLeHeM";
this.description= "Life in the Frontier - Common";
this.version	= "0.7.1";
this.licence	= "CC BY-NC-SA 4.0";

///////////////////////////////////////

// Groups labels
this.groupsLabels = ["GalCop", "GalBook", "GalChurch", "GalShady", "GalMedical", "GalCivic", "GalTourism", "GalShopping", "GalNature", "GalCulture"];

// System governments labels
this.govLabels = ["Anarchy", "Feudal", "Multigovernmental", "Dictatorship", "Communist", "Confederacy", "Democracy", "Corporate"];

this.$log = function(msg) {
	if (worldScripts.LITF.LITF_logger == 1) {
		log(this.name, msg);
	}
};


this.$formatNumber = function(number) {
	var numStr = number.toString();
	var numFormat = "";

	if (numStr.indexOf('.') > -1) {
		var length = numStr.length;
		var dot = numStr.indexOf('.');

		if (length - dot > 3) {
			numFormat = numStr.substring(0, dot + 3);
		} else {
			numFormat = numStr;
		}
	} else {
		numFormat = numStr;
	}

	return numFormat;
};


this.$replaceAll = function(find, replace, str) {
  return str.split(find).join(replace);
};


this.$extractChoices = function(pData) {
	var choiceCounter = 1;
	var valid = 1;

	var placeChoiceList = {};

	//var choiceRequirements = pData.choiceRequires;
	//this.$log("[extractChoices] choiceRequirements = " + choiceRequirements);

	while (valid == 1) {
		//var cl = "c" + choiceCounter;
		var placeChoice = eval("pData.c" + choiceCounter + ";");

		if (placeChoice) {
			placeChoice = this.$replaceConstants(placeChoice);

            var vChoice = 0;
            var choiceReq = eval("pData.r" + choiceCounter + ";");
            if (choiceReq) {
                //var vChoice = this.$validateRequirements(choiceRequirements, choiceCounter - 1);
                vChoice = this.$validateRequirements(choiceReq);
            } else {
                // No requirements - always valid
                vChoice = 1;
            }

			this.$log("[extractChoices] c" + choiceCounter + " = " + placeChoice + " :: vChoice = " + vChoice);

			if (vChoice == 1) {
				eval("placeChoiceList[\"c" + choiceCounter + "\"] = \"" + placeChoice + "\";");
			}
			choiceCounter++;
		} else {
			valid = 0;
		}
	}

	return placeChoiceList;
};


//this.$validateRequirements = function(requirements, choiceIdx) {
this.$validateRequirements = function(choiceReq) {
	//if (requirements) {
		//var reqSplit = requirements.split("|");
		//this.$log("[validateChoice] require = " + reqSplit[choiceIdx]);

		//var LITF_items = reqSplit[choiceIdx].split(",");
        var LITF_items = choiceReq.split(",");

		for (var i = 0; i < LITF_items.length; i++) {
			//var reqChoice = LITF_items[i].split(":");
			//var validate = this.$checkRequirements(reqChoice);
            var validate = this.$checkRequirements(LITF_items[i]);

			//this.$log("[validateChoice] " + reqChoice[0] + " : " + reqChoice[1] + " = " + validate);
            this.$log("[validateChoice] " + LITF_items[i] + " = " + validate);

			if (validate == 0) { 
				return 0; 
			}
		}
	//}
	
	return 1;
};


this.$readNumericParameter = function(param) {
	var value = 0;
					
	if (param.indexOf(",") == -1) {
		value = parseInt(param);
	} else {
		var _paramSplit = param.split(",");

		var minValue = parseInt(_paramSplit[0]);
		var maxValue = parseInt(_paramSplit[1]);

		value = this.$randomValue(minValue, maxValue);
	}
	
	return value;
};


this.$randomValue = function(min, max) {
	if (min > max) {
		var buf = min;
		min = max;
		max = buf;
	}

	var rv = Math.floor(Math.random() * (max - min + 1)) + min;
	
	//this.$log("[randomValue] (" + min + ", " + max + ") = " + rv);
	
	return rv;
};

this.$checkValue = function(val1, val2, oper) {
    var result = 1;
    
    if (oper == "!=" && val1 == val2) { result = 0; }
    if (oper == "=" && val1 != val2) { result = 0; }
    
    if (oper == "<=" && val1 > val2) { result = 0; }
    if (oper == "<" && val1 >= val2) { result = 0; }
    
    if (oper == ">=" && val1 < val2) { result = 0; }
    if (oper == ">" && val1 <= val2) { result = 0; }
    
    if (oper == ":") {
        result = 1;
        this.$log("[checkValue] ERROR!!! Wrong operator ':' !");
    }
    
    return result;
};


this.$checkRequirements = function(req) {
	var _litf = worldScripts.LITF;

    var oper = "";
    
    if (req.indexOf("!=") > -1) {
        oper = "!=";
    } else if (req.indexOf("<=") > -1) {
        oper = "<=";
    } else if (req.indexOf(">=") > -1) {
        oper = ">=";
    } else if (req.indexOf("=") > -1) {
        oper = "=";
    } else if (req.indexOf("<") > -1) {
        oper = "<";
    } else if (req.indexOf(">") > -1) {
        oper = ">";
    // Cannot use ':', ambiguous character for conditions
    //} else if (req.indexOf(":") > -1) {
        //oper = ":";
    }
    
    var reqSplit = req.split(oper);

	if (reqSplit[0].indexOf("flag_") == 0) {
        // flag_ management
        
		var flagName = reqSplit[0].substring(5);
		var validVal = parseInt(reqSplit[1]);

		var flagValue = eval("_litf.LITF_flags." + flagName + ";");
		if (validVal != flagValue) { return 0; }
    
    } else if (reqSplit[0].indexOf("mt_") == 0) {
        // cargo commodities management
         
        var mtLabel = reqSplit[0].substring(3);
        eval("var mtValue = player.ship.manifest." + mtLabel + ";");

        var mtCheck = parseInt(reqSplit[1]);

        var result = this.$checkValue(mtValue, mtCheck, oper);
        if (result == 0) { return 0; }	
	} else {
		// other checks

		switch (reqSplit[0]) {
            case "bountyfine":
                var bFine = player.bounty * 500;
                if (player.credits < bFine) { return 0; }
                break;

            case "checkFitBadge":
                var fitBadge = _litf.LITF_flags.fitBadge;

                var cfbVal = parseInt(reqSplit[1]); // 0 - no badge, 1 - expired badge, 2 - valid badge
    
                if (cfbVal == 0) {
                    if (fitBadge != -1) { return 0; }
                    
                } else {
                    if (fitBadge == -1) {
                        return 0;
                    } else {
                        var expired = this.$checkTimeForExpiration(fitBadge);
                        this.$log("[checkRequirements] expired(" + fitBadge + ") = " + expired);
                        
                        if (cfbVal == 1 && expired == 0) {
                            return 0;
                        } else if (cfbVal == 2 && expired == 1) {
                            return 0;
                        }
                    }
                } 
                break;
                
            case "economy":
                var syseco = this.$systemEconomy();
                if (syseco != reqSplit[1]) {
                    return 0;
                }
                break;

            case "gov":
                var sysgov = system.government;
                var govSplit = reqSplit[1].split("-");
                var valid = 0;
                for (var i = 0; i < govSplit.length; i++) {
                    if (this.$decodeGovernment(sysgov) == govSplit[i]) {
                        valid = 1;
                    }
                }
                if (valid == 0) { return 0; }
                break;

            case "paytreatment":
                var heal = 0;

                var tl = this.$systemTechLevel();
                switch (tl) {
                    case "lowtech":
                        heal = 2;
                        break;
                    case "avgtech":
                        heal = 3;
                        break;
                    case "hitech":
                        heal = 5;
                        break;
                }

                var price = heal * 50;

                if (player.credits < price) {
                    return 0;
                }
                break;
                
            case "tech":
                var systech = this.$systemTechLevel();
                if (systech != reqSplit[1]) {
                    return 0;
                }
                break;
			
			case "valid":
				var validVal = parseInt(reqSplit[1]);
				if (validVal == 0) { return 0; }
				break;


            // Value-comparing checks

            
            case "bounty":
                var checkVal = parseInt(reqSplit[1]);
                var result = this.$checkValue(player.bounty, checkVal, oper);
                if (result == 0) { return 0; }
                break;

            case "credits":
                var checkVal = parseInt(reqSplit[1]);
                var result = this.$checkValue(player.credits, checkVal, oper);
                if (result == 0) { return 0; }
                break;

            case "fuel":
                var checkVal = parseInt(reqSplit[1]);
                var result = this.$checkValue(player.ship.fuel, checkVal, oper);
                if (result == 0) { return 0; }
                break;

            case "health":
                var checkVal = parseInt(reqSplit[1]);
                var result = this.$checkValue(_litf.LITF_cmdStats.health, checkVal, oper);
                if (result == 0) { return 0; }
                break;

            case "maxstrength":
                var checkVal = parseInt(reqSplit[1]);
                var result = this.$checkValue(_litf.LITF_flags.maxStrength, checkVal, oper);
                if (result == 0) { return 0; }
                break;

            case "reputation":
                var checkVal = parseInt(reqSplit[1]);
                var result = this.$checkValue(_litf.LITF_cmdStats.reputation, checkVal, oper);
                if (result == 0) { return 0; }
                break;

            case "sickness":
                var checkVal = parseInt(reqSplit[1]);
                var result = this.$checkValue(_litf.LITF_cmdStats.sickness, checkVal, oper);
                if (result == 0) { return 0; }
                break;

            case "stance":
                var checkVal = parseInt(reqSplit[1]);
                var result = this.$checkValue(_litf.LITF_cmdStats.stance, checkVal, oper);
                if (result == 0) { return 0; }
                break;

            case "strength":
                var checkVal = parseInt(reqSplit[1]);
                var result = this.$checkValue(_litf.LITF_cmdStats.strength, checkVal, oper);
                if (result == 0) { return 0; }
                break;

			// XXX Contractordata?
			/*
			case "hintData":
				var value = parseInt(reqSplit[1]);
				var hint = worldScripts.LITF.LITF_hintData;
				if (value == 0) {
					if (hint != undefined && hint != "") { return 0; }
				} else if (value == 1) {
					if (hint == undefined || hint == "") { return 0; }
				}
				break;
			*/
		}
	}

	return 1;
};


this.$systemEconomy = function() {
	var ecoType = "balanced";

	var economy = system.economy;
	if (economy < 3) {
		ecoType = "industrial";	
	} else if (economy > 4) {
		ecoType = "agriculture";
	}

	//this.$log("[stationCategories] System Economy = " + ecoType);

	return ecoType;
};


this.$systemTechLevel = function() {
	var tech = "avgtech";
	
	var tl = system.techLevel;
	if (tl < 5) {
		tech = "lowtech";
	} else if (tl > 9) {
		tech = "hitech";
	}

	//this.$log("[stationCategories] System Technology = " + tech);

	return tech;	
};

this.$decodeGovernment = function(gov) {
	var govText = "";

	switch (gov) {
		case 0:
			govText = "anarchy";
			break;
		case 1:
			govText = "feudal";
			break;
		case 2:
			govText = "multigovernmental";
			break;
		case 3:
			govText = "dictatorship";
			break;
		case 4:
			govText = "communist";
			break;
		case 5:
			govText = "confederacy";
			break;
		case 6:
			govText = "democracy";
			break;
		case 7:
			govText = "corporate";
			break;
	}

	return govText;
};

this.$stationCategories = function() {
	/*
		List of station.allegiance values:
			hunter
			galcop
			neutral
			pirate
			chaotic
			thargoid
			private
			restricted
	*//*
	var allegiance = player.ship.dockedStation.allegiance;
	this.$log("[stationCategories] Space Station Allegiance = " + allegiance);
	*/

	/*
		List of system.government values:
			0: Anarchy
			1: Feudal
			2: Multi-Governmental
			3: Dictatorship
			4: Communist
			5: Confederacy
			6: Democracy
			7: Corporate State
	*/
	var government = "gov_" + system.government;
	//this.$log("[stationCategories] System Government = " + government);
	
	var tech = this.$systemTechLevel();

	var ecoType = this.$systemEconomy();
	
	var sCategories = [ government, tech, ecoType ];

	return sCategories;
};


this.$screenBackground = function(screenBgStr) {
	var screenBg = "";

	if (screenBgStr == undefined || screenBgStr == "") {
		screenBg = this.$genericScreenBackground();
	} else {
		var screenBgSplit = screenBgStr.split(",");
		var bgIdx = Math.floor(Math.random() * screenBgSplit.length);
		screenBg = screenBgSplit[bgIdx];
	}

	if (screenBg != "") {
		setScreenBackground(screenBg);
	}
};


this.$genericScreenBackground = function() {
	var background = "";
	
	var sCategories = this.$stationCategories();
	var catIdx = Math.floor(Math.random() * 3); 

	var cat = sCategories[catIdx];

	//if (cat == "gen") {
	//	cat = "gov_" + system.government;
	//}

	// Set background depending on system government
	switch (cat) {
		case "gov_0": 
			background = "litf_bg_anarchy.png";
			break;
		case "gov_1":
			background = "litf_bg_feudal.png";
			break;
		case "gov_2":
			background = "litf_bg_multigovernment.png";
			break;
		case "gov_3":
			background = "litf_bg_dictatorship.png";
			break;
		case "gov_4":
			background = "litf_bg_communist.png";
			break;
		case "gov_5":
			background = "litf_bg_confederacy.png";
			break;
		case "gov_6":
			background = "litf_bg_democracy.png";
			break;
		case "gov_7":
			background = "litf_bg_corporate.png";
			break;
		case "hitech":
			background = "litf_bg_hitech.png";
			break;
		case "lowtech":
			background = "litf_bg_lowtech.png";
			break;
		case "industrial":
			background = "litf_bg_industrial.png";
			break;
		case "agriculture":
			background = "litf_bg_agricultural.png";
			break;
	}

	// Random generic corridor
	if (background == "") {
		var corrIdx = Math.floor(Math.random() * 11) + 1;
		background = "litf_bg_corridor" + corrIdx + ".png";
	}

	return background;
};


this.$replaceConstants = function(text) {
	var _litf = worldScripts.LITF;
	var _ec = worldScripts.LITF_EventsCatalog;

	var constArr;
	var length;
	var rand;
	var str;

    if (text.indexOf("$MUSEUMPIECE$") > -1) {
    	/*
        constArr = _ec.arrMuseumPieces;
        length = constArr.length;
        rand = Math.floor(Math.random() * length);
        text = text.replace("$MUSEUMPIECE$", constArr[rand]);
        */
        text = text.replace("$MUSEUMPIECE$", expandDescription("[MuseumPieces]"));
    }

    if (text.indexOf("$LOBBYSNIPPET$") > -1) {
    	/*
        constArr = _ec.arrLobbySnippet;
        length = constArr.length;
        rand = Math.floor(Math.random() * length);
        text = text.replace("$LOBBYSNIPPET$", constArr[rand]);
        */
        text = text.replace("$LOBBYSNIPPET$", expandDescription("[LobbySnippet]"));
    }

	if (text.indexOf("$HOTELROOM$") > -1) {
		var hotelRoom = "";
		rand = Math.floor(Math.random() * 5);
		hotelRoom += rand;
		rand = Math.floor(Math.random() * 4);
		hotelRoom += rand;
		rand = Math.floor(Math.random() * 4);
		hotelRoom += rand;

		text = text.replace("$HOTELROOM$", hotelRoom);
	}

	if (text.indexOf("$CHARITY$") > -1) {
		/*
		constArr = _ec.arrCharity;
		length = constArr.length;
		rand = Math.floor(Math.random() * length);
		text = text.replace("$CHARITY$", constArr[rand]);
		*/
        text = text.replace("$CHARITY$", expandDescription("[Charity]"));
	} 

	if (text.indexOf("$LIQUOR$") > -1) {
		/*
		constArr = _ec.arrLiquors;
		length = constArr.length;
		rand = Math.floor(Math.random() * length);
		text = text.replace("$LIQUOR$", constArr[rand]);
		*/
        text = text.replace("$LIQUOR$", expandDescription("[Liquors]"));
	}
 
	if (text.indexOf("$GOSSIP$") > -1) {
		/*
		constArr = _ec.arrGossip;
		length = constArr.length;
		rand = Math.floor(Math.random() * length);
		text = text.replace("$GOSSIP$", constArr[rand]);
		*/
        text = text.replace("$GOSSIP$", expandDescription("[Gossip]"));
	}

	if (text.indexOf("$NAME$") > -1) {
		var name = randomName() + " " + randomName();
		text = text.replace("$NAME$", name);

		_litf.LITF_varStore["NAME"] = name;
	}

	if (text.indexOf("$SYSTEM$") > -1) {
		var sName = this.$randomSystem(); // "Diso"; // "Leesti";
		text = text.replace("$SYSTEM$", sName);
		
		_litf.LITF_varStore["SYSTEM"] = sName;
	}

	if (text.indexOf("$MINORCARGO$") > -1) {
		var rmc = this.$randomMinorCargo();
		text = text.replace("$MINORCARGO$", rmc);

		//_litf.LITF_varStore["MINORCARGO"] = rmc;
	}

	if (text.indexOf("$COMMODITY$") > -1) {
		/*
		constArr = _ec.arrCommodity;
		length = constArr.length;
		rand = Math.floor(Math.random() * length);
		text = text.replace("$COMMODITY$", constArr[rand]);
		*/
        text = text.replace("$COMMODITY$", expandDescription("[Commodity]"));

		_litf.LITF_varStore["COMMODITY"] = constArr[rand];
	}

	if (text.indexOf("$ARTGENRE$") > -1) {
		var ag = "";

		if (_litf.LITF_varStore["ARTGENRE"] == undefined || _litf.LITF_varStore["ARTGENRE"] == "") {
			/*
			constArr = _ec.arrArt1;
			length = constArr.length;
			rand = Math.floor(Math.random() * length);

			ag = constArr[rand];

			constArr = _ec.arrArt2;
			length = constArr.length;
			rand = Math.floor(Math.random() * length);

			ag += " " + constArr[rand];
			*/
			ag = expandDescription("[Art1]") + " " + expandDescription("[Art2]");

			_litf.LITF_varStore["ARTGENRE"] = ag;
		} else {
			ag = _litf.LITF_varStore["ARTGENRE"];
		}

		text = text.replace("$ARTGENRE$", ag);
	}

	if (text.indexOf("$MUSICGENRE$") > -1) {
		var mug = "";

		if (_litf.LITF_varStore["MUSICGENRE"] == undefined || _litf.LITF_varStore["MUSICGENRE"] == "") {
			/*
			constArr = _ec.arrMusic1;
			length = constArr.length;
			rand = Math.floor(Math.random() * length);

			mug = constArr[rand];

			constArr = _ec.arrMusic2;
			length = constArr.length;
			rand = Math.floor(Math.random() * length);

			mug += " " + constArr[rand];

			constArr = _ec.arrMusic3;
			length = constArr.length;

			rand = Math.floor(Math.random() * length);

			mug += " " + constArr[rand];
			*/
			mug = expandDescription("[Music1]") + " " + expandDescription("[Music2]") + " " + expandDescription("[Music3]");

			_litf.LITF_varStore["MUSICGENRE"] = mug;
		} else {
			mug = _litf.LITF_varStore["MUSICGENRE"];
		}

		text = text.replace("$MUSICGENRE$", mug);
	}

	if (text.indexOf("$MOVIEGENRE$") > -1) {
		var mog = "";

		if (_litf.LITF_varStore["MOVIEGENRE"] == undefined || _litf.LITF_varStore["MOVIEGENRE"] == "") {
			/*
			constArr = _ec.arrMovie1;
			length = constArr.length;
			rand = Math.floor(Math.random() * length);

			mog = constArr[rand];

			constArr = _ec.arrMovie2;
			length = constArr.length;
			rand = Math.floor(Math.random() * length);

			mog += " " + constArr[rand];

			constArr = _ec.arrMovie3;
			length = constArr.length;
			rand = Math.floor(Math.random() * length);

			mog += " " + constArr[rand];
			*/
			mog = expandDescription("[Movie1]") + " " + expandDescription("[Movie2]");

			_litf.LITF_varStore["MOVIEGENRE"] = mog;
		} else {
			mog = _litf.LITF_varStore["MOVIEGENRE"];
		}

		text = text.replace("$MOVIEGENRE$", mog);
	}

	if (text.indexOf("$SPECIES_S$") > -1) {
		if (_litf.LITF_varStore["SPECIES_S"] == undefined || _litf.LITF_varStore["SPECIES_S"] == "") {
			_litf.LITF_varStore["SPECIES_S"] = randomInhabitantsDescription(false);
		}

		text = text.replace("$SPECIES_S$", _litf.LITF_varStore["SPECIES_S"]);
	}

	if (text.indexOf("$SPECIES_P$") > -1) {
		if (_litf.LITF_varStore["SPECIES_P"] == undefined || _litf.LITF_varStore["SPECIES_P"] == "") {
			_litf.LITF_varStore["SPECIES_P"] = randomInhabitantsDescription(true);
		}

		text = text.replace("$SPECIES_P$", _litf.LITF_varStore["SPECIES_P"]);
	}

	if (text.indexOf("$GENDERSUBJ$") > -1) {
		if (_litf.LITF_varStore["GENDER"] == undefined || _litf.LITF_varStore["GENDER"].length == 0) {
			rand = Math.floor(Math.random() * 2);
			_litf.LITF_varStore["GENDER"] = _ec.genderSyntax[rand].split('|');
		}

		text = this.$replaceAll("$GENDERSUBJ$", _litf.LITF_varStore["GENDER"][0], text);
	}

	if (text.indexOf("$GENDEROBJ$") > -1) {
		if (_litf.LITF_varStore["GENDER"] == undefined || _litf.LITF_varStore["GENDER"].length == 0) {
			rand = Math.floor(Math.random() * 2);
			_litf.LITF_varStore["GENDER"] = _ec.genderSyntax[rand].split('|');
		}

		text = this.$replaceAll("$GENDEROBJ$", _litf.LITF_varStore["GENDER"][1], text);
	}

	if (text.indexOf("$GENDERPOSS$") > -1) {
		if (_litf.LITF_varStore["GENDER"] == undefined || _litf.LITF_varStore["GENDER"].length == 0) {
			rand = Math.floor(Math.random() * 2);
			_litf.LITF_varStore["GENDER"] = _ec.genderSyntax[rand].split('|');
		}

		text = this.$replaceAll("$GENDERPOSS$", _litf.LITF_varStore["GENDER"][2], text);
	}
	
	if (text.indexOf("$BOUNTYFINE$") > -1) {
        var bFine = (player.bounty * 500) + "cr";
        text = this.$replaceAll("$BOUNTYFINE$", bFine, text);
	}

	return text;
};


this.$randomMinorCargo = function() {
	var _ec = worldScripts.LITF_EventsCatalog;
	var _rpge = worldScripts.LITF_RPGElements;

	var reputation = worldScripts.LITF.LITF_cmdStats.reputation;
	var repLev = _rpge.$getReputationLevel(reputation);

	var constArr;

	if (repLev < 3) {
		constArr = _ec.minorCargo0;
	} else if (repLev < 6) {
		constArr = _ec.minorCargo1;
	} else if (repLev < 9) {
		constArr = _ec.minorCargo2;
	} else {
		constArr = _ec.minorCargo3;
	}

	var length = constArr.length;
	var rand = Math.floor(Math.random() * length);

	var randomMinorCargo = constArr[rand];

	return randomMinorCargo;
};


this.$randomSystem = function() {
	var valid = 0;
	var sName = "";
	while (valid == 0) {
		var rand = Math.floor(Math.random() * 256);
		sName = System.systemNameForID(rand);
		if (sName != system.name) {
			valid = 1;
		}
	}

	return sName;
};


this.$executeActions = function(actionString) {
    var aSplit = actionString.split("|");

    for (var i = 0; i < aSplit.length; i++) {
        var action = aSplit[i];
        this.$executeSingleAction(action);
    }
};


this.$executeSingleAction = function(action) {
    var _co = worldScripts.LITF_Common;
    var _re = worldScripts.LITF_RandomEvents;
    var _litf = worldScripts.LITF;
    var _nav = worldScripts.LITF_Navigation;

    // split only at the first (main) colon
    var colonPos = action.indexOf(":");
    
    //var items = action.split(":");
    var command = action.substring(0, colonPos); // items[0]
    var param = action.substring(colonPos + 1); // items[1]

    //
    
    this.$log("[executeSingleAction] action = " + action);

    if (command.indexOf("flag_") == 0) {
        // Flag management
        var flagName = command.substring(5);
        var flagValue = parseInt(param);
        
        eval("worldScripts.LITF.LITF_flags." + flagName + " = " + flagValue + ";");

    } else if (command.indexOf("mt_") == 0) {
        // Cargo commodities management
        var mtMod = this.$readNumericParameter(param);
        var mtLabel = command.substring(3);
        var mtValue = eval("player.ship.manifest." + mtLabel);

        if (mtValue + mtMod < 0) {
            mtMod = -mtValue;
        } else if (mtMod > 0 && mtMod > player.ship.cargoSpaceAvailable) {
            mtMod = player.ship.cargoSpaceAvailable;
        }

        if (mtMod < 0) {
            eval("player.ship.manifest." + mtLabel + " -= " + Math.abs(mtMod));
        } else {
            eval("player.ship.manifest." + mtLabel + " += " + mtMod);
        }

        //var mtUnit = player.ship.manifest.list[mtLabel].unit;

    } else {
        switch (command) {

            case "bounty":
                var boMod = this.$readNumericParameter(param);
                if (player.bounty + boMod < 0) { boMod = -player.bounty; }
                player.bounty += boMod;
                break;

            case "chancesick":
                var cSick = this.$readNumericParameter(param);
                if (_litf.LITF_cmdStats.sickness == 0) {
                    var chance = Math.floor(Math.random() * 100);
                    if (chance < cSick) { _litf.LITF_cmdStats.sickness = 1; }
                }
                break;

            case "checkFitDamage":
                var lfs = _litf.LITF_flags.lastFitSession;

                if (lfs > -1) {
                    var diff = clock.seconds - lfs;
                    if (diff < 172800) {
                        _litf.LITF_flags.fitDamageChance += this.$randomValue(5, 10);   
                    }
                }
                break;
                
            case "credits":
                var crMod = this.$readNumericParameter(param);
                if (player.credits + crMod < 0) { crMod = -player.credits; }
                player.credits += crMod;
                break;

            case "end":
                var tiMod = this.$readNumericParameter(param);
                clock.addSeconds(tiMod);
                _re.$resetVars(); 
               
                // Return to place screen (destination)
                _nav.$goPlace(_re.LITF_destPlace);
                break;

            case "fitdamage":
                if (_litf.LITF_cmdStats.health < 3) {
                    _litf.LITF_cmdStats.health = this.$randomValue(2, 3);
                }
                
                if (_litf.LITF_flags.maxStrength > 0) {
                    _litf.LITF_flags.maxStrength -= this.$randomValue(20, 100);
                }
                 
                break;

            case "showStatus":
                _nav.$statusScreen();
                break;

            case "endNav":
                var tiMod = this.$readNumericParameter(param);
                clock.addSeconds(tiMod);
                //_litf.$hangarScreen();
                break;

            case "fuel":
                var fuMod = this.$readNumericParameter(param);
                if (player.ship.fuel + fuMod < 0) {
                    fuMod = -player.ship.fuel;
                } else if (fuMod > 0 && fuMod > player.ship.cargoSpaceAvailable) {
                    fuMod = player.ship.cargoSpaceAvailable;
                }
                player.ship.fuel += fuMod;
                break;

            case "health":
                var heaMod = this.$readNumericParameter(param);
                _litf.LITF_cmdStats.health += heaMod;
                if (_litf.LITF_cmdStats.health < 0) { _litf.LITF_cmdStats.health = 0; }
                if (_litf.LITF_cmdStats.health > 5) { _litf.LITF_cmdStats.health = 5; }
                break;

            case "if":
                // XXX Condition management
                // param = <condition>?<ifActions>;<elseActions>
                // e.g.: if:credits<100,bounty=0?go:event1;go:event2

                // Notes:
                // - more conditions are possible, comma-separated
                // - more actions aren't possible (cannot use pipe character), execute them in follow-ups events

                var condItems = param.split("?"); // condItems[0] = <condition>
                var resItems = condItems[1].split(";"); // resItems[0] = <ifActions>; resItems[1] = <elseActions>

                var validate = this.$validateRequirements(condItems[0]);

                // A little bit of recursion
                if (validate == 1) {
                    // ifActions
                    //this.$executeSingleAction(resItems[0]);    
                    var ifcmds = this.$replaceAll("§", "|", resItems[0]);
                    this.$log("[executeActions] if = " + ifcmds);
                    this.$executeActions(ifcmds);    
                } else if (validate == 0) {
                    // elseActions
                    //this.$executeSingleAction(resItems[1]);    
                    var elsecmds = this.$replaceAll("§", "|", resItems[1]);
                    this.$log("[executeActions] else = " + elsecmds);
                    this.$executeActions(elsecmds);    
                }
                break;

            // XXX Generate contractor mission
            /*
            case "contractorMission":
                var mType = param;

                // Mandatory parameters (from mission text): SYSTEM, NAME
                // Plus MISSION TYPE, TIME NOW, HOURS VALID
                var hData = this.LITF_store["SYSTEM"] + "|" + this.LITF_store["NAME"] + "|" + mType + "|" + clock.hours;

                var timelimit = this.$getTravelTime(this.LITF_store["SYSTEM"]) * 2;
                var hTime = this.$formatNumber(timelimit);
                hData += "|" + hTime;

                _litf.LITF_contractorData = hData;
                this.$log("[selectChoice] contractor data = " + _litf.LITF_contractorData);

                // Clear temporary data, leaves only "contractor" for future checks
                delete _litf.LITF_store["SYSTEM"];
                delete _litf.LITF_store["NAME"];
                break;
            */

            case "go":
                var goSplit = param.split(",");
                var rndIdx = Math.floor(Math.random() * goSplit.length);
                _re.$fireEvent(goSplit[rndIdx]);
                break;

            case "goNav":
                var goSplit = param.split(",");
                var rndIdx = Math.floor(Math.random() * goSplit.length);
                var newPlace = goSplit[rndIdx];

                this.$log("[executeSingleAction] newPlace = (" + rndIdx + ") " + newPlace);

                if (newPlace == "back") {
                    newPlace = _nav.LITF_lastPlaceLabel;
                    this.$log("[executeSingleAction] going back to last place: " + newPlace);
                }

                _nav.$goPlace(newPlace);
                break;

            case "paybountyfine":
                var bFine = player.bounty * 500;
                player.credits -= bFine;
                if (player.credits < 0) { player.credits = 0; }
                break;

            case "paytreatment":
                // Healing amount (depends on system TL)
                var heal = 0;
                var tl = this.$systemTechLevel();
                switch (tl) {
                    case "lowtech": heal = 2; break;
                    case "avgtech": heal = 3; break;
                    case "hitech": heal = 5; break;
                }
                
                // Pay the price
                var price = heal * 50;
                player.credits -= price;

                // In case of debt insolvency, raise the bounty
                if (player.credits < 0) {
                    var debt = Math.abs(player.credits);
                    player.credits = 0;
                    player.bounty += (debt / 10);
                }

                // Healing effects
                if (param == "health") {
                    _litf.LITF_cmdStats.health -= heal;
                    if (_litf.LITF_cmdStats.health < 0) { _litf.LITF_cmdStats.health = 0; }
                } else if (param == "sickness") {
                    _litf.LITF_cmdStats.sickness -= heal;
                    if (_litf.LITF_cmdStats.sickness < 0) { _litf.LITF_cmdStats.sickness = 0; }
                }
                break;

            case "randomAndGo":
                var goSplit = param.split(",");
                var rndIdx = Math.floor(Math.random() * goSplit.length);
                var dPlace = goSplit[rndIdx];
                var chance = Math.floor(Math.random() * 101); // 1-100

                this.$log("[executeSingleAction] randomAndGO :: chance = " + chance + " dPlace = " + dPlace);

                if (chance < 20) {
                    // random event intermission
                    _re.$randomEvent(dPlace);
                } else {
                    // proceed as usual
                    _nav.$goPlace(dPlace);
                }
                break;

            case "reputation":
                var repMod = this.$readNumericParameter(param);
                _litf.LITF_cmdStats.reputation += repMod;
                if (_litf.LITF_cmdStats.reputation < 0) { _litf.LITF_cmdStats.reputation = 0; }
                break;

            case "sickness":
                var sicMod = this.$readNumericParameter(param);
                _litf.LITF_cmdStats.sickness += sicMod;
                if (_litf.LITF_cmdStats.sickness < 0) { _litf.LITF_cmdStats.sickness = 0; }
                if (_litf.LITF_cmdStats.sickness > 5) { _litf.LITF_cmdStats.sickness = 5; }
                break;

            case "stance":
                var staMod = this.$readNumericParameter(param);
                _litf.LITF_cmdStats.stance += staMod;
                break;

            case "strength":
                var strMod = this.$readNumericParameter(param);
                _litf.LITF_cmdStats.strength += strMod;
                if (_litf.LITF_cmdStats.strength < 0) { _litf.LITF_cmdStats.strength = 0; }
                if (_litf.LITF_cmdStats.strength > _litf.LITF_flags.maxStrength) { _litf.LITF_cmdStats.strength = _litf.LITF_flags.maxStrength; }
                break;

            case "time":
                var tiMod = this.$readNumericParameter(param);
                clock.addSeconds(tiMod);
                break;
                
            case "writeTime":
                var wtData = param.split("_"); // flag name, time to write (in seconds) starting from now
                
                var secondsFromNow = parseInt(wtData[1]);
                var storeTime = clock.seconds + secondsFromNow;
                
                this.$log("[executeSingleAction] writeTime: " + wtData[0] + " = " + storeTime);
                
                eval("_litf.LITF_flags." + wtData[0] + "=" + storeTime);
                break;
        }
    }
};


this.$checkTimeForExpiration = function(maxTime) {
    var result = -1; // 0 - not expired, 1 - expired
    if (clock.seconds > maxTime) {
        result = 1;
    } else {
        result = 0;
    } 
        
    return result;
};


this.$formatRemainingTime = function(maxTime) {
    var rtText = "";

    var remainTime = maxTime - clock.seconds;
    var formattedTime = this.$formatTime(remainTime);
    
    return formattedTime;
};


this.$formatTime = function(secondsTime) {
    var minute = 60;
    var hour = 60 * minute;
    var day = 24 * hour;

    var dd = 0;
    var hh = 0;
    var mm = 0;

    while (secondsTime > day) {
        dd++;
        secondsTime -= day;
    }

    while (secondsTime > hour) {
        hh++;
        secondsTime -= hour;
    }

    while (secondsTime > minute) {
        mm++;
        secondsTime -= minute;
    }

    var fTime = "" + dd + " days, " + hh + " hours, " + mm + " minutes";

    return fTime;
};



