"use strict";

this.name	= "LITF_RandomEvents";
this.author	= "BeeTLe BeTHLeHeM";
this.copyright	= "2015 BeeTLe BeTHLeHeM";
this.description= "Life In The Frontier - RandomEvents";
this.version	= "0.7.1";
this.licence	= "CC BY-NC-SA 4.0";

///////////////////////////////////////

var LITF_reLabel = "";

this.LITF_destPlace = "";

this.$randomEvent = function(dPlace) {
	this.resumeLog = "";
	this.$fireEvent();

	this.LITF_destPlace = dPlace;
};


this.$fireEvent = function(eLabel) {
	var _co = worldScripts.LITF_Common;

	var eTitle = "SOMETHING HAPPENS";

	var reLabel = this.$chooseRandomEvent(eLabel);
	LITF_reLabel = reLabel;
	_co.$log("[fireEvent] reLabel = " + LITF_reLabel);

	var rEvent = eval("worldScripts.LITF_EventsCatalog." + reLabel + ";");
	_co.$log("[fireEvent] event text = " + rEvent.text);

	var reChoices = _co.$extractChoices(rEvent);

	mission.runScreen(
		{
			title: eTitle, 
			message: _co.$replaceConstants(rEvent.text),
			choices: reChoices 
		},
		this.$selectChoice
	);

	_co.$screenBackground(rEvent.background);
};


this.$chooseRandomEvent = function(eLabel) {
	var _co = worldScripts.LITF_Common;

	var reLabel = "";

	if (eLabel == undefined || eLabel == "") {
		var totEvents = 30;

		var validEvent = 0;
		while (validEvent == 0) {
			var eIdx = Math.floor(Math.random() * totEvents) + 1;
			reLabel = "rand" + eIdx;			

			// Validate the event
			validEvent = this.$eventCheck(reLabel);
		}
	} else {
		// Specific event
		reLabel = eLabel;
	}
	
	_co.$log("[chooseRandomEvent] reLabel = " + reLabel);

	return reLabel;
};


this.$eventCheck = function(reLabel) {
	var _co = worldScripts.LITF_Common;

	var actionData = eval("worldScripts.LITF_EventsCatalog." + reLabel);
	
	if (actionData.requires) {
		var checkSplit = actionData.requires.split("|");
		for (var i = 0; i < checkSplit.length; i++) {
			var check = checkSplit[i];

			var items = check.split(",");
			for (var i = 0; i < items.length; i++) {

				_co.$log("[eventCheck] checking = " + items[i]);

				var checkItems = items[i].split(":");
				//var valid = _co.$checkRequirements(checkItems);
                var valid = _co.$checkRequirements(items[i]);
				if (valid == 0) { 
					return 0; 
				}
			}
		}
	}
		
	return 1;
};


this.$selectChoice = function(choice) {
	var _co = worldScripts.LITF_Common;

	if (choice == null || choice == "null") { return; }
	_co.$log("[selectChoice] choice = " + choice);

	var action = choice.replace('c', 'a');
	_co.$log("[selectChoice] action = " + action);

	// Retrieve the associate actions

	var actionData = eval("worldScripts.LITF_EventsCatalog." + LITF_reLabel + "." + action);

    _co.$executeActions(actionData);

    /*
	var actionsSplit = actionData.split("|");
	
	// Iterate, parse and execute the actions

	for (var i = 0; i < actionsSplit.length; i++) {
		var action = actionsSplit[i];

        _co.$executeAction(action);
	}
	*/
};

this.$resetVars = function() {
	var _litf = worldScripts.LITF;

	//this.resumeLog = "";

	_litf.LITF_varStore["SPECIES_S"] = "";
	_litf.LITF_varStore["SPECIES_P"] = "";
	_litf.LITF_varStore["GENDER"] = [];
	_litf.LITF_varStore["COMMODITY"] = "";

	//this.LITF_eventTitle = "";
};
