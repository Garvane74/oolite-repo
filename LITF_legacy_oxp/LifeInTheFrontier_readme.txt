DESCRIPTION:

Players roleplay their game session, depending on the career they choose, and the events in-flight. Space station, on the contrary, are still rather static (without considering OXP) and act more like a transition between two flights.
So I thought to help stations offering more variety, giving the player the chance to fire a random event while he is docked, an event that could requires a player choice and that can have effects (good and bad) on the player properties.

HOW IT WORKS:

When the player is docked to a station, he will find in the F4 screen a new item, "Take a look around on the station" (mainly placeholder text). If he select this item, the OXP will choose a random event from a list, and the player will have to confront it. Some events will be only description of something that happens in the vicinity of the player, while others will require choices and decisions, and usually they will have multiple resolutions.
The events are categorized depending on various factors: the system government, economy and TL, plus one "generic" category for events that can happen anywhere. Instead the specific categories are intended to create a precise atmosphere, so in dictatorial systems there will be a more oppressive feeling, and in low tech worlds it will possible to see more malfunctioning in systems.
Every category has a defined background - I have taken shots from corridors in old sci-fi movies but I don't know if I can use them, so in case I will remove them. For now consider them as placeholders.
The resolution of an event can include: variation in money, in bounty level, in ship fuel or cargo goods. In these case will be displayed a report screen showing what is changed.
Sometimes an event to happen will require a check on one of that properties, for example to avoid that the player loses more money than he actually has.
The player can fire an event for docking, to fire another event he will need to leave and dock to another (or the same) station - in this way the mechanic shouldn't be abused.

CHANGELOG:

0.7.1
- Fixed action on Museum exit.

0.7
- Completed STORAGE, special location on Docks with restricted access: 25 credits or legal stance <= -25 (friendly with criminals)
- Placed PASSENGERS LOUNGE, special location on Concourse-A
- Placed CHURCH OF THE COSMIC WANDERER, special location on Concourse-B
- Placed BOOKSHOP, special location on Concourse-B
- Placed MUSEUM, special location on Delight Avenue
- Placed FIT CENTER, special location on Olympian Square
- Implemented Station Map, visible from every lobby
- Commander stats and OXP flags are stored as a map (string with format name1=value1,name2=value2,name3=value3...) instead of an array. This should definitely fix every issue with loading and saving data.
- Placed BRIEFING ROOM, special location on Galcop Precinct.
- Placed DETAINMENT ZONE, special location on Galcop Precinct.
- Placed SOCIAL SERVICES, special location on Galcop Precinct.
- Placed PILOT COURSE ROOM, special location on Galcop Precinct.

- Implemented SOCIAL SERVICES mechanics:
	* Player can spend 4 hours doing social services. His reputation will increase of a small amount. His bounty will decrease of an amount. There's a chance of getting hurt during the work. In this case the time spent and bounty decrease are reduced.

- Implemented FIT CENTER mechanics:
	* The first time the commander visits a Fit Center, he must buy an Universal Fitness Badge for 15cr. This purchase include a month subscription.
	* The subscription can be renewed monthly for 10cr.
	* Commander mustn't be wounded or sick to enter the gym.
	* Every gym session increase commander strength.
	* If commander exercise more than once in two days, a chance of getting wounded while exercising increase.
	* If the commander get wounded, he'll must get healed before starting a new gym session.
	* If the commander get wounded, the maximum strength cap get lowered of a small amount. If this amount get very low (it requires a lot of accidents, though...) the commander can't access the gym anymore until he get a recovering therapy from an hi-tech Medical Center.
	* If the commander doesn't go to the gym for more than a week, strength will lower of a small amount after every dock.
	* If commander can't access the gym because he has endured too much damage, he can go to a hi-tech system Medical Center and get into a Recovery Therapy for 400cr. This therapy is available only when the player has sustained a lot of damage (only in the gym for now).
	* The status screen (access from the hangar) report the Universal Fitness expiration status, when the player buys it.

0.6.1
- Fixed corridor background name - this should avoid log warning messages about texture files not found.
- Fixed paytreatment action, MedCenter treatment should give no exception now.
- Deleted useless number from OXP versioning - better late then never. (Current model: MajorVersion.MinorVersion.Fix)

0.1.6 (0.6)
- When accessing the OXP from the F4 screen, the player goes directly to the Hangar.
- The player can display the Commander Status screen from the Hangar.
- Station layout updated following forum discussion on the current design.
- Changed some definitions for station locations.
- Lobby indications are more schematic.
- The Storage location has been promoted to special location.
- Implemented Hotel, moved location to Main Concourse-A because Delight Avenue has already 3 planned locations.
- Implemented the Wild Worlds Virtual Tour, on the Delight Avenue (Recreation Bridge).

0.1.5.1 (0.5.1)
- Fixed check on the variables introduced in 0.1.5.
- Fixed check on varStore array items before setting values.

0.1.5 (0.5)
- Added background to Docks Area;
- Add text snippet for drunken status (along with wounds and sickness);
- Implemented event-checks for "government", "tech" and "economy". The government check can take a list of governments, separated by "-" character. If the system government is in the list, the check is valid;
- Added events: ROBO-BARD, HUNTERS, DELIRIOUS DROID, COMMERCIAL DROID, SUPERCELEBRITY, LIGHTS MALFUNCTION, WATER MALFUNCTION, AIR MALFUNCTION, DEFACING KIDS
- "her/him/his" not capitalized anymore;
- Commander stats are saved as a string with comma-separated values rather than 5 missionVariables.
- Game flags are saved as a string with comma-separated values rather than 8 missionVariables.
- Variables still not used aren't loaded anymore from the savegame, and are saved with null values to purge them from the file.
- Refactored method for checks on event/choice requirements: it's now possible to interrogate a parameter with operators =, !=, <, <=, >, >=. The requirement syntax changes. E.g.: no more credits:500, but credits=500, or credits<500 etc.
- Event text can be present as parameter "text" (static text, always displayed) *OR* as indexed parameters (text0, text1, text2): in the latter case, it's possible to specify display conditions, with syntax "condition|text to display". If an indexed text has no condition is considered static (always displayed). The validated indexed text are concatenated.
- Event property "choiceRequires" has been removed. It has been replaced by "r"(+ choice index), that contains the facultative requirements for the single choice. If a choice has no requirements (no "r" property), the choice is always displayed.
- I believe I have removed the end-event resume screen at least one release ago, forgot to write it down.
- To avoid confusion, in the event properties requires and r I don't use the ':' char as separator anymore, but the equal sign '='.
- Refactored action executions when player select an event or a location choice. There are some differences in some actions, so I had to differentitate: go (random event) and goNav (navigation), end (random event) and endNav (navigation).
- Implemented condition if to the events action. Now it's possible to define a condition (or multiple conditions, comma-separated), and to link it to two results: the "valid condition" and the "else". I'm going to read again the various random events to introduce conditions when necessary, to streamline the event flow. Syntax is: if:<conditions>?<actions>;<elseActions>
- I've lost some features (the hotel, the wild world virtual tour and the new Medical Center text), so I'll have to rethink them in the next days. Actually I have changed the Medicla Center concept, so the options are always displayed but if the player isn't sick or wounded or drunk the receptionist will refuse to start the treatment.
- Fixed: the Galcop terminal didn't show the price for cleaning the criminal record.
- The DEFACING KIDS event can enable a flag - eventually the consequence will be shown when the player reach the docks area.

0.1.4 (0.4)
- Code optimization and refactoring. Creation of new js files for common methods, for mission-related methods, and for inside-station navigation methods.
- Restarted from scratch (taking the old code piece for piece).
- The F4 link shows a literal resume of the player. From this screen, the player can "disembark" from the ship and wander into the space station.
- "Tubes Stop" locations allow player to move across five different levels. Every level has a theme and several inner locations. The levels are to be intended as concentric.
- Events are not fired by player anymore. But moving into the station there is a 20% chance (not definitive) of firing a random event. They act like an intermission, basically.
- Added random snippet (from a list of 20) of texts when at a Tube Stop.

- Added all the events already implemented in 0.1.3 (beside the bar ones)
- Added the bar events: the gossip option has been moved to the "talk with bartender one". Bartender doesn't give hints about contractors anymore.
- It's possible to clean own criminal record at the Galcop termminal (in the Galcop precinct) paying a fine of (500cr x current bounty).
- Added the Art Gallery and the art genre random generator.
- Added the Opera House and the music genre random generator.
- Added the Cinema Hall and the movie genre random generator.
- If player is wounded or sick, randomly add short descriptive snippet of text while moving around the station.
- Added a short single-row-status text to show on top of the locations screens.
- Added two backgrounds for the "Tubes".
- Changed "Tube Stop" to "Lift Station", and "Tubes" to "Lift". The direction is relative, it's not necessary an up-down transport.

- The property background can contain more background file names separated by comma: the OXP will choose a random background between those specified.
- Added some more backgrounds, I'm trying to add variety and get an "identity" for the various levels.
- Added medical center location: here the player can cure wounds and sickness for a price. Price and amount of cure changes in relation to system tech level. The medical center can also rid of drunken status for free, but with a chance for a slight sickness.

- Added health/sickness management, with the following rules being checked every time the player docks to a station:
1) if sickness > 1, chance 10% of worsening (sickness +1);
2) if sickness = 1, chance 10% of healing (sickness = 0);
3) if health > 1, chance 10% of worsening (health + 1);
4) if health = 1, chance 10% of healing (health = 0);
5) if health > 2 and sickness < 2, chance 10% of infection (sickness +1);
6) if health > 2 or sickness > 2, chance 10% of physical shape reduction
- If player has health or sickness in critical conditions (>= 5), has a 50% of pass-out. In this case he will be automatically transported to the station Medical Center and cured (with system TL limitations). Player will receive a decrease in reputation (for careless behaviour) and the price of the opeation will be automatically withdrawn from his account - if he has't enough money his bounty will be slightly increased (for debt insolvency).

0.1.3 (0.3)
- implemented a time variable for events. It's now possible to define how much time will be spent when executing an event. Instead of creating a separate property, the variabile is set in the "end:x" command, where x is the time spent in seconds. So "end:600" means "terminate the event and go forward 10 minutes in time". Use "end:0" to avoid spending game-time;
- As a general rule for numeric command parameters, two values comma-separated will be interpreted as a range, and a random number between the specified values will be extracted. This applies even to the time variable above;
- Added a new resolution to the "elixir peddler" event, that use the time variable above;

- implemented the constants $SPECIES_S$ (singular) and $SPECIES_P$ (plural), that set a random species if requested by the event text. The species value remain fixed until the end of the event;
- implemented the constants $GENDERSUBJ$ (he/she), $GENDEROBJ$ (him/her), $GENDERPOSS$ (his/her). The random chosen gender remain fixed untile the end of the event. The gender-values are correlated to maintain coherence throughout the text;
- fixed the text of all the events implemented until now;
- added five "no-events";

- added a new script file (LITF_RPGElements.js) to keep together the RPG-lite functions;
- changed the F4 link in "Visit the station hub": it show a main screen with player current RPG-lite status (reputation, health, sickness, strength), the link to the random event and something more...;
- added description for all the status variables;
- added arrays of "minor" cargo types to be inserted in missions text: these aren't new cargo types, they will only be treated as random labels to differentiate the text depending on player reputation;

- The link on the F4 page has been changed to "Visit the station hub" and if selected shows the "main screen";
- The "main screen" displays the player status (a literal description for the LITF variables);
- Added variables: reputation, health (wounds, bruises etc.), sickness, strength (physical shape);
- Added variable: stance. Positive stance = the law; negative stance = the criminals. Similar to reputation, but to be considered more like "word on the street is"; 
- Available choices for the "main screen";
	* "Take a walk to the Main Concourse". This fire a random event, as before;

- The player won't find the locations (bar, fit center, church, med center) on every system. I wrote a basic rule to distribute their presence in the galaxy. The rule is mathematical and based on the system ID, so the final results are fixed - for now only the bar is enabled;
- Re-edited the implemented events to use the variables in effects and requirements;
- Added constant $LIQUORS$ (with an array of 20 liquor random names), $SYSTEM$ (get random system name), $COMMODITY$ (get random vanilla commodity name), $GOSSIP$ (get a random gossip), $NAME$ (for random NPC name), $MINORCARGO$ (for transport missions);
- Added flag management (not mission flags), checks and manipulation;
- Added command "time:x", if you don't want to end the events but only to spend time;

- Implemented first version mission generation. It works in two steps. First step: an NPC (in this case, the bartender) tells the player that another NPC has a job available: I store data about the NPC name, the system where he is, the type of mission and a generous time limite to reach him. When the player docks to a station I check if it's the right station and if the NPC hasn't expired: if everything's good, when the player enter the main concourse I switch to an alternate event: meeting the NPC, that introduce the job (a cargo transport), with a destination, a time limit and a type of "minor cargo" (see above). If the player accepts, I use this data to generate a real mission.
- Removed sickness effect when getting drunk, it's counter-productive. When I'll implement medical centers, player have the chance to pay to get sober.For now he can leave the station and dock to another station to rid of the drunkenness.

- Modified the cargo mission management: I don't use specialCargo or awardContract, but the OXP do the checks and display the page for completing a contract (always from the Main Concourse). It's not definitive, but it seems to work.
- Added events: CHARMING PARTNER, MUGGING, THE LOST DRUNKARD, MEDICAL CAMP

0.1.2 (0.2)
- enabled the one-event-for-dock flag (it was disabled for showing various events without leaving a station).
- changed the event management, I moved the events data from missionscript.plist to LITF_ActionsCatalog.js (renamed in LITF_EventsCatalog.js). In this way I can group everything related to an event in one object (text, requirements, choices and actions), and eventually I can make them more dynamic;
- renamed property "check" (property-checks that allow the event to happens) in "requires";
- added property "choiceRequires", contains pipe-concatenated string with checks for every event choice (see check property list) for selecting choices to display under particular circumstances; use "valid:1" for always display a choice, and "valid:0" to always hide it;
- added events:
	GENERIC: charity
	GENERIC: alien discussion
- implemented code for replace constants in event/choice text with a random element from a related array: actually used in the charity event, constant $CHARITY$.

0.1.1 (0.1)
- events category list:
	"generic" category
	station allegiance (disabled)
	system government (gov_0 - gov_7)
	system tech level (ltech, htech), values 5-9 are converted to "generic"
	system economy (ind, agri), values 3-4 are converted to "generic"
	OXP choose a value from an array with the values [ generic, gov_x, tl_x, eco_x]
	Maybe in the future I could add as new categories ranges for player rank or player bounty
- specific background picture depending on event category: system government, tech level (low tech, high tech) and economy (industrial, agricolture). "Generic" events will show the related system government background;
- event one-click for docking disabled for testing and showing OXP workings;
- passing time for events temporarily disabled: to be reintroduced it as an event property;
- event effects list:
	mt_[name]:[value] add [value] to the player ship manifest good identified by [name]. 
	bounty:[value] add [value] to the current player bounty.
	fuel:[value] add [value] to the current player ship fuel amount.
	credits[value] add [value] to the current player credits.
	go:[label] fire the event identified by [label]
	end:0 end the event
	[value] can be positive or negative. There are checks to avoid negative results or values exceeding cargo space.
- check property list:
	mt[name]:[value] check for player ship manifest good identified by [name] >= [value]
	mt<[name]:[value] check for player ship manifest good identified by [name] < [value]
	fuel:[value] check for player ship fuel amount >= [value]
	fuel<:[value] check for player ship fuel amount < [value]
	bounty:[value] check for current player bounty >= [value]
	bounty<:[value] check for current player bounty < [value]
	credits:[value] check for current player credits >= [value]
	credits<:[value] check for current player credits < [value]
- added events:
	GENERIC: beggar
	GENERIC: weird password guy
	GENERIC: fire
	GENERIC: fire + gossip
	GENERIC: shady seller
	GENERIC: partygoers
	GENERIC: running fugitive
	GENERIC: thief
	GENERIC: stranded crew (2 variants)

LICENSE:
This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike License version 4.0.
If you are re-using any piece of this OXP, please let me know by sending an e-mail to norbylite@gmail.com.
